#!/usr/bin/python

import argparse
import os
import time
import calendar
import sys

numAccounts = 0
totalBytesPerMonthPerUser = 0

# Sat Aug 18 14:06:50 2018 172.29.255.80:57167 ...
def getFirstTime(rdr):
    firstTime = 0

    while firstTime == 0:
        firstLine = rdr.readline().strip()
        firstDateStr = firstLine[:30]
        try:
            dow, mon, day, tim, year, garbage = firstDateStr.split()
            firstDateStr = ( day + '/' + mon + '/' + year + ':' + tim )
        except:
            pass

        try:
            firstTime = float(calendar.timegm(time.strptime(firstDateStr, '%d/%b/%Y:%H:%M:%S')))
        except:
            firstTime = 0
            pass

    return firstTime

def getLastTime(rdr):
    lastTime = 0

    if fstat.st_size > 5000:
        rdr.seek(-5000, 2)
    for line in rdr:
        lastDateStr = line[:30]
        try:
            dow, mon, day, tim, year, garbage = lastDateStr.split()
            lastDateStr = ( day + '/' + mon + '/' + year + ':' + tim )
        except:
            pass

        try:
            lastTime = float(calendar.timegm(time.strptime(lastDateStr, '%d/%b/%Y:%H:%M:%S')))
        except:
            lastTime = 0
            pass

    return lastTime


# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts
totalBytesPerMonthPerUser = 0

rootPath = '/var/log/openvpn'
if not os.path.exists(rootPath):
    print('openvpn,%s,%d') % (args.server, totalBytesPerMonthPerUser)
    sys.exit(0)

files = os.listdir(rootPath)
for fileName in files:
    if fileName[-4:] == '.log':
        fstat = os.stat(rootPath + '/' + fileName)
        fileSize = float(fstat.st_size)

        # print fileName
        rdr = open(rootPath + '/' + fileName)
        firstTime = getFirstTime(rdr)
        lastTime = getLastTime(rdr)
        rdr.close()

        timeDiff = (lastTime - firstTime) / 86400
        if timeDiff > 0:
            bytesPerDay = fileSize/timeDiff
            bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
            totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)
        # print fileName, timeDiff, int(bytesPerMonthPerUser)

print('openvpn,%s,%d') % (args.server, totalBytesPerMonthPerUser)

#!/usr/bin/python

import argparse
import os
import time
import calendar
# import json
# import mysql.connector
# import requests
import sys

mailIds = {}
numAccounts = 0

def getMailIds(mailLogFile):
    ftm = 0
    ltm = 0

    try:
        logfile = open(mailLogFile, 'rb')
    except IOError:
        return ftm,ltm

    for line in logfile:
        dateField = line[:15]
        dow = dateField[0:3]
        dom = int(dateField[4:6])
        tim = dateField[7:]
        dateStr = ( dow + ' ' + str(dom) + ' 2019 ' + tim )

        line = line[16:]
        mailId = line.split(' ')[2]
        if mailId == 'connect' or mailId == 'disconnect':
            continue
        if mailId == 'statistics:':
            continue
        if ftm == 0:
            try:
                ftm = float(calendar.timegm(time.strptime(dateStr, '%b %d %Y %H:%M:%S')))
            except:
                pass
        try:
            ltm = float(calendar.timegm(time.strptime(dateStr, '%b %d %Y %H:%M:%S')))
        except:
            pass
        ndx1 = mailId.find(':')
        mailId = mailId[:ndx1]
        if len(mailId) != 11:
            continue

        if mailId in mailIds:
            mailIds[mailId] = mailIds[mailId] + line.lower()
        else:
            mailIds[mailId] = line.lower()
    logfile.close()
    return ftm, ltm

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts

ftm,ltm = getMailIds('/var/log/mail.log')
# ftm,ltm2 = getMailIds('/var/log/mail.log.1')
numMailMsg = len(mailIds)
timeDiff = (ltm - ftm) / 86400
msgPerDay = 0
msgPerMonth = 0
msgPerUserPerMonth = 0
if timeDiff > 0:
    msgPerDay = float(numMailMsg) / float(timeDiff)
    msgPerMonth = msgPerDay * 30
    msgPerUserPerMonth = msgPerMonth / numAccounts

print('mail,%s,%d') % (args.server, int(msgPerUserPerMonth))

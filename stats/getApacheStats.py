#!/usr/bin/python

import argparse
import os
import time
import calendar
import sys

numAccounts = 0
totalBytesPerMonthPerUser = 0

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="Extract stats from an Apache log file.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts

interestingLogs = [ 'access.log', 'alarm.direct.access.log', 'secure.direct.access.log',
                    'error.log', 'securenet-7.95.log' ]
# interestingLogs = [ 'access.log', 'alarm.direct.access.log', 'secure.direct.access.log',
#                     'error.log' ]

if not os.path.exists('/var/log/apache2'):
    sys.exit(0)

files = os.listdir('/var/log/apache2')
for f in files:
    fileName = ( '/var/log/apache2/' + f )
    if f not in interestingLogs:
        continue
    if not os.path.exists(fileName):
        continue

    # print fileName
    fstat = os.stat(fileName)
    fileSize = float(fstat.st_size)

    rdr = open(fileName)
    firstLine = rdr.readline().strip()
    lastLine = ''
    lineCount = 0
    for line in rdr:
        lastLine = line
        lineCount += 1
    rdr.close()

    firstTime = 0
    lastTime = 0
    firstDateStr = ''
    lastDateStr = ''
    if f == 'error.log':
        # [Thu Feb 07 06:52:41.383650 2019]
        firstDateStr = firstLine[firstLine.find('[')+1:firstLine.find(']')]
        dow, mon, day, tim, year = firstDateStr.split()
        firstDateStr = ( day + '/' + mon + '/' + year + ':' + tim[0:tim.find('.')] )
        lastDateStr = lastLine[lastLine.find('[')+1:lastLine.find(']')]
        dow, mon, day, tim, year = lastDateStr.split()
        lastDateStr = ( day + '/' + mon + '/' + year + ':' + tim[0:tim.find('.')] )
    elif f in [ 'access.log', 'alarm.direct.access.log', 'secure.direct.access.log' ]:
        # ::1 - - [07/Feb/2019:06:52:41 +0000]
        firstDateStr = firstLine[firstLine.find('[')+1:firstLine.find(']')]
        firstDateStr = firstDateStr[0:firstDateStr.find(' ')]
        lastDateStr = lastLine[lastLine.find('[')+1:lastLine.find(']')]
        lastDateStr = lastDateStr[0:lastDateStr.find(' ')]
    elif f == 'securenet-7.95.log':
        # 07 Feb 2019 1:54:18
        firstDateStr = firstLine[:firstLine.find('[')]
        lastDateStr = lastLine[:lastLine.find('[')]
        day, mon, year, tim = firstDateStr.split()
        firstDateStr = ( day + '/' + mon + '/' + year + ':' + tim )
        day, mon, year, tim = lastDateStr.split()
        lastDateStr = ( day + '/' + mon + '/' + year + ':' + tim )
    else:
        print "**"

    try:
        firstTime = float(calendar.timegm(time.strptime(firstDateStr, '%d/%b/%Y:%H:%M:%S')))
    except:
        pass
    try:
        lastTime = float(calendar.timegm(time.strptime(lastDateStr, '%d/%b/%Y:%H:%M:%S')))
    except:
        pass

    timeDiff = (lastTime - firstTime) / 86400

    totalBytesPerMonthPerUser = 0
    linesPerMonthPerUser = 0
    if timeDiff > 0:
        bytesPerDay = fileSize / timeDiff
        bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
        totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)
        linesPerMonthPerUser = (lineCount / timeDiff) * 30 / numAccounts

    print('apache-%s,%s,%d,%d') % (f, args.server, totalBytesPerMonthPerUser, linesPerMonthPerUser)

# -rw-rw-r-- 1 www-data www-data      376802 Feb  7 19:37 /var/log/apache2/securenet_mobile-touch-0.4.log
# -rw-rw-r-- 1 www-data www-data   145116396 Feb  7 19:41 /var/log/apache2/securenet_mobile-touch-0.6.log
# -rw-rw-r-- 1 www-data www-data      186872 Feb  7 19:38 /var/log/apache2/securenet-prod.log

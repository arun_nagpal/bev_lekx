#!/usr/bin/python

import argparse
import os
import time
import datetime
import calendar
import sys

numAccounts = 0
totalBytesPerMonthPerUser = 0

# catalina.out: SMARTP 02-08 06:36:29 DEBUG [472405/54
# localhost_access_log.2019-01-08.txt: 127.0.0.1 - - [08/Jan/2019:00:00:11 +0000] "POST /smar

def getFirstTime(rdr, fileName):
    firstTime = 0

    rdr.seek(0, 0)
    while firstTime == 0:
        firstLine = rdr.readline().strip()
        if fileName[-4:] == '.txt' and fileName[:21] == 'localhost_access_log.':
            firstDateStr = firstLine[firstLine.find('[')+1:firstLine.find(']')]
            firstDateStr = firstDateStr[0:firstDateStr.find(' ')]
            try:
                firstTime = float(calendar.timegm(time.strptime(firstDateStr, '%d/%b/%Y:%H:%M:%S')))
            except:
                firstTime = 0
                pass
        elif fileName == 'catalina.out':
            ndx = firstLine.find('SMARTP')
            firstDateStr = firstLine[ndx+7:ndx+21]
            mon = firstDateStr[0:2]
            day = firstDateStr[3:5]
            tm = firstDateStr[6:14]
            dobj = datetime.date.today()
            firstDateStr = ( day + '/' + mon + '/' + str(dobj.year) + ':' + tm )
            try:
                firstTime = float(calendar.timegm(time.strptime(firstDateStr, '%d/%m/%Y:%H:%M:%S')))
                if firstTime > float(time.time()):
                    firstDateStr = ( day + '/' + mon + '/' + str(dobj.year-1) + ':' + tm )
                    firstTime = float(calendar.timegm(time.strptime(firstDateStr, '%d/%m/%Y:%H:%M:%S')))
            except:
                firstTime = 0
                pass

    return firstTime

def getLastTime(rdr, fileName):
    lastTime = 0

    if fstat.st_size > 5000:
        rdr.seek(-5000, 2)
    for line in rdr:
        if fileName[-4:] == '.txt' and fileName[:21] == 'localhost_access_log.':
            lastDateStr = line[line.find('[')+1:line.find(']')]
            lastDateStr = lastDateStr[0:lastDateStr.find(' ')]
            try:
                lastTime = float(calendar.timegm(time.strptime(lastDateStr, '%d/%b/%Y:%H:%M:%S')))
            except:
                lastTime = 0
                pass
        elif fileName == 'catalina.out':
            ndx = line.find('SMARTP')
            lastDateStr = line[ndx+7:ndx+21]
            mon = lastDateStr[0:2]
            day = lastDateStr[3:5]
            tm = lastDateStr[6:14]
            dobj = datetime.date.today()
            lastDateStr = ( day + '/' + mon + '/' + str(dobj.year) + ':' + tm )
            try:
                lastTime = float(calendar.timegm(time.strptime(lastDateStr, '%d/%m/%Y:%H:%M:%S')))
                if lastTime > float(time.time()):
                    lastDateStr = ( day + '/' + mon + '/' + str(dobj.year-1) + ':' + tm )
                    lastTime = float(calendar.timegm(time.strptime(lastDateStr, '%d/%m/%Y:%H:%M:%S')))
            except:
                pass

    return lastTime

def getLineCount(rdr):
    count = 0
    rdr.seek(0, 0)
    for line in rdr:
        count += 1
    return count

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts
totalBytesPerMonthPerUser = 0

rootPath = '/var/log/tomcat7'
if not os.path.exists(rootPath):
    rootPath = '/opt/tomcat7/logs'
    if not os.path.exists(rootPath):
        # print('tomcat,%s,%d') % (args.server, totalBytesPerMonthPerUser)
        sys.exit(0)

files = os.listdir(rootPath)

# Scan the localhost_access_log.*.txt files.
# Find the most recent file.
newestTxtFile = ''
newestTxtFileTime = 0
for fileName in files:
    if fileName[-4:] == '.txt' and fileName[:21] == 'localhost_access_log.':
        fstat = os.stat(rootPath + '/' + fileName)
        mtime = fstat.st_mtime
        if mtime > newestTxtFileTime:
            newestTxtFileTime = mtime
            newestTxtFile = fileName

fileName = newestTxtFile
fstat = os.stat(rootPath + '/' + fileName)
fileSize = float(fstat.st_size)
rdr = open(rootPath + '/' + fileName)
firstTime = getFirstTime(rdr, fileName)
lastTime = getLastTime(rdr, fileName)
lineCount = getLineCount(rdr)
rdr.close()
timeDiff = (lastTime - firstTime) / 86400
linesPerDay = lineCount/timeDiff
linesPerMonthPerUser = linesPerDay * 30 / numAccounts
if timeDiff > 0:
    bytesPerDay = fileSize/timeDiff
    bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
    totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)
print('tomcat-access,%s,%d,%d') % (args.server, totalBytesPerMonthPerUser, linesPerMonthPerUser)

# catalina.out
fileName = 'catalina.out'
fstat = os.stat(rootPath + '/' + fileName)
fileSize = float(fstat.st_size)
rdr = open(rootPath + '/' + fileName)
firstTime = getFirstTime(rdr, fileName)
lastTime = getLastTime(rdr, fileName)
lineCount = getLineCount(rdr)
rdr.close()
timeDiff = (lastTime - firstTime) / 86400
linesPerDay = lineCount/timeDiff
linesPerMonthPerUser = linesPerDay * 30 / numAccounts
if timeDiff > 0:
    bytesPerDay = fileSize/timeDiff
    bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
    totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)
print('tomcat-catalina,%s,%d,%d') % (args.server, totalBytesPerMonthPerUser, linesPerMonthPerUser)

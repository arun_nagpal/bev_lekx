#!/usr/bin/python

import argparse
import os
import time
import calendar
import sys

numAccounts = 0
totalBytesPerMonthPerUser = 0

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts

files = os.listdir('/home/securenet_prod/log')
for fileName in files:
    if fileName[-4:] == '.log' and '_wrapper.' not in fileName:
        fstat = os.stat('/home/securenet_prod/log/' + fileName)

        # ignore any files not touched in over a month.
        if int(time.time()) - fstat.st_mtime > 2592000:
            # print("Ignoring %s") % (fileName)
            continue

        # print fileName
        rdr = open('/home/securenet_prod/log/' + fileName)
        firstLine = rdr.readline().strip()

        lastLine = ''
        if fstat.st_size > 5000:
            rdr.seek(-5000, 2)
        for line in rdr:
            lastLine = line

        rdr.close()
        # print firstLine

        firstTime = 0
        lastTime = 0
        # rtsp.log uses a unique date format.
        # INFO   | jvm 1    | 2019/01/29 15:50:25 | Connection: close
        if fileName == 'rtsp.log':
            d = firstLine.split('|')[2].strip()
            firstTime = float(calendar.timegm(time.strptime(d, '%Y/%m/%d %H:%M:%S')))
            d = lastLine.split('|')[2].strip()
            lastTime = float(calendar.timegm(time.strptime(d, '%Y/%m/%d %H:%M:%S')))

        # push.log, notifier.log, dvr.log and snip.log use the same date format.
        # 2018-09-07 06:56:00,388 [PushNotifierProcessor] DEBUG - ...
        elif fileName == 'push.log' or fileName == 'notifier.log' or fileName == 'dvr.log' or fileName == 'snip.log':
            ndx = firstLine.find(',')
            d = firstLine[0:ndx]
            firstTime = float(calendar.timegm(time.strptime(d, '%Y-%m-%d %H:%M:%S')))
            ndx = lastLine.find(',')
            d = lastLine[0:ndx]
            lastTime = float(calendar.timegm(time.strptime(d, '%Y-%m-%d %H:%M:%S')))

        # Ignore other log files.
        else:
            bytesPerDay = 0

        timeDiff = (lastTime - firstTime) / 86400
        fileSize = float(fstat.st_size)
        if timeDiff > 0:
            bytesPerDay = fileSize/timeDiff
            bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
            # print fileName, int(bytesPerMonthPerUser)
            totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)

print('snip,%s,%d') % (args.server, totalBytesPerMonthPerUser)

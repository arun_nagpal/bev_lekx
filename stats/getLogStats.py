#!/usr/bin/python

import argparse
import os
import time
import calendar
# import json
# import mysql.connector
# import requests
import sys

# dbConfigCloud = {
#    'user': 'sn_notifier',
#    'password': '5n_n0t1f13r',
#    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }
# dbConfigCloudSlomins = {
#    'user': 'sn_slomins',
#    'password': '5n_510m1n5',
#    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
#    'port': 3306,
#    'database': 'securenet_6_slomins',
#    'raise_on_warnings': False,
# }
# dbConfigCloudEU = {
#    'user': 'sn_notifier',
#    'password': '5n_n0t1f13r',
#    'host': 'master_database',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }
# dbConfigCloudAU = {
#    'user': 'sn_notifier',
#    'password': '5n_n0t1f13r',
#    'host': 'master_database',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }
# dbConfigAlder = {
#    'user': 'david_wilson',
#    'password': 'd.s.wilson1',
#    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
#    'port': 3306,
#    'database': 'securenet_6_alder',
#    'raise_on_warnings': False,
# }
# dbConfigPA = {
#    'user': 'sn_www',
#    'password': '5n_www',
#    'host': 'master_database',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }
# dbConfigReg = {
#    'user': 'root',
#    'password': '5ecurenet',
#    'host': '172.29.253.45',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }

# serverEnvs = { 'peru': dbConfigCloud,
#                'germany': dbConfigCloud,
#                'wales': dbConfigCloud,
#                'egypt': dbConfigCloud,
#                'israel': dbConfigCloud,
#                'ghana': dbConfigCloud,
#                'cuba': dbConfigCloud,
#                'angola': dbConfigCloud,
#                'usa': dbConfigCloud,
#                'chad': dbConfigAlder,
#                'japan': dbConfigAlder,
#                'spain': dbConfigAlder,
#                'dubbo': dbConfigCloudAU,
#                'orange': dbConfigCloudAU,
#                'registration': dbConfigReg,
#                'austria': dbConfigCloudEU,
#                'poland': dbConfigCloudEU,
#                'phillies': dbConfigPA,
#                'redsox': dbConfigPA }

numAccounts = 0
totalBytesPerMonthPerUser = 0

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--server', type=str, default='',
                    required=True, help='Server.')
cmdline.add_argument('-n', '--numaccounts', type=int, default=0,
                    required=True, help='Number of accounts.')
args = cmdline.parse_args()

numAccounts = args.numaccounts

# if args.server == 'registration':
#     numAccounts = 1
# else:
#     dbConfig = {}
#     for serverEnv in serverEnvs:
#         if args.server == serverEnv:
#             dbConfig = serverEnvs[serverEnv]
#     if len(dbConfig) == 0:
#         print 'Unknown server environment for "' + args.server + '"'
#         sys.exit(1)

#     # Connect to the SecureNet database.
#     try:
#         cnx = mysql.connector.connect(**dbConfig)
#     except:
#         print("Error connecting to the database.")
#         sys.exit(1)

#     query = ('select count(*) from account where deleted_date = 0')
#     cursor = cnx.cursor()
#     cursor.execute(query)
#     rows = cursor.fetchall()
#     for row in rows:
#         numAccounts = row[0]
#     cursor.close()
#     cnx.close()

files = os.listdir('/home/securenet_prod/log')
for fileName in files:
    if fileName[-4:] == '.log' and '_wrapper.' not in fileName:
        fstat = os.stat('/home/securenet_prod/log/' + fileName)

        # ignore any files not touched in over a month.
        if int(time.time()) - fstat.st_mtime > 2592000:
            # print("Ignoring %s") % (fileName)
            continue

        rdr = open('/home/securenet_prod/log/' + fileName)
        firstLine = rdr.readline().strip()

        lastLine = ''
        if fstat.st_size > 5000:
            rdr.seek(-5000, 2)
        for line in rdr:
            lastLine = line

        rdr.close()
        # print firstLine

        firstTime = 0
        lastTime = 0
        # rtsp.log uses a unique date format.
        # INFO   | jvm 1    | 2019/01/29 15:50:25 | Connection: close
        if fileName == 'rtsp.log':
            d = firstLine.split('|')[2].strip()
            firstTime = float(calendar.timegm(time.strptime(d, '%Y/%m/%d %H:%M:%S')))
            d = lastLine.split('|')[2].strip()
            lastTime = float(calendar.timegm(time.strptime(d, '%Y/%m/%d %H:%M:%S')))

        # push.log, notifier.log, dvr.log and snip.log use the same date format.
        # 2018-09-07 06:56:00,388 [PushNotifierProcessor] DEBUG - ...
        elif fileName == 'push.log' or fileName == 'notifier.log' or fileName == 'dvr.log' or fileName == 'snip.log':
            ndx = firstLine.find(',')
            d = firstLine[0:ndx]
            firstTime = float(calendar.timegm(time.strptime(d, '%Y-%m-%d %H:%M:%S')))
            ndx = lastLine.find(',')
            d = lastLine[0:ndx]
            lastTime = float(calendar.timegm(time.strptime(d, '%Y-%m-%d %H:%M:%S')))

        # Ignore other log files.
        else:
            bytesPerDay = 0

        timeDiff = (lastTime - firstTime) / 86400
        fileSize = float(fstat.st_size)
        if timeDiff > 0:
            bytesPerDay = fileSize/timeDiff
            bytesPerMonthPerUser = bytesPerDay * 30 / numAccounts
            # print fileName, int(bytesPerMonthPerUser)
            totalBytesPerMonthPerUser += int(bytesPerMonthPerUser)

print('%s,%d') % (args.server, totalBytesPerMonthPerUser)

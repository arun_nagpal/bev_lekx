#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

servers=( "angola"
          "egypt"
          "germany"
          "peru"
          "ghana"
          "israel"
          "cuba"
          "wales" )
# servers=( "germany" )

# servers=( "dubbo"
# 	  "orange" )

# servers=( "japan"
#           "spain"
#           "chad" )

# servers=( "austria"
# 	  "poland" )

# "registration"
# "usa"

for server in ${servers[@]}
do
    # snip
    # snip, server, totalBytesPerMonthPerUser
    scp -q getSnipStats.py bev_lekx@${server}:.
    ssh bev_lekx@${server} "sudo ./getSnipStats.py -s ${server} -n 82345; rm ./getSnipStats.py"

    # email
    # mail, server, msgPerUserPerMonth
    scp -q getMailStats.py bev_lekx@${server}:.
    ssh bev_lekx@${server} "sudo ./getMailStats.py -s ${server} -n 82345; rm ./getMailStats.py"

    # apache
    # apache-??, server, totalBytesPerMonthPerUser, linesPerMonthPerUser
    # apache-secure.direct.access.log,germany,58832,234
    # apache-error.log,germany,1017594,3049
    # apache-securenet-7.95.log,germany,9713,56
    # apache-access.log,germany,16719,92
    # apache-alarm.direct.access.log,germany,176,0
    scp -q getApacheStats.py bev_lekx@${server}:.
    ssh bev_lekx@${server} "sudo ./getApacheStats.py -s ${server} -n 82345; rm ./getApacheStats.py"

    # tomcat7
    scp -q getTomcatStats.py bev_lekx@${server}:.
    ssh bev_lekx@${server} "sudo ./getTomcatStats.py -s ${server} -n 82345; rm ./getTomcatStats.py"

    # openvpn, server, bytesPerMonthPerUser
    scp -q getOpenvpnStats.py bev_lekx@${server}:.
    ssh bev_lekx@${server} "sudo ./getOpenvpnStats.py -s ${server} -n 82345; rm ./getOpenvpnStats.py"
done

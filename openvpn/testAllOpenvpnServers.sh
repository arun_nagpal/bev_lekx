#!/bin/bash

servers="54.88.128.22
        52.203.61.165"


# 52.203.61.165   wales
# 52.202.105.54   p2p1
# 54.173.197.175  p2p2
# 54.175.250.107  p2p3
# 54.84.181.103   snip1 usa
# 54.208.77.240   gsm1 gsm-primary
# 34.228.169.197  ghana
# 52.2.131.149    mantis
# 34.228.68.165   egypt
# 54.82.223.53    chad alder1
# 52.86.117.10    japan alder2
# 54.172.223.92   spain
# 54.208.60.175   gsm2  qatar  gsm-backup
# 52.202.143.108  germany
# 54.198.197.58   israel
# 54.85.197.195   tools
# 52.90.223.46    brazil
# 54.242.16.144   cuba
# 52.91.93.189    angola
# 54.88.128.22    peru

# 54.172.217.62   algeria
# 54.83.123.88    bulgaria
# 34.224.216.190  croatia
# 18.207.155.89   greece
# 34.229.83.206   malta

for server in ${servers}
do
    cp client.ovpn.template client.${server}
    sed -i -e 's/X.X.X.X/'"${server}"'/' client.${server}
    startTime=`date +%s`
    sudo openvpn --config ./client.${server} > openvpn.out 2>&1
    rm client.${server}
    endTime=`date +%s`

    runTime=$(( ${endTime} - ${startTime} ))
    echo "${runTime}"
done

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

servers="germany
         peru
         yuanda"

for server in ${servers}
do
    echo "${server}"
    ssh ${server} "sudo iptables -S INPUT"
done

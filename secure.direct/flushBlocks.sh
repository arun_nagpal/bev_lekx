#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

timestamp=`date`
echo "${timestamp}"

servers="germany
         peru
         yuanda"

for server in ${servers}
do
    echo "${server}"
    ssh ${server} "sudo iptables -S INPUT"
    ssh ${server} "sudo iptables -F INPUT"
    ssh ${server} "sudo iptables -S INPUT"
    echo ""
done

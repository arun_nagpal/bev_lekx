#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

threshold=700
servers="germany
peru
yuanda"

# Ignore these IP addresses.
ignoreList="34.228.68.165
54.198.197.58
34.228.157.48
54.242.16.144
34.228.169.197
52.202.143.108
54.88.128.22"

checkServers="germany
peru"

cd ${HOME}/secure.direct

scp bev_lekx@germany:/tmp/nms-secure.direct.access.log secure.direct.access-germany.log
scp bev_lekx@peru:/tmp/nms-secure.direct.access.log secure.direct.access-peru.log

OLDIFS=${IFS}
IFS='
'

for svr in ${checkServers}
do
    echo ${svr}
    ${HOME}/bin/analyseSecureDirectLog.py -f secure.direct.access-${svr}.log -t ${threshold} > \
           /tmp/getSecureDirectLog-${svr}.out
    if [ $? -eq 1 ]
    then
        for line in `grep -v "sendemail" /tmp/getSecureDirectLog-${svr}.out`
        do
            count=0
            ipaddress=`echo ${line} | awk 'BEGIN{FS=" "}{print $1}'`
            count=`echo ${line} | awk 'BEGIN{FS=" "}{print $2}'`

            # Ignore the IP address if it is on the "ignoreList".
            ignore=0
            for ip in ${ignoreList}
            do
                if [ "${ipaddress}" == "${ip}" ]
                then
                    ignore=1
                fi
            done

            # Need to pipe the comparison through bc because bash doesn't do floats.
            if [ "${ignore}" -eq 0 ]
            then
                if (( $(echo "${count} > ${threshold}" | bc -l) ))
                then
                    echo "Blocking ${ipaddress}"
                    for server in ${servers}
                    do
                        ssh ${server} "sudo iptables -A INPUT -s ${ipaddress} -j DROP"
                    done
                fi
            fi
        done
    fi
done

IFS=${OLDIFS}

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

ipAddress=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -a   - IP address to block."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o a:? -l help -- "$@"`
if [[ $? -ne 0 ]]
then
    Usage
    exit 1
fi
eval set -- "$TEMP"

while true
do
    case "$1" in
        -a) ipAddress=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

if [ -z "${ipAddress}" ]
then
    echo ""
    echo "IP address is needed."
    echo ""
    exit 1
fi

servers="germany
         peru
         yuanda"

for server in ${servers}
do
   ssh ${server} "sudo iptables -A INPUT -s ${ipAddress} -j DROP"
   ssh ${server} "sudo iptables -S INPUT"
done

#!/usr/bin/python

import time
import os

# Process runs every Monday at 12:23. Zabbix should check within an hour.
maxFileAge = 3600

fstat = os.stat('/tmp/checkReservedInstances.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

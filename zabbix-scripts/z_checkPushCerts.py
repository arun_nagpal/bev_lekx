#!/usr/bin/python

import time
import os

# Process runs at 12:14 weekdays. Zabbix should check this within an hour.
maxFileAge = 3600

fstat = os.stat('/tmp/checkPushCerts.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

threshold=9999999

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -t   - Restart threshold."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o t:h -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
        -t) threshold=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

pygtail.py -o /tmp/nms-offset.usersync.log /home/securenet_prod/log/username_sync.log > /tmp/nms-usersync.log
pscount=`wc -l /tmp/nms-usersync.log | awk 'BEGIN{FS=" "}{print $1}'`

echo "${pscount}" > /tmp/nms-usersync-count.out

if [ "${pscount}" -le "${threshold}" ]
then
    sudo /home/securenet_prod/bin/securenet_username_sync restart
fi


#!/usr/bin/python

#import json
import mysql.connector
import argparse
#import requests
import sys
import os

dbConfigCloud = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}
dbConfigAlder = {
   'user': 'sn_alder',
   'password': '5n_@ld3r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6_alder',
   'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Return row count of specific tables.', add_help=False)
parser.add_argument('-e', '--environment', type=str, default='',
                    required=False, help='Database environment (cloud, alder).')
parser.add_argument('-t', '--table', type=str, default='',
                    required=False, help='Table name.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -e, --environment  Database environment (required).")
   print("       -t, --table        Database table name (required).")
   sys.exit(0)

if len(args.environment) == 0:
   print("The \"environment\" argument is required.")
   sys.exit(1)
if len(args.table) == 0:
   print("The \"table\" argument is required.")
   sys.exit(1)

environment = 'cloud'
if args.environment:
   environment = args.environment
if environment == 'cloud':
   dbConfig = dbConfigCloud
elif environment == 'alder':
   dbConfig = dbConfigAlder
else:
   print 'Unknown environment"' + environment + '"'
   sys.exit(1)

dbTable = args.table
if len(dbTable) == 0:
    print("The \"table\" parameter is invalid.");
    sys.exit(1)

# Connect to the database.
try:
   cnx = mysql.connector.connect(**dbConfig)
except:
   print("Error connecting to the database.")
   sys.exit(1)

query = ('select count(*) from {table}' . format(table=dbTable))
rowCount=0
cursor = cnx.cursor()
try:
   cursor.execute(query)
except:
   cursor.close()
   cnx.close()
   sys.exit(1)
rows = cursor.fetchall()
for row in rows:
   rowCount = row[0]

cursor.close()
cnx.close()

print("%d" % rowCount)

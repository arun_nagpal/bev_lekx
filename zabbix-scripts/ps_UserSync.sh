#!/bin/bash

psstr="com.infinity.securenet.plugins.username_sync.UsernameSyncProcessor"

pscount=`ps ax | grep ${psstr} | grep -v grep | wc -l`

ok=1
if [ "${pscount}" -ne 1 ]
then
    ok=0
fi

echo "${ok}"



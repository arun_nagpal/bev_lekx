#!/usr/bin/python

# This script checks the last modification time of monitor.log to determine if the
# platform test is running.

import time
import os

# Process runs every 10 min. Allow a few extra minutes tolerance.
maxFileAge = 900

fstat = os.stat('/home/securenet/bin/Cloud_Australia/monitor.log')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

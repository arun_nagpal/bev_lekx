#!/usr/bin/python

# This script checks the last modification time of monitor.log to determine if the
# platform test is running.

import time
import os

# Process runs every 5 min. Allow almost 5 minutes tolerance.
maxFileAge = 599

fstat = os.stat('/tmp/checkPaTomcatEmail.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

#!/usr/bin/python

import argparse
import sys
import os
import datetime
import time
from ftplib import FTP
import random
import multiprocessing

USER="0123456789AB"
PASSWD="0123456789AB"

parser = argparse.ArgumentParser(description='Test DVR process on selected host.')
parser.add_argument('-s', '--server', type=str, default='', required=True, help='')
parser.add_argument('-p', '--passive', action="store_true",
                    required=False, help='Use passive mode.')
parser.add_argument('-d', '--debug', action="store_true",
                    required=False, help='Enable debug.')
args = parser.parse_args()

def ftptest(server, passive, debug):
   returnCode = 1

   try:
      ftp = FTP(server)
   except:
      debugLog.write('Failed to open ftp connection.\n')
      returnCode = 0
      pass

   if debug:
      ftp.set_debuglevel(2)

   if returnCode == 1:
      try:
         ftp.login(USER, PASSWD)
      except:
         returnCode = 0
         pass

   if returnCode == 1:
      try:
         ftp.set_pasv(passive)
      except:
         returnCode = 0
         pass

   if returnCode == 1:
      try:
         ftp.voidcmd('pwd')
      except:
         returnCode = 0
         pass

   if returnCode == 1:
      try:
         rn = str(random.randint(100000, 999999))
         tmpFileName = ('nms' + rn + '.jpg')
         f = open('/home/bev_lekx/bin/test001.jpg')
         ftp.storbinary('STOR ' + tmpFileName, f)
         f.close()
      except:
         returnCode = 0
         pass

   if returnCode == 1:
      try:
         found = False
         ftpFiles = []
         ftp.dir(ftpFiles.append)
         for f in ftpFiles:
            if tmpFileName in f:
               found = True
               print("%s") % (f)
            if not found:
               returnCode = 0
      except:
         returnCode = 0
         pass

   try:
      ftp.quit()
   except:
      returnCode = 0
      pass

   return returnCode

returnCode = 1

print("--------------------------------------------------------------")
print datetime.datetime.now()
print("")

# Start a process for the ftp test.
p = multiprocessing.Process(target=ftptest, args=(args.server, args.passive, args.debug,))
p.start()

counter = 0
while True:
   counter += 1
   p.join(0.1)
   if not p.is_alive():
      break
   if counter >= 20:
      p.terminate()
      p.join(0.1)
      print("Timeout waiting for ftp thread to complete.")
      returnCode = 0
      break
print("Counter: %d") % (counter)

if returnCode == 0:
   try:
      os.system('sudo /home/securenet_prod/bin/securenet_dvr restart')
   except:
      pass

print returnCode
if returnCode == 0:
   counter = 0
   debugFileName = '/home/bev_lekx/dvrtest-' + time.strftime("%Y%m%d%H%M%S", time.gmtime()) + '.txt'
   os.system('mv /tmp/dvrtest.out ' + debugFileName)
else:
   os.system('rm -f /tmp/dvrtest.out')

try:
   resultOut = open('/tmp/nms-dvrtest.out', 'w')
   resultOut.write(str(counter))
   resultOut.close()
except:
   pass

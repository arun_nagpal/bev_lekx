#!/bin/bash

retVal=0

count=`tail -100 /home/securenet_prod/log/snip.log | grep 'New panel connection. Total' | tail -1 | awk 'BEGIN{FS=" "}{print $10}' | sed 's/\[//g' | sed 's/\]//g'`
if [ "${count}" != "" ]
then
   retVal=${count}
fi

echo "${retVal}"


#!/usr/bin/python

import time
import os

# Process runs at 13:06 weekdays. Zabbix should check this within an hour.
maxFileAge = 3600

fstat = os.stat('/tmp/createIotegaDataUsageReport.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

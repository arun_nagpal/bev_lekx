#!/usr/bin/python

import time
import os

# Process runs at 14:02 on Friday. This script takes a long time to run.
# Zabbix should just verify it started.
maxFileAge = 3600

fstat = os.stat('/tmp/collectCellUsageData.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

#!/bin/bash

restartFlag="/tmp/nms-restart_dvr.txt"

if [ -f "${restartFlag}" ]
then
   echo "--------------------------------------"
   date
   sudo rm ${restartFlag}
   sudo /home/securenet_prod/bin/securenet_dvr restart
fi


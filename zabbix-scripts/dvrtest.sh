#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

USER="0123456789AB"
PASSWD="0123456789AB"
HOST=$1
PASSIVE=""
tmpFile="/tmp/dvrtest.out"

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -h   - DVR host."
   echo "           -p   - Use passive mode."
   echo ""
}

TEMP=`getopt -o h:p -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
        -h) HOST=$2
            shift 2
            ;;
        -p) PASSIVE="-p"
            shift
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

/usr/bin/ftp ${PASSIVE} -n -v ${HOST} > ${tmpFile} 2>&1 <<SCRIPT
user ${USER} ${PASSWD}
binary
dir
quit
SCRIPT

grepOut=`/bin/grep '150 File status okay; about to open data connection.' ${tmpFile}`
echo "${grepOut}"

returnCode=0
if [ -n "${grepOut}" ]
then
   returnCode=1
fi

echo ${returnCode}

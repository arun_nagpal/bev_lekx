#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

threshold=9999999

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -t   - Restart threshold."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o t:h -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
        -t) threshold=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

pygtail.py -o /tmp/nms-offset.notifier.log /home/securenet_prod/log_local/notifier.log > /tmp/nms-notifier.log
pscount=`wc -l /tmp/nms-notifier.log | awk 'BEGIN{FS=" "}{print $1}'`

echo "${pscount}" > /tmp/nms-notifier.count
grep ERROR /tmp/nms-notifier.log | wc -l > /tmp/nms-notifier.error

if [ "${pscount}" -le "${threshold}" ]
then
    sudo /home/securenet_prod/bin/securenet_notifier restart
fi


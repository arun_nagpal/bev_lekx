#!/usr/bin/python

import boto3
import argparse
import datetime
import time
import csv
import os

parser = argparse.ArgumentParser(description='Check a service.')
parser.add_argument('-m', '--metric', type=str, default='',
                    required=True, help='Service metric.')
args = parser.parse_args()

totalRcvrs = 0
failedRcvrs = 0
running = 0
value = 0

datFile = open('/tmp/ping_receivers_partners.dat', 'rb')
fileReader = csv.reader(datFile, quotechar="\"")
for row in fileReader:
   totalRcvrs = row[0]
   failedRcvrs = row[1]
   break
datFile.close()
      
fstat = os.stat('/tmp/ping_receivers_partners.dat')
tdiff = time.time() - fstat.st_mtime
if tdiff < 1500:
   running = 1

if running == 0:
   value = 0
else:
   if args.metric == 'total':
      value = int(totalRcvrs)
   elif args.metric == 'fails':
      value = int(failedRcvrs)
   elif args.metric == 'running':   
      value = running

print("%d") % (value)



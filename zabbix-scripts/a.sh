#!/bin/bash

pygtail.py -o /tmp/nms-offset.notifier.log /home/securenet_dev/abc/notifier.log > /tmp/nms-notifier.log
pscount=`wc -l /tmp/nms-notifier.log | awk 'BEGIN{FS=" "}{print $1}'`

grep ERROR /tmp/nms-notifier.log | wc -l > /tmp/nms-notifier.error

echo "${pscount}"


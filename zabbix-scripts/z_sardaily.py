#!/usr/bin/python

import time
import os

# Process runs once a day. Zabbix should check it within an hour.
maxFileAge = 3600

fstat = os.stat('/tmp/sarDaily.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

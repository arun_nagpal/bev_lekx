#!/bin/bash

# Tests if a receiver process is running and if there are errors in the log.
# Returns:
#   0 - running error free.
#   1 - not running.
#   2 - running with errors.

receiver=$1

pscount=`ps ax | grep "${receiver}.properties" | grep -v grep | wc -l`

retcode=0
if [ "${pscount}" -eq 0 ]
then
    retcode=1
fi

if [ "${retcode}" -eq 0 ]
then
    count=`tail -50 /home/securenet_prod/log/${receiver}.log | egrep -v 'Errors \[' | egrep '[Ee]xception|FATAL|ERROR' | wc -l`
    if [ "${count}" -gt 0 ]
    then
	retcode=2
    fi
fi

echo "${retcode}"

#!/usr/bin/python

import argparse
import sys

# Command-line arguments.
parser = argparse.ArgumentParser(description='', add_help=False)
parser.add_argument('-p', '--partner', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   sys.exit(0)

if not args.partner:
   sys.exit(0)

outFileName = '/tmp/dbCheckAutoInc-' + args.partner + '.out'
try:
    outFile = open(outFileName, 'rb')
except IOError:
    sys.exit(1)
outFileData = outFile.read()
outFile.close()

print("%s") % (outFileData)

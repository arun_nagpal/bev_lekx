#!/usr/bin/python

import time
import os

# Process runs every 3 hours. Zabbix checks every hour.
maxFileAge = 10800

fstat = os.stat('/tmp/checkLogAccountUsage.out')
tdiff = time.time() - fstat.st_mtime
running = 0
if tdiff < maxFileAge:
   running = 1

print("%d") % (running)

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

maxdays=5
offset=0
count=0
account=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -a   - Account (optional)."
   echo "           -c   - Record count."
   echo "           -o   - Offset."
   echo "           -m   - Update time limit in days."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o c:o:a:m:h -- "$@"`
eval set -- "$TEMP"
while true
do
    case "$1" in
        -c) count=$2
            shift 2
            ;;
        -o) offset=$2
            shift 2
            ;;
        -a) account=$2
            shift 2
            ;;
        -m) maxdays=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

if [ -n "${account}" ]
then
   SQL="SELECT CONCAT(proxy_id,\"~\",host_address,\"~\",tcp_port,\"~\",account_id_new,\"~\",updated_date) from account_gateway where account_id_new=\"${account}\""
elif [ "${count}" -gt 0 ]
then
   SQL="SELECT CONCAT(proxy_id,\"~\",host_address,\"~\",tcp_port,\"~\",account_id_new,\"~\",updated_date) from account_gateway limit ${offset},${count}"
else
   Usage
   exit 1
fi
alldata=`mysql -N -h master_database -u sn_www -p'5n_www' securenet_6 -e "$SQL" 2>/dev/null`

maxsec=$((maxdays * 86400))

for data in $alldata
do
   if [ -n "${data}" ] && [ "${data}" != "NULL" ]
   then
      proxy_id=`echo $data|cut -d'~' -f1`
      hostdb=`echo $data|cut -d'~' -f2`
      port=`echo $data|cut -d'~' -f3`
      account=`echo $data|cut -d'~' -f4`
      updateddate=`echo $data|cut -d'~' -f5`
      port=$((port + 10))

      curtime=`date +%s`
      a=$((updateddate / 1000))
      difftime=$(($curtime - $a))
      updStr=`date -d @${a} +"%Y-%m-%d %H:%M:%S"`

      output=""
      hostdiffers=0
      success=0
      host=""

      echo "---------------------------------------------------------------------"
      echo "Panel: $host $port $proxy_id $account $updStr"

      if [ "${difftime}" -gt "${maxsec}" ]
      then
         echo "Result: No update in more than ${maxdays} days or more"
         echo "---------------------------------------------------------------------"
         echo ""
         continue
      fi

      grepOut=`echo ${proxy_id} | grep ":"`
      if [ -z "${grepOut}" ]
      then
         echo "Result: Not testing this panel due to invalid proxy_id"
         echo "---------------------------------------------------------------------"
         echo ""
         continue
      fi

      host=$hostdb
      output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE 2>&1`
      echo $output
      grepOut=`echo $output | grep '{"status": "OK","payload": "ONLINE"}'`
      if [ -n "${grepOut}" ]
      then
         success=1
      else
         if [ "${hostdb}" == "server1" ]
         then
            host="server2"
         else
            host="server1"
         fi
         output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE 2>&1`
         echo $output
         grepOut=`echo $output | grep '{"status": "OK","payload": "ONLINE"}'`
         if [ -n "${grepOut}" ]
         then
            success=1
         fi
      fi

      status="OFFLINE"
      if [ "${success}" -eq 1 ]
      then
         status="ONLINE"
      fi
      differs=""
      if [ "${hostdb}" != "${host}" ] && [ "${success}" -eq 1 ]
      then
         differs="DIFFERS"
         query="update account_gateway set host_address=\"${host}\" where account_id_new=${account}"
         echo ""
         echo $query
         mysql -N -h master_database -u sn_www -p'5n_www' securenet_6 -e "${query}" 2>/dev/null
      fi
      echo ""
      echo "Result: $host $port $proxy_id $account $updStr $status $differs"
      echo "---------------------------------------------------------------------"
      echo ""
   fi
done


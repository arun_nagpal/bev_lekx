#!/bin/bash

#account_id_new=$1
offset=$1
count=$2
#if [ -n "${account_id_new}" ]
#then
#   SQL="SELECT CONCAT(proxy_id,\":\",host_address,\":\",tcp_port) from account_gateway WHERE account_id_new=${account_id_new}"
#else
if [ -n "${offset}" ] && [ -n "${count}" ]
then
   SQL="SELECT CONCAT(proxy_id,\":\",host_address,\":\",tcp_port) from account_gateway limit ${offset},${count}"
else
   SQL="SELECT CONCAT(proxy_id,\":\",host_address,\":\",tcp_port) from account_gateway"
fi
#fi
echo $SQL
alldata=`echo $SQL|mysql -N -h master_database -u sn_www -p'5n_www' securenet_6`

for data in $alldata
do
   if [ -n "${data}" ] && [ "${data}" != "NULL" ]
   then
      echo "\"$data\""
      proxy_id_1=`echo $data|cut -d':' -f1`
      proxy_id_2=`echo $data|cut -d':' -f2`
      host=`echo $data|cut -d':' -f3`
      port=`echo $data|cut -d':' -f4`
      port=$((port + 10))
      proxy_id="$proxy_id_1:$proxy_id_2"

      #host=server1
      #host=server2

      echo "Proxy Id $host $port $proxy_id"
      output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE`
      echo $output

      grepOut=`echo $output | grep OFFLINE`
      if [ -n "${grepOut}" ]
      then
         echo "Panel is offline."
         output=`./send_test_command_http.sh server1 $port $proxy_id REQUEST_SNIP_RESET 1`
         echo $output
         output=`./send_test_command_http.sh server2 $port $proxy_id REQUEST_SNIP_RESET 1`
         echo $output
      fi
   fi
done


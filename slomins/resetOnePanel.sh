#!/bin/bash

account_id_new=$1
SQL="SELECT CONCAT(proxy_id,\":\",host_address,\":\",tcp_port) from account_gateway WHERE account_id_new=${account_id_new}"
echo $SQL
data=`echo $SQL|mysql -N -h master_database -u sn_www -p'5n_www' securenet_6`

echo "\"$data\""
proxy_id_1=`echo $data|cut -d':' -f1`
proxy_id_2=`echo $data|cut -d':' -f2`
host=`echo $data|cut -d':' -f3`
port=`echo $data|cut -d':' -f4`
port=$((port + 10))
proxy_id="$proxy_id_1:$proxy_id_2"

#host=server1
#host=server2

echo "Proxy Id $host $port $proxy_id"
output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE`
echo $output
echo ""

grepOut=`echo $output | grep OFFLINE`
if [ -n "${grepOut}" ]
then
   output=`./send_test_command_http.sh server1 $port $proxy_id REQUEST_SNIP_RESET 1`
   echo $output
   echo ""
   output=`./send_test_command_http.sh server2 $port $proxy_id REQUEST_SNIP_RESET 1`
   echo $output
   echo ""
fi


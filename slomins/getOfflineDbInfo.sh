#!/bin/bash

offlinePanels=`grep -B13 -A1 '{"status": "ERROR","payload": "OFFLINE"}' getAllIsOnline*.out | grep '"' | grep -v 'ERROR'`

# getAllIsOnline_56000-57000.out-"10.2.80.146:1234:server2:9999:605994"
echo "account_id_new, vendor_id, proxy_id, host_address, tcp_port, updated_date, panel_type"

for offlinePanel in $offlinePanels
do
   if [ -n "${offlinePanel}" ] && [ "${offlinePanel}" != "NULL" ]
   then
      #echo "\"$offlinePanel\""

      data=`echo $offlinePanel | cut -d '"' -f2`

      proxy_id_1=`echo $data|cut -d':' -f1`
      proxy_id_2=`echo $data|cut -d':' -f2`
      host=`echo $data|cut -d':' -f3`
      port=`echo $data|cut -d':' -f4`
      account=`echo $data|cut -d':' -f5`

      query="select account_id_new, vendor_id, proxy_id, host_address, tcp_port, from_unixtime(updated_date /1000) as updated_date, get_name_value(\"panel_type\", parameters) as panel_type from account_gateway where account_id_new=${account}"
      mysql -N -h master_database -u sn_www -p'5n_www' securenet_6 -e "${query}" 2>/dev/null
   fi
done


#!/usr/bin/expect -f

set gateway_host [lrange $argv 0 0]
set gateway_port [lrange $argv 1 1]
set panel_id [lrange $argv 2 2]
set command [lrange $argv 3 3]
set parameters [lrange $argv 4 4]
set parameters2 [lrange $argv 5 5]
set timeout -1
# now connect to remote UNIX box (ipaddr) with given script to execute
spawn telnet $gateway_host $gateway_port
send -- "$panel_id\r"
send -- "$command\r"
send -- "$parameters\r"
send -- "$parameters2\r"
expect eof


#!/bin/bash

account_id_new=$1

SQL="SELECT CONCAT(proxy_id,\"~\",host_address,\"~\",tcp_port,\"~\",account_id_new,\"~\",updated_date) from account_gateway where account_id_new=${account_id_new}"
data=`mysql -N -h master_database -u sn_www -p'5n_www' securenet_6 -e "$SQL" 2>/dev/null`

proxy_id=`echo $data|cut -d'~' -f1`
hostdb=`echo $data|cut -d'~' -f2`
port=`echo $data|cut -d'~' -f3`
account=`echo $data|cut -d'~' -f4`
updateddate=`echo $data|cut -d'~' -f5`
port=$((port + 10))

a=$((updateddate / 1000))
updStr=`date -d @${a} +"%Y-%m-%d %H:%M:%S"`

output=""
hostdiffers=0
success=0
grepOut=`echo ${proxy_id} | grep ":"`
if [ -z "${grepOut}" ]
then
   output="Not testing this panel due to invalid proxy_id"
   echo $output
else
   host=$hostdb
   output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE 2>&1`
   echo $output
   grepOut=`echo $output | grep '{"status": "OK","payload": "ONLINE"}'`
   if [ -n "${grepOut}" ]
   then
      success=1
   fi

   if [ "${success}" -eq 0 ]
   then
      if [ "${hostdb}" == "server1" ]
      then
         host="server2"
      else
         host="server1"
      fi
      output=`./send_test_command_http.sh $host $port $proxy_id IS_ONLINE 2>&1`
      echo $output
      grepOut=`echo $output | grep '{"status": "OK","payload": "ONLINE"}'`
      if [ -n "${grepOut}" ]
      then
         success=1
         if [ "${hostdb}" != "${host}" ]
         then
            hostdiffers=1
         fi
      fi
   fi
fi

status="OFFLINE"
if [ "${success}" -eq 1 ]
then
   status="ONLINE"
fi
echo "$host ($hostdb) $port $proxy_id $account $updStr $status"
echo ""


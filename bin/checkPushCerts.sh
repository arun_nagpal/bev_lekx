#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

tmpCert="tmpcert.crt"
secPerWeek=604800
secPerMonth=2419200
notifyMsg="notifyMsg.txt"

checkCerts()
{
    # Check each cert.
    counter=1
    while [ "${counter}" -lt 120 ]
    do
        p12cert="pa-push.p12_"${counter}
        if [ -f "${p12cert}" ]
        then
            openssl pkcs12 -nokeys -in ${p12cert} -out ${tmpCert} -clcerts -password pass:0002D10942DC > /dev/null 2>&1
            if [ "$?" -eq 0 ]
            then
                # Extract the expiration date from the certificate
                certExpiry=$(openssl x509 -in ${tmpCert} -enddate -noout | sed 's/notAfter\=//')

                # Extract the issuer from the certificate
                certIssuer=$(openssl x509 -in ${tmpCert} -issuer -noout | \
                                 awk 'BEGIN {RS="/" } $0 ~ /^O=/ { print substr($0,3,17)}')

                # Grab the common name (CN) from the X.509 certificate
                certCn=$(openssl x509 -in ${tmpCert} -subject -noout | \
                             sed -e 's/.*CN=//' | \
                             sed -e 's/\/.*//')

                # Grab the serial number from the X.509 certificate
                certSerial=$(openssl x509 -in ${tmpCert} -serial -noout | \
                                 sed -e 's/serial=//')

                # Convert the date to seconds, and get the diff between NOW and the expiration date
                certJulian=$(date -d "${certExpiry}" +%s)
                nowJulian=$(date +%s)
                secDiff=$((${certJulian} - ${nowJulian}))

                if [ "${secDiff}" -lt "${secPerMonth}" ]
                then
                    echo -e "File: ${p12cert}\r\n" >> ${notifyMsg}
                    echo -e "Issuer: ${certIssuer}\r\n" >> ${notifyMsg}
                    echo -e "CN: ${certCn}\r\n" >> ${notifyMsg}
                    echo -e "Serial: ${certSerial}\r\n" >> ${notifyMsg}
                    if [ "${secDiff}" -le 0 ]
                    then
                        echo -e "Certificate has expired.\r\n" >> ${notifyMsg}
                    else
                        certExpire=$(date -d "${certExpiry}")
                        echo -e "Certificate will expire on ${certExpire}.\r\n" >> ${notifyMsg}
                    fi
                    echo -e "\r\n" >> ${notifyMsg}
                fi
            fi
        fi
        counter=$((${counter} + 1))
    done
}

cd /home/bev_lekx/pushcert

# "alder"
for prod in "cloud" "pa" "au" "eu" "p1"
do
    rm -f *

    # Copy the push certs from Germany or Phillies.
    if [ "${prod}" = "cloud" ]
    then
        scp -q bev_lekx@germany:/home/securenet_prod/etc/*.p12* .
    elif [ "${prod}" = "pa" ]
    then
        scp -q bev_lekx@phillies:/home/securenet_prod/etc/*.p12* .
    elif [ "${prod}" = "alder" ]
    then
        scp -q bev_lekx@chad:/home/securenet_prod/etc/*.p12* .
    elif [ "${prod}" = "eu" ]
    then
        scp -q bev_lekx@austria:/home/securenet_prod/etc/*.p12* .
    elif [ "${prod}" = "au" ]
    then
        scp -q bev_lekx@orange:/home/securenet_prod/etc/*.p12* .
    elif [ "${prod}" = "p1" ]
    then
        scp -q bev_lekx@server1.p1:/home/securenet_prod/etc/*.p12* .
    fi

    if [ "$?" -ne 0 ]
    then
        echo "Error copying push certs from ${prod}"
    else
        checkCerts

        # Send an email if any cert has expired or is close to expiry.
        if [ -f "${notifyMsg}" ]
        then
            /usr/bin/sendemail -s aspmx.l.google.com \
                               -f no-reply@securenettech.com \
                               -t alex.romanyuk@securenettech.com \
                                  dinh.duong@securenettech.com \
                               -u "Expiring push notification certificates for ${prod}" \
                               -m `cat ${notifyMsg}`
        fi
    fi
done

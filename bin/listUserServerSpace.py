#!/usr/bin/python

import boto3
import os
import subprocess
import sys
import argparse

ignoreServers = ['Sugar-CRM', 'Udp load balancer test 2', 'Udp load balancer test 1',
                 'Test SNIP server', 'Cisco CSR 1000V Primary', 'Cisco CSR 1000V Secondary',
                 'Default-Environment' ]
cmdline = 'awk -F":" -v "min=1000" -v "max=60000" "{ if ( \$3 >= min && \$3 <= max ) print \$1}" /etc/passwd'

def checkOneUser(ipAddress, username):
    spaceCmdLine = ('du -sh /home/' + username)
    ssh = subprocess.Popen(["ssh", '-o', 'ConnectTimeout=5', ipAddress, spaceCmdLine],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = ssh.stdout.readlines()
    for r in result:
        value = r.split('\t')[0]
        print("%s %s %s") % (ipAddress, username, value)

def checkOneServer(ipAddress, username):
    if len(username) > 0:
        checkOneUser(ipAddress, username)
    else:
        ssh = subprocess.Popen(["ssh", '-o', 'ConnectTimeout=5', ipAddress, cmdline],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
        result = ssh.stdout.readlines()
        for r in result:
            print("%s %s") % (ipAddress, r.strip())

userName = ''
server = ''

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get home disk space in servers.', add_help=False)
parser.add_argument('-u', '--user', type=str, default='',
                    required=False, help='Username.')
parser.add_argument('-s', '--server', type=str, default='',
                    required=False, help='Server name or IP address.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -u, --user       Username (required).")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

if args.user:
    userName = args.user
if args.server:
    server = args.server

if len(server) == 0:
    # Retrieves all regions/endpoints that work with EC2
    ec2 = boto3.client('ec2')
    response = ec2.describe_regions()
    for region in response['Regions']:
        regionName = region['RegionName']
        ec2_client = boto3.client('ec2', region_name=region['RegionName'])
        ec2_instances = ec2_client.describe_instances()
        for reservation in ec2_instances['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Name'] != "running":
                    continue

                instance_name=''
                for tag in instance['Tags']:
                    if tag['Key'] == 'Name' or tag['Key'] == 'name':
                        instance_name = tag['Value']

                if instance_name in ignoreServers:
                    continue

                privateIpAddress = ''
                if 'PrivateIpAddress' in instance:
                    privateIpAddress = instance['PrivateIpAddress']
                    checkOneServer(instance_name, userName)
    checkOneServer('phillies', userName)
    checkOneServer('redsox', userName)
    checkOneServer('austria', userName)
    checkOneServer('poland', userName)
    checkOneServer('denmark', userName)
    checkOneServer('server1.p1', userName)
    checkOneServer('server2.p1', userName)
else:
    checkOneServer(server, userName)

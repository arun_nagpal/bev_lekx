#!/usr/bin/python

from pprint import pprint
import boto3
import os
import subprocess
import sys
import argparse

ignoreServers = ['Sugar-CRM',
                 'Udp load balancer test 2',
                 'Udp load balancer test 1',
                 'Test SNIP server',
                 'Cisco CSR 1000V Primary',
                 'Cisco CSR 1000V Secondary',
                 'Default-Environment' ]
instance_name = ''

def disableUser(server, username, dryrun):
    cmdline = ('sudo awk -F":" -v "min=1000" -v "max=60000" '
               '"{ if ( \$3 >= min && \$3 <= max ) print \$1}" /etc/passwd')
    ssh = subprocess.Popen(["ssh", '-o', 'ConnectTimeout=5', server, cmdline],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = ssh.stdout.readlines()
    for r in result:
        rs = r.strip()
        if rs == username:
            print("%s %s %s") % (instance_name, server, rs)

            if dryrun == False:
                # Lock the account.
                cmd = ('ssh -o ConnectTimeout=5 ' + server + ' sudo usermod -L ' + username)
                print subprocess.call(cmd, shell=True)

                # As a safety option, remove any SSH keys from the user's home directory.
                cmd = ('ssh -o ConnectTimeout=5 ' + server + ' sudo rm -f /home/' + username + '/.ssh/authorized_keys')
                print subprocess.call(cmd, shell=True)

                # To disable an account, use this command. This sets the account's expire
                # date to Jan 2, 1970.
                cmd = ('ssh -o ConnectTimeout=5 ' + server + ' sudo usermod --expiredate 1 ' + username)
                print subprocess.call(cmd, shell=True)
            else:
                cmd = ('ssh -o ConnectTimeout=5 ' + server + ' sudo du -sh /home/' + username)
                print subprocess.call(cmd, shell=True)

                cmd = ('ssh -o ConnectTimeout=5 ' + server + ' sudo chage -l ' +
                       username + ' | grep "Account expires"')
                print subprocess.call(cmd, shell=True)

                cmd = ('ssh -o ConnectTimeout=5 ' + server +
                       ' sudo ls -l /home/' + username + '/.ssh/authorized_keys')
                print subprocess.call(cmd, shell=True)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Disable a user in all AWS servers.', 
                                 add_help=False)
parser.add_argument('-u', '--user', type=str, default='',
                    required=False, help='Username.')
parser.add_argument('-s', '--server', type=str, default='',
                    required=False, help='Server name or IP address.')
parser.add_argument('-d', '--dryrun', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -u, --user       Username (required).")
   print("       -s, --server     Disable user on this server only.")
   print("       -d, --dryrun     Don't actually disable the user.")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

if not args.user:
    print("Username is required.")
    sys.exit(0)

if not args.server:
    # Retrieves all regions/endpoints that work with EC2
    ec2 = boto3.client('ec2')
    response = ec2.describe_regions()
    for region in response['Regions']:
        regionName = region['RegionName']
        ec2_client = boto3.client('ec2', region_name=region['RegionName'])
        ec2_instances = ec2_client.describe_instances()
        for reservation in ec2_instances['Reservations']:
            for instance in reservation['Instances']:
                if instance['State']['Name'] != "running":
                    continue

                instance_name=''
                for tag in instance['Tags']:
                    if tag['Key'] == 'Name' or tag['Key'] == 'name':
                        instance_name = tag['Value']

                if instance_name in ignoreServers:
                    continue

                privateIpAddress = ''
                if 'PrivateIpAddress' in instance:
                    privateIpAddress = instance['PrivateIpAddress']

                disableUser(privateIpAddress, args.user, args.dryrun)
else:
    disableUser(args.server, args.user, args.dryrun)

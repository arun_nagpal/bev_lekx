#!/usr/bin/python

import sys
import os
import datetime
import time
import calendar
import signal
import argparse

startTimeStr = ''
endTimeStr = ''
logFileFormat = 'snip'
snipLogs = []
programRunning = True

def getDateFromLine(line, format):
    int_datetime = 0
    str_datetime = ''
    if format == "snip":
        # 2019-01-20 09:42:30,012-0600
        ndx = line.find(',')
        str_datetime = line[0:ndx]

    try:
        int_datetime = int(calendar.timegm(time.strptime(str_datetime, '%Y-%m-%d %H:%M:%S')))
    except:
        pass

    return str_datetime, int_datetime

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
cmdline = argparse.ArgumentParser(description="this does something.")
cmdline.add_argument('-s', '--starttime', type=str, default='',
                    required=False, help='Start date and time (YYYY-MM-DD hh:mm:ss).')
cmdline.add_argument('-e', '--endtime', type=str, default='',
                    required=False, help='End date and time (YYYY-MM-DD hh:mm:ss).')
cmdline.add_argument('-f', '--format', type=str, default='snip',
                    required=False, help='Log file format (snip, apache-access, apache-error).')
cmdline.add_argument('-p', '--printonly', action="store_true",
                    help='Just print start and end dates of the log file(s).')
cmdline.add_argument('logfiles', nargs='*')
args = cmdline.parse_args()

if len(args.logfiles) == 0:
    cmdline.print_help()
    exit(1)
if args.format != "snip":
    print("Invalid log file format")
    exit(1)
if not args.printonly and (len(args.starttime) == 0 or len(args.endtime) == 0):
    cmdline.print_help()
    exit(1)

if not args.printonly:
    startSec = int(calendar.timegm(time.strptime(args.starttime, '%Y-%m-%d %H:%M:%S')))
    endSec = int(calendar.timegm(time.strptime(args.endtime, '%Y-%m-%d %H:%M:%S')))
    numElements = (endSec - startSec) / 60
    data = {}
    counter = 0
    while counter < numElements:
        data[counter] = 0
        counter += 1

for log in args.logfiles:
    logFd = open(log, "r")

    if args.printonly:
        firstdate = ''
        lastdate = ''
        for line in logFd:
            str_datetime, int_datetime = getDateFromLine(line, args.format)
            if len(str_datetime) > 0:
                firstdate = str_datetime
                break
        logFd.seek(-5000, 2)
        for line in logFd:
            str_datetime, int_datetime = getDateFromLine(line, args.format)
            if int_datetime > 0:
                lastdate = str_datetime
        print log, firstdate, lastdate
    else:
        for line in logFd:
            if not programRunning:
                break
            str_datetime, int_datetime = getDateFromLine(line, args.format)
            if int_datetime < startSec or int_datetime > endSec:
                continue
            index = (s_datetime - startSec) / 60
            if index >= 0 and index < numElements:
                data[index] += 1

    logFd.close()
    if not programRunning:
        break

if not args.printonly:
    counter = 0
    while counter < numElements:
        g_sec = startSec + (counter * 60)
        tstamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(g_sec))
        print tstamp, data[counter]
        counter += 1

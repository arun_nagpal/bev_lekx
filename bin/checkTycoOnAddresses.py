#!/usr/bin/python

import csv
import json
import argparse
import sys
import dns.resolver     # sudo pip install dnspython
import subprocess
import os

tmp_installgo_com_att_zone = '/tmp/tmp_tycoon_installgo.com.att.zone'
tmp_installgo_com_zone = '/tmp/tmp_tycoon_installgo.com.zone'
tmp_tyco_io_zone = '/tmp/tmp_tycoon_tyco.io.zone'
tmp_myiptables = '/tmp/tmp_tycoon_myiptables'

def checkDns(dns_name):
    results = []
    myResolver = dns.resolver.Resolver()
    myAnswers = myResolver.query(dns_name, "A")
    for rdata in myAnswers:
        results.append(str(rdata))
    return results

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Check TycoOn IP addresses.')
parser.add_argument('-c', '--checkonly', action='store_true')
parser.add_argument('-f', '--force', action='store_true')
parser.add_argument('-w', '--workfile', type=str, default='/tmp/TycoOnAddresses.csv',
                    required=False, help='Work file.')
args = parser.parse_args()

# Read the csv file.
csvfile = None
try:
    csvfile = open(args.workfile, 'rb')
except IOError:
    print "1"
    sys.exit(1)

retCode = 0
hosts = {}

# Read the current TycoOn IP addresses.
csvreader = csv.reader(csvfile, quotechar="\"")
for line in csvreader:
    if len(line) < 2:
        continue
    dnsName = line[0].strip()
    ipAddress = line[1].strip()

    if dnsName not in hosts:
        hosts[dnsName] = {}
        hosts[dnsName]['old'] = []
        hosts[dnsName]['new'] = []
        hosts[dnsName]['mismatch'] = False
    hosts[dnsName]['old'].append(ipAddress)
csvfile.close()

# Read the current DNS values for each TycoOn host.
for host in hosts:
    dns_recs = checkDns(host)
    for dns_rec in dns_recs:
        hosts[host]['new'].append(dns_rec)

haveMismatch = False
for host in hosts:
    oneHostMismatch = False
    print("%s") % (host)

    oldValues = hosts[host]['old']
    newValues = hosts[host]['new']

    # Check current IP addresses against DNS results.
    for ip in oldValues:
        if ip not in newValues:
            haveMismatch = True
            oneHostMismatch = True
            hosts[host]['mismatch'] = True

    # Print the results for a host.
    if oneHostMismatch:
        print("  Old")
        for ip in oldValues:
            print("    %s") % (ip)

        print("  New")
        for dns_rec in newValues:
            print("    %s") % (dns_rec)
    else:
        for ip in oldValues:
            print("  %s") % (ip)

# Handle changes.
# Update gsm-primary and cisco1.
# if (haveMismatch and not args.checkonly) or args.force:
    # print("Update gsm-primary")

    # # /etc/my-iptables.rules
    # # outFile = open(tmp_myiptables, 'w')
    # for host in hosts:
    #     newValues = hosts[host]['new']
    #     outFile.write("# {host}\n" . format(host=host))
    #     for dns_rec in newValues:
    #         outFile.write("-A POSTROUTING -s 10.0.0.0/8 -d {ip}/32 -j MASQUERADE\n" . format(ip=dns_rec))
    #     outFile.write("\n")
    # outFile.close()

    # # /etc/bind/zones
    # # 1. tyco.io.zone
    # outFile = open(tmp_tyco_io_zone, 'w')
    # outFile.write("tyco.io. IN      SOA     ns1.tyco.io. admin.scl365.com. (\n")
    # outFile.write("          yyyymmddss \n")
    # outFile.write("        86400           ; refresh, seconds\n")
    # outFile.write("7200            ; retry, seconds\n")
    # outFile.write("3600000         ; expire, seconds\n")
    # outFile.write("86400 )\n")
    # outFile.write("\n")
    # outFile.write("tyco.io.\tIN\tNS\tns1.tyco.io.\n")
    # outFile.write("\n")
    # outFile.write("ns1\t\tIN\tA\t172.29.255.4\n")
    # for host in hosts:
    #     if '.tyco.io' in host:
    #         newValues = hosts[host]['new']
    #         for dns_rec in newValues:
    #             outFile.write("{host}\tIN\tA\t{ip}\n" . format(host=host, ip=dns_rec))
    # outFile.close()

    # # 2. installgo.com.zone
    # outFile = open(tmp_installgo_com_zone, 'w')
    # outFile.write("installgo.com. IN      SOA     ns1.installgo.com. admin.scl365.com. (\n")
    # outFile.write("          yyyymmddss \n")
    # outFile.write("        86400           ; refresh, seconds\n")
    # outFile.write("7200            ; retry, seconds\n")
    # outFile.write("3600000         ; expire, seconds\n")
    # outFile.write("86400 )\n")
    # outFile.write("\n")
    # outFile.write("installgo.com.\tIN\tNS\tns1.installgo.com.\n")
    # outFile.write("\n")
    # outFile.write("ns1\t\tIN\tA\t172.29.255.4\n")
    # for host in hosts:
    #     if '.installgo.com' in host:
    #         newValues = hosts[host]['new']
    #         for dns_rec in newValues:
    #             outFile.write("{host}\tIN\tA\t{ip}\n" . format(host=host, ip=dns_rec))
    # outFile.close()

    # # 3. installgo.com.att.zone
    # outFile = open(tmp_installgo_com_att_zone, 'w')
    # outFile.write("installgo.com. IN      SOA     ns1.installgo.com. admin.scl365.com. (\n")
    # outFile.write("          yyyymmddss \n")
    # outFile.write("        86400           ; refresh, seconds\n")
    # outFile.write("7200            ; retry, seconds\n")
    # outFile.write("3600000         ; expire, seconds\n")
    # outFile.write("86400 )\n")
    # outFile.write("\n")
    # outFile.write("installgo.com.\tIN\tNS\tns1.installgo.com.\n")
    # outFile.write("\n")
    # outFile.write("ns1\t\tIN\tA\t172.29.255.4\n")
    # for host in hosts:
    #     if '.installgo.com' in host:
    #         newValues = hosts[host]['new']
    #         for dns_rec in newValues:
    #             outFile.write("{host}\tIN\tA\t{ip}\n" . format(host=host, ip=dns_rec))
    # outFile.close()

    # Update gsm-primary with the new TycoOn IP address.
    # os.system('scp "%s" "%s:%s"' % (tmp_installgo_com_att_zone, 'gsm-primary', tmp_installgo_com_att_zone))
    # os.system('scp "%s" "%s:%s"' % (tmp_installgo_com_zone, 'gsm-primary', tmp_installgo_com_zone))
    # os.system('scp "%s" "%s:%s"' % (tmp_tyco_io_zone, 'gsm-primary', tmp_tyco_io_zone))
    # os.system('scp "%s" "%s:%s"' % (tmp_myiptables, 'gsm-primary', tmp_myiptables))
    # os.system('ssh %s "%s"' % ('gsm-primary', '/home/bev_lekx/bin/tycoOnIpChange-gsmprimary.sh'))

    # # Update cisco1 with new routes.
    # print("Update cisco1")
    # cmds = []
    # for host in hosts:
    #     if hosts[host]['mismatch']:
    #         oldValues = hosts[host]['old']
    #         newValues = hosts[host]['new']
    #         for ip in oldValues:
    #             cmds.append('"no ip route {ip} 255.255.255.255 172.29.255.4" ' . format(ip=ip))
    #         for ip in newValues:
    #             cmds.append('"ip route {ip} 255.255.255.255 172.29.255.4" ' . format(ip=ip))
    #         cmdline = '/home/bev_lekx/bin/cisco1Config.exp '
    #         for cmd in cmds:
    #             cmdline += cmd
    #         os.system(cmdline)

# Update /tmp/TycoOnAddresses.csv
if (haveMismatch and not args.checkonly) or args.force:
    tycoOnFile = open(args.workfile, 'w')
    for host in hosts:
        dns_recs = checkDns(host)
        for dns_rec in dns_recs:
            tycoOnFile.write(host + ',' + dns_rec + '\n')
    tycoOnFile.close()

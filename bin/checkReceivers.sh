#!/bin/bash

ps="innerrange
oh_receiver
oh_receiver_a1
oh_receiver_alarm_central
oh_receiver_alarm_force
oh_receiver_atlas
oh_receiver_bensaus1
oh_receiver_bensaustralia
oh_receiver_centra_larm
oh_receiver_central_bpg
oh_receiver_coastcomaus2
oh_receiver_curtis_elite
oh_receiver_dependable_alarm
oh_receiver_first_county_monitoring
oh_receiver_nmc
oh_receiver_nyconaus1
oh_receiver_nyconaus2
oh_receiver_post_alarms
oh_receiver_safeguard
oh_receiver_samsaustralia
oh_receiver_samsaustralia2
oh_receiver_us_monitoring
oh_receiver_ztech
ucc_a1air
ucc_ahs
ucc_dome_alert
ucc_entouch
ucc_guru
ucc_home_alert
ucc_master_alert
ucc_monitronics"

for p in ${ps}
do
   clear
   echo "${p}"

   ssh snip1 "ps ax | grep \"${p}.properties\" | grep -v grep" 

   echo ""
   echo -n "Press ENTER to check the next receiver."
   read
done


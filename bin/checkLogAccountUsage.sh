#!/bin/bash

dbHost="db-cluster.cluster-cd8ohozdxf8u.us-east-1.rds.amazonaws.com"
dbName="securenet_6"
cfgFile="/home/bev_lekx/.mysqloptions.cfg"

month=`date +%m`
year=`date +%Y`

date

# Check dvr_archive.
query="select dvr_archive from log_account_usage where account_id_new=\"419005\" and month=\"${month}\" and year=\"${year}\";"
dvrUsage=`mysql --defaults-file=${cfgFile} --batch -L -N -h ${dbHost} -D ${dbName} -e "${query}"`
echo "dvr_archive: ${dvrUsage}"
if [ "${dvrUsage}" -gt 500 ]
then
   echo "Resetting dvr_archive in log_account_usage"
   query="update log_account_usage set dvr_archive='0' where account_id_new='419005' and month=${month} and year=${year}"
   mysql --defaults-file=${cfgFile} -h ${dbHost} -D ${dbName} -e "${query}"
fi


# Check email.
query="select email from log_account_usage where account_id_new=\"419005\" and month=\"${month}\" and year=\"${year}\";"
emailUsage=`mysql --defaults-file=${cfgFile} --batch -L -N -h ${dbHost} -D ${dbName} -e "${query}"`
echo "email: ${emailUsage}"
if [ "${emailUsage}" -gt 9500 ]
then
   echo "Resetting email in log_account_usage"
   query="update log_account_usage set email='0' where account_id_new='419005' and month=${month} and year=${year}"
   mysql --defaults-file=${cfgFile} -h ${dbHost} -D ${dbName} -e "${query}"
fi


#!/usr/bin/python

#
#

import mysql.connector
import argparse
import sys
import os
import datetime
import time
import boto3

# Cloud database parameters.
# dbUser = 'bev_lekx'
dbUser = 'sn_notifier'
# dbPassword = 'ahNg7IfooPaechee3beiriegeeSeesh1'
dbPassword = '5n_n0t1f13r'
# dbHost = 'db2.cd8ohozdxf8u.us-east-1.rds.amazonaws.com'
dbHost = 'db1.cd8ohozdxf8u.us-east-1.rds.amazonaws.com'
dbDatabase = 'securenet_6'

# {
# u'Endpoint': {u'HostedZoneId': 'Z2R2ITUGPM61AM', u'Port': 3306, u'Address': 'db5.cd8ohozdxf8u.us-east-1.rds.amazonaws.# com'},
# u'DBInstanceStatus': 'available',
# u'DBClusterIdentifier': 'db-cluster',
# u'DBInstanceClass': 'db.r3.8xlarge',
# u'DBInstanceIdentifier': 'db5'
# }

# ec2 = boto3.client('rds')
# response = ec2.describe_regions()
# for region in response['Regions']:
#     print region['RegionName']

# response = ec2.describe_db_instances()
# r = response['DBInstances']
# for a in r:
#    dbHost = a['Endpoint']['Address']

# Connect to the SecureNet database.
connectOK = False
try:
    cnx = mysql.connector.connect(user=dbUser, password=dbPassword,
                                  database=dbDatabase, host=dbHost)
    connectOK = True
except:
    print("Error connecting to the database.")
    pass

if connectOK == True:
    queryOK = False
    cursor = cnx.cursor()
    query = ('SELECT ID,USER,HOST,DB,COMMAND,TIME,STATE '
             'FROM INFORMATION_SCHEMA.PROCESSLIST '
             'WHERE command != "Sleep" AND '
             '      command != "Killed" AND '
             '      time >= 30 order by time DESC')
#             '      USER = "sn_smarttech" AND '
    try:
        cursor.execute(query)
        queryOK = True
    except:
        cursor.close()
        cnx.close()
        pass

    if queryOK == True:
        rows = cursor.fetchall()

        for row in rows:
            procid = row[0]
            user = row[1]
            host = row[2]
            db = row[3]
            cmd = row[4]
            runtime = row[5]
            state = row[6]

            print('%s %s %s %s %s %d %s') % (procid, user, host, db, cmd, runtime, state)

            # killCursor = cnx.cursor()
            # killQuery = ('CALL mysql.rds_kill(' + str(procid) + ')')
            # killCursor.execute(killQuery)
            # killCursor.close()

    # Close the cursor.
    cursor.close()

    # Close the database connection.
    cnx.close()

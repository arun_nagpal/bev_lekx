#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

user=""
addr=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -a <addr> - Address to whitelist."
   echo "           -h        - Display this help message."
   echo ""
}

TEMP=`getopt -o u:a:h -- "$@"`
if [[ $? != 0 || $# < 1 ]]
then
   echo "Insufficient parameters."
   Usage
   exit 1
fi

eval set -- "$TEMP"
while true
do
   case "$1" in
      -a) addr=$2
          shift 2
          ;;
      --) shift
          break
          ;;
      *)  Usage
          exit 1
          ;;
   esac
done

if [ -z "${addr}" ]
then
    echo "${scriptName}: an address is required."
    Usage
    exit 1
fi

servers="germany peru wales egypt israel ghana angola cuba"
for server in ${servers}
do
    echo -n "Update snip.properties on ${server}? (y/N): "
    REPLY=""
    read
    if [[ $REPLY == "y" || $REPLY == "Y" ]]
    then
        echo "${scriptName}: copying snip.properties from ${server}."
        scp ${server}:/home/securenet_prod/etc/snip.properties .
        if [ "$?" -ne 0 ]
        then
            echo "${scriptName}: error obtaining snip.properties from ${server}."
            exit 1
        fi

        stg=`grep pa_processor.panel.ignore_port_regex snip.properties | grep -v '#'`
        if [ -z "${stg}" ]
        then
            echo "${scriptName}: error parsing snip.properties."
            exit 1
        fi

        stg2=`echo "${stg}" | sed 's/)\.\*)\\$/|'"${addr}"')\.\*)\\$/'`

        sed -i "/^pa_processor.panel.ignore_port_regex=*/c\\${stg2}" snip.properties
        if [ "$?" -ne 0 ]
        then
            echo "${scriptName}: error updating snip.properties from ${server}."
            exit 1
        fi

        result=`grep pa_processor.panel.ignore_port_regex snip.properties | grep -v '#'`

        echo "Whitelist was:"
        echo "${stg}"
        echo ""
        echo "Whitelist is now:"
        echo "${result}"
        echo ""
        echo -n "Apply the new whitelist to ${server}? (y/N): "
        REPLY=""
        read
        if [[ $REPLY == "y" || $REPLY == "Y" ]]
        then
            echo "Applying the new whitelist to ${server}"
            scp snip.properties ${user}@${server}:.
            if [ "$?" -ne 0 ]
            then
                echo "${scriptName}: error copying snip.properties to ${server}."
            else
                ssh ${server} "sudo cp snip.properties /home/securenet_prod/etc"
                if [ "$?" -ne 0 ]
                then
                    echo "${scriptName}: error copying snip.properties to /home/securenet_prod/etc on ${server}."
                else
                    echo -n "Restart SNIP on ${server}? (y/N): "
                    REPLY=""
                    read
                    if [[ $REPLY == "y" || $REPLY == "Y" ]]
                    then
                        echo "${scriptName}: restarting SNIP on ${server}"
                        ssh ${server} "sudo /home/securenet_prod/bin/securenet_snip restart"
                        numproc=`ssh ${server} "ps ax | grep snip | grep -v grep | wc -l"`
                        echo "${scriptName}: number of SNIP processes = ${numproc} (should be 2)."
                    fi
                fi
            fi
        fi
    fi
done

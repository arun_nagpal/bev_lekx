#!/bin/bash

dbHost="db-cluster.cluster-cd8ohozdxf8u.us-east-1.rds.amazonaws.com"
dbName="securenet_6"
cfgFile="/home/bev_lekx/.mysqloptions.cfg"

month=`date +%m`
year=`date +%Y`

query="update receiver set parameters=NULL where parameters LIKE \"%not_responding=1%\""
mysql --defaults-file=${cfgFile} --batch -L -N -h ${dbHost} -D ${dbName} -e "${query}"
if [ $? -ne 0 ]
then
   echo `date` "Error clearing reciever parameters." >> /tmp/clearReceiverParameters.err
fi

echo "Complete"

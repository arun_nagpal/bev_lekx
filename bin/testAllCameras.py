#!/usr/bin/python

# http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_instances

from pprint import pprint
import boto3
import os

ec2 = boto3.client('ec2')
# Retrieves all regions/endpoints that work with EC2
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            if instance['State']['Name'] != "running":
                continue

            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']

            availZone = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']

            location = 'ec2'
            if "VpcId" in instance:
                location = 'vpc'

            platform = 'Linux'
            if "Platform" in instance:
                platform = instance['Platform']

            instance_id = instance['InstanceId']
            image_id = instance['ImageId']

            publicIpAddress = ''
            if 'PublicIpAddress' in instance:
                publicIpAddress = instance['PublicIpAddress']
            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']

            volumes = ec2_client.describe_volumes(Filters = [
                {
                    'Name' : "attachment.instance-id",
                    'Values' : [instance_id]
                }
            ])

            if instance_name.lower() == 'india':
                continue
            if instance_name.lower() == 'slovakia':
                continue
            if instance_name.lower() == 'slovenia':
                continue
            if instance_name.lower() == 'nms':
                continue
            if instance_name.lower() == 'tools':
                continue
            if instance_name.lower() == 'sweden':
                continue
            if instance_name.lower() == 'gsm-primary':
                continue
            if instance_name.lower() == 'qatar':
                continue
            if instance_name.lower() == 'cisco csr 1000v primary':
                continue
            if instance_name.lower() == 'cisco csr 1000v secondary':
                continue
            if instance_name.lower() == 'udp load balancer test 1':
                continue
            if instance_name.lower() == 'udp load balancer test 2':
                continue
            if instance_name.lower() == 'sugar-crm':
                continue
            if instance_name.lower() == 'andrey-test-1':
                continue
            if instance_name.lower() == 'test snip server':
                continue
            if instance_name.lower() == 'default-environment':
                continue

            hostname = instance_name.lower().replace(' ', '-')
            print(("%s\t%s") % (hostname, privateIpAddress))
            os.system('scp /home/bev_lekx/bin/testCameras.py bev_lekx@' + privateIpAddress + ':.')
            os.system('ssh bev_lekx@' + privateIpAddress + ' "sudo ./testCameras.py > testCameras-' +
                      hostname + '.out"')
            os.system('scp bev_lekx@' + privateIpAddress + ':testCameras-' + hostname + '.out .')
            os.system('ssh bev_lekx@' + privateIpAddress + ' "rm testCameras-' + hostname + '.out"')
            os.system('ssh bev_lekx@' + privateIpAddress + ' "rm ./testCameras.py"')

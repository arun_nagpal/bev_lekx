#!/bin/bash

# Restart the snip application on each of the snip servers.

scriptName=`basename $0`
scriptDir=`dirname $0`

Usage()
{
    echo ""
    echo "${scriptName} restarts the snip application on each of the snip servers."
    echo "Usage:"
    echo "  ${scriptName} <OPTIONS>"
    echo "  OPTIONS: -h    - Display this help message."
    echo ""
}

if [ "$#" -gt 0 ]
then
    Usage
    exit 1
fi

servers="germany peru wales egypt israel ghana angola cuba"
for server in ${servers}
do
    echo -n "Restart SNIP on ${server}? (y/N): "
    REPLY=""
    read
    if [[ $REPLY == "y" || $REPLY == "Y" ]]
    then
        echo "${scriptName}: restarting SNIP on ${server}"
        ssh ${server} "sudo /home/securenet_prod/bin/securenet_snip restart"
        sleep 2
        numproc=`ssh ${server} "ps ax | grep snip | grep -v grep | wc -l"`
        echo "${scriptName}: number of SNIP processes = ${numproc} (should be 2)."
    fi
done

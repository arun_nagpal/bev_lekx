#!/usr/bin/env python

import boto3.session
import datetime
import time
from dateutil.tz import tzutc

def dateConvertAwsToYYYYMMDD(sourceDate):
    result = ''
    try:
        if '.' in str(sourceDate):
            result = datetime.datetime.strptime(str(sourceDate), "%Y-%m-%d %H:%M:%S.%f+00:00").strftime("%Y-%m-%d")
        else:
            result = datetime.datetime.strptime(str(sourceDate), "%Y-%m-%d %H:%M:%S+00:00").strftime("%Y-%m-%d")
    except FormatError:
        result = ''

    return result

now = datetime.datetime.utcnow().replace(tzinfo=tzutc())

sgs = []

ec2 = boto3.client('ec2')
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=regionName)

    # Create a list of security groups.
    # ec2_sec_groups = ec2_client.describe_security_groups(GroupIds=['sg-02ad0477'])
    ec2_sec_groups = ec2_client.describe_security_groups()
    for sec_group in ec2_sec_groups['SecurityGroups']:
        from_port = ''
        to_port = ''
        ip_protocol = ''
        for perm in sec_group['IpPermissions']:
            sec_group_name = sec_group['GroupName']
            sec_group_id = sec_group['GroupId']
            if 'FromPort' in perm:
                from_port = str(perm['FromPort'])
            if 'ToPort' in perm:
                to_port = str(perm['ToPort'])
            if 'IpProtocol' in perm:
                ip_protocol = perm['IpProtocol']

            for ip_range in perm['IpRanges']:
                ip_range_descr = ''
                ip_range_cidr = ''
                if 'Description' in ip_range:
                    ip_range_descr = ip_range['Description']
                if 'CidrIp' in ip_range:
                    ip_range_cidr = ip_range['CidrIp']

                g = (regionName, sec_group_name, sec_group_id, from_port, to_port, ip_protocol,
                     ip_range_cidr, ip_range_descr)
                sgs.append(g)

for a in sgs:
    regionName, groupName, groupId, from_port, to_port, ip_protocol, ip_range_cidr, ip_range_descr = a
    print(('%s,%s,%s,%s,%s,%s,%s,%s') %
          (regionName, groupName, groupId, from_port, to_port, ip_protocol,
           ip_range_cidr, ip_range_descr))

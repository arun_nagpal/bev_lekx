#!/usr/bin/python

# This script returns the Ethernet MAC address of all Slomins panels.

import json
import mysql.connector
import argparse
import requests
import sys

dbConfigCloudSlomins = {
   'user': 'sn_slomins',
   'password': '5n_510m1n5',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6_slomins',
   'raise_on_warnings': False,
}

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**dbConfigCloudSlomins)
except:
   print("Error connecting to the database.")
   sys.exit(1)

query = ('select account_id_new, vendor_id from account_gateway')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   account_id_new = str(row[0])
   vendor_id = str(row[1])

   mac = ''
   query2 = ('select get_name_value("mac", json) as mac ' +
             'from account_panel_configuration ' +
             'where account_id_new=' + account_id_new +
             ' and id=16')
   cursor2 = cnx.cursor()
   cursor2.execute(query2)
   apc_rows = cursor2.fetchall()
   for apc_row in apc_rows:
      mac = str(apc_row[0]).replace('"','')
   if len(mac) > 1:
      if mac[0:1] == ':':
         mac = mac[1:]
   cursor2.close()

   print ("%s %s %s") % (vendor_id, account_id_new, mac)
cursor.close()
cnx.close()

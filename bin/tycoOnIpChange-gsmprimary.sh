scriptName=`basename $0`
scriptDir=`dirname $0`

zoneDir="/etc/bind/zones"

# Update the gsm-primary my-iptables.rules file.
sed '/---TycoOnTycoOnTycoOnStart---/q' /etc/my-iptables.rules > /tmp/my-iptables.rules; \
cat /tmp/tmp_tycoon_myiptables >> /tmp/my-iptables.rules; \
sed -ne '/---TycoOnTycoOnTycoOnEnd---/,$ p' /etc/my-iptables.rules >> /tmp/my-iptables.rules; \
sudo mv /tmp/my-iptables.rules /etc/my-iptables.rules
sudo sh -c "iptables-restore < /etc/my-iptables.rules"
sudo sh -c "iptables-save > /etc/iptables.rules"

for zoneFile in installgo.com.zone installgo.com.att.zone tyco.io.zone
do
    # Get the current revision number.
    currRev=`sudo head -2 ${zoneDir}/${zoneFile} | tail -1 | awk 'BEGIN{FS=" "}{print $1}'`
    currRevDate=${currRev:0:8}
    currRevNum=${currRev:8:2}

    # Update the revision number.
    dateStg=`date +%Y%m%d`
    newRev=""
    if [ "${currRevDate}" == "${dateStg}" ]
    then
        newRevNum=$(( 10#$currRevNum + 1 ))
        newRev="${dateStg}$(printf '%02d' ${newRevNum})"
    else
        newRev="${dateStg}01"
    fi
    echo ${currRev}
    echo ${newRev}

    # Copy the tmp zone file to the production zone file.
    tmpZoneFile="/tmp/tmp_tycoon_${zoneFile}"
    cp ${tmpZoneFile} ${zoneDir}/${zoneFile}

    # Update the zone file serial number.
    sudo sed -i -e "s/yyyymmddss/${newRev}/g" ${zoneDir}/${zoneFile}
done

# Reload bind.
sudo kill -SIGHUP `ps ax | grep named | grep -v grep | awk 'BEGIN{FS=" "}{print $1}'`

# Verify bind reload was successful.
sudo tail -100 /var/log/syslog | grep named | grep reloading

#!/usr/bin/python

# http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_instances

from pprint import pprint
import boto3

# print("Region,Zone,Name,Public IP,Private IP,Type,Instance ID,Image ID,Platform,Volume ID,Volume Size")
counter = 0
ec2 = boto3.client('ec2')
# Retrieves all regions/endpoints that work with EC2
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            instance_state = instance['State']['Name']

            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']

            availZone = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']
            instance_id = instance['InstanceId']
            image_id = instance['ImageId']

            location = 'ec2'
            if "VpcId" in instance:
                location = 'vpc'

            networkInterfaceList = []
            networkInterfaces = instance['NetworkInterfaces']
            for networkInterface in networkInterfaces:
                networkInterfaceList.append(networkInterface['NetworkInterfaceId'])

            platform = 'Linux'
            if "Platform" in instance:
                platform = instance['Platform']

            publicIpAddress = ''
            if 'PublicIpAddress' in instance:
                publicIpAddress = instance['PublicIpAddress']
            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']

            secGrps = ''
            for secgrp in instance['SecurityGroups']:
                secgrpName = secgrp['GroupName']
                secgrpId = secgrp['GroupId']
                if len(secGrps) > 0:
                    secGrps += ', '
                secGrps += secgrpName

            volumes = ec2_client.describe_volumes(Filters = [
                {
                    'Name' : "attachment.instance-id",
                    'Values' : [instance_id]
                }
            ])

#            pstg = ''
#            firstVol = True
#            for volume in volumes.get('Volumes',[]):
#                volume_id = volume.get('VolumeId')
#                volume_size = str(volume.get('Size'))
#                if firstVol:
#                    pstg = (regionName + "," + az + "," + instance_name + "," + publicIpAddress +
#                            "," + privateIpAddress + "," + instance_type + "," + instance_id +
#                            "," + image_id + "," + platform + "," + volume_id + "," + volume_size)
#                else:
#                    pstg = (",,,,,,,,," + volume_id + "," + volume_size)
#                firstVol = False
#                print(("%s") %(pstg))

            # print(("    %s &  & %s & %s & %s & %s & %s \\\\") %
            #       (instance_name, publicIpAddress, privateIpAddress,
            #        instance_state, regionName, str(networkInterfaceList)))

            if counter == 0:
                print("Region, Avail Zone, Instance Type, Instance ID, Name, State, Location, Platform")

            print(("%s, %s, %s, %s, %s, %s, %s, %s (secgrps: %s)") %
                  (regionName, availZone, instance_type, instance_id, instance_name,
                   instance_state, location, platform, secGrps))

            counter += 1

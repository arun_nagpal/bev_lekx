#!/usr/bin/python

from pprint import pprint
import boto3
import os
import subprocess
import sys

# cmdline = 'awk -F":" -v "min=1000" -v "max=65535" "{ if ( \$3 >= min && \$3 <= max ) print \$1}" /etc/passwd'
cmdline = 'awk -F":" -v "min=1" -v "max=65535" "{ if ( \$3 >= min && \$3 <= max ) print \$1}" /etc/passwd'
ignoreServers = ['Sugar-CRM', 'Udp load balancer test 2', 'Udp load balancer test 1',
                 'Test SNIP server', 'Cisco CSR 1000V Primary', 'Cisco CSR 1000V Secondary',
                 'Default-Environment' ]

def checkOneServer(instance_name, ipAddress):
    ssh = subprocess.Popen(["ssh", '-o', 'ConnectTimeout=5', ipAddress, cmdline],
                           shell=False,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = ssh.stdout.readlines()
    for r in result:
        # print("%s %s %s") % (r.strip(), instance_name, ipAddress)
        print("%s %s") % (r.strip(), instance_name)
        # print("%s") % (r.strip())

# Retrieves all regions/endpoints that work with EC2
ec2 = boto3.client('ec2')
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            if instance['State']['Name'] != "running":
                continue

            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']

            if instance_name in ignoreServers:
                continue

            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']
                checkOneServer(instance_name, privateIpAddress)

checkOneServer('phillies', '172.24.32.35')
checkOneServer('redsox', '172.24.32.34')
checkOneServer('austria', '172.30.0.4')
checkOneServer('poland', '172.30.0.5')
checkOneServer('denmark', '172.30.0.7')
checkOneServer('server1.p1', 'server1.p1')
checkOneServer('server2.p1', 'server2.p1')

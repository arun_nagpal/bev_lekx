#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys
import os
import socket

dbConfigCloud = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}
dbConfigCloudSlomins = {
    'user': 'sn_slomins',
    'password': '5n_510m1n5',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6_slomins',
    'raise_on_warnings': False,
}
dbConfigCloudEurope = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'denmark',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}
dbConfigCloudAustralia = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'securenet-cluster.cluster-cujy6m6qroe4.ap-southeast-2.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}
dbConfigAlder = {
    'user': 'david_wilson',
    'password': 'd.s.wilson1',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6_alder',
    'raise_on_warnings': False,
}
dbConfigProtectAmerica = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'master_database',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}

def get_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        # Doesn't have to be reachable
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get account information.', add_help=False)
parser.add_argument('-a', '--account', type=str, default='')
parser.add_argument('-v', '--vendorid', type=str, default='')
parser.add_argument('-x', '--proxyid', type=str, default='')
parser.add_argument('-e', '--environment', type=str, default='')
parser.add_argument('-b', '--bureau', action="store_true")
parser.add_argument('-k', '--keyholder', action="store_true")
parser.add_argument('-p', '--panelcodes', action="store_true")
parser.add_argument('-d', '--dealerlogins', action="store_true")
parser.add_argument('-r', '--receiver', action="store_true")
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Get SecureNet account information.")
    print("Usage: -a, --account       Securenet account number.")
    print("       -v, --vendorid      Vendor ID.")
    print("       -x, --proxyid       Proxy ID.")
    print("       -e, --environment   Cloud environment (default = cloud).")
    print("                              cloud")
    print("                              cloud-eu")
    print("                              cloud-au")
    print("                              cloud-slomins")
    print("                              alder")
    print("                              pa")
    print("       -b, --bureau        Include bureau information.")
    print("       -k, --keyholder     Include keyholders.")
    print("       -p, --panelcodes    Include panel codes.")
    print("       -d, --dealerlogins  Include dealer logins.")
    print("       -r, --receiver      Include receivers.")
    print("       -h, -?, --help      Display this helpful message.")
    sys.exit(0)

environment = 'cloud'
if args.environment:
    environment = args.environment
else:
    # If the environment was not specified, we will make a guess using the local IP
    # address. For now I will assume this script is being run from:
    # cloud - tools
    # cloud-eu - austria
    # cloud-au - orange
    # cloud-slomins - slovakia
    # alder - chad
    # pa - phillies
    myIpAddress = get_ip()
    if myIpAddress == '172.29.253.45':
        environment = 'cloud'
    elif myIpAddress == '172.29.255.70':
        environment = 'alder'
    elif myIpAddress == '172.30.0.4':
        environment = 'cloud-eu'
    elif myIpAddress == '172.31.254.105':
        environment = 'cloud-au'
    elif myIpAddress == '172.29.250.10':
        environment = 'cloud-slomins'
    elif myIpAddress == '172.24.32.35':
        environment = 'pa'

if environment == 'cloud':
    dbConfig = dbConfigCloud
elif environment == 'cloud-slomins':
    dbConfig = dbConfigCloudSlomins
elif environment == 'cloud-eu':
    dbConfig = dbConfigCloudEurope
elif environment == 'cloud-au':
    dbConfig = dbConfigCloudAustralia
elif environment == 'alder':
    dbConfig = dbConfigAlder
elif environment == 'pa':
    dbConfig = dbConfigProtectAmerica
else:
    print('Unknown environment"' + environment + '"')
    sys.exit(1)

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**dbConfig)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Validate the options. Get an account_id from the vendor_id.
# Exit if we don't have an account_id.
account_id_new = ''
if args.account:
   account_id_new = args.account
elif args.vendorid:
   query = ('select account_id_new, vendor_id from account_gateway where vendor_id = "' +
            args.vendorid + '"')
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   if len(rows) > 1:
      for row in rows:
         print("%s, %s") % (str(row[0]), str(row[1]))
      cnx.close()
      sys.exit(0)
   else:
      for row in rows:
         account_id_new = str(row[0])
   cursor.close()
elif args.proxyid:
   query = ('select account_id_new, proxy_id from account_gateway where proxy_id = "' +
            args.proxyid + '"')
   print(query)
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   if len(rows) > 1:
      for row in rows:
         print(row[0], row[1])
      cnx.close()
      sys.exit(0)
   else:
      for row in rows:
         account_id_new = str(row[0])
   cursor.close()
else:
   print("Either an account, vendor ID or proxy ID must be specified.")
   cnx.close()
   sys.exit(1)

# Start with the account table.
# The receiver_id field links to the id field in the receiver table.
system_id = ''
receiver_id = ''
bureau_id = ''
panel_id = ''
name = ''
city = ''
state = ''
start_date = ''
last_alarm_message_date = ''
query = ('select system_id, receiver_id, bureau_id, name, city, state, '
         'from_unixtime(start_date/1000, "%c/%e/%Y %h:%i:%s") as start_date, '
         'from_unixtime(last_alarm_message_date/1000, "%c/%e/%Y %h:%i:%s") as last_alarm_message_date '
         'from account where id_new = "' + account_id_new + '"')
# print query
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
found = False
if len(rows) == 0:
   query = ('select system_id, "", bureau_id, name, city, state, '
            'from_unixtime(start_date/1000, "%c/%e/%Y %h:%i:%s") as start_date, '
            'from_unixtime(last_alarm_message_date/1000, "%c/%e/%Y %h:%i:%s") as last_alarm_message_date '
            'from account_new where id_new = "' + account_id_new + '"')
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()

for row in rows:
   found = True
   # print row
   system_id = row[0]
   receiver_id = row[1]
   bureau_id = row[2]
   name = row[3]
   city = row[4]
   state = row[5]
   start_date = row[6]
   last_alarm_message_date = row[7]
cursor.close()

if not found:
   print("Account not found.")
   cnx.close()
   sys.exit(0)

# Get account_gateway info.
proxy_id = ''
host_address = ''
tcp_port = 0
vendor_id = ''
vendor_id_2 = ''
online = 0
offline_reason = ''
updated_date = ''
fw_snet = ''
fw_vendor = ''
gateway_type_id = 0
panel_type_id = 0
cell_signal = ''
query = ('select proxy_id, host_address, tcp_port, vendor_id, vendor_id_2, online, '
         'offline_reason, from_unixtime(updated_date/1000, "%c/%e/%Y %h:%i:%s") as updated_date, '
         'get_name_value("firmware", parameters) as fw, '
         'get_name_value("fw_vendor", parameters) as pa, '
         'gateway_type_id, panel_type_id, cell_signal '
         'from account_gateway where account_id_new=' + account_id_new)
# print query
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   proxy_id = row[0]
   host_address = row[1]
   tcp_port = int(str(row[2]))
   vendor_id = row[3]
   vendor_id_2 = row[4]
   online = str(row[5])
   offline_reason = row[6]
   updated_date = row[7]
   fw_snet = row[8]
   fw_vendor = row[9]
   gateway_type_id = row[10]
   panel_type_id = row[11]
   cell_signal = row[12]
cursor.close()

print('Account ID ......: %s') % (account_id_new)
print('System ID .......: %s') % (system_id)
print('Receiver ID .....: %s') % (receiver_id)
print('Bureau ID .......: %s') % (bureau_id)
print('Customer name ...: %s') % (name)
print('City ............: %s') % (city)
print('State ...........: %s') % (state)
print('Start date ......: %s') % (start_date)
print('Last alarm date .: %s') % (last_alarm_message_date)
print('Proxy ID ........: %s') % (proxy_id)
print('SNIP host .......: %s') % (host_address)
print('SNIP port .......: %d') % (tcp_port)
print('Vendor ID .......: %s') % (vendor_id)
print('Vendor ID 2 .....: %s') % (vendor_id_2)
print('Snet firmware ...: %s') % (fw_snet)
print('Vendor firmware..: %s') % (fw_vendor)
print('Online ..........: %s') % (online)
print('Offline reason ..: %s') % (offline_reason)
print('Updated .........: %s') % (updated_date)
print('Cell signal .....: %s') % (cell_signal)
# print("")

# Gateway type
model = ''
query = ('select model from gateway_type where id=' + str(gateway_type_id))
# print query
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   model = row[0]
cursor.close()
print('Gateway type ....: %s (%d)') % (model, gateway_type_id)

# Panel type
panel_name = ''
query = ('select name from panel where id=' + str(panel_type_id))
# print query
try:
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   for row in rows:
      panel_name = row[0]
      cursor.close()
      print('Panel type ......: %s (%d)') % (panel_name, panel_type_id)
except:
   pass

# Panel status (arm/disarm).
arming_level = ''
query = ('select '
         'get_name_value("ac_failure", status) as ac_failure, '
         'get_name_value("alarm", status) as alarm, '
         'get_name_value("alarm_type", status) as alarm_type, '
         'get_name_value("arming_level", status) as arming_level, '
         'get_name_value("arming_protest", status) as arming_protest, '
         'get_name_value("bypassed_zones", status) as bypassed_zones, '
         'get_name_value("cs_comm_fail", status) as cs_comm_fail, '
         'get_name_value("low_battery", status) as low_battery, '
         'get_name_value("server_comms_fail", status) as server_comms_fail '
         'from account_panel where account_id_new=' + account_id_new)
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   ac_failure = row[0]
   alarm = row[1]
   alarm_type = row[2]
   arming_level = row[3]
   arming_protest = row[4]
   bypassed_zones = row[5]
   cs_comm_fail = row[6]
   low_battery = row[7]
   server_comms_fail = row[8]
print("AC failure ......: %s") % (ac_failure)
print("Alarm ...........: %s") % (alarm)
print("Alarm type ......: %s") % (alarm_type)
print("Arming level ....: %s") % (arming_level)
print("Arming protest ..: %s") % (arming_protest)
print("Bypassed zones ..: %s") % (bypassed_zones)
print("CS comm fail ....: %s") % (cs_comm_fail)
print("Low battery .....: %s") % (low_battery)
print("Server comms fail: %s") % (server_comms_fail)
print("")
cursor.close()

# Dealer portal logins.
# The bureau_id field in accounts links to bureau_id in the bureau_technician table.
if args.dealerlogins:
   query = ('SELECT '
            'bureau_technician.name, '
            'securenet_user.username, '
            'bureau_technician.password_plain as password '
            'FROM bureau_technician, securenet_user '
            'WHERE bureau_technician.system_id = securenet_user.id_1 '
            'AND bureau_technician.bureau_id = securenet_user.id_2 '
            'AND bureau_technician.id = securenet_user.id_3 '
            'AND securenet_user.role_id =2 '
            'AND system_id = "' + str(system_id) + '" '
            'AND bureau_id = "' + str(bureau_id) + '" ')
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   print("Dealer Logins:")
   print("%-25s %-15s %-15s") % ("Name", "username", "password")
   print("------------------------- --------------- ---------------")
   for row in rows:
      fullname = row[0]
      username = row[1]
      password = row[2]
      print("%-25s %-15s %-15s") % (fullname, username, password)
   print("")
   cursor.close()

# Panel codes.
if args.panelcodes:
   query = ('select name, pin, account_keyholder_id from account_user where account_id_new=' + account_id_new)
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   print("Panel codes:")
   print("%-20s %6s") % ("Name", "PIN")
   print("-------------------- ------")
   for row in rows:
      username = row[0]
      userpin = row[1]
      print("%-20s %6s") % (username, userpin)
   print("")
   cursor.close()

# Keyholders.
if args.keyholder:
   query = ('select name, email_address, '
            'from_unixtime(active_from_date/1000, "%c/%e/%Y %h:%i:%s") as active_from_date, '
            'from_unixtime(last_login_date/1000, "%c/%e/%Y %h:%i:%s") as last_login_date '
            'from account_keyholder where account_id_new=' + account_id_new)
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   print("Keyholders:")
   print("%-20s %-19s %-19s %s") % ("Name", "Active Since", "Last Login", "Email Address")
   print("-------------------- ------------------- ------------------- -----------------------------")
   for row in rows:
      username = row[0]
      emailaddr = row[1]
      activedate = row[2]
      lastlogin = row[3]
      print("%-20s %-19s %-19s %s") % (username, activedate, lastlogin, emailaddr)
   print("")
   cursor.close()

# Bureau
if args.bureau:
   query = ('select id_new, company_name, trading_name, company_registration_number, '
            'authorised_integrator_network, trial_bureau, address_line_1, address_line_2, suburb, '
            'city, state, post_code, country_id, world_time_zone_id, email_address, '
            'email_address_account_updates, email_address_email_replies, email_address_sms_replies, '
            'reports_email_address, phone_number_1, phone_number_2, contact_name_1, contact_name_2, '
            'from_unixtime(start_date/1000, "%c/%e/%Y %h:%i:%s") as start_date '
            'from bureau where id=' + str(bureau_id) + ' and system_id=' + str(system_id) + ' limit 1')
   cursor = cnx.cursor()
   cursor.execute(query)
   rows = cursor.fetchall()
   print("Bureau:")
   found = False
   for row in rows:
      found = True
      id_new = row[0]
      company_name = row[1]
      trading_name = row[2]
      company_registration_number = row[3]
      authorised_integrator_network = row[4]
      trial_bureau = row[5]
      address_line_1 = row[6]
      address_line_2 = row[7]
      suburb = row[8]
      city = row[9]
      state = row[10]
      post_code = row[11]
      country_id = row[12]
      world_time_zone_id = row[13]
      email_address = row[14]
      email_address_account_updates = row[15]
      email_address_email_replies = row[16]
      email_address_sms_replies = row[17]
      reports_email_address = row[18]
      phone_number_1 = row[19]
      phone_number_2 = row[20]
      contact_name_1 = row[21]
      contact_name_2 = row[22]
      start_date = row[23]

      if found:
         print("Bureau ID ......................: %s") % (id_new)
         print("Company name ...................: %s") % (company_name)
         print("Trading name ...................: %s") % (trading_name)
         # print("Company registration : %s") % (company_registration_number)
         # print(" : %s") % (authorised_integrator_network)
         # print(" : %s") % (trial_bureau)
         print("Address 1 ......................: %s") % (address_line_1)
         print("Address 2 ......................: %s") % (address_line_2)
         print("Suburb .........................: %s") % (suburb)
         print("City ...........................: %s") % (city)
         print("State ..........................: %s") % (state)
         print("Postal code ....................: %s") % (post_code)
         print("Country ........................: %s") % (country_id)
         print("Timezone ID ....................: %s") % (world_time_zone_id)
         print("Email address ..................: %s") % (email_address)
         print("Email address (account updates) : %s") % (email_address_account_updates)
         print("Email address (replies) ........: %s") % (email_address_email_replies)
         print("Email address (SMS replies) ....: %s") % (email_address_sms_replies)
         print("Email address (reports) ........: %s") % (reports_email_address)
         print("Phone numbers ..................: %s, %s") % (phone_number_1, phone_number_2)
         print("Contact names ..................: %s, %s") % (contact_name_1,contact_name_2)
         print("Start date .....................: %s") % (start_date)
         cursor.close()
         print("")

# Receiver
if args.receiver:
    query = ('select cs_receiver_id, cs_line_id, cs_prefix, cs_id, name, '
             'primary_public_host, primary_public_ip, primary_public_port, primary_private_host, '
             'primary_private_ip, primary_private_port, '
             'secondary_public_host, secondary_public_ip, secondary_public_port, secondary_private_host, '
             'secondary_private_ip, secondary_private_port, receiver_3_public_ip, '
             'receiver_3_public_port, receiver_3_private_ip, receiver_3_private_port, receiver_4_public_ip, '
             'receiver_4_public_port, receiver_4_private_ip, receiver_4_private_port, '
             'encryption_key, monitoring_account, monitoring_account_secondary, parameters, '
             'from_unixtime(active_from_date/1000, "%c/%e/%Y %h:%i:%s") as active_from_date, '
             'from_unixtime(inactive_from_date/1000, "%c/%e/%Y %h:%i:%s") as inactive_from_date, '
             'description, from_unixtime(deleted_date/1000, "%c/%e/%Y %h:%i:%s") as deleted_date, '
             'line_id '
             'from receiver where id=' + str(receiver_id) + ' and system_id=' + str(system_id))
    cursor = cnx.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    print("Receiver:")
    found = False
    for row in rows:
        found = True
        cs_receiver_id = row[0]
        cs_line_id = row[1]
        cs_prefix = row[2]
        cs_id = row[3]
        name = row[4]
        primary_public_host = row[5]
        primary_public_ip = row[6]
        primary_public_port = row[7]
        primary_private_host = row[8]
        primary_private_ip = row[9]
        primary_private_port = row[10]
        secondary_public_host = row[11]
        secondary_public_ip = row[12]
        secondary_public_port = row[13]
        secondary_private_host = row[14]
        secondary_private_ip = row[15]
        secondary_private_port = row[16]
        receiver_3_public_ip = row[17]
        receiver_3_public_port = row[18]
        receiver_3_private_ip = row[19]
        receiver_3_private_port = row[20]
        receiver_4_public_ip = row[21]
        receiver_4_public_port = row[22]
        receiver_4_private_ip = row[23]
        receiver_4_private_port = row[24]
        encryption_key = row[25]
        monitoring_account = row[26]
        monitoring_account_secondary = row[27]
        parameters = row[29]
        active_from_date = row[29]
        inactive_from_date = row[30]
        description = row[31]
        deleted_date = row[32]
        line_id = row[33]

        if found:
            print("cs_receiver_id: ................: %s") % (cs_receiver_id)
            print("cs_line_id: ....................: %s") % (cs_line_id)
            print("cs_prefix: .....................: %s") % (cs_prefix)
            print("cs_id: .........................: %s") % (cs_id)
            print("name: ..........................: %s") % (name)
            print("primary_public_host: ...........: %s") % (primary_public_host)
            print("primary_public_ip: .............: %s") % (primary_public_ip)
            print("primary_public_port: ...........: %s") % (primary_public_port)
            print("primary_private_host: ..........: %s") % (primary_private_host)
            print("primary_private_ip: ............: %s") % (primary_private_ip)
            print("primary_private_port: ..........: %s") % (primary_private_port)
            print("secondary_public_host: .........: %s") % (secondary_public_host)
            print("secondary_public_ip: ...........: %s") % (secondary_public_ip)
            print("secondary_public_port: .........: %s") % (secondary_public_port)
            print("secondary_private_host: ........: %s") % (secondary_private_host)
            print("secondary_private_ip: ..........: %s") % (secondary_private_ip)
            print("secondary_private_port: ........: %s") % (secondary_private_port)
            print("receiver_3_public_ip: ..........: %s") % (receiver_3_public_ip)
            print("receiver_3_public_port: ........: %s") % (receiver_3_public_port)
            print("receiver_3_private_ip: .........: %s") % (receiver_3_private_ip)
            print("receiver_3_private_port: .......: %s") % (receiver_3_private_port)
            print("receiver_4_public_ip: ..........: %s") % (receiver_4_public_ip)
            print("receiver_4_public_port: ........: %s") % (receiver_4_public_port)
            print("receiver_4_private_ip: .........: %s") % (receiver_4_private_ip)
            print("receiver_4_private_port: .......: %s") % (receiver_4_private_port)
            print("encryption_key: ................: %s") % (encryption_key)
            print("monitoring_account: ............: %s") % (monitoring_account)
            print("monitoring_account_secondary: ..: %s") % (monitoring_account_secondary)
            print("parameters: ....................: %s") % (parameters)
            print("active_from_date: ..............: %s") % (active_from_date)
            print("inactive_from_date: ............: %s") % (inactive_from_date)
            print("description: ...................: %s") % (description)
            print("deleted_date: ..................: %s") % (deleted_date)
            print("line_id: .......................: %s") % (line_id)

cnx.close()

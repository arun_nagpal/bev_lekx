#!/usr/bin/python

import argparse
import sys

def ip2int(ip):
    o = map(int, ip.split('.'))
    res = (pow(256, 3) * o[0]) + (pow(256, 2) * o[1]) + (256 * o[2]) + o[3]
    return res

def int2ip(ipnum):
    o1 = int(ipnum / pow(256, 3)) % 256
    o2 = int(ipnum / pow(256, 2)) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()

# Command-line arguments.
parser = argparse.ArgumentParser(description='Analyze flow log.', add_help=False)
parser.add_argument('-i', '--ipaddress', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -i, --ipaddress  Filter on one or more IP addresses.")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

if args.ipaddress:
    print(("%s: %d") % (args.ipaddress, ip2int(args.ipaddress)))


#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys

dbConfigCloud = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

dbConfigAlder = {
   'user': 'sn_alder',
   'password': '5n_@ld3r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6_alder',
   'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get account information.', add_help=False)
parser.add_argument('-e', '--environment', type=str, default='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Get offline accounts.")
    print("Usage: -e, --environment   Cloud environment (default = cloud).")
    print("                              cloud")
    print("                              alder")
    print("       -h, -?, --help      Display this helpful message.")
    sys.exit(0)

environment = 'cloud'
dbConfig = dbConfigCloud
if args.environment:
    environment = args.environment
if environment == 'cloud':
    dbConfig = dbConfigCloud
elif environment == 'alder':
    dbConfig = dbConfigAlder
else:
    print('Unknown environment"' + environment + '"')
    sys.exit(1)

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**dbConfig)
except:
   print("Error connecting to the database.")
   sys.exit(1)

proxyIdOut = None
offlineOut = None
if environment == 'cloud':
    proxyIdOut = open('/tmp/offlineaccounts.out', 'w')
    offlineOut = open('/tmp/offlineAccountsCloud.csv', 'w')
elif environment == 'alder':
    proxyIdOut = open('/tmp/offlineaccountsAlder.out', 'w')
    offlineOut = open('/tmp/offlineAccountsAlder.csv', 'w')

query = ('SELECT a.country_id, a.post_code, a.state, a.suburb, ag.proxy_id, a.id_new, '
                'a.system_id, ag.vendor_id, ag.host_address '
         'FROM account_gateway ag, account a '
         'WHERE ag.updated_date BETWEEN (get_current_time_in_millis() - 24*60*60000) '
         'AND (get_current_time_in_millis() - 90*60000) '
         'AND proxy_id IS NOT NULL AND ag.account_id_new = a.id_new')

cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
    country_id = row[0]
    post_code = row[1]
    state = row[2]
    suburb = row[3]
    proxy_id = row[4]
    id_new = row[5]
    system_id = row[6]
    vendor_id = row[7]
    host_address = row[8]
    proxyIdOut.write(proxy_id + '\n')
    offlineOut.write(str(country_id) + ',' + str(post_code) + ',' + state + ','
                     + suburb + ',' + str(proxy_id) + ',' + str(id_new) + ','
                     + str(system_id) + ',' + str(vendor_id) + ',' + str(host_address)
                     + '\n')

proxyIdOut.close()
offlineOut.close()
cursor.close()
cnx.close()

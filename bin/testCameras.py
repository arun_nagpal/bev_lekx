#!/usr/bin/python

import os
import subprocess
import time
import datetime

etcOV = "/etc/openvpn"
tcpBaseFile = "/etc/openvpn/openvpn-status-vivotek-tcp"
udpBaseFile = "/etc/openvpn/openvpn-status-vivotek-udp"
baseIppTcpFilename = '/etc/openvpn/ipp-vivotek-tcp'
baseIppUdpFilename = '/etc/openvpn/ipp-vivotek-udp'

totalClients = 0
cameras = {}

def loadStatusFile(pathname):
    count = 0
    startCounting = False
    stopCounting = False

    # SNP-062916-FZSMG,172.29.252.72:38416,113863,156339,Wed Mar 27 01:13:58 2019
    f = open(pathname, 'r')
    for line in f:
        if "ROUTING TABLE" in line:
            stopCounting = True

        if startCounting and not stopCounting:
            count = count + 1
            camera_id, c1, c2, c3, c4 = line.split(',')
            cameras[camera_id] = ('', False)

        if 'Common Name,Real Address,Bytes Received,' in line:
            startCounting = True
    f.close()
    return count

def searchIppFile(pathname, camera_id):
    # SNP-038604-LDLCP,10.252.4.238
    ipAddress = ''
    f = open(pathname, 'r')
    for line in f:
        c1, c2 = line.strip().split(',')
        if camera_id == c1:
            ipAddress = c2
            break
    f.close()
    return ipAddress

currentTime = int(time.time())

fileCounter = 0
ippFileList = []
ippFileList.append(baseIppTcpFilename + '.txt')
ippFileList.append(baseIppUdpFilename + '.txt')
while fileCounter < 50:
    fileCounter += 1
    pathname = baseIppTcpFilename + str(fileCounter) + '.txt'
    ippFileList.append(pathname)

fileCounter = 0
fileList = []
fileList.append(tcpBaseFile + '.log')
fileList.append(udpBaseFile + '.log')
while fileCounter < 50:
    fileCounter += 1
    pathname = tcpBaseFile + str(fileCounter) + '.log'
    fileList.append(pathname)

for pathname in fileList:
    if os.path.exists(pathname):
        mtime = os.stat(pathname).st_mtime
        if currentTime - mtime < 86400:
            count = loadStatusFile(pathname)
            totalClients += count
            print("%s: %s") % (pathname, count)

print("Total: %s") % (totalClients)

# Find the IP address for each camera.
for c in cameras:
    camera_id = c
    ipAddress = ''
    for ippfile in ippFileList:
        if os.path.exists(ippfile):
            ipAddress = searchIppFile(ippfile, camera_id)
            if len(ipAddress) > 0:
                break;
    cameras[camera_id] = ipAddress

# Test each camera.
for c in cameras:
    camera_id = c
    ipAddress = cameras[c]

    tcp_port = 5555
    success = False
    if len(ipAddress) > 0:
        rc = subprocess.call(['nc', '-z', '-w', '3', ipAddress, str(tcp_port)])
        if rc == 0:
            success = True

    print("%s,%s,%s") % (camera_id, ipAddress, str(success))

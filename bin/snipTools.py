#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys
import os

# ps w | grep -v grep
# netstat -n
# opkg list
# ls -l /data/fwupg/dl
# df
# ifconfig
# date
# cat /opt/vendor/.ssh/id_rsa.pub
# swconfig dev rt305x show

dbConfigCloud = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}
dbConfigCloudSlomins = {
    'user': 'sn_slomins',
    'password': '5n_510m1n5',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6_slomins',
    'raise_on_warnings': False,
}
dbConfigCloudEurope = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'denmark',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='SNIP tools.', add_help=False)
parser.add_argument('-a', '--account', type=str, default='', required=False)
parser.add_argument('-e', '--environment', type=str, default='', required=False)
parser.add_argument('-t', '--tool', type=str, default='', required=False)
parser.add_argument('-v', '--value', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -a, --account      Securenet account number (required).")
    print("       -e, --environment  Cloud environment (default = cloud).")
    print("                              cloud")
    print("                              cloud-europe")
    print("                              cloud-slomins")
    print("       -t, --tool         Execute this tool (required).")
    print("                              getkeepalive")
    print("                              isonline")
    print("                              reset")
    print("                              getstatus")
    print("       -v, --value        Value associated with the tool.")
    print("       -h, -?, --help     Display this helpful message.")
    sys.exit(0)

if len(args.account) == 0:
    print("The \"account\" argument is required.")
    sys.exit(1)
if len(args.tool) == 0:
    print("The \"tool\" argument is required.")
    sys.exit(1)

environment = 'cloud'
if args.environment:
    environment = args.environment

if environment == 'cloud':
    dbConfig = dbConfigCloud
elif environment == 'cloud-slomins':
    dbConfig = dbConfigCloudSlomins
elif environment == 'cloud-europe':
    dbConfig = dbConfigCloudEurope
else:
    print 'Unknown environment"' + environment + '"'
    sys.exit(1)

# Connect to the SecureNet database.
try:
    cnx = mysql.connector.connect(**dbConfig)
except:
    print("Error connecting to the database.")
    sys.exit(1)

#
query = ('select proxy_id, host_address, tcp_port from account_gateway ' +
         'where account_id_new = "' + args.account + '"')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
    # print row
    proxy_id = row[0]
    host_address = row[1]
    tcp_port = row[2]
pin = '5555'
cursor.close()
cnx.close()

if args.tool.lower() == 'getkeepalive':
    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=4e')
    print req
    r = requests.get(req)
    j = r.json()
    print j

    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=5')
    print req
    r = requests.get(req)
    j = r.json()
    print j

elif args.tool.lower() == 'isonline':
    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=IS_ONLINE')
    print req
    r = requests.get(req)
    if(r.ok):
        j = json.loads(r.content)
        print j['payload']
    else:
        print r.content

# <option value="3">Reset Alarm System Process only</option>
# <option value="1">Reset Securenet to Factory Defaults</option>
# <option value="2">Soft Reset</option>
elif args.tool.lower() == 'reset':
    value = 2
    if args.value:
        value = args.value
    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_RESET&p=2')
    print req
    r = requests.get(req)
    j = r.json()
    print j

elif args.tool.lower() == 'getstatus':
    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=36')
    print req
    r = requests.get(req)
    j = r.json()
    print j

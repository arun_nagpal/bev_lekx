#!/usr/bin/python

import argparse
import requests
import sys
import os
import gzip
import math
import csv
import mysql.connector
import time

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'iplocation',
  'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Analyze flow log.', add_help=False)
parser.add_argument('-f', '--file', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --file         Flow log file (required).")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

if len(args.file) == 0:
   print("The \"file\" argument is required.")
   sys.exit(1)

# Connect to the database.
cnx = mysql.connector.connect(**config)

print("Clearing existing db records.")
cursor = cnx.cursor()
cursor.execute('delete from iplocation')
cnx.commit()
cursor.close()

# "16777216","16777471","US","United States","California","Los Angeles","34.052230","-118.243680","90001"
try:
    csvfile = open(args.file, 'rb')
except IOError:
    print "Error: \"" + args.file + "\" could not be opened."
    sys.exit(1)

counter = 0
csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
    counter += 1
    startip = row[0]
    endip = row[1]
    country_id = row[2]
    country = row[3]
    state = row[4]
    city = row[5]
    latitude = row[6]
    longitude = row[7]
    zipcode = row[8].strip()

    query = ('insert into iplocation '
             + '(startip, endip, country_id, country, state, city, latitude, longitude, zipcode) '
             + 'values(' + startip + ',' + endip + ',"' + country_id + '","' + country + '","'
             + state + '","' + city + '","' + latitude + '","' + longitude + '","' + zipcode + '")')
    cursor = cnx.cursor()
    try:
        cursor.execute(query)
    except:
        print("Error in query.")
        pass
    cursor.close()

    time.sleep(0.2)
    if counter % 100 == 0:
        cnx.commit()
        print(counter)

    # print(query)
    # if counter == 3:
    #     break

cnx.commit()
cnx.close()
csvfile.close()

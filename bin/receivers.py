#!/usr/bin/python

#
#

import csv
import requests
import json
import base64
import sqlite3
import mysql.connector
import argparse
import sys
import os
import datetime
import time
import unicodedata, re

# Cloud database parameters.
config = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

colNames = [ 'id', 'system_id', 'name', 'primary_public_ip', 'primary_public_port',
             'primary_private_ip', 'primary_private_port', 'secondary_public_ip',
             'secondary_public_port', 'secondary_private_ip', 'secondary_private_port',
             'receiver_3_public_ip', 'receiver_3_public_port', 'receiver_3_private_ip',
             'receiver_3_private_port', 'receiver_4_public_ip', 'receiver_4_public_port',
             'receiver_4_private_ip', 'receiver_4_private_port' ]

control_chars = ''.join(map(unichr, range(0,32) + range(127,254)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))
def remove_control_chars(s):
   return control_char_re.sub('', s)

def scrub(s):
   r = ''
   try:
      r = remove_control_chars(s)
      # r = str(stmp).encode('ascii')
   except:
      pass
   return r

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**config)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Select all wsa's from the SecureNet database.
query = ('select id, system_id, name, primary_public_ip, primary_public_port, '
         'primary_private_ip, primary_private_port, secondary_public_ip, '
         'secondary_public_port, secondary_private_ip, secondary_private_port, '
         'receiver_3_public_ip, receiver_3_public_port, receiver_3_private_ip, '
         'receiver_3_private_port, receiver_4_public_ip, receiver_4_public_port, '
         'receiver_4_private_ip, receiver_4_private_port '
         'from receiver where system_id=90')

cursor = cnx.cursor()
try:
   cursor.execute(query)
except:
   cnx.close()
   sys.exit(1)
rows = cursor.fetchall()

print(("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s") %
      (colNames[0], colNames[1], scrub(colNames[2]), colNames[3], colNames[4],
       colNames[5], colNames[6], colNames[7], colNames[8], colNames[9], colNames[10],
       colNames[11], colNames[12], colNames[13], colNames[14], colNames[15],
       colNames[16], colNames[17], colNames[18]))

for row in rows:
   print(("%d,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s") %
         (row[0], row[1], scrub(row[2]), row[3], row[4], row[5], row[6], row[7],
         row[8], row[9], row[10], row[11], row[12], row[13], row[14], row[15],
         row[16], row[17], row[18]))
   # print(("%d,%d,%s,%s") %
   #       (row[0], row[1], scrub(row[2]), row[3]))

cursor.close()
cnx.close()

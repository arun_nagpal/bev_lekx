#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

verbose="n"

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -v   - Display additional snip debug lines"
   echo "           -h   - Display this help message."
   echo ""
}

TEMP=`getopt -o vh -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
        -v) verbose="y"
            shift
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

servers="germany wales peru egypt israel ghana angola cuba chad japan"
for server in ${servers}
do
    echo "Server: ${server}"
    if [ "${verbose}" == "y" ]
    then
        ssh bev_lekx@${server} "timeout 15 tail -f /home/securenet_prod/log/snip.log | grep -A5 \"Publishing. Topic \[VirtualTopic\""
    else
        # ssh bev_lekx@${server} "timeout 15 tail -f /home/securenet_prod/log/snip.log | grep -A5 \"Publishing. Topic \[VirtualTopic\" | grep -A5 -B5 ERROR"
        ssh bev_lekx@${server} "grep -A8 \"Publishing. Topic \" /home/securenet_prod/log/snip.log | tail -20"
    fi
    echo ""
    echo -n "Press ENTER for the next server."
    read REPLY
done

# tahiti
# grep 'Publishing. Topic \[VirtualTopic.' /home/securenet_dev/log/snip.log

# ssh bev_lekx@cuba "grep WARN /etc/activemq/data/activemq.log"

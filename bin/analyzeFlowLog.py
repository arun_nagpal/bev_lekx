#!/usr/bin/python

import boto3
import argparse
import csv
import gzip
import math
import mysql.connector
import os
import requests
import sys
import time
import signal

# Flow log IDs are in the daily notes from July 30, 2019.

# Sprint:
#  10.144.0.0/12 = 10.144.0.0 - 10.159.255.255
#  10.192.0.0/12 = 10.192.0.0 - 10.207.255.255
#  64.162.222.0/24 = 64.162.222.0 - 64.162.222.255

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}
cloudUsDbConfig = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

secGrpCache = '.secgrpCache'
ec2InstanceCache = '.ec2InstanceCache'
receiverCache = '.receiverCache'
proxyIdCache = '.proxyIdCache'

dstPorts = {}
outsideSources = {}
proxyIpAddresses = {}
receiverIpAddresses = {}
ec2Instances = []
awsSecGroups = []
ipRanges = []
portRanges = []
internalIpRanges = []
programRunning = True

def sortKey(elem):
   return elem[0]

def ip2int(ip):
    o = map(int, ip.split('.'))
    res = (pow(256, 3) * o[0]) + (pow(256, 2) * o[1]) + (256 * o[2]) + o[3]
    return res

def int2ip(ipnum):
    o1 = int(ipnum / pow(256, 3)) % 256
    o2 = int(ipnum / pow(256, 2)) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()

def getAwsSecurityGroups():
    global awsSecGroups
    awsSecGroups = []

    # If a cache files exists, read from the cache.
    if os.path.exists(secGrpCache):
        csvfile = open(secGrpCache, 'rb')
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            from_port = int(row[0].strip())
            to_port = int(row[1].strip())
            ip_protocol = row[2].strip()
            ip_start = int(row[3].strip())
            ip_end = int(row[4].strip())
            sec_group_id = row[5].strip()
            awsSecGroups.append((from_port, to_port, ip_protocol, ip_start, ip_end, sec_group_id))
        csvfile.close()
    else:
        ec2 = boto3.client('ec2')
        response = ec2.describe_regions()
        for region in response['Regions']:
            regionName = region['RegionName']
            ec2_client = boto3.client('ec2', region_name=regionName)
            ec2_sec_groups = ec2_client.describe_security_groups()
            for sec_group in ec2_sec_groups['SecurityGroups']:
                from_port = 0
                to_port = 0
                ip_protocol = ''
                for perm in sec_group['IpPermissions']:
                    sec_group_name = sec_group['GroupName']
                    sec_group_id = sec_group['GroupId']
                    if 'FromPort' in perm:
                        from_port = int(perm['FromPort'])
                    if 'ToPort' in perm:
                        to_port = int(perm['ToPort'])
                    if 'IpProtocol' in perm:
                        ip_protocol = perm['IpProtocol']
                        if ip_protocol == '-1':
                            ip_protocol = 'any'
                    if ip_protocol == 'icmp':
                        continue
                    for ip_range in perm['IpRanges']:
                        ip_start = 0
                        ip_end = 0
                        ip_range_cidr = ''
                        if 'CidrIp' in ip_range:
                            ip_range_cidr = ip_range['CidrIp']
                            baseIP = ip_range_cidr[0:ip_range_cidr.find('/')]
                            subnetSize = pow(2, 32 - int(ip_range_cidr[ip_range_cidr.find('/')+1:])) - 1
                            ip_start = ip2int(baseIP)
                            ip_end = ip_start + subnetSize
                            g = (from_port, to_port, ip_protocol, ip_start, ip_end, sec_group_id)
                            awsSecGroups.append(g)

        # Write the security groups to the cache file.
        fh = open(secGrpCache, 'w')
        for secgrp in awsSecGroups:
            from_port, to_port, ip_protocol, ip_start, ip_end, sec_group_id = secgrp
            s = (('{fp},{tp},{proto},{ips},{ipe},{id}\n') .
                  format(fp=from_port, tp=to_port, proto=ip_protocol, ips=ip_start,
                         ipe=ip_end, id=sec_group_id))
            fh.write(s)
        fh.close()

    # print(("AWS security groups: %d.") % (len(awsSecGroups)))

def getAwsEc2Instances():
    global ec2Instances
    ec2Instances = []

    # If a cache files exists, read from the cache.
    if os.path.exists(ec2InstanceCache):
        csvfile = open(ec2InstanceCache, 'rb')
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            privateIpAddressInt = int(row[0].strip())
            instance_name = row[1].strip()
            instance_id = row[2].strip()
            secgrpId = row[3].strip()
            ec2Instances.append((privateIpAddressInt, instance_name, instance_id, secgrpId))
        csvfile.close()
    else:
        ec2 = boto3.client('ec2')
        response = ec2.describe_regions()
        for region in response['Regions']:
            regionName = region['RegionName']
            ec2_client = boto3.client('ec2', region_name=region['RegionName'])
            ec2_instances = ec2_client.describe_instances()
            for reservation in ec2_instances['Reservations']:
                for instance in reservation['Instances']:
                    instance_state = instance['State']['Name']

                    instance_name=''
                    for tag in instance['Tags']:
                        if tag['Key'] == 'Name' or tag['Key'] == 'name':
                            instance_name = tag['Value']

                    instance_id = instance['InstanceId']
                    privateIpAddress = instance['PrivateIpAddress']
                    privateIpAddressInt = ip2int(privateIpAddress)

                    for secgrp in instance['SecurityGroups']:
                        secgrpId = secgrp['GroupId']
                        ec2Instances.append((privateIpAddressInt, instance_name, instance_id, secgrpId))

        # Write the EC2 instances to the cache file.
        fh = open(ec2InstanceCache, 'w')
        for instance in ec2Instances:
            privateIpAddressInt, instance_name, instance_id, secgrpId = instance
            s = (('{ip},{name},{id},{secid}\n') .
                  format(ip=privateIpAddressInt, name=instance_name, id=instance_id, secid=secgrpId))
            fh.write(s)
        fh.close()

    # print(("AWS EC2 instance security groups: %d.") % (len(ec2Instances)))

def getReceivers():
    global receiverIpAddresses
    receiverIpAddresses = {}

    # If a cache files exists, read from the cache.
    if os.path.exists(receiverCache):
        csvfile = open(receiverCache, 'rb')
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            ipInt = int(row[0])
            receiverIpAddresses[ipInt] = 0
        csvfile.close()
    else:
        snetDb = mysql.connector.connect(**cloudUsDbConfig)
        query = ('select primary_public_ip, secondary_public_ip, receiver_3_public_ip, '
                 'receiver_4_public_ip from receiver')
        cursor = snetDb.cursor()
        cursor.execute(query)
        done = False
        while not done:
            row = cursor.fetchone()
            if not row:
                done = True
            else:
                for rcvrIP in row:
                    if rcvrIP and len(rcvrIP) >= 7:
                        ipInt = ip2int(rcvrIP)
                        if ipInt not in receiverIpAddresses:
                            receiverIpAddresses[ipInt] = 0
        cursor.close()
        snetDb.close()

        # Write the receivers to the cache file.
        fh = open(receiverCache, 'w')
        for ip in receiverIpAddresses:
            s = (('{ip}\n') . format(ip=ip))
            fh.write(s)
        fh.close()

    # print(("SecureNet receivers: %d.") % (len(receiverIpAddresses)))

def getProxyIds(locDb):
    global proxyIpAddresses
    proxyIpAddresses = {}

    # If a cache files exists, read from the cache.
    if os.path.exists(proxyIdCache):
        csvfile = open(proxyIdCache, 'rb')
        csvreader = csv.reader(csvfile, delimiter=',')
        for row in csvreader:
            ipInt = int(row[0])
            proxyIpAddresses[ipInt] = 0
        csvfile.close()
    else:
        query = ('select proxy_id from snet')
        cursor = locDb.cursor()
        cursor.execute(query)
        done = False
        while not done:
            row = cursor.fetchone()
            if not row:
                done = True
            else:
                value = row[0]
                if ':' in value:
                    value = value[0:value.find(':')]
                if len(value) >= 7:
                    ipInt = ip2int(value)
                    if ipInt not in proxyIpAddresses:
                        proxyIpAddresses[ipInt] = 0
        cursor.close()

        # Write the proxy IDs to the cache file.
        fh = open(proxyIdCache, 'w')
        for ip in proxyIpAddresses:
            s = (('{ip}\n') . format(ip=ip))
            fh.write(s)
        fh.close()

    # print(("SecureNet proxy IDs: %d") % (len(proxyIpAddresses)))

def analyzeFile(locDb, fileName, ipAddresses, ports):
    if not os.path.exists(fileName):
        return
    fh = None
    try:
        if fileName.endswith('.gz'):
            fh = gzip.open(fileName, 'r')
        else:
            fh = open(fileName, "r")
    except:
        print("Error opening: %s") % (fileName)
        return

    for line in fh:
        values = map(str, line.strip().split(' '))
        if values[0] == 'version':
            continue
        if 'SKIPDATA' in line:
            continue
        if 'NODATA' in line:
            continue

        interface = values[2]
        srcaddr = values[3]
        dstaddr = values[4]
        srcaddrInt = ip2int(srcaddr)
        dstaddrInt = ip2int(dstaddr)
        srcport = 0
        try:
            srcport = int(values[5])
        except:
            print(line)
        try:
            dstport = int(values[6])
        except:
            print(line)
        protocol = int(values[7])
        packets = int(values[8])
        numbytes = int(values[9])
        starttime = values[10]
        endtime = values[11]
        action = values[12]

        starttimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(starttime)))
        endtimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(endtime)))

        # Determine if the source IP or destination IP is an internal address.
        # internalSrc = False
        # internalDst = False
        # for internalIpRange in internalIpRanges:
        #     ipStart, ipEnd = internalIpRange
        #     if srcaddrInt >= ipStart and srcaddrInt <= ipEnd:
        #         internalSrc = True
        #     if dstaddrInt >= ipStart and dstaddrInt <= ipEnd:
        #         internalDst = True

        interesting = False
        inIpRange = False
        inPortRange = False
        if ipRanges:
            for ipRange in ipRanges:
                ipStart, ipEnd = ipRange
                if srcaddrInt >= ipStart and srcaddrInt <= ipEnd:
                    inIpRange = True
                if dstaddrInt >= ipStart and dstaddrInt <= ipEnd:
                    inIpRange = True
        else:
            inIpRange = True

        if portRanges:
            for portRange in portRanges:
                portStart, portEnd = portRange
                if srcport >= portStart and srcport <= portEnd:
                    inPortRange = True
                if dstport >= portStart and dstport <= portEnd:
                    inPortRange = True
        else:
            inPortRange = True

        if inIpRange and inPortRange:
            interesting = True

        # if action == 'REJECT':
        #     interesting = False
        # if action == 'ACCEPT':
        #     interesting = False

        # Ignore traffic between internal hosts.
        # if internalSrc and internalDst:
        #     interesting = False

        # Ignore traffic from internal to outside.
        # if internalSrc:
        #     interesting = False

        # Ignore ICMP traffic.
        # if protocol == 1:
        #     interesting = False

        # # Determine if the outside IP address is a proxy IP address.
        # proxyMsg = ''
        # if srcaddrInt in proxyIpAddresses or dstaddrInt in proxyIpAddresses:
        #     proxyMsg = 'proxyIP'

        # # Determine if the outside IP address is a receiver IP address.
        # receiverMsg = ''
        # if srcaddrInt in receiverIpAddresses or dstaddrInt in receiverIpAddresses:
        #     receiverMsg = 'receiver'

        # # Determine if the outside IP address is in an AWS security group.
        # # This needs to be matched to an internal destination as well.
        # secGrpMsg = ''
        # if internalDst == True and internalSrc == False:
        #     # Search for the internal destination IP address.
        #     for ec2Instance in ec2Instances:
        #         privateIpAddressInt, instance_name, instance_id, secgrpId = ec2Instance
        #         if dstaddrInt == privateIpAddressInt:
        #             # Search the security groups associated with this ec2 instance.
        #             for a in awsSecGroups:
        #                 from_port, to_port, ip_protocol, ip_start, ip_end, sec_group_id = a
        #                 sgProtoNum = 0
        #                 if ip_protocol == 'tcp':
        #                     sgProtoNum = 6
        #                 if ip_protocol == 'udp':
        #                     sgProtoNum = 17
        #                 if secgrpId == sec_group_id:
        #                     if dstport >= from_port and dstport <= to_port and \
        #                       srcaddrInt >= ip_start and srcaddrInt <= ip_end and \
        #                       (protocol == sgProtoNum or ip_protocol == 'any'):
        #                         if (len(secGrpMsg)) == 0:
        #                             secGrpMsg = 'secgrp:'
        #                         secGrpMsg += ' '
        #                         secGrpMsg += secgrpId
        #                         # if interesting:
        #                         #     print(from_port, to_port, ip_protocol, int2ip(ip_start), int2ip(ip_end), sec_group_id)

        if interesting:
            print(("%s %15s:%-5d %15s:%-5d %-3d %s %s %s") %
                    (starttimeStr, srcaddr, srcport, dstaddr, dstport, protocol, packets,
                    numbytes, action))

            # outside_addr = 0
            # if not internalSrc:
            #     outside_addr = srcaddrInt
            # if not internalDst:
            #     outside_addr = dstaddrInt
            # if outside_addr not in outsideSources:
            #     outsideSources[outside_addr] = (0, 0, 0, 0)
            # (t_packets, t_numbytes, t_accept, t_reject) = outsideSources[outside_addr]
            # t_packets += packets
            # t_numbytes += numbytes
            # if action == 'ACCEPT':
            #     t_accept += packets
            # else:
            #     t_reject += packets
            # outsideSources[outside_addr] = (t_packets, t_numbytes, t_accept, t_reject)

        if not programRunning:
            break
    fh.close()

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Analyze flow log.', add_help=False)
parser.add_argument('-d', '--dir', type=str, default='.', required=False, help='')
parser.add_argument('-f', '--file', type=str, default='', required=False, help='')
parser.add_argument('-i', '--ipaddress', type=str, default='', required=False, help='')
parser.add_argument('-p', '--port', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -d, --dir        Flowlog directory (default = current directory).")
   print("       -f, --file       Flow log file name or part of a name (default = all files).")
   print("       -i, --ipaddress  Filter on one or more IP addresses.")
   print("       -p, --port       Filter on one or more port numbers.")
#   print("       -a, --action     Filter on action (ACCEPT, REJECT, ALL) (Default = ALL)")
#   print("       -g, --graph      Graph the results.")
#   print("       -s, --summary    Print a traffic summary only.")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

# IP addresses can be a single IP address or a range. A range is defined as:
#   a.b.c.d-e.f.g.h
# Do not put spaces before or after the dash.
ipAddresses = []
ports = []
if args.ipaddress:
    ipAddresses = map(str, args.ipaddress.split(' '))
if args.port:
    ports = map(str, args.port.split(' '))

flowLogFiles = []
for root, dirs, files in os.walk(args.dir):
    for file in files:
        if args.file in file or not args.file:
            flowLogFiles.append(os.path.join(root, file))
            if not programRunning:
                break

# Create a list of AWS servers with their security groups.
getAwsEc2Instances()

# Create a list of IP addresses from the AWS security groups.
# protocols = tcp, udp, icmp or all
getAwsSecurityGroups()

# Create a list of receiver IP addresses.
getReceivers()

# Connect to the Tools database.
locDb = mysql.connector.connect(**config)

# Create a list of proxy IP addresses from the database.
getProxyIds(locDb)

for ipAddress in ipAddresses:
    if '-' in ipAddress:
        ipStart = ip2int(ipAddress[0:ipAddress.find('-')])
        ipEnd = ip2int(ipAddress[ipAddress.find('-')+1:])
        ipRanges.append((ipStart, ipEnd))
    else:
        ipRanges.append((ip2int(ipAddress), ip2int(ipAddress)))

for port in ports:
    if '-' in port:
        portStart = int(port[0:port.find('-')])
        portEnd = int(port[port.find('-')+1:])
        portRanges.append((portStart, portEnd))
    else:
        portRanges.append((int(port), int(port)))

internalIpRanges.append((ip2int('172.16.0.0'), ip2int('172.31.255.255')))

for flowLogFile in sorted(flowLogFiles):
    # print("File: %s") % (flowLogFile)
    analyzeFile(locDb, flowLogFile, ipAddresses, ports)

# for outsideSource in sorted(outsideSources):
#     (packets, numbytes, accept, reject) = outsideSources[outsideSource]
#     # print(("%s %d %d %d") %
#     #           (outsideSource, packets, accepted, rejected))
#     print(("%15s %d %d %d %d") % (int2ip(outsideSource), packets, numbytes, accept, reject))
#     # if rejected > 0:
#     #     for port in sorted(ports):
#     #         (p_accepted, p_rejected) = ports[port]
#     #         print("   %d %d %d") % (port, p_accepted, p_rejected)
#     # if accepted > 0:
#     #     for port in sorted(ports):
#     #         (p_accepted, p_rejected) = ports[port]
#     #         print("   %d %d %d") % (port, p_accepted, p_rejected)

locDb.close()

#!/usr/bin/python

import json
import argparse
import requests
import sys
import os

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get iotega logs.', add_help=False)
parser.add_argument('-s', '--server', type=str, default='',
                    required=False, help='SNIP server.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -s, --server      Securenet account number (required).")
   print("       -h, -?, --help    Display this helpful message.")
   sys.exit(0)

if len(args.server) == 0:
   print("The \"server\" argument is required.")
   sys.exit(1)

req = ('http://' + args.server + ':10008/snip/?id=0&cmd=RELOAD_CONFIG')
print req
r = requests.get(req)
j = r.json()
print j

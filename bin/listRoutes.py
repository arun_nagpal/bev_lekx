#!/usr/bin/python

import json
#import boto.ec2
#import boto3.session
import boto3
#from pprint import pprint

# session = boto3.session.Session('us-east-1')
# ec2 = session.resource('ec2')
ec2 = boto3.resource('ec2')

# Create an instance ID to name dictionary.
instanceNames = {}
instances = ec2.instances.all()
for i in instances:
    instanceID = i.instance_id
    tags = i.tags
    for tag in tags:
        if tag['Key'] == 'Name':
            instanceNames[instanceID] = tag['Value']

# Print the route table subnets with the associated instance.
routeTable = ec2.RouteTable('rtb-281eb646')
routeAttr = routeTable.routes_attribute
for attr in routeAttr:
    cidr = ''
    instanceID = ''

    if 'DestinationCidrBlock' in attr:
        cidr = attr['DestinationCidrBlock']
    if 'InstanceId' in attr:
        instanceID = attr['InstanceId']

    # Search for the 'name' tag.
    instanceName = ''
    if instanceID in instanceNames.keys():
        instanceName = instanceNames[instanceID]

    print("%s %s") % (instanceName, cidr)

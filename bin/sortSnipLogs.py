#!/usr/bin/python

import argparse
import sys
import os

snipLogs = open('sniplogs-tmp.txt', 'w')

# Accumulate all of the snip logs into one file.
for server in [ 'angola', 'cuba', 'ghana', 'israel', 'egypt', 'peru', 'germany', 'wales' ]:
    print server
    if os.path.exists(server + '.txt'):
        rdr = open(server + '.txt')

        # For each line in the snip log, strip off the log file name so the lines
        # can be sorted by the timestamp.
        for line in rdr:
            l = line.strip()
            ndx = l.find(':')
            logFileName = l[0:ndx]
            server = logFileName.split('/')[4]
            rem = l[ndx+1:]
            ndx = rem.find(' ')
            sdate = rem[0:ndx]
            rem2 = rem[ndx+1:]
            ndx = rem2.find(' ')
            stime = rem2[0:ndx]
            rem = rem2[ndx+1:]

            snipLogs.write(sdate + ' ' + stime + ' ' + server + ' ' + rem + '\n')
        rdr.close()

snipLogs.close()

# Sort the accumulated snip log by timestamp.
os.system('sort < sniplogs-tmp.txt > sniplogs.log')
os.system('rm *.txt')

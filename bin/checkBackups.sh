#!/bin/bash

# Tools
echo "Tools"
ls -l /home/efs/snet/tools/backup
ls -l /home/efs/snet/tools/backup/`date +%a`/database
echo ""

# Registration
echo "Registration"
ls -l /home/efs/snet/registration/database
echo ""

# Zabbix
echo "NMS"
ls -l /home/efs/snet/nms/backup
ls -l /home/efs/snet/nms/backup/`date +%a`
echo ""

# EU
echo "EU"
ssh austria "ls -l /home/backup/snet/denmark/database"
echo ""

# AU
echo "AU"
ssh orange "ls -l /home/backup/snet/orange/database"
echo ""

#!/usr/bin/python

import os

etcOV = "/etc/openvpn"
tcpBaseFile = "openvpn-status-vivotek-tcp"
udpBaseFile = "openvpn-status-vivotek-udp"

totalClients = 0
cameras = {}

def loadStatusFile(pathname):
    count = 0
    startCounting = False
    stopCounting = False

    # SNP-062916-FZSMG,172.29.252.72:38416,113863,156339,Wed Mar 27 01:13:58 2019
    f = open(pathname, 'r')
    for line in f:
        if "ROUTING TABLE" in line:
            stopCounting = True

        if startCounting and not stopCounting:
            count = count + 1
            camera_id, c1, c2, c3, c4 = line.split(',')
            cameras[camera_id] = ('', False)

        if 'Common Name,Real Address,Bytes Received,' in line:
            startCounting = True
    f.close()
    return count

def searchIppFile(pathname, camera_id):
    # SNP-038604-LDLCP,10.252.4.238
    ipAddress = ''
    f = open(pathname, 'r')
    for line in f:
        c1, c2 = line.strip().split(',')
        if camera_id == c1:
            ipAddress = c2
            break
    f.close()
    return ipAddress

def searchIppFiles(camera_id):
    baseIppTcpFilename = '/etc/openvpn/ipp-vivotek-tcp'
    baseIppUdpFilename = '/etc/openvpn/ipp-vivotek-udp'
    fileCounter = 0
    ipAddress = ''

    while fileCounter < 50:
        fileCounter += 1
        pathname = baseIppTcpFilename + str(fileCounter) + '.txt'
        if not os.path.exists(pathname):
            break
        ipAddress = searchIppFile(pathname, camera_id)
        if len(ipAddress) > 0:
            break;

    return ipAddress

fileCounter = 0
while fileCounter < 50:
    fileCounter += 1
    pathname = etcOV + '/' + tcpBaseFile + str(fileCounter) + '.log'
    if not os.path.exists(pathname):
        break
    count = loadStatusFile(pathname)
    totalClients += count
    print("%s: %s") % (pathname, count)

pathname = etcOV + '/' + udpBaseFile + '.log'
if os.path.exists(pathname):
    count = loadStatusFile(pathname)
    totalClients += count
    print("%s: %s") % (pathname, count)

print("Total: %s") % (totalClients)

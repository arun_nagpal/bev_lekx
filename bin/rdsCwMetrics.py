#!/usr/bin/python

import boto3
import argparse
import datetime

parser = argparse.ArgumentParser(
   description='Obtain CloudWatch RDS statistics.')
parser.add_argument('-d', '--dbserver', type=str, default='',
                    required=True, help='Database server.')
parser.add_argument('-m', '--metric', type=str, default='',
                    required=True, help='CloudWatch RDS metric.')
args = parser.parse_args()

varStatistic = 'Average'
varPeriod = 300
varStartTime = (datetime.datetime.now() - datetime.timedelta(0, (varPeriod+60))).strftime("%Y-%b-%dT%H:%M:%S")
varEndTime = (datetime.datetime.now() - datetime.timedelta(0, 60)).strftime("%Y-%b-%dT%H:%M:%S")

# Create CloudWatch client
client = boto3.client('cloudwatch')

response = client.get_metric_statistics(
   Namespace = 'AWS/RDS',
   MetricName = args.metric,
   Dimensions = [
      {
         'Name': 'DBInstanceIdentifier',
         'Value': args.dbserver
      },
   ],
   StartTime = varStartTime,
   EndTime = varEndTime,
   Period = varPeriod,
   Statistics=[
      varStatistic,
   ],
   )

value = 0.0
dataPoints = response['Datapoints']
for dataPoint in dataPoints:
   value = dataPoint[varStatistic]
   break
if value >= 1000:
    print("%.0f") % (value)
elif value >= 100:
    print("%.1f") % (value)
else:
    print("%.2f") % (value)

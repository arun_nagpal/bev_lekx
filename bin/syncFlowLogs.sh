#!/bin/bash

find /home/bev_lekx/flowlogs/us-east-1 -name "*.gz" -type f -mmin +1440 -delete
find /home/bev_lekx/flowlogs/ap-southeast-2 -name "*.gz" -type f -mmin +1440 -delete

aws s3 sync s3://com.securenet.flowlogs/AWSLogs/898116962670/vpcflowlogs/us-east-1/`date +%Y`/`date +%m`/`date +%d`/ /home/bev_lekx/flowlogs/us-east-1
aws s3 sync s3://com.securenet.flowlogs/AWSLogs/898116962670/vpcflowlogs/ap-southeast-2/`date +%Y`/`date +%m`/`date +%d`/ /home/bev_lekx/flowlogs/ap-southeast-2
aws s3 sync s3://com.securenet.flowlogs/AWSLogs/898116962670/vpcflowlogs/ca-central-1/`date +%Y`/`date +%m`/`date +%d`/ /home/bev_lekx/flowlogs/ca-central-1


#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

servers="
Albania
Algeria
Angola
austria
Barbados
Botswana
Brazil
Canada
Chad
China
Columbia
Cuba
denmark
Dubbo
Egypt
Fiji
finland
france
Germany
Ghana
gsm-primary
India
Israel
Japan
Kiama
Mantis
nms
Orange
p2p1
p2p2
p2p3
Peru
poland
portugal
Qatar
Registration
Russia
Samoa
slovakia
slovenia
Spain
Tahiti
Tools
USA
Wales
Yuanda
"

msg=""
allauthlines=""
for server in ${servers}
do
    authlines=`ssh bev_lekx@${server} "sudo egrep \"authentication failure|Failed password|user unknown\" /var/log/auth.log | grep -v grep"`

    currsec=`date +%s`
    diffsec=0
    count=0

    IFS=$'\n'
    for authline in ${authlines}
    do
        datestr="`echo ${authline} | awk 'BEGIN{FS=" "}{print $1}'` `echo ${authline} | awk 'BEGIN{FS=" "}{print $2}'` `echo ${authline} | awk 'BEGIN{FS=" "}{print $3}'`"
        sec=`date --date="${datestr}" +%s`
        let diffsec=${currsec}-${sec}
        if [ "${diffsec}" -le 3600 ]
        then
            let count=${count}+1
            allauthlines="${allauthlines}\n${authline}"
        fi
    done
    IFS=${OLDIFS}

    if [ "${count}" -gt 0 ]
    then
        msg="${msg}${server} ${count}\n"
    fi
done

if [ -n "${msg}" ]
then
    sendemail -f no-reply@securenettech.com \
              -t dinh.duong@securenettech.com \
                 arun.nagpal@securnettech.com \
              -o tls=no \
              -u "Failed server logins" \
              -m "${msg} ${allauthlines}"
fi

#!/bin/bash

searchText=""
servers="angola
         cuba
         ghana
         israel
         egypt
         peru
         germany
         wales"

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -s   - Search text."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o s:h -- "$@"`
if [[ $? != 0 || $# < 1 ]]
then
   echo "Insufficient parameters."
   Usage
   exit 1
fi

eval set -- "$TEMP"
while true
do
   case "$1" in
      -s) searchText=$2
          shift 2
          ;;
      --) shift
          break
          ;;
      *)  Usage
          exit 1
          ;;
   esac
done

if [ -s "${searchText}" ]
then
    echo "${scriptName}: search text is required."
    Usage
    exit 1
fi

for server in ${servers}
do
    echo "${server}"
    rm -f ${server}.txt
    grep -a "${searchText}" \
         /home/efs/snet/${server}/log/snip.log  \
         /home/efs/snet/${server}/log/snip.log.? \
         /home/efs/snet/${server}/log/snip.log.[12345]? \
         >> ${server}.txt
#         /home/efs/snet/${server}/log/snip.log.[123456789]? \
done

~/bin/sortSnipLogs.py

#!/bin/bash

# This script searches for the supplied vendor IDs in each of the servers' dvr.log
# files. The most recent dvr.log timestamp is shown. The idea is this is the most
# probable current camera server.

scriptName=`basename $0`
scriptDir=`dirname $0`

servers=( "angola"
          "cuba"
          "egypt"
          "germany"
          "ghana"
          "israel"
          "peru"
          "wales"
          # "chad"
          # "japan"
          # "dubbo"
          # "kiama"
          # "orange"
          # "austria"
          # "poland"
           )

# "moscow"
# "paris"
# "p2p1"
# "p2p2"
# "p2p3"

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <list of vendor IDs>"
   echo ""
}

if [ "$#" -lt 1 ]
then
    Usage
    exit 1
fi

for vendorid in "$@"
do
    newestTimeInSec=0
    newestTimeText=""
    newestLogEntry=""
    newestServer=""

    for server in ${servers[@]}
    do
        # grepOut=`ssh bev_lekx@${server} "sudo grep -i ${vendorid} /etc/openvpn/ipp-vivotek-*.txt"`
        grepOut=`ssh bev_lekx@${server} "grep \"${vendorid}\" /home/securenet_prod/log/dvr.log | tail -1"`
        if [ -n "${grepOut}" ]
        then
            dateStg=`echo ${grepOut} | awk 'BEGIN{FS=","}{print $1}'`
            timeInSec=`date -d "${dateStg}" +%s`
            if [ "${timeInSec}" -gt "${newestTimeInSec}" ]
            then
                newestTimeInSec=${timeInSec}
                newestTimeText=${dateStg}
                newestLogEntry=${grepOut}
                newestServer=${server}
            fi
        fi
    done

    echo "${vendorid}: ${newestServer} ${newestTimeText}"
done

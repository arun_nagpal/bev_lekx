#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

Usage()
{
   echo ""
   echo "${scriptName}:"
   echo "  OPTIONS: -h <host>         - MySQL host name or IP address."
   echo "           -u <user name>    - MySQL user name."
   echo "           -p <password>     - MySQL user password."
   echo "           -d <database>     - Database name."
   echo "           -n <DNIS>         - One or more DNIS id's (all are listed if none is specified)."
   echo "           -s <system ID>    - System ID."
   echo "           -?                - Display this helpful message."
   echo ""
}

mysqlHost="172.29.255.219"
mysqlUser="sn_notifier"
mysqlPassword="5n_n0t1f13r"
mysqlDatabase="securenet_6"
dnis=""
systemId=""

TEMP=`getopt -o h:u:p:d:n:s:? -l help -- "$@"`
if [[ $? -ne 0 ]]
then
    Usage
    exit 1
fi
eval set -- "$TEMP"

while true
do
    case "$1" in
        -h) mysqlHost=$2
            shift 2
            ;;
        -u) mysqlUser=$2
            shift 2
            ;;
        -p) mysqlPassword=$2
            shift 2
            ;;
        -d) mysqlDatabase=$2
            shift 2
            ;;
        -n) dnis=$2
            shift 2
            ;;
        -s) systemId=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

if [ -z "${mysqlHost}" ] || [ -z "${mysqlDatabase}" ]  || [ -z "$mysqlUser{}" ] || \
   [ -z "${mysqlPassword}" ]
then
   echo "${scriptName}: one or more parameters is null."
   exit 1
fi

query="select system_id,name from receiver where system_id=\"${systemId}\" and id=\"${dnis}\"\G"
mysql -E -h "${mysqlHost}" -D "${mysqlDatabase}" \
      --user="${mysqlUser}" --password="${mysqlPassword}" -e "${query}"
echo ""

endQuery=""
if [ -n "${systemId}" ]
then
   endQuery="system_id=\"${systemId}\""
fi
if [ -n "${dnis}" ]
then
   if [ -n "${endQuery}" ]
   then
      endQuery="${endQuery} and "
   fi
   endQuery="${endQuery}id=\"${dnis}\""
fi

query="select id, 
      system_id, 
      primary_public_host, 
      primary_public_ip, 
      primary_public_port, 
      primary_private_host, 
      primary_private_ip, 
      primary_private_port, 
      secondary_public_host, 
      secondary_public_ip, 
      secondary_public_port, 
      secondary_private_host, 
      secondary_private_ip, 
      secondary_private_port, 
      receiver_3_public_ip, 
      receiver_3_public_port, 
      receiver_3_private_ip, 
      receiver_3_private_port, 
      receiver_4_public_ip, 
      receiver_4_public_port, 
      receiver_4_private_ip, 
      receiver_4_private_port 
   from receiver where ${endQuery};" 

echo "${query}"

if [ -n "${endQuery}" ]
then
   mysql -E -h "${mysqlHost}" -D "${mysqlDatabase}" \
         --user="${mysqlUser}" --password="${mysqlPassword}" -e "${query}"
   if [ $? -ne "0" ]
   then
      echo "${scriptName}: error executing SQL statement."
      exit 1
   fi
else
   echo "${scriptName}: error: need system_id or dnis or both."
   exit 1
fi


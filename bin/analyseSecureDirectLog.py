#!/usr/bin/python

import datetime
import time
import argparse
import calendar
import sys
import os
import operator

# Command-line arguments.
parser = argparse.ArgumentParser(description='script')
parser.add_argument('-f', '--logfile', type=str, default='',
                    required=True, help='file')
parser.add_argument('-t', '--threshold', type=int, default=10,
                    required=False, help='Report threshold')
args = parser.parse_args()

try:
 logfile = open(args.logfile, 'rb')
except IOError:
   print "Error: \"" + args.logfile + "\" could not be opened."
   sys.exit(1)

firstLineTime = 0
lastLineTime = 0
ipAddresses = {}
for line in logfile:
    ipAddress = line.split(' ')[0]
    a = line.split('[')[1]
    b = a.split(']')[0]
    c = b.split(' ')[0]
    int_datetime = int(calendar.timegm(time.strptime(c, '%d/%b/%Y:%H:%M:%S')))
    if firstLineTime == 0:
        firstLineTime = int_datetime
    lastLineTime = int_datetime
    if ipAddress in ipAddresses:
        ipAddresses[ipAddress] += 1
    else:
        ipAddresses[ipAddress] = 1

logfile.close()

haveProblem = False
emailMsg = ''
duration = float(lastLineTime - firstLineTime)
sorted_addr = sorted(ipAddresses.items(), key=operator.itemgetter(1), reverse=True)
for addr in sorted_addr:
    hitsPerMinute = addr[1] / (duration / 60)
    if hitsPerMinute >= args.threshold:
        emailMsg = emailMsg + addr[0] + '\t' + " {:0.2f}".format(hitsPerMinute) + '\n'
        haveProblem = True
    print("%-15s %.2f") % (addr[0], hitsPerMinute)

retcode = 0
if haveProblem:
   os.system('/usr/bin/sendemail ' +
             '-f no-reply@securenettech.com ' +
             '-t arun.nagpal@securenettech.com ' +
             '-o tls=no ' +
             '-u "Secure.direct usage warning" ' +
             '-m "' + emailMsg + '" ')
    retcode = 1
sys.exit(retcode)

#!/bin/bash

TIMEOUT=5
DB_HOST="db-cluster.cluster-cd8ohozdxf8u.us-east-1.rds.amazonaws.com"
LIMIT=20

counter=0
onlineCount=0
offlineCount=0
arrNets=()
arrNetOnline=()
arrNetOffline=()

netResult=""
network()
{
    local IP=$1
    local PREFIX=$2
    OLDIFS=${IFS}
    IFS=. read -r i1 i2 i3 i4 <<< $IP
    IFS=. read -r xx m1 m2 m3 m4 <<< $(for a in $(seq 1 32); do if [ $(((a - 1) % 8)) -eq 0 ]; then echo -n .; fi; if [ $a -le $PREFIX ]; then echo -n 1; else echo -n 0; fi; done)
    netResult=`printf "%d.%d.%d.%d\n" "$((i1 & (2#$m1)))" "$((i2 & (2#$m2)))" "$((i3 & (2#$m3)))"`
    IFS=${OLDIFS}
}

updateArrays()
{
    local ipaddress=$1
    local online=$2
    local netFound=0
    local netIndex=0

    network ${ipaddress} 16
    # echo "${netResult}"

    while [ "${netIndex}" -lt "${#arrNets[@]}" ]
    do
        if [ "${arrNets[${netIndex}]}" == "${netResult}" ]
        then
            netFound=1
            if [ "${online}" -eq "0" ]
            then
                arrNetOffline[${netIndex}]=$((arrNetOffline[${netIndex}] + 1))
            else
                arrNetOnline[${netIndex}]=$((arrNetOnline[${netIndex}] + 1))
            fi
        fi
        netIndex=$((netIndex + 1))
    done
    if [ "${netFound}" -ne 1 ]
    then
        num=${#arrNets[@]}
        arrNets[${num}]=${netResult}
        if [ "${online}" -eq 0 ]
        then
            arrNetOffline[${netIndex}]=1
            arrNetOnline[${netIndex}]=0
        else
            arrNetOffline[${netIndex}]=0
            arrNetOnline[${netIndex}]=1
        fi
    fi
}

query="SELECT account_id_new, id, dvr_type_id, vendor_id, host_address_vpn, tcp_port_vpn FROM account_dvr WHERE host_address_vpn IS NOT NULL"

OLDIFS=${IFS}
IFS='
'

rows=`mysql securenet_6 -h ${DB_HOST} -u sn_www -p'5n_www' -B -N -s -e "${query}"`
for ROW in ${rows}
do
    counter=$((counter + 1))

#    ROW=`echo ${line} | cut -f1`   # outputs row
    #echo "\"${ROW}\""
    ID_NEW=`echo $ROW | awk 'BEGIN{FS=" "}{print $1}'`
    DVR_ID=`echo $ROW | awk 'BEGIN{FS=" "}{print $2}'`
    DVR_TYPE_ID=`echo $ROW | awk 'BEGIN{FS=" "}{print $3}'`
    VENDOR_ID=`echo $ROW | awk 'BEGIN{FS=" "}{print $4}'`
    HOST_ADDRESS_VPN=`echo $ROW | awk 'BEGIN{FS=" "}{print $5}'`
    TCP_PORT_VPN=`echo $ROW | awk 'BEGIN{FS=" "}{print $6}'`

    stg="${counter}: ${ID_NEW} ${DVR_ID} ${DVR_TYPE_ID} ${VENDOR_ID} ${HOST_ADDRESS_VPN} ${TCP_PORT_VPN}"
    # echo ${stg}

    if [ "$HOST_ADDRESS_VPN" == "NULL" ]
    then
        echo "${stg} -"
    else
        nc -w ${TIMEOUT} -z ${HOST_ADDRESS_VPN} ${TCP_PORT_VPN}
        if [ "$?" -eq 0 ]
        then
            echo "${stg} online"
            onlineCount=$((onlineCount + 1))
            updateArrays ${HOST_ADDRESS_VPN} 1
        else
            echo "${stg} offline"
            offlineCount=$((offlineCount + 1))
            updateArrays ${HOST_ADDRESS_VPN} 0
        fi
    fi
done
echo "Total: ${counter}"
echo "Online: ${onlineCount}"
echo "Offline: ${offlineCount}"
i=0
while [ "${i}" -lt "${#arrNets[@]}" ]
do
    echo "${arrNets[${i}]} ${arrNetOnline[${i}]} ${arrNetOffline[${i}]}"
    i=$((i + 1))
done

IFS=${OLDIFS}

#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import argparse
import sys
import os
import mysql.connector
import unicodedata, re
import xlsxwriter

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

control_chars = ''.join(map(unichr, range(0,32) + range(127,254)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))

colNames = [ 'File name', 'Archive Date', 'Description', 'Vault', 'Encryption Key',
             'Archive ID' ]

def remove_control_chars(s):
   return control_char_re.sub('', s)

def scrub(s):
   r = ''
   if s != None:
      try:
         # stmp = remove_control_chars(s)
         r = str(s).encode('ascii')
      except:
         pass

   if r == 'None':
      r = ''
   return r

# Start the output file.
dateString = time.strftime("%B %d, %Y", time.gmtime())
baseFileName = 'glacier-' + time.strftime("%Y%m%d-%H%M", time.gmtime())
xlsxFileName = baseFileName + '.xlsx'
xlsxFile = open(xlsxFileName, 'w')
xlsxRow = 0

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook(xlsxFileName)
worksheet = workbook.add_worksheet()
xlsxCol = 0
for colName in colNames:
   worksheet.write(xlsxRow, xlsxCol, colName)
   xlsxCol += 1
xlsxRow += 1
worksheet.set_column(0, 0, 40)
worksheet.set_column(1, 1, 20)
worksheet.set_column(2, 2, 40)
worksheet.set_column(3, 3, 10)
worksheet.set_column(4, 4, 20)
worksheet.set_column(5, 5, 20)

# Connect to the database.
try:
   cnx = mysql.connector.connect(**config)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Select all wsa's from the SecureNet database.
query = ('select filename, '
         'encryptkey, '
         'vault, '
         'archiveid, '
         'archivedate, '
         'description '
         'from glacier '
         'order by archivedate')

cursor = cnx.cursor()
try:
   cursor.execute(query)
except:
   cnx.close()
   sys.exit(1)
rows = cursor.fetchall()

for row in rows:
   filename = scrub(row[0])
   encryptkey = scrub(row[1])
   vault = scrub(row[2])
   archiveid = scrub(row[3])
   archivedate = row[4]
   description = scrub(row[5])

   datestg = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(archivedate))

   # Write the worksheet data.
   worksheet.write(xlsxRow, 0, filename)
   worksheet.write(xlsxRow, 1, datestg)
   worksheet.write(xlsxRow, 2, description)
   worksheet.write(xlsxRow, 3, vault)
   worksheet.write(xlsxRow, 4, encryptkey)
   worksheet.write(xlsxRow, 5, archiveid)
   xlsxRow += 1

# Close the workbook.
workbook.close()

cursor.close()
cnx.close()

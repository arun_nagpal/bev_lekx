#!/usr/bin/python

import csv
import json
import mysql.connector
import argparse
import sys
import dns.resolver     # sudo pip install dnspython
import subprocess
import os

tmp_myiptables = '/tmp/tmp_rdc_myiptables'

dbConfigCloud = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'db1.cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}

def checkDns(dns_name):
    results = []
    myResolver = dns.resolver.Resolver()
    myAnswers = myResolver.query(dns_name, "A")
    for rdata in myAnswers:
        results.append(str(rdata))
    return results

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Check RDC receiver IP addresses.')
parser.add_argument('-c', '--checkonly', action='store_true')
parser.add_argument('-f', '--force', action='store_true')
parser.add_argument('-w', '--workfile', type=str, default='/tmp/RdcAddresses.csv',
                    required=False, help='Work file.')
args = parser.parse_args()

# Read the csv file.
csvfile = None
try:
    csvfile = open(args.workfile, 'rb')
except IOError:
    print "1"
    sys.exit(1)

retCode = 0
hosts = {}
haveChange = False

# Read the current RDC IP addresses.
csvreader = csv.reader(csvfile, quotechar="\"")
for line in csvreader:
    if len(line) < 2:
        continue
    dnsName = line[0].strip()
    oldAddress = line[1].strip()
    hosts[dnsName] = (oldAddress, '')
csvfile.close()

cnx = mysql.connector.connect(**dbConfigCloud)
for host in hosts:
    oldAddress, newAddress = hosts[host]
    newAddresses = checkDns(dnsName)
    if len(newAddresses) >= 1:
        newAddress = newAddresses[0]
        hosts[host] = (oldAddress, newAddress)
        if oldAddress != newAddress:
            haveChange = True
            cmd = ('sudo sed -i -e "s/{oldip}/{newip}/g" /etc/my-iptables.rules' .
                    format(oldip=oldAddress, newip=newAddress))
            print(cmd)
            if not args.checkonly:
                os.system(('ssh gsm-primary "{cmd}"') . format(cmd=cmd))
                os.system('ssh gsm-primary "bin/reloadIpTables.sh"')

                cursor = cnx.cursor()
                query = ('update receiver set primary_public_host="{newhost}", '
                         'primary_public_ip="{newip}" where id=10014;' .
                         format(newhost=newAddress, newip=newAddress))
                print(query)
                cursor.execute(query)
                cnx.commit()
                cursor.close()
cnx.close()

if not args.checkonly and haveChange:
    tycoOnFile = open(args.workfile, 'w')
    for host in hosts:
        oldAddress, newAddress = hosts[host]
        tycoOnFile.write(host + ',' + newAddress + '\n')
    tycoOnFile.close()

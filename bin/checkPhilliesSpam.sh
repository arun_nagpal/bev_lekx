#!/bin/bash

result=`ssh bev_lekx@phillies "email/checkEmailSpam.sh"`
if [ "${result}" == "1" ]
then
   curDate=`date`
   echo "${curDate} Spam in PA mail log."

   sendEmail=0
   if [ ! -f "/tmp/paspamcheck.txt" ]
   then
      sendEmail=1
   fi
   if [ "${sendEmail}" == 0 ]
   then
      # Send the email no more than once per hour.
      lastMod=`stat -t /tmp/paspamcheck.txt | awk 'BEGIN{FS=" "}{print $14}'`
      curSec=`date +%s`
      timeDiff=$((curSec-lastMod))
      echo "timeDiff: ${timeDiff}"
      if [ "${timeDiff}" -ge 3600 ]
      then
         sendEmail=1
      fi
   fi

   if [ "${sendEmail}" == 1 ]
   then
      /usr/bin/sendemail -f no-reply@securenettech.com \
                         -t dinh.duong@securenettech.com \
                            arun.nagpal@securnettech.com \
                         -o tls=no \
                         -u "Comcast is blocking Phillies due to spam" \
                         -m "Comcast is blocking Phillies due to spam"

      touch /tmp/paspamcheck.txt
   fi
fi

#!/bin/bash

workFile="/etc/zabbix/test_scripts/rootfs_test.txt"
ok=1

rm -f ${workFile}
touch ${workFile}
if [ $? -ne 0 ]
then
    ok=0
fi
if [ ! -f "${workFile}" ]
then
    ok=0
fi

rm -f ${workFile}
if [ $? -ne 0 ]
then
    ok=0
fi
if [ -f "${workFile}" ]
then
    ok=0
fi

echo "${ok}"


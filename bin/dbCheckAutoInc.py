#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import argparse
import sys
import os
import mysql.connector
import re
import subprocess
import math

def getDbCreds(partner):
    dbUser = ''
    dbPassword = ''
    dbHost = ''
    dbDatabase = ''
    cmd = subprocess.Popen(['/usr/bin/my_print_defaults', '-s', partner],
                            shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result = cmd.stdout.readlines()
    for r in result:
        rs = r.strip().split('=')
        if len(rs) == 2:
            if rs[0] == '--user':
                dbUser = rs[1]
            elif rs[0] == '--password':
                dbPassword = rs[1]
            elif rs[0] == '--host':
                dbHost = rs[1]

    if partner == 'tools':
        dbDatabase = 'celldata'
    elif partner == 'alder':
        dbDatabase = 'securenet_6_alder'
    else:
        dbDatabase = 'securenet_6'

    return dbUser, dbPassword, dbHost, dbDatabase

def usage():
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -p, --partner    Partner (cloud, cloud-eu, cloud-au, alder, pa, tools).")
   print("       -v, --verbose    Print all auto-increment fields.")
   print("       -h, -?, --help   Display this helpful message.")

# Command-line arguments.
parser = argparse.ArgumentParser(description='', add_help=False)
parser.add_argument('-p', '--partner', type=str, default='', required=False)
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   usage()
   sys.exit(0)

if not args.partner:
   usage()
   sys.exit(0)

# Get database credentials from ~/.mylogin.cnf
dbUser, dbPassword, dbHost, dbDatabase = getDbCreds(args.partner.lower())

# Login to the database.
db = mysql.connector.connect(user=dbUser, password=dbPassword, host=dbHost, database=dbDatabase)

tables = []
query = ('show tables')
cursor = db.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
    tables.append(row[0])
cursor.close()

warningPercent = 75
fieldTypes = [ 'bigint', 'mediumint', 'tinyint', 'smallint', 'int' ]
fieldMaxLimits = [ (((math.pow(2, 63) -1)*warningPercent)/100),
                   ((16777215*warningPercent)/100),
                   ((255*warningPercent)/100),
                   ((65535*warningPercent)/100),
                   ((4294967295*warningPercent)/100) ]

for table in tables:
    query = ('describe ' + table)
    cursor = db.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        fieldName = row[0]
        if 'auto_increment' in row:
            fieldMaxLimit = 0
            fieldType = ''
            counter = 0
            for z in fieldTypes:
                if z in row[1].lower():
                    fieldType = z
                    fieldMaxLimit = fieldMaxLimits[counter]
                    break
                counter += 1

            maxValue = 0
            q2 = ('select max(' + row[0] + ') from ' + table)
            c2 = db.cursor()
            c2.execute(q2)
            rows2 = c2.fetchall()
            for row2 in rows2:
                if isinstance(row2[0], int):
                    maxValue = row2[0]
            c2.close()
            status = 'OK'
            if maxValue >= fieldMaxLimit:
                status = 'Warning'
            if status == 'Warning' or args.verbose:
                print(('%-45s %-16s %-10s %d %s') % (table, fieldName, fieldType, maxValue, status))

    cursor.close()

db.close()

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

server=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -s   - Server."
   echo "           -h   - Display this help message."
   echo ""
}

TEMP=`getopt -o s:h -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
	"-s")
	    server="$2"
            shift 2
	    ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

if [ "${server}" == "" ]
then
    exit
fi

cd /home/bev_lekx/serverInfo
scriptDir=`pwd`

topDir=`pwd`
echo "${server}"
mkdir -p ${server}

nc -zv -w3 ${server} 22
if [ "$?" -eq 0 ]
then
    cd ${server}

    rm -f *
    ssh bev_lekx@${server} "uname -a" > uname.txt
    ssh bev_lekx@${server} "ps -eo pid,ppid,%mem,%cpu,cmd" > ps.out
    scp bev_lekx@${server}:/etc/crontab .

    ssh bev_lekx@${server} "sudo tar cfz etc.tgz /etc --exclude /etc/activemq/data"
    scp bev_lekx@${server}:etc.tgz .
    ssh bev_lekx@${server} "sudo rm etc.tgz"

    ssh bev_lekx@${server} "sudo tar cfz snet_etc.tgz /home/securenet*/etc"
    scp bev_lekx@${server}:snet_etc.tgz .
    ssh bev_lekx@${server} "sudo rm snet_etc.tgz"

    ssh bev_lekx@${server} "sudo lshw" > lshw.txt

    cd ${topDir}
fi

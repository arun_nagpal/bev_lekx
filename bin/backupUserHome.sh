#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

username=""
servers=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -s   - Server (option may be repeated)."
   echo "           -u   - user name."
   echo "           -h   - Display this helpful message."
   echo ""
}

TEMP=`getopt -o u:s:h -- "$@"`

eval set -- "$TEMP"
while true
do
    case "$1" in
        -s) servers="${servers} $2"
            shift 2
            ;;
        -u) username=$2
            shift 2
            ;;
        --) shift
            break
            ;;
        *)  Usage
            exit 1
            ;;
    esac
done

if [ "${username}" == "" ] || [ "${servers}" == "" ]
then
    echo "Error: a username and server are required."
    echo ""
    Usage
    exit 1
fi

dateStg=`date +%Y%m%d`
for server in ${servers}
do
    tarFile="/tmp/${server}-${username}-${dateStg}.tgz"
    ssh -o ConnectTimeout=5 bev_lekx@${server} "sudo nice -n19 tar cfz ${tarFile} /home/${username}"
    scp bev_lekx@${server}:${tarFile} .
    ssh -o ConnectTimeout=5 bev_lekx@${server} "sudo rm ${tarFile}"
done

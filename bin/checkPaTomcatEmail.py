#!/usr/bin/python

# This script is intended to be run from cron.
# The script checks an email inbox for tomcat error messages from Redsox or
# Phillies. If back-to-back errors are received from one or both of these servers,
# the tomcat service on the affected server is restarted.
# Email received dates and times are in UTC.

import time
import datetime
#from datetime import datetime
from pytz import timezone
from dateutil import parser
import os
import smtplib
import imaplib
import email
from os.path import expanduser

EMAIL_ADDR  = "bev.lekx@securenettech.com"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

def checkEmail(host):
    retVal = False
    homeDir = expanduser("~")
    credsFile = open(homeDir + '/.emailcreds.txt')
    credsData = credsFile.read()
    credsFile.close()

    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(EMAIL_ADDR, credsData)
    mail.select('inbox')

    # Search for all main, in the inbox, received since 24 hours ago.
    # Email search granularity is days.
    date = (datetime.date.today() - datetime.timedelta(1)).strftime("%d-%b-%Y")
    search = '(FROM "monitoring@securenettech.com" SUBJECT "Warning: PA {host} Mobile App API (tomcat7) monitor test failed" SENTSINCE {date})' . format(date=date, host=host)
    result, data = mail.search(None, search)
    mail_ids = data[0]

    # If no mail, return.
    if len(mail_ids) == 0:
         mail.logout()
         return retVal

    # Get the first and last email message IDs.
    id_list = mail_ids.split()
    first_email_id = int(id_list[0])
    latest_email_id = int(id_list[-1])

    firstMsg = True
    done = False
    lastMdate = None

    # Ignore message older than 9 minutes.
    maxMsgAge = datetime.timedelta(minutes=9)

    # We are interested in messages no more than 6 minutes apart.
    diff = datetime.timedelta(minutes=6)

    # Iterate through the messages from newest to oldest.
    for i in range(latest_email_id, first_email_id-1, -1):
        typ, data = mail.fetch(i, '(RFC822)' )

        for response_part in data:
            if isinstance(response_part, tuple):
                # Convert the email message to a string.
                msg = email.message_from_string(response_part[1])

                # Get the subject, from lines and the received date.
                email_subject = msg['subject']
                email_from = msg['from']
                mdate = parser.parse(msg['Date'])

                # print email_subject
                # print email_from
                # print mdate

                # Compare the received date and time with the current date and
                # time in UTC. If the message is more than 15 minutes old we
                # ignore it.
                msgAge = datetime.datetime.now(timezone('UTC')) - mdate
                if msgAge > maxMsgAge:
                    # print "Message is too old."
                    # print msgAge
                    continue

                # Get the received date from the first message.
                if firstMsg:
                    firstMsg = False
                else:
                    # If the first message and the second message are less than
                    # 5 minutes apart we return "True". Otherwise return "False".
                    if (lastMdate - mdate) <= diff:
                        retVal = True
                    done = True
                    break

                lastMdate = mdate

        if done:
            break

    mail.logout()
    return retVal

# If checkEmail() returns True then we received 2 back-to-back errors. In this case
# we want to restart Tomcat on the affected server.

# Check for tomcat errors from Redsox.
if checkEmail('Redsox'):
    print('Tomcat on Redsox needs to be restarted.')
    os.system("ssh bev_lekx@redsox \"sudo /etc/init.d/tomcat7 restart\"")
    os.system('/usr/bin/sendemail ' +
              '-f no-reply@securenettech.com ' +
              '-t "dinh.duong@securenettech.com arun.nagpal@securnettech.com" ' +
              '-o tls=no ' +
              '-m "Restarted tomcat on redsox" ')

# Check for tomcat errors from Phillies.
if checkEmail('Phillies'):
    print('Tomcat on Phillies needs to be restarted.')
    os.system("ssh bev_lekx@phillies \"sudo /etc/init.d/tomcat7 restart\"")
    os.system('/usr/bin/sendemail ' +
              '-f no-reply@securenettech.com ' +
              '-t "dinh.duong@securenettech.com arun.nagpal@securnettech.com" ' +
              '-o tls=no ' +
              '-m "Restarted tomcat on phillies" ')

print('Complete')

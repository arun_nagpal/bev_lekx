#!/bin/bash

# Alder
mysql --login-path=cloud-rw -D securenet_6_alder -A \
-e "delete from new_contact_id_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

mysql --login-path=cloud-rw -D securenet_6_alder -A \
-e "delete from new_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

# Cloud
mysql --login-path=cloud-rw -D securenet_6 -A \
-e "delete from new_contact_id_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

mysql --login-path=cloud-rw -D securenet_6 -A \
-e "delete from new_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

# Cloud-EU
mysql --login-path=cloud-eu -D securenet_6 -A -e "delete from new_contact_id_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

mysql --login-path=cloud-eu -D securenet_6 -A \
-e "delete from new_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

# Cloud-AU
mysql --login-path=cloud-au -D securenet_6 -A \
-e "delete from new_contact_id_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

mysql --login-path=cloud-au -D securenet_6 -A \
-e "delete from new_alarm_message where errors > 0 and transaction_date < (get_current_time_in_millis() - 24*60*60000);"

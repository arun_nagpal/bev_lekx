#!/usr/bin/python

import boto3
import argparse
import sys
import os

# Command-line arguments.
parser = argparse.ArgumentParser(description='Search EC2 instances.', add_help=False)
parser.add_argument('-n', '--name', type=str, default='',
                    required=False, help='Search for this instance name.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -n, --name      Search for an instance name.")
   print("       -h, -?, --help  Display this helpful message.")
   sys.exit(0)

if len(args.name) == 0:
   print("The \"name\" argument is required.")
   sys.exit(1)

searchName = args.name

ec2 = boto3.client('ec2')

# Retrieves all regions/endpoints that work with EC2
instanceFound = False
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            # if instance['State']['Name'] != "running":
            #     continue

            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']
                    if instance_name.lower() == searchName.lower():
                        instanceFound = True

            az = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']

            location = 'ec2'
            if "VpcId" in instance:
                location = 'vpc'

            platform = 'Linux'
            if "Platform" in instance:
                platform = instance['Platform']

            instance_id = instance['InstanceId']
            image_id = instance['ImageId']

            publicIpAddress = ''
            if 'PublicIpAddress' in instance:
                publicIpAddress = instance['PublicIpAddress']
            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']

            volumes = ec2_client.describe_volumes(Filters = [
                {
                    'Name' : "attachment.instance-id",
                    'Values' : [instance_id]
                }
            ])

            if instanceFound:
                print("Instance ID .: %s") % (instance_id)
                print("Instance type: %s") % (instance_type)
                print("Region ......: %s") % (regionName)
                print("Avail zone ..: %s") % (az)
                print("Private IP ..: %s") % (privateIpAddress)
                print("Public IP ...: %s") % (publicIpAddress)
                print("Volumes:")
                for volume in volumes.get('Volumes',[]):
                    print("%s %s") % (volume.get('VolumeId'), volume.get('Size'))
                for sg in instance['SecurityGroups']:

                break
        if instanceFound:
            break
    if instanceFound:
        break

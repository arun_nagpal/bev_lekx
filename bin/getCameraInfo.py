#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys
import os

dbConfigCloud = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}
dbConfigCloudSlomins = {
   'user': 'sn_slomins',
   'password': '5n_510m1n5',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6_slomins',
   'raise_on_warnings': False,
}
dbConfigCloudEurope = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'master_database',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get camera information from the db.')
parser.add_argument('-v', '--vendorid', type=str, default='',
                    required=False, help='Snet vendor ID.')
parser.add_argument('-e', '--environment', type=str, default='',
                    required=False, help='Environment: cloud, cloud-slomins.')
args = parser.parse_args()

environment = 'cloud'
if args.environment:
   environment = args.environment

if environment == 'cloud':
   dbConfig = dbConfigCloud
elif environment == 'cloud-slomins':
   dbConfig = dbConfigCloudSlomins
elif environment == 'cloud-europe':
   dbConfig = dbConfigCloudEurope
else:
   print 'Unknown environment"' + environment + '"'
   sys.exit(1)

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**dbConfig)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Validate the options. We need a vendor_id.
if len(args.vendorid) == 0:
   print("A vendor ID must be specified.")
   cnx.close()
   sys.exit(1)

vendor_id = args.vendorid

print('account_dvr record(s):')
account_id_new = ''
name = ''
dvr_type_id = ''
host_address_vpn = ''
tcp_port_vpn = ''
rtsp_port_vpn = ''
rtsp_access_name = ''
username = ''
password = ''
updated_date = ''
query = ('select account_id_new, name, dvr_type_id, host_address_vpn, '
         'tcp_port_vpn, rtsp_port_vpn, rtsp_access_name, username, password, '
         'from_unixtime(updated_date/1000, "%c/%e/%Y %h:%i:%s") as updated_date '
         'from account_dvr where vendor_id = "{vendorID}"' . format(vendorID=vendor_id))
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   account_id_new = row[0]
   name = row[1]
   dvr_type_id = row[2]
   host_address_vpn = row[3]
   tcp_port_vpn = row[4]
   rtsp_port_vpn = row[5]
   rtsp_access_name = row[6]
   username = row[7]
   password = row[8]
   updated_date = row[9]

   print('Account ID .......: %s') % (account_id_new)
   print('Camera name ......: %s') % (name)
   print('DVR type ID ......: %s') % (dvr_type_id)
   print('OpenVPN host .....: %s') % (host_address_vpn)
   print('TCP port VPN .....: %s') % (tcp_port_vpn)
   print('RTSP port VPN ....: %s') % (rtsp_port_vpn)
   print('RTSP access name .: %s') % (rtsp_access_name)
   print('Username .........: %s') % (username)
   print('Password .........: %s') % (password)
   print('Last access date .: %s') % (updated_date)
   print('')
cursor.close()

print('tunnel record(s):')
print("OpenVPN host    IP address      Port  Connected         Disconnected")
print("--------------- --------------- ----- ----------------- -----------------")
remote_public_host = ''
remote_private_host = ''
remote_private_port = ''
connected_date = ''
disconnected_date = ''
query = ('select remote_public_host, remote_private_host, remote_private_port, '
         'from_unixtime(connected_date/1000, "%c/%e/%Y %h:%i:%s") as connected_date, '
         'from_unixtime(disconnected_date/1000, "%c/%e/%Y %h:%i:%s") as disconnected_date '
         'from tunnel where username="{vendorID}" order by connected_date DESC' . format(vendorID=vendor_id))
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   remote_public_host = row[0]
   remote_private_host = row[1]
   remote_private_port = row[2]
   connected_date = row[3]
   disconnected_date = row[4]

   print("%-15s %-15s %-5s %-16s %-16s") % (
      remote_public_host, remote_private_host, remote_private_port, connected_date,
      disconnected_date)
   print('')
cursor.close()

print("Registration access log:")
os.system('ssh reg "sudo grep \"{vendorID}\" /var/log/apache2/access.log"' . format(vendorID=vendor_id))


cnx.close()

#!/usr/bin/python

import argparse
import csv
import gzip
import math
import os
import requests
import sys
import time
import calendar

dstPorts = {}
elements = {}

def ip2int(ip):
    o = map(int, ip.split('.'))
    res = (pow(256, 3) * o[0]) + (pow(256, 2) * o[1]) + (256 * o[2]) + o[3]
    return res

def int2ip(ipnum):
    o1 = int(ipnum / pow(256, 3)) % 256
    o2 = int(ipnum / pow(256, 2)) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()

def analyzeFile(fileName):
    fh = None
    if fileName.endswith('.gz'):
        fh = gzip.open(fileName, 'r')
    else:
        fh = open(fileName, "r")

    for line in fh:
        values = map(str, line.strip().split(' '))
        if values[0] == 'version':
            continue

        interface = values[2]
        srcaddr = values[3]
        dstaddr = values[4]
        srcport = values[5]
        dstport = values[6]
        protocol = values[7]
        packets = values[8]
        numbytes = values[9]
        starttime = values[10]
        endtime = values[11]
        action = values[12]

        starttimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(starttime)))
        endtimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(endtime)))

        outside_addr = ''
        outside_port = ''
        inside_addr = ''
        inside_port = ''
        # if srcaddr[0:7] == '172.29.' and dstaddr[0:7] == '172.29.':
        #     continue
        if srcport != '443' and dstport != '443':
            continue
        if srcaddr[0:7] == '172.29.':
            outside_addr = dstaddr
            outside_port = dstport
            inside_addr = srcaddr
            inside_port = srcport
        else:
            outside_addr = srcaddr
            outside_port = srcport
            inside_addr = dstaddr
            inside_port = dstport

        # print(("%s %s %s %s %s %s %s %s %s %s") %
        #       (starttimeStr, endtimeStr, outside_addr, outside_port, inside_addr, inside_port,
        #        protocol, packets, numbytes, action))

        if starttimeStr not in elements:
            elements[starttimeStr] = 0
        intNumBytes = 0
        try:
            intNumBytes = int(numbytes)
        except:
            pass
        elements[starttimeStr] += intNumBytes

    fh.close()

# Command-line arguments.
parser = argparse.ArgumentParser(description='Analyze flow log.', add_help=False)
parser.add_argument('-f', '--flowid', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --flowid     Flow log ID (required).")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

if len(args.flowid) == 0:
   print("The \"flowid\" argument is required.")
   sys.exit(1)

flowids = []
flowids = map(str, args.flowid.split(' '))

flowLogFiles = []
for root, dirs, files in os.walk('.'):
    for file in files:
        if args.flowid in file:
            flowLogFiles.append(os.path.join(root, file))

for flowLogFile in sorted(flowLogFiles):
    for flowid in flowids:
        analyzeFile(flowLogFile)

# File names.
epoch_time = int(time.time())
datFileName = 'flowLog-' + str(epoch_time) + '.dat'
gnuFileName = 'flowLog-' + str(epoch_time) + '.gplot'
pngFileName = 'flowLog-' + str(epoch_time) + '.png'

# Print results to a data file.
datFile = open(datFileName, 'w')
for element in sorted(elements):
    t = int(calendar.timegm(time.strptime(element, '%Y-%m-%d %H:%M:%S')))
    datFile.write(str(t) + ' ' + str(elements[element]) + '\n')
datFile.close()

# Create the gnuplot file.
gnuFile = open(gnuFileName, 'w')
gnuFile.write('reset\n')
gnuFile.write('set terminal pngcairo enhanced font "Verdana,8" size 800,400\n')
gnuFile.write('set output "' + pngFileName + '"\n')
gnuFile.write('set title "Flow Log KBytes Per Second"\n')
gnuFile.write('set key bmargin\n')
gnuFile.write('set style line 1 lc rgb "#00a0a0" pt 1 ps 1 lt 1 lw 2 # line1\n')
gnuFile.write('set style line 11 lc rgb "#2c3e50" lt 1 lw 1.5 # Axis line\n')
gnuFile.write('set border 3 back ls 11\n')
gnuFile.write('set tics nomirror\n')
gnuFile.write('set autoscale xy\n')
gnuFile.write('set xdata time\n')
gnuFile.write('set timefmt "%s"\n')
gnuFile.write('set format x "%d,%H:%M"\n')
gnuFile.write('set xlabel "Time"\n')
gnuFile.write('set ylabel "KB-per-second"\n')
gnuFile.write('set style line 11 lc rgb "#aeb6bf" lt 0 lw 2\n')
gnuFile.write('set grid back ls 11\n')
# gnuFile.write('plot "' + datFileName + '" using ($1+(-4*3600)):($2/1024/60) title "KB/sec" with l ls 1\n')
gnuFile.write('plot "' + datFileName + '" using 1:($2/1024/60) title "KB/sec" with l ls 1\n')
gnuFile.close()

# Run the plot.
os.system('gnuplot ' + gnuFileName)

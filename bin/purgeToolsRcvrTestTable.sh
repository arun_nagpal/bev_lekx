#!/bin/bash

# Delete receiver_test records older than 1 month.
query="delete from receiver_test where starttime < `date +%s --date=\"last month\"`"
mysql --login-path=tools -D celldata -A -e "${query}"


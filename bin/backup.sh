#!/bin/bash

backupDir="/home/efs/snet/tools/backup/`date +%a`"
mkdir -p ${backupDir}
mkdir -p ${backupDir}/etc
mkdir -p ${backupDir}/home/bev_lekx
mkdir -p ${backupDir}/home/securenet
mkdir -p ${backupDir}/var/www
mkdir -p ${backupDir}/database
mkdir -p ${backupDir}/var/spool/cron/crontabs
touch ${backupDir}
touch ${backupDir}/etc
touch ${backupDir}/home/bev_lekx
touch ${backupDir}/home/securenet
touch ${backupDir}/var/www
touch ${backupDir}/database
touch ${backupDir}/var/spool/cron/crontabs

# Crontabs
rsync -avr /etc/crontab ${backupDir}/etc/crontab
sudo rsync -avr /var/spool/cron/crontabs/ {backupDir}/var/spool/cron/crontabs

# /home/bev_lekx
rsync -avr --delete \
      --exclude='*flowlogs/us-east-1/*.gz' \
      --exclude='*flowlogs/ap-southeast-2/*.gz' \
      --exclude='*outageMap/*' \
      --exclude='*Maildir' \
      /home/bev_lekx/ \
      ${backupDir}/home/bev_lekx

# /home/securenet
rsync -avr --delete \
      --exclude *.err \
      --exclude *.lck \
      --exclude *.log* \
      /home/securenet/ \
      ${backupDir}/home/securenet

# /var/www
rsync -avr --delete /var/www/ ${backupDir}/var/www

# Database
nice -n 20 mysqldump --login-path=tools --quick --all-databases --routines --events \
   --triggers --single-transaction --lock-tables=false > ${backupDir}/database/tools.sql

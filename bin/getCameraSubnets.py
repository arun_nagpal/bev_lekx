#!/usr/bin/python

# http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#EC2.Client.describe_instances

from pprint import pprint
import boto3
import os

ec2 = boto3.client('ec2')
# Retrieves all regions/endpoints that work with EC2
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            if instance['State']['Name'] != "running":
                continue

            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']

            availZone = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']

            location = 'ec2'
            if "VpcId" in instance:
                location = 'vpc'

            platform = 'Linux'
            if "Platform" in instance:
                platform = instance['Platform']

            instance_id = instance['InstanceId']
            image_id = instance['ImageId']

            publicIpAddress = ''
            if 'PublicIpAddress' in instance:
                publicIpAddress = instance['PublicIpAddress']
            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']

            volumes = ec2_client.describe_volumes(Filters = [
                {
                    'Name' : "attachment.instance-id",
                    'Values' : [instance_id]
                }
            ])

            if instance_name == 'gsm-primary':
                continue
            if instance_name == 'Cisco CSR 1000V Primary':
                continue
            if instance_name == 'Cisco CSR 1000V Secondary':
                continue
            if instance_name == 'Udp load balancer test 1':
                continue
            if instance_name == 'Udp load balancer test 2':
                continue
            if instance_name == 'Sugar-CRM':
                continue
            if instance_name == 'Andrey-test-1':
                continue
            if instance_name == 'Test SNIP server':
                continue

            print(("%s\t%s\t%s\t%s") %
                  (instance_name, availZone, privateIpAddress, publicIpAddress))

            if regionName == 'us-east-1':
                os.system('ssh bev_lekx@' + privateIpAddress +
                          ' "if [ -d \"/etc/openvpn\" ]; then sudo grep \"ifconfig \" /etc/openvpn/server-vivotek-*.conf; fi"')
                print ""

#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import logging
import signal

# output
# ------
# -o w  warnings
# -o u  Unknown
# -o s  success
# -o f  Fail
# -o fa All failures
# -o fb Fail - blocked
# -o fd Fail - DNS
# -o fh Fail - host error
# -o fu Fail - user error

programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='script')
parser.add_argument('-f', '--logfile', type=str, default='',
                    required=True, help='file')
parser.add_argument('-o', '--output', type=str, default='fa',
                    required=False, help='Output type')
parser.add_argument('-d', '--detail', default=False, action="store_true",
                    required=False, help='Include message details.')
args = parser.parse_args()

outputtype = 'f'
if args.output:
    if args.output.lower() == 'w':
        outputtype = 'w'
    elif args.output.lower() == 'u':
        outputtype = 'u'
    elif args.output.lower() == 's':
        outputtype = 's'
    elif args.output.lower() == 'f':
        outputtype = 'f'
    elif args.output.lower() == 'fa':
        outputtype = 'fa'
    elif args.output.lower() == 'fb':
        outputtype = 'fb'
    elif args.output.lower() == 'fd':
        outputtype = 'fd'
    elif args.output.lower() == 'fh':
        outputtype = 'fh'
    elif args.output.lower() == 'fu':
        outputtype = 'fu'
    else:
        print("Unknown output type.")
        sys.exit(1)

mailIds = {}
warnings = {}

try:
 logfile = open(args.logfile, 'rb')
except IOError:
   print "Error: \"" + args.logfile + "\" could not be opened."
   sys.exit(1)

for line in logfile:
    line = line[16:]
    mailId = line.split(' ')[2]
    if mailId == 'connect' or mailId == 'disconnect':
        continue
    if mailId == 'warning:':
        ndx1 = line.find('warning:')
        warning = line[ndx1+9:].strip()
        if warning not in warnings:
            warnings[warning] = 0
        continue
    if mailId == 'statistics:':
        continue
    ndx1 = mailId.find(':')
    mailId = mailId[:ndx1]
    if len(mailId) != 11:
        # print mailId
        continue

    if mailId in mailIds:
        mailIds[mailId] = mailIds[mailId] + line.lower()
    else:
        mailIds[mailId] = line.lower()

    if not programRunning:
        break
logfile.close()

successStrings = [ 'status=sent', 'mail accepted for delivery',
                   'Queued mail for delivery' ]
failureStrings = [ 'status=bounced', 'status=deferred',
                   'non-delivery notification',
                   'message has been blocked',
                   'Spam detected by content scanner' ]
blockedStrings = [ '_is_blocked', 'Spam detected by content scanner',
                   'too many connections from your ip' ]
dnsStrings = [ 'Sender\'s SPF Policy Failure', 'Message not accepted for policy reasons',
               'message rejected due to spf fail' ]
hostErrorStrings = [ 'Connection timed out', 'lost connection',
                     'Connection refused', 'No route to host',
                     'Host not found', 'Relay access denied' ]
userErrorStrings = [ 'Recipient address rejected', 'over quota', 'overquota',
                     'recipient does not exist here', 'This user doesn\'t have',
                     'The email account that you tried to reach does not exist',
                     'User unknown', 'No Such User Here', 'email address is invalid',
                     'Invalid recipient', 'not a valid user', 'User unknown' ]

if outputtype == 'w':
    for warning in warnings:
        print warning
else:
    for mailId in mailIds:
        success = False
        failure = False
        status = "Unknown"

        for s in successStrings:
            if s.lower() in mailIds[mailId]:
                success = True
                status = "success"

        for s in failureStrings:
            if s.lower() in mailIds[mailId]:
                failure = True
                status = "Fail"
        for s in blockedStrings:
            if s.lower() in mailIds[mailId]:
                failure = True
                status = "Fail - blocked"
        for s in dnsStrings:
            if s.lower() in mailIds[mailId]:
                failure = True
                status = "Fail - DNS"
        for s in hostErrorStrings:
            if s.lower() in mailIds[mailId]:
                failure = True
                status = "Fail - host error"
        for s in userErrorStrings:
            if s.lower() in mailIds[mailId]:
                failure = True
                status = "Fail - user error"

        printline = False
        if outputtype == 'u' and status == 'Unknown':
            printline = True
        elif outputtype == 's' and success:
            printline = True
        elif outputtype == 'fa' and failure:
            printline = True
        elif outputtype == 'f' and failure and status == 'Fail':
            printline = True
        elif outputtype == 'fb' and failure and status == 'Fail - blocked':
            printline = True
        elif outputtype == 'fd' and failure and status == 'Fail - DNS':
            printline = True
        elif outputtype == 'fh' and failure and status == 'Fail - host error':
            printline = True
        elif outputtype == 'fu' and failure and status == 'Fail - user error':
            printline = True

        if printline == True:
            print("%s (%s)") % (mailId, status)
            if args.detail:
                print mailIds[mailId]

#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys
import os

# ps w | grep -v grep
# netstat -n
# opkg list
# ls -l /data/fwupg/dl
# df
# ifconfig
# date
# cat /opt/vendor/.ssh/id_rsa.pub
# swconfig dev rt305x show

dbConfigCloud = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}
dbConfigCloudSlomins = {
   'user': 'sn_slomins',
   'password': '5n_510m1n5',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6_slomins',
   'raise_on_warnings': False,
}
dbConfigCloudEurope = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'master_database',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}
dbConfigCloudAustralia = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'securenet-cluster.cluster-cujy6m6qroe4.ap-southeast-2.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get iotega logs.', add_help=False)
parser.add_argument('-a', '--account', type=str, default='',
                    required=False, help='SNet account number.')
parser.add_argument('-e', '--environment', type=str, default='',
                    required=False, help='Environment: cloud, cloud-slomins.')
parser.add_argument('-t', '--tool', type=str, default='',
                    required=False, help='Tool.')
parser.add_argument('-c', '--command', type=str, default='',
                    required=False, help='Linux command.')
# Maybe this should be a generic value argument?
parser.add_argument('-k', '--keepalive', type=int, default=3600000,
                    required=False, help='Keepalive in mS.')
parser.add_argument('-v', '--value', type=str, default='',
                    required=False, help='Value.')
parser.add_argument('-p', '--pin', type=str, default='5555',
                    required=False, help='PIN.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -a, --account      Securenet account number (required).")
   print("       -e, --environment  Cloud environment (default = cloud).")
   print("                              cloud")
   print("                              cloud-au")
   print("                              cloud-eu")
   print("                              cloud-slomins")
   print("       -t, --tool         Execute this tool (required).")
   print("                              getkeepalive")
   print("                              setkeepalivecell")
   print("                              setkeepaliveeth")
   print("                              getfwver")
   print("                              isonline")
   print("                              cmd")
   print("                              getlogs")
   print("                              enablecellfwupgrade")
   print("                              enablelogging")
   print("                              startssh")
   print("                              stopssh")
   print("                              unharden")
   print("                              uploadsnetgatewayconf")
   print("                              getsnetbinary")
   print("                              putsnetbinary")
   print("                              reset")
   print("                              getfwstatus")
   print("                              fwupdate")
   print("                              getstatus")
   print("                              ftpupdate")
   print("       -c, --command      Command to run with the \"cmd\" tool.")
   print("       -k, --keepalive    Value to use when setting keepalive.")
   print("       -v, --value        Data value to set.")
   print("       -p, --pin          PIN number (default = 5555).")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

if len(args.account) == 0:
   print("The \"account\" argument is required.")
   sys.exit(1)
if len(args.tool) == 0:
   print("The \"tool\" argument is required.")
   sys.exit(1)
if args.tool.lower() == 'cmd' and len(args.command) == 0:
   print("The \"cmd\" tool requires a \"command\" argument.")
   sys.exit(1)

environment = 'cloud'
if args.environment:
   environment = args.environment

if environment == 'cloud':
   dbConfig = dbConfigCloud
elif environment == 'cloud-slomins':
   dbConfig = dbConfigCloudSlomins
elif environment == 'cloud-au':
   dbConfig = dbConfigCloudAustralia
elif environment == 'cloud-eu':
   dbConfig = dbConfigCloudEurope
else:
   print 'Unknown environment"' + environment + '"'
   sys.exit(1)

pin = '5555'
if args.pin:
    pin = args.pin

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**dbConfig)
except:
   print("Error connecting to the database.")
   sys.exit(1)

#
query = ('select proxy_id, host_address, tcp_port from account_gateway ' +
         'where account_id_new = "' + args.account + '"')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   proxy_id = row[0]
   host_address = row[1]
   tcp_port = row[2]
cursor.close()
cnx.close()

if args.tool.lower() == 'getkeepalive':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=4e')
   print req
   r = requests.get(req)
   j = r.json()
   print j

   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=5')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'setkeepalivecell':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WRITE_TIME_MILLISECONDS&p=4e' +
          '&p=' + str(args.keepalive))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'setkeepaliveeth':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WRITE_TIME_MILLISECONDS&p=5' +
          '&p=' + str(args.keepalive))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getfwver':
   data = {}
   data['apiCall'] = 'WsaSdkReq_getFwVersion'
   data['msgId'] = 10
   json_data = json.dumps(data)
   # print data
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=passThrough&p=' +
          str(json_data))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getsnetipcell':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=4a')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'setsnetipcell':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WRITE_HOST&p=4a' +
          '&p=' + args.value + '&p=1234')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'isonline':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=IS_ONLINE')
   print req
   r = requests.get(req)
   if(r.ok):
      j = json.loads(r.content)
      print j['payload']
   else:
      print r.content

elif args.tool.lower() == 'cmd':
   cmd = {}
   cmd['cmd'] = (args.command + ' > /tmp/cmd-' + args.account + '.out; '
                    'curl ftp://tahiti.securenettech.com '
                    '--user iotega:ioteg123 -T /tmp/cmd-' + args.account + '.out; '
                    'rm /tmp/cmd-' + args.account + '.out')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getlogs':
   cmd = {}
   # cmd['cmd'] = ('tar zcf /data/logs-' + args.account + '.tar.gz '
   #               '/opt/vendor/wsa_messages.json /opt/vendor/snet_gateway.conf /logs/*.log /logs/*.gz; '
   #               'curl ftp://tahiti.securenettech.com/logs-' +  args.account + '.tar.gz '
   #               '--user iotega:ioteg123 -T /data/logs-' + args.account + '.tar.gz; ' +
   #               'rm /data/logs-' + args.account + '.tar.gz')
   cmd['cmd'] = ('tar zcf /data/logs-' + args.account + '.tar.gz '
                 '/logs/*.log /logs/*.gz; '
                 'curl ftp://tahiti.securenettech.com/logs-' +  args.account + '.tar.gz '
                 '--user iotega:ioteg123 -T /data/logs-' + args.account + '.tar.gz; ' +
                 'rm /data/logs-' + args.account + '.tar.gz')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'uploadsnetgatewayconf':
   cmd = {}
   cmd['cmd'] = ('curl ftp://tahiti.securenettech.com/snet_gateway.conf '
                 '--user iotega:ioteg123 -o /opt/vendor/snet_gateway.conf')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'enablecellfwupgrade':
    data = {}
    pMsg = {}
    pMsg['comms'] = {}
    pMsg['comms']['fwUpgOverCell'] = True
    data['pMsg'] = pMsg
    data['apiCall'] = 'WsaSdkCfg_updateComms'
    data['pCode'] = pin
    data['authType'] = 'WSA_LOCAL_INSTALLER'
    data['msgId'] = 10
    json_data = json.dumps(data)
    # print data
    req = ('http://' + host_address + ':' + str(tcp_port+10) +
           '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=passThrough&p=' +
           str(json_data))
    print req
    r = requests.get(req)
    j = r.json()
    print j

elif args.tool.lower() == 'enablelogging':
   # {'msgId': 10, 'pCode': '5555', 'apiCall': 'WsaSdkCfg_updateDiagnostic',
   #  'authType': 'WSA_LOCAL_INSTALLER',
   #  'pMsg': {'apps': {'logLevel': 'DBG', 'name': 'all'}}}
   # pMsg = {"apps": [{"name": "all", "logLevel": "DBG"}]}
   data = {}
   pMsg = {}
   pMsg['apps'] = []
   app_name = {}
   app_name['name'] = 'all'
   app_name['logLevel'] = 'DBG'
   pMsg['apps'].append(app_name)
   data['pMsg'] = pMsg
   data['apiCall'] = 'WsaSdkCfg_updateDiagnostic'
   data['pCode'] = installerCode
   data['authType'] = 'WSA_LOCAL_INSTALLER'
   data['msgId'] = 10
   json_data = json.dumps(data)
   # print data
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=passThrough&p=' +
          str(json_data))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'startssh':
   cmd = {}
   cmd['cmd'] = ('/usr/bin/ssh -R 65501:localhost:22 -N -T -I 1200 -K 30'
                 ' -y iotega@tahiti.securenettech.com')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'startssh_tyco':
   cmd = {}
   cmd['cmd'] = ('/usr/bin/ssh -R 65501:localhost:22 -N -T -I 3600 -K 30'
                 ' -y iotega@54.174.240.44')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'stopssh':
   cmd = {}
   cmd['cmd'] = ('killall ssh')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'unharden':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WRITE_SNIP_FIRMWARE_TYCO_UNHARDEN')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getsnetbinary':
   cmd = {}
   cmd['cmd'] = ('tar zcf /data/snetbinary-' + args.account + '.tar.gz '
                 '/opt/vendor/snet_gateway /opt/vendor/lib/libwsasdkapi.so.0.1.45; '
                 'curl ftp://tahiti.securenettech.com/snetbinary-' +  args.account + '.tar.gz '
                '--user iotega:ioteg123 -T /data/snetbinary-' + args.account + '.tar.gz; ' +
                'rm /data/snetbinary-' + args.account + '.tar.gz')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'putsnetbinary':
   cmd = {}
   cmd['cmd'] = ('wget http://tahiti.securenettech.com/tools/snet_gateway.tar.bz2 ' +
                 '-O /opt/vendor/snet_gateway.tar.bz2; ' +
                 'bzcat /opt/vendor/snet_gateway.tar.bz2 > /opt/vendor/snet_gateway.tar; ' +
                 'tar -xf /opt/vendor/snet_gateway.tar -C /opt/vendor; ' +
                 'chown vendor:vendor -R /opt/vendor; ' +
                 'chmod 700 /opt/vendor; ' +
                 'chmod 700 -R /opt/vendor/.ssh; ' +
                 'rm /opt/vendor/snet_gateway.tar.bz2; ' +
                 'rm /opt/vendor/snet_gateway.tar; ' +
                 'killall snet_gateway')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

# <option value="3">Reset Alarm System Process only</option>
# <option value="1">Reset Securenet to Factory Defaults</option>
# <option value="2">Soft Reset</option>
elif args.tool.lower() == 'reset':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_RESET&p=2')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getfwstatus':
   msgType = {}
   msgType['pMsgType'] = 201
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=getMessage&p=' + str(json.dumps(msgType)))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'fwupdate':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WRITE_SNIP_FIRMWARE&p=0')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'getstatus':
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=REQUEST_SNIP_DATA&p=36')
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'ftpupdate':
   data = {}
   data['apiCall'] = 'WsaSdkCtrl_fwDownload'
   data['msgId'] = 1
   data['check'] = 'WSA_FW_DOWNLOAD_CHECK_SKIP_ALL'
   data['autoApply'] = True
   # data['pPath'] = 'ftp://updates/Daily_Build/1.30.4133.509673/WSA-P-Apps-ST-OS-Vendor_Production_App-Tablet.tar.gz'
   data['pPath'] = 'ftp://updates/Release_Line_Build/1.21.4005.515616/WSA-P-Apps-ST-OS-Vendor_Production_App-Tablet.tar.gz'
   data['pCode'] = pin
   data['pAlgorithm'] = 'AES/CBC/PKCS7Padding'
   data['pIv'] = '36806ca34c7eb93f112197f66c1c8b02'
   data['pSecureKeyId'] = '1'
   data['pCredentialBlob'] = 'thisisafakeblobfordemoonly'

   json_data = json.dumps(data)
   print json_data
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=passThrough&p=' +
          str(json_data))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'fixtoken':
   data = {}
   data['apiCall'] = 'WsaSdkCtrl_fwDownload'
   data['msgId'] = 1
   data['check'] = 'WSA_FW_DOWNLOAD_CHECK_SKIP_ALL'
   data['autoApply'] = True
   data['pPath'] = 'ftp://fwtest/Steven/WSA-P-Forced_Reg.tar.gz.enc'
   data['pCode'] = pin
   data['pAlgorithm'] = 'AES/CBC/PKCS7Padding'
   data['pIv'] = '36806ca34c7eb93f112197f66c1c8b02'
   data['pSecureKeyId'] = '1'
   data['pCredentialBlob'] = 'thisisafakeblobfordemoonly'

   json_data = json.dumps(data)
   print json_data
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=passThrough&p=' +
          str(json_data))
   print req
   r = requests.get(req)
   j = r.json()
   print j

elif args.tool.lower() == 'ftptest':
   cmd = {}
   cmd['cmd'] = ('curl ftp://ftp.dsc.com/updates/Release_Line_Build/1.21.4005.515616/WSA-P-Apps-ST-OS-Vendor_Production_App-Tablet.tar.gz --user DSC-WSA2:DSC-read2 --ftp-pasv --output /data/WSA-P-Apps-ST-OS-Vendor_Production_App-Tablet.tar.gz > /data/curl.out')
   req = ('http://' + host_address + ':' + str(tcp_port+10) +
          '/snip/?id=' + proxy_id + '&cmd=WSA_UPDATE_CONFIG&p=remote' +
          '&p=' + str(json.dumps(cmd)))
   print str(json.dumps(cmd))
   print req
   r = requests.get(req)
   j = r.json()
   print j

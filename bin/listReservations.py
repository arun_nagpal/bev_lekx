#!/usr/bin/env python

import re
import os
import boto3.session
import six
import datetime
import time
from dateutil.tz import tzutc
# import operator
# import sys

textFileName = '/tmp/awsReservations.csv'

def dateConvertAwsToYYYYMMDD(sourceDate):
    result = ''
    try:
        if '.' in str(sourceDate):
            result = datetime.datetime.strptime(str(sourceDate), "%Y-%m-%d %H:%M:%S.%f+00:00").strftime("%Y-%m-%d")
        else:
            result = datetime.datetime.strptime(str(sourceDate), "%Y-%m-%d %H:%M:%S+00:00").strftime("%Y-%m-%d")
    except FormatError:
        result = ''

    return result

now = datetime.datetime.utcnow().replace(tzinfo=tzutc())

textFile = open(textFileName, 'w')

ec2 = boto3.client('ec2')
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']

    ec2_client = boto3.client('ec2', region_name=region['RegionName'])

    # Create a list of instances.
    totalInstances = 0
    instances = {}
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            if instance['State']['Name'] != "running":
                continue
            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']
            availZone = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']
            instance_id = instance['InstanceId']

            totalInstances += 1
            key = instance_type + '|' + availZone
            if key in instances:
                instances[key] += 1
            else:
                instances[key] = 1
    if totalInstances > 0:
        for instance in sorted(instances.keys()):
            (t, s) = instance.split('|')
            textFile.write(('instance, {t}, {s}, , {count}\n') .
                             format(t=t, s=s, count=instances[instance]))

    # Create a list of reservations.
    totalReservations = 0
    reservations = {}
    ec2_reserved = ec2_client.describe_reserved_instances()
    for reservedInstance in ec2_reserved['ReservedInstances']:
        r = reservedInstance
        if r['State'] not in ('active', 'payment-pending'):
            continue
        az = ''
        if r['Scope'] == 'Region':
            az = region['RegionName']
        else:
            az = r['AvailabilityZone']
        instance_type = r['InstanceType']
        instance_count = r['InstanceCount']
        expiry_date = dateConvertAwsToYYYYMMDD(r['End'])
        #if r['End']:
            # reserved_instances[r_counter] = (r['InstanceType'], az, r['End'], int(r['InstanceCount']))
            # r_counter += 1
        totalReservations += instance_count
        key = instance_type + '|' + az + '|' + expiry_date
        if key in reservations:
            reservations[key] += instance_count
        else:
            reservations[key] = instance_count

        # print(instance_type, az, instance_count, expiry_date)
    if totalReservations > 0:
        for reservation in sorted(reservations.keys()):
            (t, s, e) = reservation.split('|')
            textFile.write(('reservation, {t}, {s}, {e}, {count}\n') .
                            format(t=t, s=s, e=e, count=reservations[reservation]))

textFile.close()

# Email the results.
dateString = time.strftime("%B %d, %Y", time.gmtime())
dobj = datetime.date.today()
dow = dobj.weekday()
recipients = ('arun.nagpal@securenettech.com ')
subject = ('AWS reservations for ' + dateString)
msg = ('AWS reservation report.\n\n')
os.system('/usr/bin/sendemail ' +
          '-f no-reply@securenettech.com ' +
          '-t ' + recipients + ' ' +
          '-o tls=no ' +
          '-u "' + subject + '" ' +
          '-m "' + msg + '" ' +
          '-a ' + textFileName)

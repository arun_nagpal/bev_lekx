#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import argparse
import sys
import os

def usage():
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --filename     Inventory file (required).")
   print("       -a, --archiveid    Include archive ID in the listing.")
   print("       -h, -?, --help     Display this helpful message.")

# Command-line arguments.
parser = argparse.ArgumentParser(description='Print Glacier vault inventory.', add_help=False)
parser.add_argument('-f', '--filename', type=str, default='', required=False)
parser.add_argument('-a', '--archiveid', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   usage()
   sys.exit(0)

if not args.filename:
   usage()
   print("")
   print("A json-formatted inventory file is required.")
   sys.exit(1)

try:
    invfile = open(args.filename, 'rb')
except IOError:
    print "Error: \"" + args.filename + "\" could not be opened."
    sys.exit(1)

invdata = invfile.read()
invfile.close()

jData = json.loads(invdata)

if 'VaultARN' in jData:
    print("Vault: %s") % (jData['VaultARN'])
if 'InventoryDate' in jData:
    print("Inventory date: %s") % (jData['InventoryDate'])
if 'ArchiveList' in jData:
    archiveList = jData['ArchiveList']
    for archiveItem in archiveList:
        descrip = ''
        createDate = ''
        size = 0
        archiveId = ''
        if 'ArchiveDescription' in archiveItem:
            descrip = archiveItem['ArchiveDescription']
        if 'CreationDate' in archiveItem:
            createDate = archiveItem['CreationDate']
        if 'Size' in archiveItem:
            size = archiveItem['Size']
        if args.archiveid:
            if 'ArchiveId' in archiveItem:
                archiveId = archiveItem['ArchiveId']
        print("%-40s %s %d %s") % (descrip, createDate, size, archiveId)

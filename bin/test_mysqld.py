#!/usr/bin/python

import datetime
import mysql.connector
import argparse
import sys
import os
import datetime
import time
import boto3

dbUser = 'sn_notifier'
dbPassword = '5n_n0t1f13r'
# dbHost = 'db2.cd8ohozdxf8u.us-east-1.rds.amazonaws.com'
# dbHost = 'db1.cd8ohozdxf8u.us-east-1.rds.amazonaws.com'
dbDatabase = 'securenet_6'

parser = argparse.ArgumentParser(
   description='Test database response time.')
parser.add_argument('-d', '--server', type=str, default='',
                    required=True, help='Database server.')
args = parser.parse_args()

startTime = datetime.datetime.now()

try:
    count = 0
    cnx = mysql.connector.connect(user=dbUser, password=dbPassword,
                                  database=dbDatabase, host=args.server)
    cursor = cnx.cursor()
    query = ("select count(*) from account_gateway")
    cursor.execute(query)
    row = cursor.fetchone()
    while row is not None:
        count = row[0]
        row = cursor.fetchone()

except:
    pass

finally:
    cursor.close()
    cnx.close()

stopTime = datetime.datetime.now()
diff = stopTime - startTime

# Result in milli-seconds.
queryTime = ((diff.days * 86400000000) + (diff.seconds * 1000000) + diff.microseconds) / 1000

print("%d") % (queryTime)


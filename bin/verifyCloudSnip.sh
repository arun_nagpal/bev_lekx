#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

servers="germany peru wales egypt israel ghana angola cuba"
for server in ${servers}
do
    clear
    echo "Checking ${server}"
    ssh ${server} "ps ax | grep snip | grep -v grep"

    echo -n "Check the next server? (y/N): "
    REPLY=""
    read
    if [[ $REPLY != "y" && $REPLY != "Y" ]]
    then
        exit 0
    fi
done

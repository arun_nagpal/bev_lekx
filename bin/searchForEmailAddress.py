#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys

config = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get iotega keeyalive.')
parser.add_argument('-m', '--email', type=str, default='',
                    required=True, help='Email address.')
args = parser.parse_args()

# Connect to the SecureNet database.
try:
   cnx = mysql.connector.connect(**config)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Search for an account number in the account_keyholder table.
account_id_new = ''
query = ('select account_id_new from account_keyholder where email_address="' + args.email + '"')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
account_found = False
for row in rows:
   account_found = True
   account_id_new = str(row[0])
cursor.close()

if not account_found:
   print("Email address not found.")
   cnx.close()
   sys.exit(1)

print("Account ID ..: %s") % (account_id_new)

# Start with the account table.
# The receiver_id field links to the id field in the receiver table.
system_id = ''
bureau_id = ''
query = ('select system_id, bureau_id from account where id_new = "' + account_id_new + '"')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   system_id = str(row[0])
   bureau_id = str(row[1])
   # print("System ID ...: %s") % (system_id)
   # print("Bureau ID ...: %s") % (bureau_id)
cursor.close()

# Keyholder.
query = ('select name, email_address, '
         'from_unixtime(active_from_date/1000, "%c/%e/%Y %h:%i:%s") as active_from_date, '
         'from_unixtime(last_login_date/1000, "%c/%e/%Y %h:%i:%s") as last_login_date '
         'from account_keyholder where account_id_new="' + account_id_new + '" ' +
         'and email_address="' + args.email + '"')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   username = row[0]
   emailaddr = row[1]
   activedate = row[2]
   lastlogin = row[3]
   print("Name ........: %s") % (username)
   print("Email address: %s") % (emailaddr)
   # print("Last login ..: %s") % (lastlogin)
   # print("Active date .: %s") % (activedate)
cursor.close()

# Bureau
query = ('select id_new, company_name, trading_name, company_registration_number, '
         'authorised_integrator_network, trial_bureau, address_line_1, address_line_2, suburb, '
         'city, state, post_code, country_id, world_time_zone_id, email_address, '
         'email_address_account_updates, email_address_email_replies, email_address_sms_replies, '
         'reports_email_address, phone_number_1, phone_number_2, contact_name_1, contact_name_2, '
         'from_unixtime(start_date/1000, "%c/%e/%Y %h:%i:%s") as start_date '
         'from bureau where id=' + str(bureau_id) + ' and system_id=' + str(system_id) + ' limit 1')
cursor = cnx.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
   id_new = row[0]
   company_name = row[1]
   address_line_1 = row[6]
   address_line_2 = row[7]
   suburb = row[8]
   city = row[9]
   state = row[10]
   post_code = row[11]
   email_address = row[14]
   phone_number_1 = row[19]
   phone_number_2 = row[20]
   contact_name_1 = row[21]
   contact_name_2 = row[22]

   print("Company name : %s") % (company_name)
   print("Address 1 ...: %s") % (address_line_1)
   if len(address_line_2) > 0:
      print("Address 2 ...: %s") % (address_line_2)
   if len(suburb) > 0 and city != suburb:
      print("Suburb ......: %s") % (suburb)
   if len(city) > 0:
      print("City ........: %s") % (city)
   print("State .......: %s") % (state)
   print("Postal code .: %s") % (post_code)
   print("Email address: %s") % (email_address)
   phone_numbers = phone_number_1
   if len(phone_numbers) > 0 and len(phone_number_2) > 0:
      phone_numbers = (phone_numbers + ', ' + phone_number_2)
   print("Phone numbers: %s") % (phone_numbers)
   contact_names = contact_name_1
   if len(contact_names) > 0 and len(contact_name_2) > 0:
      contact_names = (contact_names + ', ' + contact_name_2)
   print("Contact names: %s") % (contact_names)
   print("")
   cursor.close()

cnx.close()

#!/usr/bin/python

import boto3
import argparse
import os
import sys

def printSecurityGroup(region, securityGroupId):
    ec2 = boto3.client('ec2', region_name=region)
    ec2_sec_groups = ec2.describe_security_groups(GroupIds=[securityGroupId])
    for sec_group in ec2_sec_groups['SecurityGroups']:
        from_port = ''
        to_port = ''
        ip_protocol = ''
        for perm in sec_group['IpPermissions']:
            if 'FromPort' in perm:
                from_port = str(perm['FromPort'])
            if 'ToPort' in perm:
                to_port = str(perm['ToPort'])
            if 'IpProtocol' in perm:
                ip_protocol = perm['IpProtocol']

            for ip_range in perm['IpRanges']:
                ip_range_descr = ''
                ip_range_cidr = ''
                if 'Description' in ip_range:
                    ip_range_descr = ip_range['Description']
                if 'CidrIp' in ip_range:
                    ip_range_cidr = ip_range['CidrIp']
                print(('    %s %s %s %s %s %s %s') %
                      (sec_group['GroupName'], sec_group['GroupId'], from_port,
                       to_port, ip_protocol, ip_range_cidr, ip_range_descr))

# Command-line arguments.
parser = argparse.ArgumentParser(description='Get iotega logs.', add_help=False)
parser.add_argument('-d', '--details', action='store_true')
parser.add_argument('-s', '--server', type=str, default='')
parser.add_argument('-r', '--region', type=str, default='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -d, --details      Print security group details.")
   print("       -s, --server       Print security groups for this server only.")
   print("       -r  --region       Selected region.")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

# Create a list of regions by using the one specified on the command line or a
# list of all regions.
regionNames = []
if not args.region:
    ec2 = boto3.client('ec2')
    response = ec2.describe_regions()
    for region in response['Regions']:
        regionNames.append(region['RegionName'])
else:
    regionNames.append(args.region)

for regionName in regionNames:
    ec2 = boto3.client('ec2', region_name=regionName)

    # Get a list of all ec2 instances in use in the region.
    ec2_instances = ec2.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            instance_id = instance['InstanceId']
            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value']

            # Set "printSecGrps" to True if no servers were specified or if a server
            # was specified and either the instance_id or instance name matches.
            printSecGrps = False
            if not args.server:
                printSecGrps = True
            else:
                if (args.server.lower() == instance_name.lower()) or (args.server.lower() == instance_id):
                    printSecGrps = True

            # Print the list of security groups for the ec2 instance.
            if 'SecurityGroups' in instance and printSecGrps:
                securityGroups = instance['SecurityGroups']
                for securityGroup in securityGroups:
                    print(("%s %s %s %s") %
                          (instance_name, instance_id, securityGroup['GroupId'], securityGroup['GroupName']))

                    # Print the security group rules if the "details" option was
                    # specified.
                    if args.details:
                        printSecurityGroup(regionName, securityGroup['GroupId'])

#!/usr/bin/python

# http://jhshi.me/2017/03/06/backing-up-files-using-amazon-glacier/index.html#.XPL1WCZE2V4
# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/glacier.html

import json
import argparse
import requests
import sys
import os
import gnupg
import subprocess
import botocore
import boto3
import mysql.connector
import time
import hashlib
import math

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

vault = 'SNArchive'
cipher = 'AES256'
partsize = 1048576 * 64

def archiveOneFile(db, fileName, descrip):
    epoch_time = int(time.time())

    print("File: %s") % (fileName)
    print("Obtaining the key.")
    key = ''
    p = subprocess.Popen(["pwgen", '-N', '1', '32'],
                         shell=False,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    result = p.stdout.readlines()
    for r in result:
        key = r.strip()

    print("Encrypting the file using key \"%s\".") % (key)
    encFile = fileName + '.enc'
    afile = open(fileName, "rb")
    enc_data = gpg.encrypt_file(afile, None, passphrase=key,
                                symmetric=cipher.upper(), output=encFile)
    afile.close()

    # If necessary, split the encrypted file.
    fstat = os.stat(encFile)
    fileSize = int(fstat.st_size)
    if fileSize > partsize:
        print("Initiating a multi-part upload.")
        response = awsClient.initiate_multipart_upload(accountId='-',
                                                       vaultName=vault,
                                                       archiveDescription=encFile,
                                                       partSize=str(partsize))
        uploadId = response['uploadId']
        print("UploadId: %s") % (uploadId)

        # Determine the number of file chunks to upload.
        # Convert the file size and chunk size to floats so ceil will
        # calculate the value correctly.
        parts = int(math.ceil(float(fileSize) / float(partsize)))
        with open(encFile, 'rb') as f:
            for p in range(parts):
                # Calculate lower and upper bounds for the byte ranges. The last
                # range will probably be smaller than the others.
                lower = (p * (partsize))
                upper = ((p + 1) * partsize) - 1
                if upper > fileSize:
                    upper = fileSize-1

                read_size = upper-lower+1
                file_part = f.read(read_size)
                crange = 'bytes {}-{}/*'.format(lower, upper)
                print("Uploading chunk \"%s\"") % (crange)

                result = awsClient.upload_multipart_part(vaultName=vault,
                                           uploadId=uploadId,
                                           range=crange,
                                           body=file_part)

        print("Complete the multi-part upload.")
        response = awsClient.complete_multipart_upload(vaultName=vault,
                                    uploadId=uploadId,
                                    archiveSize=str(fileSize),
                                    checksum=botocore.utils.calculate_tree_hash(open(encFile, 'rb')))
        archiveId = response['archiveId']
        print("Archive ID: %s") % (archiveId)
    else:
        archiveId = ''
        response = ''
        with open(encFile, 'rb') as f:
            response = awsClient.upload_archive(vaultName=vault,
                                                archiveDescription=encFile,
                                                body=f)
        archiveId = response['archiveId']
        print("Archive ID: %s") % (archiveId)

    cursor = db.cursor()
    query = ('INSERT INTO glacier (filename, encryptkey, vault, archiveid, '
             'archivedate, description) '
             'VALUES ("' + fileName + '", "' + key + '", "' + vault +
             '", "' + archiveId + '", ' + str(epoch_time) + ', "' + descrip + '")')
    cursor.execute(query)
    db.commit()
    cursor.close()

    print("%s %s %s %s") % (fileName, key, vault, archiveId)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Archive files(s).', add_help=False)
parser.add_argument('-d', '--descrip', type=str, default='',
                    required=False, help='Description.')
parser.add_argument('-f', '--filename', type=str, default='',
                    required=False, help='File name.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -d, --descrip      Description.")
   print("       -f, --filename     File name.")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

if not args.descrip:
    print("A description must be provided.")
    sys.exit(1)

gpg = gnupg.GPG()
cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()
awsClient = boto3.client('glacier')

if args.filename:
    archiveOneFile(cnx, args.filename, args.descrip)
else:
    files = os.listdir('.')
    for fileName in files:
        if fileName[-4:] != '.tgz':
            continue
        archiveOneFile(cnx, fileName, args.descrip)

cnx.commit()
cnx.close()

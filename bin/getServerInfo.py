#!/usr/bin/python

import boto3
import os

ignoreServers = [
                  'sugar-crm',
                  'udp load balancer test 2',
                  'udp load balancer test 1',
                  'test snip server',
                  'cisco csr 1000v primary',
                  'cisco csr 1000v secondary',
                  'default-environment',
                  'tools',
                  'gsm-primary'
                ]

ec2 = boto3.client('ec2')
# Retrieves all regions/endpoints that work with EC2
response = ec2.describe_regions()
for region in response['Regions']:
    regionName = region['RegionName']
    ec2_client = boto3.client('ec2', region_name=region['RegionName'])
    ec2_instances = ec2_client.describe_instances()
    for reservation in ec2_instances['Reservations']:
        for instance in reservation['Instances']:
            instance_name=''
            for tag in instance['Tags']:
                if tag['Key'] == 'Name' or tag['Key'] == 'name':
                    instance_name = tag['Value'].lower()

            az = instance['Placement']['AvailabilityZone']
            instance_type = instance['InstanceType']

            location = 'ec2'
            if "VpcId" in instance:
                location = 'vpc'

            platform = 'Linux'
            if "Platform" in instance:
                platform = instance['Platform']

            instance_id = instance['InstanceId']
            image_id = instance['ImageId']

            publicIpAddress = ''
            if 'PublicIpAddress' in instance:
                publicIpAddress = instance['PublicIpAddress']
            privateIpAddress = ''
            if 'PrivateIpAddress' in instance:
                privateIpAddress = instance['PrivateIpAddress']

            volumes = ec2_client.describe_volumes(Filters = [
                {
                    'Name' : "attachment.instance-id",
                    'Values' : [instance_id]
                }
            ])

            if instance_name in ignoreServers:
                continue

            print(("%s &  & %s & %s & %s & %s \\\\") %
                  (instance_name, publicIpAddress, privateIpAddress,
                   instance['State']['Name'], regionName))

            if instance['State']['Name'] == "running":
                tmpName = instance_name
                if len(instance_name) == 0:
                    tmpName = privateIpAddress
                os.system('/home/bev_lekx/bin/getServerInfo.sh -s {server}' .
                          format(server=tmpName))

otherServers = [ 'austria', 'poland', 'denmark', 'portugal', 'mali', 'turkey', 'nepal' ]
for otherServer in otherServers:
    print(("%s") % (otherServer))
    os.system('/home/bev_lekx/bin/getServerInfo.sh -s {server}' .
              format(server=otherServer))

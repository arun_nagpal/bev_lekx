#!/usr/bin/python

import json
import mysql.connector
import argparse
import requests
import sys
import os
import base64
from PIL import Image, ImageDraw, ImageFont
import math
import csv
import shutil
import datetime
import time
import gzip

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

xSteps = 120
ySteps = 50
xStep = 0.0
yStep = 0.0

url = 'http://ec2-54-209-203-60.compute-1.amazonaws.com/zabbix/api_jsonrpc.php'
headers = {}
headers['content-type'] = 'application/json-rpc'

def zabbixLogin(username, password):
    params = {}
    params['user'] = username
    params['password'] = password
    data = {}
    data['jsonrpc'] = '2.0'
    data['method'] = 'user.login'
    data['params'] = params
    data['id'] = 1
    data['auth'] = None

    r = requests.post(url=url, data=json.dumps(data), headers=headers)
    result = ''
    status = False
    if(r.ok):
        status = True
        try:
            j = r.json()
            result = j['result']
        except:
            status = False
    return status, result

# Draw one circle to represent one outage array element.
def drawOutage(im, size, xi, yi):
    draw = ImageDraw.Draw(im)

    # Find the middle of the array element on the map image.
    circleX = int(xi * xStep + (xStep / 2))
    circleY = int(yi * yStep + (yStep / 2))

    circleRadius = size

    # Use a green gradient if the circle radius is 9 pixels or less. Otherwise
    # use a red gadient.
    if size <= 9:
        outerColor = [0x07, 0x4c, 0x00]
        innerColor = [0x42, 0xa3, 0x41]
    else:
        outerColor = [0xf1, 0x27, 0x11]
        innerColor = [0xf5, 0xaf, 0x19]
    # outerColor = [0xf1, 0x27, 0x11]
    # innerColor = [0xf5, 0xaf, 0x19]

    # Determine the color step for each circle.
    rStep = float((innerColor[0] - outerColor[0])) / float(circleRadius)
    gStep = float((innerColor[1] - outerColor[1])) / float(circleRadius)
    bStep = float((innerColor[2] - outerColor[2])) / float(circleRadius)

    # Draw the circles as individual rings using the calculated color.
    for z in range(circleRadius, 0, -1):
        distanceToCenter = float(z) / float(circleRadius)
        r = outerColor[0] + (z * rStep)
        g = outerColor[1] + (z * gStep)
        b = outerColor[1] + (z * bStep)
        color = (int(r), int(g), int(b))
        xy = [(circleX-z, circleY-z), (circleX+z, circleY+z)]
        draw.ellipse(xy, fill=color, outline=color)

# Increment the outage count in the array element that corresponds to the
# longitude and latitude.
def updateArray(latitude, longitude):
    # 3 degrees per array element.
    x = int(float(longitude + 180) / 3)
    y = int(float(180 - (latitude + 90)) / 3)
    arr[x][y] += 1

# Draw all of the outage circles
def drawIndicators():
    xi = 0
    while xi < xSteps:
        yi = 0
        while yi < ySteps:
            # Swag to adjust circle sizes. Otherwise 500 offline accounts fills
            # half of the map image.
            val = int(math.pow(math.log1p(arr[xi][yi]), 1.6))
            # Should we show outages of 1 or 2 accounts?
            if val > 0:
                drawOutage(im, val, xi, yi)
            yi += 1
        xi += 1

# Draw one of the reference circles in the lower left corner.
def drawReference(im, size, latitude, longitude):
    # Determine the array element.
    x = int(float(longitude + 180) / 3)
    y = int(float(180 - (latitude + 90)) / 3)

    # Set the outage number in the array element.
    arr[x][y] += size

    # Find the center of the array element.
    tx = int(x * xStep + (xStep / 2))
    ty = int(y * yStep + (yStep / 2))

    # Draw the text (move it over a bit to avoid the circle).
    draw = ImageDraw.Draw(im)
    font = ImageFont.truetype("/usr/share/fonts/truetype/liberation/LiberationSerif-Regular.ttf", 16)
    msg = str(size)
    draw.text((tx+35, ty-8), msg, fill=(0,0,0), font=font)

# Draw the reference circles in the lower left corner.
def drawReferences(im):
    drawReference(im, 5, -22, -170)
    drawReference(im, 50, -28, -170)
    drawReference(im, 500, -38, -170)
    drawReference(im, 5000, -50, -170)

def getCoords(db, country_id, postal_code):
    latitude = 0.0
    longitude = 0.0
    pcode = postal_code.upper()
    found = False

    # Shorten the loop for Canada because we know only the first 3 characters of the
    # postal codes are in the database.
    if country_id == 0:
        pcode = pcode[0:3]

    # Loop, using a shorter postal code each time, until we find the country and postal
    # code in the database.
    while not found:
        if len(pcode) > 0:
            query = ('select latitude, longitude from geonames '
                    'where snet_country_code={scode} and postal_code like "{pcode}%"'
                    . format(scode=country_id, pcode=pcode))
        else:
            query = ('select latitude, longitude from geonames '
                    'where snet_country_code={scode}' . format(scode=country_id))

        cursor.execute(query)
        rows = cursor.fetchall()
        totLat = 0
        totLong = 0
        foundCount = 0
        for row in rows:
            totLat += float(row[0])
            totLong += float(row[1])
            foundCount += 1
        if foundCount > 0:
            latitude = totLat / foundCount
            longitude = totLong / foundCount
            found = True
            break
        if len(pcode) == 0:
            break
        pcode = pcode[0:len(pcode)-1]

    return latitude, longitude

db = None
try:
    db = mysql.connector.connect(**usageDbCfg)
except:
   print("Error connecting to the database.")
   sys.exit(1)
cursor = db.cursor()

# Open the image.
im = Image.open('/home/bev_lekx/outageMap/world.png')
(x, y) = im.size

# Initialize the array.
arr = [[0 for i in range(y)] for j in range(x)]

# Determine the number of pixels in each array element.
xStep = float(x) / float(xSteps)    # 3 degrees in each x block.
yStep = float(y) / float(ySteps)    # 3 degress in each y block.

os.system('rm -f /home/bev_lekx/outageMap/offlineAccounts.csv')
os.system('cat /tmp/offlineAccountsCloud.csv >> /home/bev_lekx/outageMap/offlineAccounts.csv')
os.system('ssh orange "cat /tmp/nms-offlineAccountsCloudAU.csv" >> /home/bev_lekx/outageMap/offlineAccounts.csv')
os.system('ssh austria "cat /tmp/nms-offlineAccountsCloudEU.csv" >> /home/bev_lekx/outageMap/offlineAccounts.csv')

# Copy the offline accounts file to a history file.
historyCsvFileName = ('/home/bev_lekx/outageMap/offlineAccounts-' +
                      time.strftime("%Y%m%d%H%M%S", time.gmtime()) + '.csv')
shutil.copy('/home/bev_lekx/outageMap/offlineAccounts.csv', historyCsvFileName)

# Read the offline accounts csv file.
accts = []
with open('/home/bev_lekx/outageMap/offlineAccounts.csv') as f:
    reader = csv.reader(f, delimiter=",")
    accts = list(reader)

# Get the latitude and longitude for each account in the offline accounts file.
acctNdx = 0
while acctNdx < len(accts):
    country_id = accts[acctNdx][0]
    postal_code = accts[acctNdx][1]
    state = accts[acctNdx][2]
    acctNdx += 1

    if state == "PR":
        country_id = 189
    latitude, longitude = getCoords(db, country_id, postal_code)

    # print(country_id, postal_code, state, latitude, longitude)

    # Increment the data array. The element is determined by longitude and latitude.
    if latitude != 0.0 or longitude != 0.0:
        updateArray(float(latitude), float(longitude))

cursor.close()
db.close()

# Draw reference indicators.
drawReferences(im)

# Draw the outage indicators.
drawIndicators()

# Save the resulting heatmap image to "tmp.png".
im.save("/home/bev_lekx/outageMap/tmp.png", "PNG")
im.close()

# Copy the heatmap image to a dated file for historical viewing.
historyMapFileName = ('/home/bev_lekx/outageMap/heatmap-' +
                      time.strftime("%Y%m%d%H%M%S", time.gmtime()) + '.png')
shutil.copy('/home/bev_lekx/outageMap/tmp.png', historyMapFileName)

# Login to Zabbix.
status, authToken = zabbixLogin('bev_lekx', 'XZrsdG9ZihnfsU9Qg2Dsifx6vaa2m5tv')
if not status:
    print("Failed to login.")
    sys.exit(1)

# Read the temporary image and encode using base64.
imgFile = open('/home/bev_lekx/outageMap/tmp.png', 'rb')
imageRd = imgFile.read()
imgFile.close()
imageData = base64.b64encode(imageRd)

# Update the image in Zabbix.
data = {}
data['jsonrpc'] = '2.0'
data['id'] = 2
data['auth'] = authToken
data['method'] = 'image.update'
params = {}
params['imageid'] = '206'
params['name'] = 'World'
params['image'] = imageData
data['params'] = params

# print(json.dumps(data))
r = requests.post(url=url, data=json.dumps(data), headers=headers)
j = r.json()

# print(acctTotal)
# print(acctLocNotFound)

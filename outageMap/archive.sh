#!/bin/bash

fileDir="/home/bev_lekx/outageMap"

year=`date --date=yesterday +"%Y"`
month=`date --date=yesterday +"%m"`
day=`date --date=yesterday +"%d"`
echo "${year} ${month} ${day}"

cd ${fileDir}
tar cvfz heatmap-${year}${month}${day}-png.tgz heatmap-${year}${month}${day}??????.png
tar cvfz offlineAccounts-${year}${month}${day}-csv.tgz offlineAccounts-${year}${month}${day}??????.csv
mv heatmap-${year}${month}${day}-png.tgz /home/efs/snet/tools/outageMap
mv offlineAccounts-${year}${month}${day}-csv.tgz /home/efs/snet/tools/outageMap
rm -f heatmap-${year}${month}${day}??????.png
rm -f offlineAccounts-${year}${month}${day}??????.csv

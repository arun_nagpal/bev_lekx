#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cd ${HOME}/emailAnalysis

workdir=$1
cd ${workdir}

${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o s > successSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o s -d > success.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o w > warnings.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o u -d > unknown.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fa > failsAllSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fu > failsUserSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fd > failsDNSSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fh > failsHostSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fb > failsBlockedSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fa -d > failsAll.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fu -d > failsUser.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fd -d > failsDNS.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fh -d > failsHost.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fb -d > failsBlocked.txt

wc -l *Summary* > stats.txt

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cd ${HOME}/emailAnalysis

server="germany"

workdir=${server}
mkdir -p ${workdir}
cd ${workdir}
rm *

ssh bev_lekx@${server} "sudo bin/pygtail.py -o /home/bev_lekx/offset.maillog /var/log/mail.log > mail-${workdir}.log"
scp bev_lekx@${server}:mail-${workdir}.log .
ssh bev_lekx@${server} "sudo rm mail-${workdir}.log"

${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o s > successSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o s -d > success.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o w > warnings.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o u -d > unknown.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fa > failsAllSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fu > failsUserSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fd > failsDNSSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fh > failsHostSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fb > failsBlockedSummary.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fa -d > failsAll.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fu -d > failsUser.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fd -d > failsDNS.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fh -d > failsHost.txt
${HOME}/bin/analyseMailLog.py -f mail-${workdir}.log -o fb -d > failsBlocked.txt

wc -l *Summary* | grep -v total > stats.txt
wc -l successSummary.txt failsAllSummary.txt | grep total | sed -e 's/total/Total emails (failsAll + success)/g' >> stats.txt
statsMsg=`cat stats.txt`
cd ..

sendemail -f no-reply@securenettech.com \
          -t dinh.duong@securenettech.com \
             arun.nagpal@securnettech.com \
          -o tls=no \
          -u "Email analysis summary" \
          -m "${statsMsg}"

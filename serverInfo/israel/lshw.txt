israel
    description: Computer
    product: HVM domU
    vendor: Xen
    version: 4.2.amazon
    serial: ec2c0af7-5a2d-dbf3-83c0-4249cb16299e
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 vsyscall32
    configuration: boot=normal uuid=F70A2CEC-2D5A-F3DB-83C0-4249CB16299E
  *-core
       description: Motherboard
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Xen
          physical id: 0
          version: 4.2.amazon
          date: 08/24/2006
          size: 96KiB
          capabilities: pci edd
     *-cpu:0
          description: CPU
          product: Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz
          vendor: Intel Corp.
          physical id: 401
          bus info: cpu@0
          slot: CPU 1
          size: 2300MHz
          capacity: 2300MHz
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx pdpe1gb rdtscp x86-64 constant_tsc rep_good nopl xtopology pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single kaiser fsgsbase bmi1 avx2 smep bmi2 erms invpcid xsaveopt
     *-cpu:1
          description: CPU
          vendor: Intel
          physical id: 402
          bus info: cpu@1
          slot: CPU 2
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:2
          description: CPU
          vendor: Intel
          physical id: 403
          bus info: cpu@2
          slot: CPU 3
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:3
          description: CPU
          vendor: Intel
          physical id: 404
          bus info: cpu@3
          slot: CPU 4
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:4
          description: CPU
          vendor: Intel
          physical id: 405
          bus info: cpu@4
          slot: CPU 5
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:5
          description: CPU
          vendor: Intel
          physical id: 406
          bus info: cpu@5
          slot: CPU 6
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:6
          description: CPU
          vendor: Intel
          physical id: 407
          bus info: cpu@6
          slot: CPU 7
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:7
          description: CPU
          vendor: Intel
          physical id: 408
          bus info: cpu@7
          slot: CPU 8
          size: 2300MHz
          capacity: 2300MHz
     *-memory
          description: System Memory
          physical id: 1000
          size: 32GiB
        *-bank:0
             description: DIMM RAM
             physical id: 0
             slot: DIMM 0
             size: 16GiB
             width: 64 bits
        *-bank:1
             description: DIMM RAM
             physical id: 1
             slot: DIMM 1
             size: 16GiB
             width: 64 bits
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 02
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master
             configuration: latency=0
        *-ide
             description: IDE interface
             product: 82371SB PIIX3 IDE [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1.1
             bus info: pci@0000:00:01.1
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: ide bus_master
             configuration: driver=ata_piix latency=64
             resources: irq:0 ioport:1f0(size=8) ioport:3f6 ioport:170(size=8) ioport:376 ioport:c100(size=16)
        *-bridge UNCLAIMED
             description: Bridge
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bridge bus_master
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             product: GD 5446
             vendor: Cirrus Logic
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller bus_master
             configuration: latency=0
             resources: memory:f0000000-f1ffffff memory:f3008000-f3008fff
        *-network
             description: Ethernet interface
             product: 82599 Ethernet Controller Virtual Function
             vendor: Intel Corporation
             physical id: 3
             bus info: pci@0000:00:03.0
             logical name: ens3
             version: 01
             serial: 0a:66:9c:3c:37:b8
             size: 10Gbit/s
             width: 64 bits
             clock: 33MHz
             capabilities: msix bus_master cap_list ethernet physical
             configuration: autonegotiation=off broadcast=yes driver=ixgbevf driverversion=2.12.1-k duplex=full ip=172.29.255.83 latency=64 link=yes multicast=yes speed=10Gbit/s
             resources: irq:0 memory:f3000000-f3003fff memory:f3004000-f3007fff
        *-generic
             description: Unassigned class
             product: Xen Platform Device
             vendor: XenSource, Inc.
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bus_master
             configuration: driver=xen-platform-pci latency=0
             resources: irq:47 ioport:c000(size=256) memory:f2000000-f2ffffff
  *-network:0
       description: Ethernet interface
       physical id: 1
       logical name: tap6
       serial: be:fa:3d:26:2b:f8
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.7.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:1
       description: Ethernet interface
       physical id: 2
       logical name: tap2
       serial: f2:c9:26:ce:cb:28
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.4.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:2
       description: Ethernet interface
       physical id: 3
       logical name: tap9
       serial: f6:05:0f:b9:cd:00
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.10.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:3
       description: Ethernet interface
       physical id: 4
       logical name: tap5
       serial: aa:b1:64:8e:62:da
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.2.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:4
       description: Ethernet interface
       physical id: 5
       logical name: tap1
       serial: ce:38:81:90:af:95
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.6.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:5
       description: Ethernet interface
       physical id: 6
       logical name: tap8
       serial: ba:68:ce:86:42:c5
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.9.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:6
       description: Ethernet interface
       physical id: 7
       logical name: tap4
       serial: 7a:58:9b:c7:2c:95
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.5.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:7
       description: Ethernet interface
       physical id: 8
       logical name: tap0
       serial: 1e:2c:33:e9:6a:80
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.1.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:8
       description: Ethernet interface
       physical id: 9
       logical name: tap7
       serial: 2e:5f:27:5c:f5:1b
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.8.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:9
       description: Ethernet interface
       physical id: a
       logical name: tap3
       serial: 3a:57:3e:29:bd:f4
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.253.3.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s

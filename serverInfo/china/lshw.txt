china
    description: Computer
    product: m5.xlarge
    vendor: Amazon EC2
    serial: ec209db1-6fa2-9458-51c3-53e944a82d5d
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 vsyscall32
    configuration: uuid=B19D20EC-A26F-5894-51C3-53E944A82D5D
  *-core
       description: Motherboard
       vendor: Amazon EC2
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Amazon EC2
          physical id: 0
          version: 1.0
          date: 10/16/2017
          size: 64KiB
          capabilities: pci edd acpi virtualmachine
     *-memory
          description: System memory
          physical id: 1
          size: 15GiB
     *-cpu
          product: Intel(R) Xeon(R) Platinum 8175M CPU @ 2.50GHz
          vendor: Intel Corp.
          physical id: 2
          bus info: cpu@0
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp x86-64 constant_tsc rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single kaiser fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx avx512f rdseed adx smap clflushopt clwb avx512cd xsaveopt xsavec xgetbv1 ida arat pku
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 00
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa
             configuration: latency=0
        *-generic UNCLAIMED
             description: Non-VGA unclassified device
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 08
             width: 32 bits
             clock: 33MHz
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller
             configuration: latency=0
             resources: memory:fe400000-fe7fffff memory:febd0000-febdffff
        *-storage
             description: Non-Volatile memory controller
             physical id: 4
             bus info: pci@0000:00:04.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: storage pciexpress msix nvm_express bus_master cap_list
             configuration: driver=nvme latency=0
             resources: irq:11 memory:febf0000-febf3fff
        *-network
             description: Ethernet interface
             physical id: 5
             bus info: pci@0000:00:05.0
             logical name: ens5
             version: 00
             serial: 0e:9e:b4:c7:08:c0
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress msix bus_master cap_list ethernet physical
             configuration: broadcast=yes driver=ena driverversion=2.0.1K ip=172.29.253.49 latency=0 link=yes multicast=yes
             resources: irq:0 memory:febf4000-febf7fff memory:fe800000-fe8fffff memory:febe0000-febeffff

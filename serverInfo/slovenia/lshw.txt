slovenia
    description: Computer
    product: m5.xlarge
    vendor: Amazon EC2
    serial: ec2486cf-511e-cf8e-9f1b-3f6118b2a890
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 smp vsyscall32
    configuration: uuid=CF8624EC-1E51-8ECF-9F1B-3F6118B2A890
  *-core
       description: Motherboard
       vendor: Amazon EC2
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Amazon EC2
          physical id: 0
          version: 1.0
          date: 10/16/2017
          size: 64KiB
          capabilities: pci edd acpi virtualmachine
     *-memory
          description: System memory
          physical id: 1
          size: 15GiB
     *-cpu
          product: Intel(R) Xeon(R) Platinum 8175M CPU @ 2.50GHz
          vendor: Intel Corp.
          physical id: 2
          bus info: cpu@0
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp x86-64 constant_tsc rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single pti fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx avx512f avx512dq rdseed adx smap clflushopt clwb avx512cd avx512bw avx512vl xsaveopt xsavec xgetbv1 xsaves ida arat pku ospke
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 00
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa
             configuration: latency=0
        *-generic UNCLAIMED
             description: Non-VGA unclassified device
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 08
             width: 32 bits
             clock: 33MHz
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller
             configuration: latency=0
             resources: memory:fe400000-fe7fffff memory:c0000-dffff
        *-storage
             description: Non-Volatile memory controller
             physical id: 4
             bus info: pci@0000:00:04.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: storage pciexpress msix nvm_express bus_master cap_list
             configuration: driver=nvme latency=0
             resources: irq:11 memory:febf0000-febf3fff
        *-network
             description: Ethernet interface
             physical id: 5
             bus info: pci@0000:00:05.0
             logical name: ens5
             version: 00
             serial: 0e:07:b3:bd:4e:88
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress msix bus_master cap_list ethernet physical
             configuration: broadcast=yes driver=ena driverversion=2.0.2K ip=172.29.251.10 latency=0 link=yes multicast=yes
             resources: irq:0 memory:febf4000-febf7fff memory:fe800000-fe8fffff memory:febe0000-febeffff

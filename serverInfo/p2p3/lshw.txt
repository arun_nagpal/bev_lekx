p2p3
    description: Computer
    product: HVM domU ()
    vendor: Xen
    version: 4.2.amazon
    serial: ec2fae49-fb2c-7c1d-84bc-2123d2dff07e
    width: 64 bits
    capabilities: smbios-2.4 dmi-2.4 vsyscall32
    configuration: boot=normal uuid=EC2FAE49-FB2C-7C1D-84BC-2123D2DFF07E
  *-core
       description: Motherboard
       physical id: 0
     *-firmware:0
          description: BIOS
          vendor: Xen
          physical id: 0
          version: 4.2.amazon
          date: 03/31/2016
          size: 96KiB
          capabilities: pci edd
     *-cpu:0
          description: CPU
          product: Intel(R) Xeon(R) CPU E5-2676 v3 @ 2.40GHz
          vendor: Intel Corp.
          physical id: 1
          bus info: cpu@0
          slot: CPU 1
          size: 2400MHz
          capacity: 2400MHz
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp x86-64 constant_tsc rep_good nopl xtopology eagerfpu pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm xsaveopt fsgsbase bmi1 avx2 smep bmi2 erms invpcid
     *-memory:0
          description: System Memory
          physical id: 2
          capacity: 2GiB
        *-bank:0
             description: DIMM RAM
             physical id: 0
             slot: DIMM 0
             size: 2GiB
             width: 64 bits
        *-bank:1
             description: DIMM RAM
             physical id: 1
             slot: DIMM 0
             size: 2GiB
             width: 64 bits
     *-firmware:1
          description: BIOS
          vendor: Xen
          physical id: 3
          version: 4.2.amazon
          date: 03/31/2016
          size: 96KiB
          capabilities: pci edd
     *-cpu:1
          description: CPU
          vendor: Intel
          physical id: 4
          bus info: cpu@1
          slot: CPU 1
          size: 2400MHz
          capacity: 2400MHz
     *-memory:1
          description: System Memory
          physical id: 5
          capacity: 2GiB
     *-memory:2 UNCLAIMED
          physical id: 6
     *-memory:3 UNCLAIMED
          physical id: 7
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 02
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master
             configuration: latency=0
        *-ide
             description: IDE interface
             product: 82371SB PIIX3 IDE [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1.1
             bus info: pci@0000:00:01.1
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: ide bus_master
             configuration: driver=ata_piix latency=64
             resources: irq:0 ioport:1f0(size=8) ioport:3f6 ioport:170(size=8) ioport:376 ioport:c100(size=16)
        *-bridge UNCLAIMED
             description: Bridge
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bridge bus_master
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             product: GD 5446
             vendor: Cirrus Logic
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller bus_master
             configuration: latency=0
             resources: memory:f0000000-f1ffffff memory:f3000000-f3000fff
        *-generic
             description: Unassigned class
             product: Xen Platform Device
             vendor: XenSource, Inc.
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bus_master
             configuration: driver=xen-platform-pci latency=0
             resources: irq:28 ioport:c000(size=256) memory:f2000000-f2ffffff
  *-network:0
       description: Ethernet interface
       physical id: 1
       logical name: tap10
       serial: b6:1e:92:e3:3d:d9
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.32.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:1
       description: Ethernet interface
       physical id: 2
       logical name: eth0
       serial: 0a:00:b2:05:7c:5d
       capabilities: ethernet physical
       configuration: broadcast=yes driver=vif ip=172.29.255.52 link=yes multicast=yes
  *-network:2
       description: Ethernet interface
       physical id: 3
       logical name: tap0
       serial: ae:e8:48:93:b6:5a
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.10.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:3
       description: Ethernet interface
       physical id: 4
       logical name: tap1
       serial: 36:21:aa:43:44:36
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.1.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:4
       description: Ethernet interface
       physical id: 5
       logical name: tap2
       serial: 86:09:2f:f8:a3:1c
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.2.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:5
       description: Ethernet interface
       physical id: 6
       logical name: tap3
       serial: 0e:d8:19:9a:60:98
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.3.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:6
       description: Ethernet interface
       physical id: 7
       logical name: tap4
       serial: 62:9b:5e:8e:30:a2
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.4.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:7
       description: Ethernet interface
       physical id: 8
       logical name: tap5
       serial: fe:23:5e:15:df:fa
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.5.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:8
       description: Ethernet interface
       physical id: 9
       logical name: tap6
       serial: 5e:7f:b9:5e:34:ab
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.6.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:9
       description: Ethernet interface
       physical id: a
       logical name: tap7
       serial: 02:9a:e7:f5:80:f2
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.7.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:10
       description: Ethernet interface
       physical id: b
       logical name: tap8
       serial: de:dc:9f:c8:54:e0
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.8.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:11
       description: Ethernet interface
       physical id: c
       logical name: tap9
       serial: 2e:9a:c9:a0:78:c6
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.22.9.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s

angola
    description: Computer
    product: m5.xlarge
    vendor: Amazon EC2
    serial: ec23e65a-711b-99b8-3fd4-9b8f67384ceb
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 vsyscall32
    configuration: uuid=5AE623EC-1B71-B899-3FD4-9B8F67384CEB
  *-core
       description: Motherboard
       vendor: Amazon EC2
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Amazon EC2
          physical id: 0
          version: 1.0
          date: 10/16/2017
          size: 64KiB
          capabilities: pci edd acpi virtualmachine
     *-memory
          description: System memory
          physical id: 1
          size: 15GiB
     *-cpu
          product: Intel(R) Xeon(R) Platinum 8175M CPU @ 2.50GHz
          vendor: Intel Corp.
          physical id: 2
          bus info: cpu@0
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss ht syscall nx pdpe1gb rdtscp x86-64 constant_tsc rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm 3dnowprefetch invpcid_single kaiser fsgsbase tsc_adjust bmi1 hle avx2 smep bmi2 erms invpcid rtm mpx avx512f rdseed adx smap clflushopt clwb avx512cd xsaveopt xsavec xgetbv1 ida arat pku
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 00
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa
             configuration: latency=0
        *-generic UNCLAIMED
             description: Non-VGA unclassified device
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 08
             width: 32 bits
             clock: 33MHz
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller
             configuration: latency=0
             resources: memory:fe400000-fe7fffff memory:febd0000-febdffff
        *-storage
             description: Non-Volatile memory controller
             physical id: 4
             bus info: pci@0000:00:04.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: storage pciexpress msix nvm_express bus_master cap_list
             configuration: driver=nvme latency=0
             resources: irq:11 memory:febf0000-febf3fff
        *-network
             description: Ethernet interface
             physical id: 5
             bus info: pci@0000:00:05.0
             logical name: ens5
             version: 00
             serial: 0a:e7:77:c4:9b:e2
             width: 32 bits
             clock: 33MHz
             capabilities: pciexpress msix bus_master cap_list ethernet physical
             configuration: broadcast=yes driver=ena driverversion=2.0.1K ip=172.29.252.72 latency=0 link=yes multicast=yes
             resources: irq:0 memory:febf4000-febf7fff memory:fe800000-fe8fffff memory:febe0000-febeffff
  *-network:0
       description: Ethernet interface
       physical id: 1
       logical name: tap6
       serial: aa:b5:2f:f6:c0:4b
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.6.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:1
       description: Ethernet interface
       physical id: 2
       logical name: tap2
       serial: 46:ee:d5:ee:a9:30
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.2.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:2
       description: Ethernet interface
       physical id: 3
       logical name: tap9
       serial: 32:22:44:d8:c1:53
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.5.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:3
       description: Ethernet interface
       physical id: 4
       logical name: tap5
       serial: 42:e9:98:a4:78:3c
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.7.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:4
       description: Ethernet interface
       physical id: 5
       logical name: tap1
       serial: 92:cf:ce:5e:a6:60
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.10.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:5
       description: Ethernet interface
       physical id: 6
       logical name: tap8
       serial: 4e:6f:99:00:dd:a8
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.1.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:6
       description: Ethernet interface
       physical id: 7
       logical name: tap4
       serial: 16:c6:98:51:24:a3
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.9.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:7
       description: Ethernet interface
       physical id: 8
       logical name: tap0
       serial: 32:93:d9:45:1d:63
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.4.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:8
       description: Ethernet interface
       physical id: 9
       logical name: tap7
       serial: fe:31:15:63:34:d3
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.8.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:9
       description: Ethernet interface
       physical id: a
       logical name: tap3
       serial: 3e:cd:26:b7:78:30
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=10.252.3.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s

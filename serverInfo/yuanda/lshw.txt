ip-172-29-255-9
    description: Computer
    width: 64 bits
    capabilities: vsyscall32
  *-core
       description: Motherboard
       physical id: 0
     *-memory
          description: System memory
          physical id: 0
          size: 3579MiB
     *-cpu
          product: Intel(R) Xeon(R) CPU E5-2670 v2 @ 2.50GHz
          vendor: Intel Corp.
          physical id: 1
          bus info: cpu@0
          width: 64 bits
          capabilities: fpu fpu_exception wp de tsc msr pae cx8 sep cmov pat clflush mmx fxsr sse sse2 ht syscall nx x86-64 constant_tsc up rep_good nopl pni pclmulqdq ssse3 cx16 sse4_1 sse4_2 x2apic popcnt aes rdrand hypervisor lahf_lm fsgsbase erms
  *-network
       description: Ethernet interface
       physical id: 1
       logical name: eth0
       serial: 0a:d7:1d:3c:32:c1
       capabilities: ethernet physical
       configuration: broadcast=yes driver=vif ip=172.29.255.9 link=yes multicast=yes

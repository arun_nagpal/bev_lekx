kiama
    description: Computer
    product: HVM domU
    vendor: Xen
    version: 4.2.amazon
    serial: ec2f9ba4-f562-60f6-e01e-107b956f585e
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 vsyscall32
    configuration: boot=normal uuid=A49B2FEC-62F5-F660-E01E-107B956F585E
  *-core
       description: Motherboard
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Xen
          physical id: 0
          version: 4.2.amazon
          date: 08/24/2006
          size: 96KiB
          capabilities: pci edd
     *-cpu:0
          description: CPU
          product: Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz
          vendor: Intel Corp.
          physical id: 401
          bus info: cpu@0
          slot: CPU 1
          size: 2300MHz
          capacity: 2300MHz
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx rdtscp x86-64 constant_tsc rep_good nopl xtopology pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single kaiser fsgsbase bmi1 avx2 smep bmi2 erms invpcid xsaveopt
     *-cpu:1
          description: CPU
          vendor: Intel
          physical id: 402
          bus info: cpu@1
          slot: CPU 2
          size: 2300MHz
          capacity: 2300MHz
     *-memory
          description: System Memory
          physical id: 1000
          size: 4GiB
        *-bank
             description: DIMM RAM
             physical id: 0
             slot: DIMM 0
             size: 4GiB
             width: 64 bits
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 02
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master
             configuration: latency=0
        *-ide
             description: IDE interface
             product: 82371SB PIIX3 IDE [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1.1
             bus info: pci@0000:00:01.1
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: ide isa_compatibility_mode-only_controller__supports_bus_mastering bus_master
             configuration: driver=ata_piix latency=64
             resources: irq:0 ioport:1f0(size=8) ioport:3f6 ioport:170(size=8) ioport:376 ioport:c100(size=16)
        *-bridge UNCLAIMED
             description: Bridge
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bridge bus_master
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             product: GD 5446
             vendor: Cirrus Logic
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller bus_master
             configuration: latency=0
             resources: memory:f0000000-f1ffffff memory:f3000000-f3000fff
        *-generic
             description: Unassigned class
             product: Xen Platform Device
             vendor: XenSource, Inc.
             physical id: 3
             bus info: pci@0000:00:03.0
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bus_master
             configuration: driver=xen-platform-pci latency=0
             resources: irq:28 ioport:c000(size=256) memory:f2000000-f2ffffff
  *-network:0
       description: Ethernet interface
       physical id: 1
       logical name: tap6
       serial: c6:55:1a:e5:21:8e
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.3.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:1
       description: Ethernet interface
       physical id: 2
       logical name: tap2
       serial: 1e:e9:a5:f1:1e:af
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.10.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:2
       description: Ethernet interface
       physical id: 3
       logical name: tap9
       serial: fa:db:f0:19:8e:d0
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.17.32.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:3
       description: Ethernet interface
       physical id: 4
       logical name: tap5
       serial: 12:28:e5:22:86:c0
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.4.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:4
       description: Ethernet interface
       physical id: 5
       logical name: tap1
       serial: 02:4b:1c:4d:95:e3
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.8.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:5
       description: Ethernet interface
       physical id: 6
       logical name: tap8
       serial: ee:7d:c0:96:1f:a8
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.5.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:6
       description: Ethernet interface
       physical id: 7
       logical name: tap4
       serial: 5a:0d:cb:f5:57:ed
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.7.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:7
       description: Ethernet interface
       physical id: 8
       logical name: tap0
       serial: 92:99:96:97:fc:51
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.6.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:8
       description: Ethernet interface
       physical id: 9
       logical name: tap10
       serial: 5e:a1:56:ae:ba:96
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.1.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:9
       description: Ethernet interface
       physical id: a
       logical name: tap7
       serial: 12:a4:87:05:3f:8d
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.2.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:10
       description: Ethernet interface
       physical id: b
       logical name: tap3
       serial: 02:bc:bc:ed:8c:a3
       size: 10Mbit/s
       capabilities: ethernet physical
       configuration: autonegotiation=off broadcast=yes driver=tun driverversion=1.6 duplex=full ip=172.18.9.1 link=yes multicast=yes port=twisted pair speed=10Mbit/s
  *-network:11
       description: Ethernet interface
       physical id: c
       logical name: eth0
       serial: 0a:13:f8:5a:c5:b5
       capabilities: ethernet physical
       configuration: broadcast=yes driver=vif ip=172.31.254.186 link=yes multicast=yes

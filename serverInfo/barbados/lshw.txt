barbados
    description: Computer
    product: HVM domU
    vendor: Xen
    version: 4.2.amazon
    serial: ec253c69-aee2-78ea-b09b-6aa4868f16f2
    width: 64 bits
    capabilities: smbios-2.7 dmi-2.7 vsyscall32
    configuration: boot=normal uuid=693C25EC-E2AE-EA78-B09B-6AA4868F16F2
  *-core
       description: Motherboard
       physical id: 0
     *-firmware
          description: BIOS
          vendor: Xen
          physical id: 0
          version: 4.2.amazon
          date: 08/24/2006
          size: 96KiB
          capabilities: pci edd
     *-cpu:0
          description: CPU
          product: Intel(R) Xeon(R) CPU E5-2686 v4 @ 2.30GHz
          vendor: Intel Corp.
          physical id: 401
          bus info: cpu@0
          slot: CPU 1
          size: 2300MHz
          capacity: 2300MHz
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx pdpe1gb rdtscp x86-64 constant_tsc rep_good nopl xtopology pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm invpcid_single kaiser fsgsbase bmi1 avx2 smep bmi2 erms invpcid xsaveopt
     *-cpu:1
          description: CPU
          vendor: Intel
          physical id: 402
          bus info: cpu@1
          slot: CPU 2
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:2
          description: CPU
          vendor: Intel
          physical id: 403
          bus info: cpu@2
          slot: CPU 3
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:3
          description: CPU
          vendor: Intel
          physical id: 404
          bus info: cpu@3
          slot: CPU 4
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:4
          description: CPU
          vendor: Intel
          physical id: 405
          bus info: cpu@4
          slot: CPU 5
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:5
          description: CPU
          vendor: Intel
          physical id: 406
          bus info: cpu@5
          slot: CPU 6
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:6
          description: CPU
          vendor: Intel
          physical id: 407
          bus info: cpu@6
          slot: CPU 7
          size: 2300MHz
          capacity: 2300MHz
     *-cpu:7
          description: CPU
          vendor: Intel
          physical id: 408
          bus info: cpu@7
          slot: CPU 8
          size: 2300MHz
          capacity: 2300MHz
     *-memory
          description: System Memory
          physical id: 1000
          size: 32GiB
        *-bank:0
             description: DIMM RAM
             physical id: 0
             slot: DIMM 0
             size: 16GiB
             width: 64 bits
        *-bank:1
             description: DIMM RAM
             physical id: 1
             slot: DIMM 1
             size: 16GiB
             width: 64 bits
     *-pci
          description: Host bridge
          product: 440FX - 82441FX PMC [Natoma]
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 02
          width: 32 bits
          clock: 33MHz
        *-isa
             description: ISA bridge
             product: 82371SB PIIX3 ISA [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: isa bus_master
             configuration: latency=0
        *-ide
             description: IDE interface
             product: 82371SB PIIX3 IDE [Natoma/Triton II]
             vendor: Intel Corporation
             physical id: 1.1
             bus info: pci@0000:00:01.1
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: ide bus_master
             configuration: driver=ata_piix latency=64
             resources: irq:0 ioport:1f0(size=8) ioport:3f6 ioport:170(size=8) ioport:376 ioport:c100(size=16)
        *-bridge UNCLAIMED
             description: Bridge
             product: 82371AB/EB/MB PIIX4 ACPI
             vendor: Intel Corporation
             physical id: 1.3
             bus info: pci@0000:00:01.3
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bridge bus_master
             configuration: latency=0
        *-display UNCLAIMED
             description: VGA compatible controller
             product: GD 5446
             vendor: Cirrus Logic
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller bus_master
             configuration: latency=0
             resources: memory:f0000000-f1ffffff memory:f3008000-f3008fff
        *-network
             description: Ethernet interface
             product: 82599 Ethernet Controller Virtual Function
             vendor: Intel Corporation
             physical id: 3
             bus info: pci@0000:00:03.0
             logical name: ens3
             version: 01
             serial: 0a:cd:04:62:4e:3c
             size: 10Gbit/s
             width: 64 bits
             clock: 33MHz
             capabilities: msix bus_master cap_list ethernet physical
             configuration: autonegotiation=off broadcast=yes driver=ixgbevf driverversion=2.12.1-k duplex=full ip=172.29.255.27 latency=64 link=yes multicast=yes speed=10Gbit/s
             resources: irq:0 memory:f3000000-f3003fff memory:f3004000-f3007fff
        *-generic
             description: Unassigned class
             product: Xen Platform Device
             vendor: XenSource, Inc.
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 01
             width: 32 bits
             clock: 33MHz
             capabilities: bus_master
             configuration: driver=xen-platform-pci latency=0
             resources: irq:47 ioport:c000(size=256) memory:f2000000-f2ffffff

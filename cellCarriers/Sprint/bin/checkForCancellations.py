#!/usr/bin/python

# Scan the rows in the sprint_suspensions table.
#   - Check the suspended imsi in the sprint_devices table.
#     - If the imsi is no longer in the "Suspended" state, remove the row from the
#       suspensions table.
#     - If the imsi has been suspended for longer than the minimum number of
#       days, add the imsi to the list of lines to be cancelled.

import datetime
import time
import os
import mysql.connector
import argparse
import sys

carrier = 'sprint'

# Database parameters.
config = {
    'user': 'bev_lekx',
    'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
    'host': '172.29.253.45',
    'database': 'celldata',
    'raise_on_warnings': False,
}

# Command-line arguments.
parser = argparse.ArgumentParser(add_help=False)
parser.add_argument('-o', '--outfile', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -o, --outfile   Output file.")
    print("       -h, -?, --help  Display this helpful message.")
    sys.exit(0)
if not args.outfile:
    print("Missing output file.")
    sys.exit(1)

curDate = int(time.time())

# Allow a line to be suspended for 30 days before it is cancelled.
secondsPerDay = 60 * 60 * 24
maxAge = 30

cancellations = []

# Connect to the database.
cnx = mysql.connector.connect(**config)

query = ('SELECT imsi, state FROM sprint_devices')
cursor = cnx.cursor()
cursor.execute(query)
deviceRows = cursor.fetchall()
cursor.close()
for deviceRow in deviceRows:
    imsi = deviceRow[0]
    deviceState = deviceRow[1]

    suspendFound = False
    query = ('SELECT suspend_date FROM sprint_suspensions WHERE imsi="{imsi}"' . format(imsi=imsi))
    cursor = cnx.cursor()
    cursor.execute(query)
    suspendRows = cursor.fetchall()
    cursor.close()
    for suspendRow in suspendRows:
        suspendDate = suspendRow[0]
        suspendFound = True

    # Remove the device from the suspended list if it has been cancelled.
    if deviceState == 'Cancelled':
        if suspendFound == True:
            print("IMSI %s has been cancelled.") % (imsi)
            query = ('DELETE FROM sprint_suspensions WHERE imsi="{imsi}"' . format(imsi=imsi))
            cursor = cnx.cursor()
            cursor.execute(query)
            cnx.commit()
            cursor.close()
    elif deviceState == 'Suspended':
        if suspendFound == True:
            suspendedDays =  (curDate - suspendDate) / secondsPerDay
            if suspendedDays > maxAge:
                print("IMSI %s has been suspended for %d days. It can be cancelled") % (imsi, suspendedDays)
                cancellations.append(imsi)
            else:
                print("IMSI %s suspended for %d days.") % (imsi, suspendedDays)
        else:
            # INSERT INTO sprint_suspensions (imsi, suspend_date) VALUES ("310006183025398", 1560785575)
            print("New suspended IMSI %s.") % (imsi)
            query = ('INSERT INTO sprint_suspensions (imsi, suspend_date) '
                     'VALUES ("{imsi}", {suspend_date})' .
                     format(imsi=imsi, suspend_date=curDate))
            print query
            cursor = cnx.cursor()
            cursor.execute(query)
            cnx.commit()
            cursor.close()
    else:
        if suspendFound:
            print("IMSI %s has returned to bill state.") % (imsi)
            query = ('DELETE FROM sprint_suspensions WHERE imsi="{imsi}"' . format(imsi=imsi))
            cursor = cnx.cursor()
            cursor.execute(query)
            cnx.commit()
            cursor.close()
cnx.close()

# Write out the cancellation list.
csvFile = open(args.outfile, 'w')
if len(cancellations) > 0:
    for imsi in cancellations:
        csvFile.write(imsi + '\n')
csvFile.close()

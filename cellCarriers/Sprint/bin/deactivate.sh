#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/Sprint"
workDir="${carrierDir}/work"
cdate=`date +%Y%m%d`

# Thresholds.
th_delDeleted=90      # Account deleted threshold in days (-t).
th_delActivate=365     # Activated threshold in days for lines with deleted accounts (-a).
th_delLastused=365     # Last used threshold in days for lines with deleted accounts (-l).
th_noacctActivate=365  # Activated threshold for lines without accounts in days (-y).
th_noacctLastused=365  # Last used threshold for lines without accounts in days (-z).

# Print a detailed reports of the lines to be suspended.
${cellDir}/bin/findCellLines.py -d -v -c sprint > \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    > ${workDir}/sprint-suspensions-report-${cdate}.txt

# Suspensions.
${cellDir}/bin/findCellLines.py -d -o imsi -c sprint \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/sprint-suspensions-${cdate}.csv

# Cancellations.
# Check for Sprint lines returning to bill state and possible cancellations.
${cellDir}/Sprint/bin/checkForCancellations.py \
          -o ${workDir}/sprint-cancellations-${cdate}.csv \
          > ${workDir}/sprint-cancellations-${cdate}.out 2>&1

# Send an email.
/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t dinh.duong@securenettech.com \
                      arun.nagpal@securnettech.com \
                   -o tls=no \
                   -u "Sprint cell cancellations and suspensions" \
                   -m "Sprint cell cancellations and suspensions for `date +"%B %d, %Y"` are ready."

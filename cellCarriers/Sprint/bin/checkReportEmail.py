#!/usr/bin/python

# This script checks an email address for mail and acts upon it.
# References:
#   https://docs.python.org/2/library/imaplib.html
#   https://pymotw.com/2/imaplib/
# Potentially install packages:
#  sudo apt install python-pip
#  sudo pip install requests
#  sudo pip install email

import os
import imaplib
import email
import requests
import time
import datetime
from pytz import timezone
import calendar
import zipfile
from dateutil import parser
import mysql.connector
import base64
import csv

carrier = 'sprint'

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

EMAIL_ADDR  = "bev.lekx@securenettech.com"
SMTP_SERVER = "imap.gmail.com"
SMTP_PORT   = 993

# This function checks email.
# The email search criteria is currently all unread messages. However we could
# search for a subject or from or a combination of these fields.
# Optionally the messages could be deleted and expunged if preferred.
def checkEmail():
    downloadLinks = []
    credsFile = open('/home/bev_lekx/.emailcreds.txt')
    credsData = credsFile.read()
    credsFile.close()

    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(EMAIL_ADDR, credsData)
    mail.select('inbox')

    # Search for mail in the inbox matching the search criteria.
    search = '(FROM "noreply@sprint.com" SUBJECT "Command Center Schedule Device List Report")'

    result, data = mail.search(None, search)
    mail_ids = data[0]

    # Get the message IDs.
    id_list = mail_ids.split()

    # Iterate through the messages.
    for i in id_list:
        # Fetching a message marks it as read.
        typ, data = mail.fetch(i, '(RFC822)' )

        msg = email.message_from_string(data[0][1])
        mdate = parser.parse(msg['Date'])
        print msg['from'], msg['subject'], mdate

        # Compare the received date and time with the current date and
        # time in UTC. If the message is more than 50 hours old we
        # ignore it.
        msgAge = datetime.datetime.now(timezone('UTC')) - mdate
        if msgAge > datetime.timedelta(hours=50):
            print("Message is too old: %s.") % (str(msgAge))
            continue

        # Search for the part of the email message containing the report link.
        for part in msg.walk():
            if part.get_content_type() == 'text/plain':
                msgtext = part.get_payload(decode=True)
                if 'To review the report, click on the <a href="' in msgtext:
                    ndx = msgtext.find('<a href="')
                    s01 = msgtext[ndx+9:]
                    # Extract the report link.
                    downloadLinks.append(s01[:s01.find('">Device List</a>')])

    mail.close()
    mail.logout()
    return downloadLinks

def downloadReport(url):
    result = False
    newFileName = ''

    print("Downloading: %s") % (url)
    r = requests.get(url)
    with open('sprintdownload.zip', 'wb') as code:
        code.write(r.content)
    if os.path.exists('sprintdownload.zip'):
        print("sprintdownload.zip has been downloaded.")
        zip = zipfile.ZipFile('sprintdownload.zip', 'r')
        zip.extractall()
        os.remove('sprintdownload.zip')
        files = os.listdir(os.getcwd())
        for oldFileName in files:
            s01 = 'Account_Devices_Detail_'
            newPath = '/home/bev_lekx/cellCarriers/Sprint/exports/'
            if oldFileName[0:len(s01)] == s01 and oldFileName[-4:] == '.csv':
                newFileName = newPath + 'sprintDevices-' + time.strftime("%Y%m%d", time.gmtime()) + '.csv'
                os.rename(oldFileName, newFileName)
                print("Moving %s to %s") % (oldFileName, newFileName)
                result = True
    print("Download result: %s") % (str(result))
    return result, newFileName

def handleUrl(url):
    fileName = ''
    encUrl = base64.b64encode(url)

    # Check the database to see if we have already downloaded this report.
    count = 0
    cursor = cnx.cursor()
    query = ('SELECT count(*) FROM sprint_reports WHERE url="{url}"' . format(url=encUrl))
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
         count = row[0]
    cursor.close()

    if count == 0:
        print("%s has not been downloaded before.") % (url)
    else:
        print("%s has already been downloaded.") % (url)

    result = False
    if count == 0:
        # Download the report file in the url.
        result, fileName = downloadReport(url)

        # If the file download was successful, update the database so we don't
        # download this file again.
        if result == True:
            print("Updating the database: %s.") % (url)
            cursor = cnx.cursor()
            query = ('INSERT INTO sprint_reports (url) VALUES ("{url}")' . format(url=encUrl))
            cursor.execute(query)
            cursor.close()
            cnx.commit()

    return fileName

def importReport(csvfile):
    try:
        csvfile = open(csvfile, 'rb')
    except IOError:
        print "Error: \"" + csvfile + "\" could not be opened."
        return

    counter = 0
    csvreader = csv.reader(csvfile, quotechar="\"")
    for row in csvreader:
        counter += 1
        accountId = row[0].strip()
        if not accountId.isdigit():
            continue

        mdn = row[3].strip().replace('\'', '')
        meid = row[5].strip()
        iccid = row[8].strip().replace('\'', '')
        imsi = row[9].strip().replace('\'', '')
        state = row[11].strip()
        statusDate = row[12].strip()
        rateplan = row[15].strip()
        ipaddress = row[20].strip()

        statusDateInt = int(calendar.timegm(time.strptime(statusDate, '%Y-%m-%d')))

        rowCount = 0
        r_date = 0
        cursor = cnx.cursor()
        query = ('SELECT id, status_date, state from sprint_devices where imsi="{imsi}"' . format(imsi=imsi))
        cursor.execute(query)
        rs = cursor.fetchall()
        cursor.close()
        for r in rs:
            r_id = r[0]
            r_date = int(r[1])
            r_state = r[2]
            rowCount += 1

        if rowCount == 0:
            query = ('INSERT INTO sprint_devices (mdn, meid, iccid, imsi, state, '
                     'rateplan, ipaddress, status_date) '
                     'VALUES ("' + mdn + '", "' + meid + '", "' + iccid + '", "' +
                     imsi + '", "' + state + '", "' + rateplan + '", "' + ipaddress +
                     '", ' + str(statusDateInt) + ')')
            cursor = cnx.cursor()
            cursor.execute(query)
            cnx.commit()
            cursor.close()
        elif rowCount == 1:
            # Update the record in the database if it is older than the imported record.
            if r_date < statusDateInt:
                query = ('UPDATE sprint_devices set state="{state}", status_date={status_date} '
                         'WHERE imsi="{imsi}"' .
                         format(state=state, status_date=statusDateInt, imsi=imsi))
                cursor = cnx.cursor()
                cursor.execute(query)
                cnx.commit()
                cursor.close()
        else:
            print("Duplicate row found when updating sprint_device with IMSI: %s") % (imsi)

        if counter % 1000 == 0:
            print("%d") %(counter)

    csvfile.close()

# scriptDir = os.getcwd()
scriptDir = '/home/bev_lekx/cellCarriers/Sprint/bin'

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Check email.
urls = checkEmail()

# If we have received mail, download the zip file in the link, extract the csv
# from the zip file and move the csv into the Sprint exports directory. Then
# import the file into the tools database. Finally, delete the file.
if len(urls) > 0:
    for url in urls:
        fileName = handleUrl(url)

        # If a file name was returned, it is a new report file and needs to be
        # managed.
        if len(fileName) > 0:
            # Import the report file into the sprint_device tables.
            print("Import %s to sprint_devices") % (fileName)
            importReport(fileName)

            # Import the report file into the cellusage table as well.
            print("Import %s to cellusage") % (fileName)
            os.system(scriptDir + '/devicesFromExport.py -f ' + fileName)

            # Delete the report file.
            print("Deleting %s.") % (fileName)
            os.system('rm ' + fileName)

cnx.close()

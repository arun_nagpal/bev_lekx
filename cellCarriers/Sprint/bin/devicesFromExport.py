#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'sprint'
programRunning = True

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import Sprint device spreadsheet into celldata.db.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Sprint spreadsheet in csv format.')
args = parser.parse_args()

# Connect to the database.
cnx = mysql.connector.connect(**config)

# ##Report Type ,Account_Devices_Detail,
# ##Data Source Last Updated Date/Time ,2018-07-30 12:03:10Z,
# ##Report Created Date/Time,2018-07-30 12:03:14Z,
# ##Reserved,
# ##Reserved,
#      0         1         2       3       4     5        6       7      8     9          10          11
# ACCOUNT ID,ACCOUNT NAME,MIN,MDN/MSISDN,MEID,HEX-MEID,MFR-ESN,HEX-ESN,ICCID,IMSI,DEVICE-PROFILE-ID,Status,
#      12         13        14            15              16               17             18           19
# Status Date,Rate Plan,Pool Name,Pending Rate Plan,Pending Pool Name,Effective Date,Report Group,Service Name,
#      20
# Static IP,Application Type,Activation Date,Traffic Policy,Firmware Version,Hardware Version,Current Location,Device Name,Custom Field 1,Custom Field 2,Custom Field 3,Custom Field 4,Custom Field 5,Agent Id,
# "41169","SecureNet Technologies, LLC","7732136459","5776876442","'270113183804287138'","A100003E416AA2","128-10844485","80A57945"," ","'310007732136459'","SPR0000000185596","Cancelled","2018-07-17","SPR_SecNet150K_1MB","POOL F","","","","0","SecureNetProduction","","Fixed","2015-04-21 20:49:31.0","","","","","","","","","","",""

try:
    csvfile = open(args.csvfile, 'rb')
except IOError:
    print "Error: \"" + args.csvfile + "\" could not be opened."
    sys.exit(1)

counter = 0

csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
    counter += 1
    accountId = row[0].strip()
    if not accountId.isdigit():
        continue

    dmin = row[2].strip()
    mdn = row[3].strip().replace('\'', '')
    msisdn = mdn
    meid = row[5].strip()
    iccid = row[8].strip().replace('\'', '')
    imsi = row[9].strip().replace('\'', '')
    state = row[11].strip()
    rateplan = row[13].strip()
    ipaddress = row[20].strip()
    activation_date = row[22].strip()
    datausage = 0.0
    imei = ''

    rowCount = 0
    rowid = 0
    cursor = cnx.cursor()
    query = ('SELECT id from cellusage where carrier="{carrier}" and imsi="{imsi}"' .
             format(carrier=carrier, imsi=imsi))
    cursor.execute(query)
    rs = cursor.fetchall()
    cursor.close()
    for r in rs:
        rowid = r[0]
        rowCount += 1

    if rowCount == 0:
        query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                 'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                 'activation_date) '
                 'VALUES ("{carrier}", "{state}", "{iccid}", "{imsi}", "{msisdn}", '
                 '"{imei}", "{mdn}", "{meid}", "{dmin}", "{ipaddress}", "{rateplan}", '
                 '{usage}, 0, 0)' .
                 format(carrier=carrier, state=state, iccid=iccid, imsi=imsi, msisdn=msisdn,
                        imei=imei, mdn=mdn, meid=meid, dmin=dmin, ipaddress=ipaddress,
                        rateplan=rateplan, usage="{:0.2f}".format(datausage)))
        # print(query)
        cursor = cnx.cursor()
        cursor.execute(query)
        cnx.commit()
        cursor.close()
    elif rowCount == 1:
        query = ('UPDATE cellusage set datausage={usage}, '
                 'state="{state}", iccid="{iccid}", msisdn="{msisdn}", '
                 'imei="{imei}", mdn="{mdn}", meid="{meid}", min="{dmin}", '
                 'ipaddress="{ipaddress}", rateplan="{rateplan}" '
                 'WHERE id={rowid}' .
                 format(usage="{:0.2f}".format(datausage), state=state, iccid=iccid,
                        msisdn=msisdn, imei=imei, mdn=mdn, meid=meid, dmin=dmin,
                        ipaddress=ipaddress, rateplan=rateplan, rowid=rowid))
        # print(query)
        cursor = cnx.cursor()
        cursor.execute(query)
        cnx.commit()
        cursor.close()
    else:
        print("Duplicate row found when updating cellusage with IMSI: %s") % (imsi)

    if counter % 1000 == 0:
        print("%d") %(counter)

cnx.close()

#!/usr/bin/python

from pprint import pprint
import boto3
import os
import subprocess
import sys
import argparse
import xml.etree.ElementTree as ET
import csv

def suspendOneLine(identifier, kind, state):
    if kind.lower() == 'meid':
        kind = 'meId'
    if kind.lower() == 'imsi':
        kind = 'imsi'

    request = ''
    if state == 'suspend':
        request = 'SuspendRequest'
    elif state == 'cancel':
        request = 'CancelRequest'

    fileName=('request-' + identifier + '.xml')
    reqFile = open(fileName, 'w')
    reqFile.write('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="service.aeris.com/aeradmin/v1/API">\n')
    reqFile.write('  <soapenv:Header/>\n')
    reqFile.write('  <soapenv:Body>\n')
    reqFile.write('    <api:' + request + '>\n')
    reqFile.write('    <accountID>41169</accountID>\n')
    reqFile.write('    <technology>CDMA</technology>\n')
    reqFile.write('    <email>bev.lekx@securenettech.com</email>\n')
    reqFile.write('    <deviceID>\n')
    reqFile.write('      <' + kind + '>' + identifier + '</' + kind + '>\n')
    reqFile.write('    </deviceID>\n')
    reqFile.write('    </api:' + request + '>\n')
    reqFile.write('  </soapenv:Body>\n')
    reqFile.write('</soapenv:Envelope>\n')
    reqFile.close()

    resultCode = ''
    resultMsg = ''
    cmd = ['/usr/bin/curl',
           '--user-agent', 'Apache-HttpClient/4.1.1 (java 1.5)',
           '--header', 'Accept-Encoding: gzip,deflate',
           '--header', 'Content-Type: text/xml;charset=UTF-8',
           '--header', 'SOAPAction: ' + state,
           '--header', 'apiKey: 99030b10-2193-11e4-ad58-0d8ed2604563',
           '--data', '@' + fileName,
           'https://cc.m2mweb.net:9443/AerAdmin_WS_5_0/ws?wsdl']
    cmdout = subprocess.Popen(cmd, shell=False,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
    result = cmdout.stdout.readlines()
    # print(result)
    if 'Service Unavailable' in str(result):
        resultCode = -1
    else:
        for xmlResult in result:
            root = ET.fromstring(xmlResult)
            for elem in root.iter():
                if elem.tag == 'resultCode':
                    resultCode = elem.text
                if elem.tag == 'resultMessage':
                    resultMsg = elem.text
    try:
        os.remove(fileName)
    except:
        pass
    return resultCode, resultMsg

# Command-line arguments.
parser = argparse.ArgumentParser(description='Suspend Sprint cell lines.', add_help=False)
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='CSV file of identifiers.')
parser.add_argument('-i', '--identifier', type=str, default='',
                    required=False, help='Single identifier.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=False, help='"imsi" or "meid".')
parser.add_argument('-s', '--state', type=str, default='',
                    required=False, help='Desired line state.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --csvfile    Suspend all lines in the CSV file.")
   print("       -i, --identifier Suspend the given identifier.")
   print("       -k, --kind       Type of identifier (imsi or meid).")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

state = 'suspend'
if args.state:
    state = args.state
state = state.lower()
if state != 'suspend' and state != 'cancel':
    print("Lines state must be suspend or cancel.")
    sys.exit(1)

if not args.kind:
    print("Identifier kind (imsi or meid) is required.")
    sys.exit(1)

if len(args.identifier) == 0 and len(args.csvfile) == 0:
    print("Either a CSV file or an identifier must be specified.")
    sys.exit(1)

if len(args.identifier) > 0:
    resultCode, resultMsg = suspendOneLine(args.identifier, args.kind, state)
    print args.identifier, resultCode, resultMsg
if len(args.csvfile) > 0:
    try:
        csvfile = open(args.csvfile, 'rb')
    except IOError:
        print "Error: \"" + args.csvfile + "\" could not be opened."
        sys.exit(1)

    csvreader = csv.reader(csvfile, quotechar="\"")
    for row in csvreader:
        identifier = row[0].strip()
        if len(identifier) == 0:
            continue
        resultCode, resultMsg = suspendOneLine(identifier, args.kind, state)
        print(identifier, resultCode, resultMsg)
        if resultCode == -1:
            print("Terminating script due to carrier error.")
            break
    csvfile.close()

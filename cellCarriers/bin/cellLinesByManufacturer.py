#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys

programRunning = True

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def searchSnet(db, dbFilter):
    response = {}
    response['rowFound'] = False
    response['bureau_id'] = ''
    response['panel_type'] = ''
    response['deleted'] = False
    response['deleted_date'] = 0

    cursor = db.cursor()
    query = ('SELECT bureau_id, panel_type '
             'FROM snet '
             'WHERE  account_id_new={account} and partner="{partner}"'
             . format(account=dbFilter['account_id_new'], partner=dbFilter['partner']))
    # print(query)
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        # print("Found by %s %s") % (columnName, columnValue)
        response['bureau_id'] = row[0]
        response['panel_type'] = row[1]
        response['rowFound'] = True

    if not response['rowFound']:
        query = ('SELECT bureau_id, panel_type, deleted_date '
                 'FROM snet_changes '
                 'WHERE account_id_new={account} and partner="{partner}"'
                . format(account=dbFilter['account_id_new'], partner=dbFilter['partner']))
        cursor.execute(query)
        rows = cursor.fetchall()
        for row in rows:
            response['bureau_id'] = row[0]
            response['panel_type'] = row[1]
            response['deleted_date'] = row[2]
            response['deleted'] = True
            response['rowFound'] = True
    cursor.close()

    return response

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Match cell lines to Snet accounts', add_help=False)
parser.add_argument('-a', '--alllines', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -a, --alllines     Include all lines regardless of activation status.")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

# Start the output file.
dateString = time.strftime("%B %d, %Y", time.gmtime())
baseFileName = 'cellLinesByManufacturer-' + time.strftime("%Y%m%d%H%M%S", time.gmtime())
xlsxFileName = baseFileName + '.xlsx'
xlsxFile = open(xlsxFileName, 'w')
xlsxRow = 0

colNames = [ 'Account Status', 'Account ID', 'Partner', 'Carrier', 'ICCID', 'MEID',
             'IP Address', 'State', 'Panel Type', 'Manufacturer' ]

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook(xlsxFileName)
worksheet = workbook.add_worksheet()
xlsxCol = 0
for colName in colNames:
   worksheet.write(xlsxRow, xlsxCol, colName)
   xlsxCol += 1
xlsxRow += 1
worksheet.set_column(0, 0, 15)
worksheet.set_column(1, 1, 10)
worksheet.set_column(2, 2, 10)
worksheet.set_column(3, 3, 10)
worksheet.set_column(4, 4, 25)
worksheet.set_column(5, 5, 25)
worksheet.set_column(6, 6, 25)
worksheet.set_column(7, 7, 15)
worksheet.set_column(7, 8, 15)
worksheet.set_column(8, 9, 18)

totalLines = 0
activatedCount = 0
linesWithAccounts = 0
preactiveCount = 0
deactivatedCount = 0
unknownState = 0
linesWithUnknownManuf = 0
linesWithIccids = 0
linesWithMeids = 0
count = 0

# Connect to the Tools database.
usageDb = None
try:
    usageDb = mysql.connector.connect(**usageDbCfg)
except:
   print("Error connecting to the database.")
   sys.exit(1)

query = ('select carrier, iccid, imsi, msisdn, imei, mdn, meid, min, ipaddress, '
         'datausage, last_used, activation_date, state, manufacturer, account_id_new, '
         'partner from cellusage order by iccid, meid')
cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    carrier = cellRow[0].lower()
    iccid = cellRow[1]
    imsi = cellRow[2]
    msisdn = cellRow[3]
    imei = cellRow[4]
    mdn = cellRow[5]
    meid = cellRow[6]
    dmin = cellRow[7]
    ipaddress = cellRow[8]
    usage = cellRow[9]
    last_used = cellRow[10]
    activation_date = cellRow[11]
    state = cellRow[12].lower()
    manufacturer = str(cellRow[13])
    account_id_new = cellRow[14]
    partner = cellRow[15]

    lastUsed = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(last_used))

    if account_id_new:
        linesWithAccounts += 1
    if len(iccid) > 0:
        linesWithIccids += 1
    if len(meid) > 0:
        linesWithMeids += 1
    if len(manufacturer) == 0 or manufacturer == 'None':
        linesWithUnknownManuf += 1

    snetResult = {}
    snetResult['rowFound'] = False
    snetResult['bureau_id'] = ''
    snetResult['panel_type'] = ''
    snetResult['deleted'] = False
    snetResult['deleted_date'] = 0
    if account_id_new:
        dbFilter = {}
        dbFilter['account_id_new'] = account_id_new
        dbFilter['partner'] = partner
        snetResult = searchSnet(usageDb, dbFilter)

    accountStatus = ''
    if snetResult['rowFound']:
        if snetResult['deleted']:
            accountStatus = 'deleted'
        else:
            accountStatus = 'active'
    else:
        accountStatus = 'no account'

    activationStatus = ''
    if state == 'activated' or state == 'bill' or state == 'active' or \
      state == 'active' or state == 'active.live':
        activatedCount += 1
        activationStatus = 'activated'
    elif state == 'activation_ready' or state == 'active.ready' or state == 'active.sleep' or \
      state == 'active.test' or state == 'pre-active' or state == 'test ready' or \
      state == 'test_ready':
        preactiveCount += 1
        activationStatus = 'pre-activated'
    elif state == 'active.suspend' or state == 'cancelled' or state == 'deactivated' or \
      state == 'deactive' or state == 'inactive' or state == 'onhold' or \
      state == 'suspend' or state == 'syspended' or state == 'terminated':
        deactivatedCount += 1
        activationStatus = 'deactivated'
    else:
        unknownState += 1

    if args.alllines or activationStatus == 'activated':
        worksheet.write(xlsxRow,  0, accountStatus)
        worksheet.write(xlsxRow,  1, account_id_new)
        worksheet.write(xlsxRow,  2, partner)
        worksheet.write(xlsxRow,  3, carrier)
        worksheet.write(xlsxRow,  4, iccid)
        worksheet.write(xlsxRow,  5, meid)
        worksheet.write(xlsxRow,  6, ipaddress)
        worksheet.write(xlsxRow,  7, activationStatus)
        worksheet.write(xlsxRow,  8, snetResult['panel_type'])
        worksheet.write(xlsxRow,  9, manufacturer)
        xlsxRow += 1

    totalLines += 1
    count += 1
    if count % 100 == 0:
        print("%d          \r") % (count),
        sys.stdout.flush()

    if not programRunning:
        break

workbook.close()
cellCursor.close()
usageDb.close()

print("")
print(('Total lines .....................: %d') % (totalLines))
print(('Activated lines .................: %d') % (activatedCount))
print(('Pre-active lines ................: %d') % (preactiveCount))
print(('Deactivated lines ...............: %d') % (deactivatedCount))
print(('Lines with unknown state ........: %d') % (unknownState))
print(('Lines with ICCIDs ...............: %d') % (linesWithIccids))
print(('Lines with MEIDs ................: %d') % (linesWithMeids))
print(('Lines with accounts .............: %d') % (linesWithAccounts))
print(('Lines with unknown manufacturer .: %d') % (linesWithUnknownManuf))

#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}
usageDb = None

def searchSnetDbColumn(prevResponse, table, columnName, dbFilter, filterElement):
    curtime = int(time.time())
    response = prevResponse

    columnValue = dbFilter[filterElement]
    if not columnValue:
        return response

    # Search the snet table.
    query = ('SELECT id, account_id_new, partner, bureau_id, panel_type, vendor_id, vendor_id_2, '
             'proxy_id, imei, iccid, phone_number')
    if table == 'snet_changes':
        query += (', deleted_date')
    query += ((' FROM {table} WHERE {column}="{value}"') .
              format(table=table, column=columnName, value=columnValue))
    # if table == 'snet_changes':
    #     query += (' ORDER BY deleted_date')

    cursor = usageDb.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        response['rowFound'] = True
        response['num_responses'] += 1

        respRow = {}
        respRow['table'] = table
        respRow['id'] = row[0]
        respRow['account_id_new'] = row[1]
        respRow['partner'] = row[2]
        respRow['bureau_id'] = row[3]
        respRow['panel_type'] = row[4]
        respRow['vendor_id'] = row[5]
        respRow['vendor_id_2'] = row[6]
        respRow['proxy_id'] = row[7]
        respRow['imei'] = row[8]
        respRow['iccid'] = row[9]
        respRow['phone_number'] = row[10]
        respRow['deleted'] = False
        respRow['deleted_date'] = 0

        deleted_date_str = ''
        if table == 'snet_changes':
            respRow['deleted_date'] = row[11] / 1000
            if row[11] > 0:
                respRow['deleted'] = True
                deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(row[11] / 1000))

        respRow['text_msg1'] = (
                ('Found {filterElement} "{value}" in {column} column in {table}.\n'
                 '  {acct} {partner} {deldate} {vend1} {vend2} {proxid} {imei} {iccid} {phone}\n'
                 '  {query}') .
                 format(filterElement=filterElement, value=columnValue, column=columnName,
                        table=table, count=response['num_responses'], acct=row[1],
                        partner=row[2], deldate=deleted_date_str, vend1=row[5], vend2=row[6],
                        proxid=row[7], imei=row[8], iccid=row[9], phone=row[10], query=query)
                )
        response['rows'].append(respRow)

    cursor.close()

    return response

def searchSnet(prevResponse, table, dbFilter):
    response = prevResponse

    # Search for iccid in snet.iccid, snet.phone_number
    if 'iccid' in dbFilter:
        if dbFilter['iccid']:
            response = searchSnetDbColumn(response, table, 'iccid', dbFilter, 'iccid')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'iccid')

    # Search for imei in snet.imei, snet.phone_number
    if 'imei' in dbFilter:
        if dbFilter['imei']:
            response = searchSnetDbColumn(response, table, 'imei', dbFilter, 'imei')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'imei')

    # Search for meid in snet.vendor_id
    if 'meid' in dbFilter:
        if dbFilter['meid']:
            vendor_id = dbFilter['meid']
            if dbFilter['meid'][0:2] == 'A1':
                vendor_id = dbFilter['meid'][2:]
            dbFilter['meid'] = vendor_id
            response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'meid')

    # Search for ipaddress in snet.proxy_id
    if 'ipaddress' in dbFilter:
        if dbFilter['ipaddress']:
            response = searchSnetDbColumn(response, table, 'proxy_id', dbFilter, 'ipaddress')
            # if response['rowFound']:
            #     response['found_by_ipaddress'] = True

    # Search for imsi in snet.iccid, snet.phone_number, snet.vendor_id
    if 'imsi' in dbFilter:
        if dbFilter['imsi']:
            response = searchSnetDbColumn(response, table, 'iccid', dbFilter, 'imsi')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'imsi')
            response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'imsi')

            # For Numerex lines, try to match the IMSI to the vendor_id.
            if 'carrier' in dbFilter:
                if dbFilter['carrier'] == 'numerex':
                    imsiSubStr = dbFilter['imsi'][3:]
                    dbFilter['imsi'] = imsiSubStr
                    response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'imsi')

    # Search for msisdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'msisdn' in dbFilter:
        if dbFilter['msisdn']:
            response = searchSnetDbColumn(response, table, 'iccid', dbFilter, 'msisdn')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'msisdn')
            response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'msisdn')

    # Search for mdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'mdn' in dbFilter:
        if dbFilter['mdn']:
            response = searchSnetDbColumn(response, table, 'iccid', dbFilter, 'mdn')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'mdn')
            response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'mdn')

    # Search for dmin in snet.iccid, snet.phone_number, snet.vendor_id
    if 'dmin' in dbFilter:
        if dbFilter['dmin']:
            response = searchSnetDbColumn(response, table, 'iccid', dbFilter, 'dmin')
            response = searchSnetDbColumn(response, table, 'phone_number', dbFilter, 'dmin')
            response = searchSnetDbColumn(response, table, 'vendor_id', dbFilter, 'dmin')

    # For Vodafone lines, we need to try to match an equivalent IP address and vendor_id
    # to the Snet db record. For example Vodafone's 10.20.x.x subnet is mapped to P1's
    # 10.246.x.x subnet, and CMS's 10.248.0.0/16 subnet.
    if 'carrier' in dbFilter:
        if dbFilter['carrier'] == 'vodafone':
            if dbFilter['ipaddress'][0:6] == '10.20.':
                tmpIpAddress = '10.246.' + dbFilter['ipaddress'][6:]
                dbFilter['ipaddress'] = tmpIpAddress
                response = searchSnetDbColumn(response, table, 'proxy_id', dbFilter, 'ipaddress')
            if dbFilter['ipaddress'][0:6] == '10.20.':
                tmpIpAddress = '10.248.' + dbFilter['ipaddress'][6:]
                dbFilter['ipaddress'] = tmpIpAddress
                response = searchSnetDbColumn(response, table, 'proxy_id', dbFilter, 'ipaddress')

    # print(response)

    return response

def search(dbFilter):
    response = {}
    response['rowFound'] = False
    response['num_responses'] = 0
    response['rows'] = []

    global usageDb

    # Connect to the Tools database.
    if not usageDb:
        try:
            usageDb = mysql.connector.connect(**usageDbCfg)
        except:
            print("Error connecting to the database.")
            sys.exit(1)

    response = searchSnet(response, 'snet', dbFilter)
    response = searchSnet(response, 'snet_changes', dbFilter)

    return response

def init():
    return True

def close():
    global usageDb
    if usageDb:
        usageDb.close()

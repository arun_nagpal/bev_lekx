#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`
cd ${scriptDir}
scriptDir=`pwd`

cellCarrierDir="/home/bev_lekx/cellCarriers"

echo "AT&T"
nohup ${cellCarrierDir}/ATT/bin/usageFromApi.py -t 25 \
      > ${cellCarrierDir}/ATT/work/attUsageFromApi.out 2>&1 &

echo "Numerex"
nohup ${cellCarrierDir}/Numerex/bin/usageFromExport.py \
                -f ${cellCarrierDir}/Numerex/exports/DeviceListFileOf1.csv \
                > ${cellCarrierDir}/Numerex/work/numerexUsageFromExport.out 2>&1 &
echo "Sprint"
nohup ${cellCarrierDir}/Sprint/bin/checkReportEmail.py \
      > ${cellCarrierDir}/Sprint/work/sprintCheckReportEmail.out 2>&1 &

echo "Telstra"

echo "Verizon"
nohup ${cellCarrierDir}/Verizon/bin/usageFromApi.py \
      > ${cellCarrierDir}/Verizon/work/verizonUsageFromApi.out 2>&1 &
nohup ${cellCarrierDir}/Verizon/bin/listFromApi.py \
      > ${cellCarrierDir}/Verizon/work/verizonListFromApi.out 2>&1 &

echo "Vodafone"
nohup ${cellCarrierDir}/Vodafone/bin/devicesFromExport.py \
      -f ${cellCarrierDir}/Vodafone/exports/vodafoneDevices.csv \
      > ${cellCarrierDir}/Vodafone/work/vodafoneDevices.out 2>&1 &
nohup ${cellCarrierDir}/Vodafone/bin/usageFromExport.py \
      -m ${cellCarrierDir}/Vodafone/exports/vodafoneUsage.csv \
      -y ${cellCarrierDir}/Vodafone/exports/vodafoneUsageYearly.csv \
      > ${cellCarrierDir}/Vodafone/work/vodafoneUsage.out 2>&1 &

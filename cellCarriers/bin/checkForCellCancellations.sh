#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cd ${scriptDir}

cellDir="${HOME}/cellCarriers"

${cellDir}/bin/getAllDb.sh
${cellDir}/bin/snetDbImportAll.sh

# AT&T
${cellDir}/ATT/bin/deactivate.sh

# Sprint
${cellDir}/Sprint/bin/deactivate.sh

# Numerex
${cellDir}/Numerex/bin/deactivate.sh

# Telstra

# Verizon
${cellDir}/Verizon/bin/deactivate.sh

# Vodafone
${cellDir}/Vodafone/bin/deactivate.sh

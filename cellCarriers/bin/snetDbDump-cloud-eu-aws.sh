#!/bin/bash

dbHost="master_database"
dbUser="sn_www"
dbPassword="5n_www"
dbDatabase="securenet_6"

query="SELECT ag.account_id_new, ag.vendor_id, ag.vendor_id_2, ag.proxy_id, apc.json, get_name_value(\"phone_number\", ag.parameters) as phone_number, get_name_value(\"panel_type\", ag.parameters) as panel_type FROM (account_gateway ag) LEFT JOIN account_panel_configuration apc ON apc.id = 24 AND apc.account_id_new = ag.account_id_new"

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cloud-eu-aws.csv

query="SELECT ag.account_id_new, ag.vendor_id, ag.vendor_id_2, ag.proxy_id, apc.json as json, get_name_value(\"phone_number\", ag.parameters) as phone_number, get_name_value(\"panel_type\", ag.parameters) as panel_type, ag.date as deldate FROM (account_gateway_change ag) LEFT JOIN account_panel_configuration apc ON apc.id = 24 AND apc.account_id_new = ag.account_id_new WHERE ag.type = \"delete\""

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cloud-eu-aws-deleted.csv

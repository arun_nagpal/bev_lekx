#!/bin/bash

cellDir="/home/bev_lekx/cellCarriers"
snetDir="${cellDir}/SecureNet"

for partner in alder cloud-au cloud cloud-eu-azure p1 pa cms
do
    ${cellDir}/bin/snetDbImport.py -c -f ${snetDir}/snetDbDump-${partner}.csv -p ${partner}
done

for partner in alder cloud-au cloud cloud-eu-azure p1 pa
do
    if [ -f "${snetDir}/snetDbDump-${partner}-deleted.csv" ]
    then
	${cellDir}/bin/snetDbImport_deleted.py -c -f ${snetDir}/snetDbDump-${partner}-deleted.csv -p ${partner}
    fi
done

for partner in alder cloud-au cloud cloud-eu-azure pa
do
    ${cellDir}/bin/snetDbImport-bureau.py -c -f ${snetDir}/snetDbDump-${partner}-bureau.csv -p ${partner}
done

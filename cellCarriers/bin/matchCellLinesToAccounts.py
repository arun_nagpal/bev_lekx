#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys
import searchSnet

programRunning = True
verboseOutput = False

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Match cell lines to Snet accounts', add_help=False)
parser.add_argument('-c', '--carrier', type=str, default=None, required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -c, --carrier      Select cell lines for this carrier only.")
    print("       -h, -?, --help     Display this helpful message.")
    sys.exit(0)

# Create the panel type to manufacturer dictionary.
manufacturers = {}
manufacturers['caddx'] = 'Alula'
manufacturers['concord'] = 'Alula'
manufacturers['d3'] = 'D3'
manufacturers['dsc'] = 'Alula'
manufacturers['gocontrol'] = 'Nortek'
manufacturers['gocontrol3'] = 'Nortek'
manufacturers['guru'] = 'Alula'
manufacturers['napco'] = 'Alula'
manufacturers['napco_gem_p80x'] = 'Alula'
manufacturers['neo'] = 'JCI'
manufacturers['ness'] = 'Ness'
manufacturers['paradox'] = 'Alula'
manufacturers['rdc'] = 'RDC'
manufacturers['rdc_bosch_2000_3000'] = 'RDC'
manufacturers['rdc_bosch_6000'] = 'RDC'
manufacturers['rdc_dsc'] = 'RDC'
manufacturers['rdc_dsc_neo'] = 'RDC'
manufacturers['rdc_dsc_power_series'] = 'RDC'
manufacturers['rdc_hills_reliance_nx4'] = 'RDC'
manufacturers['rdc_ids_805'] = 'RDC'
manufacturers['rdc_ids_x64'] = 'RDC'
manufacturers['rdc_paradox_evo192'] = 'RDC'
manufacturers['rdc_paradox_mg5050'] = 'RDC'
manufacturers['rdc_texecom_832'] = 'RDC'
manufacturers['rely'] = 'Nortek'
manufacturers['resolution'] = 'Alula'
manufacturers['simon'] = 'Alula'
manufacturers['simon xt'] = 'Alula'
manufacturers['texecom'] = 'Alula'
manufacturers['vista'] = 'Alula'
manufacturers['vista 15'] = 'Alula'
manufacturers['vista 20'] = 'Alula'
manufacturers['wsa'] = 'JCI'

rowCounter = 0

# Connect to the Tools database.
usageDb = None
try:
    usageDb = mysql.connector.connect(**usageDbCfg)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Scan all cell lines and attempt to match them to snet accounts.
rowCounter = 0
query = ('select carrier, iccid, imsi, msisdn, imei, mdn, meid, min, ipaddress, '
         'datausage, last_used, activation_date, state, id from cellusage')
if args.carrier:
    query = (query + ' WHERE carrier = "{carrier}"' . format(carrier=args.carrier))
# print(query)
# sys.exit(0)

searchSnet.init()

cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    rowCounter += 1
    carrier = cellRow[0]
    iccid = cellRow[1]
    imsi = cellRow[2]
    msisdn = cellRow[3]
    imei = cellRow[4]
    mdn = cellRow[5]
    meid = cellRow[6]
    dmin = cellRow[7]
    ipaddress = cellRow[8]
    usage = cellRow[9]
    last_used = cellRow[10]
    activation_date = cellRow[11]
    state = cellRow[11]
    cellusage_id = cellRow[13]

    # if iccid != '89148000005787490895':
    #     continue

    dbFilter = {}
    dbFilter['carrier'] = carrier
    dbFilter['iccid'] = iccid
    dbFilter['imsi'] = imsi
    dbFilter['msisdn'] = msisdn
    dbFilter['imei'] = imei
    dbFilter['mdn'] = mdn
    dbFilter['meid'] = meid
    dbFilter['dmin'] = dmin
    dbFilter['ipaddress'] = ipaddress

    snetResult = searchSnet.search(dbFilter)

    lastused_str = ''
    activation_date_str = ''
    deleted_date_str = ''
    msg = 'Not found'

    printLine = False
    if snetResult['num_responses'] > 1:
        # printLine = False
        printLine = True
    
        continue

    if snetResult['rowFound']:
        msg = 'Found'
    if last_used:
        lastused_str = time.strftime('%Y-%m-%d', time.localtime(last_used))
    if activation_date:
        activation_date_str = time.strftime('%Y-%m-%d', time.localtime(activation_date))

    print(('%d: "%s" %s %s %s %s %s %s %s %d') %
          (rowCounter, msg, carrier, iccid, imsi, imei, meid, ipaddress, state,
           snetResult['num_responses']))

    for resultRow in snetResult['rows']:
        if resultRow['deleted']:
            # Handle the case where a deleted account has the same IP address as a cell line
            # that was activated after the account was deleted. I.e.: the IP address was re-used
            # after deactivating the line.
            # if activation_date > snetResult['deleted_date']:
            #     snetResult['deleted_date'] = 0
            #     deleted_date_str = ''
            #     msg = 'Not found'
            #     printLine = True
            # else:
            #     deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(snetResult['deleted_date']))
            #     msg = 'Deleted'
            deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(resultRow['deleted_date']))
            msg = 'Deleted'
            # if activation_date > resultRow['deleted_date'] and resultRow['found_by_ipaddress']:
            #     printLine = True

        # printLine = False
        # if 'Found ipaddress' in resultRow['text_msg2'] and 'snet_changes' in resultRow['text_msg1']:
        #     printLine = True

        # Print detailed info.
        if printLine:
            print(("  Row id ..........: %s") % (str(resultRow['id'])))
            print(("  Activation date .: %s") % (activation_date_str))
            print(("  Last used date ..: %s") % (lastused_str))
            print(("  Acct deleted date: %s") % (deleted_date_str))
            print(("  Account .........: %s (%s)") % (str(resultRow['account_id_new']), resultRow['partner']))
            print(("  Usage ...........: %0.3f") % (usage))
            if len(resultRow['text_msg1']) > 0:
                print(("  %s") % (resultRow['text_msg1']))
            print("")

    if not programRunning:
        break

searchSnet.close()
cellCursor.close()
usageDb.close()

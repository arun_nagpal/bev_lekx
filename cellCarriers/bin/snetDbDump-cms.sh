#!/bin/bash

# CMS
dbHost="database"
dbUser="sn_www"
dbPassword="5n_www"
dbDatabase="securenet_6_mas_prod"

query="SELECT ag.account_id, ag.vendor_id, ag.vendor_id_2, ag.proxy_id, apc.json as json, get_name_value(\"phone_number\", ag.parameters) as phone_number, get_name_value(\"panel_type\", ag.parameters) as panel_type FROM (account_gateway ag) LEFT JOIN account_panel_configuration apc ON apc.id = 24 AND apc.account_id = ag.account_id"

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cms.csv

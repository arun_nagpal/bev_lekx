#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import calendar
import signal
import os
import xlsxwriter

programRunning = True
dataArray = []

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

# Cloud database parameters.
cloudDbCfg = {
   'user': 'sn_notifier',
   'password': '5n_n0t1f13r',
   'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
   'port': 3306,
   'database': 'securenet_6',
   'raise_on_warnings': False,
}

def searchSnetDb(snetDb, account):
    result = {}
    result['systemId'] = ''
    result['bureauId'] = ''
    result['dealerName'] = ''
    result['accountType'] = ''
    result['accountName'] = ''
    result['firmware'] = ''

    query = ('select account.system_id, '
                    'account.bureau_id, '
                    'bureau.company_name, '
                    'account.account_type, '
                    'account.name, '
                    'get_name_value("fw_vendor", parameters) as fwv, '
                    'get_name_value("firmware", parameters) as fw '
                    'FROM (account_gateway, bureau) '
                    'LEFT JOIN account on account_gateway.account_id_new = account.id_new '
                    'WHERE account.id_new={account} '
                    'AND account.system_id = bureau.system_id '
                    'AND account.bureau_id = bureau.id' . format(account=account))
    cursor = snetDb.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        result['systemId'] = row[0]
        result['bureauId'] = row[1]
        result['dealerName'] = row[2]
        result['accountType'] = row[3]
        result['accountName'] = row[4]
        if row[5] != None:
            if len(row[5].strip()) > 2:
                result['firmware'] = row[5]
            else:
                result['firmware'] = row[6]
    cursor.close()

    return result

def searchUsageSnetDb(usageDb, account):
    result = {}
    result['panel_type'] = ''

    query = ('select panel_type '
             'FROM snet '
             'WHERE account_id_new={account} ' . format(account=account))
    cursor = usageDb.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        result['panel_type'] = row[0]
    cursor.close()

    return result

def sortKey(elem):
   return elem[1]

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

snetDb = mysql.connector.connect(**cloudDbCfg)
usageDb = mysql.connector.connect(**usageDbCfg)

# Get the epoch time of the beginning of the month.
# a = (datetime.datetime.now()).strftime("%Y-%m-01T00:00:01")
today = datetime.date.today()
a = (today - datetime.timedelta(31)).strftime("%Y-%m-%dT%H:%M:%S")
oldest_used_date = int(calendar.timegm(time.strptime(a, '%Y-%m-%dT%H:%M:%S')))

first = today.replace(day=1)
lastMonth = first - datetime.timedelta(days=1)
eighteenth = lastMonth.replace(day=18)

e = int(calendar.timegm(time.strptime(eighteenth, '%Y-%m-%dT%H:%M:%S')))

attDiff = datetime.date.today() - eighteenth
# othersDiff =

query = ('select account_id_new, datausage, carrier, iccid, meid, imei, imsi '
         'from cellusage where partner="cloud" '
         'and last_used >= {used_threshold} '. format(used_threshold=oldest_used_date))

counter = 0
cursor = usageDb.cursor()
cursor.execute(query)
rows = cursor.fetchall()
for row in rows:
    counter += 1
    account_id_new = row[0]
    datausage = float(row[1])
    carrier = row[2]
    iccid = row[3]
    meid = row[4]
    imei = row[5]
    imsi = row[6]

    # snetRow = searchSnetDb(snetDb, account_id_new)
    # usageSnetRow = searchUsageSnetDb(usageDb, account_id_new)

    dataArray.append((account_id_new, datausage, carrier))

    if not programRunning:
        break

cursor.close()

# colNames = [ 'Account', 'SystemID', 'Bureau ID', 'Dealer Name', 'Account Name', 'Firmware',
#              'Carrier', 'Panel Type', 'MTD Usage' ]

# # Start the output file.
# dateString = time.strftime("%B %d, %Y", time.gmtime())
# baseFileName = '/home/bev_lekx/cellUsageByAccount-' + time.strftime("%Y%m%d%H%M%S", time.gmtime())
# # baseFileName = '/home/bev_lekx/cellUsageByAccount'
# xlsxFileName = baseFileName + '.xlsx'
# xlsxFile = open(xlsxFileName, 'w')
# xlsxRow = 0

# # Create a workbook and add a worksheet.
# workbook = xlsxwriter.Workbook(xlsxFileName)
# worksheet = workbook.add_worksheet()
# xlsxCol = 0
# for colName in colNames:
#    worksheet.write(xlsxRow, xlsxCol, colName)
#    xlsxCol += 1
# xlsxRow += 1
# worksheet.set_column(0, 0, 9)
# worksheet.set_column(1, 1, 9)
# worksheet.set_column(2, 2, 9)
# worksheet.set_column(3, 3, 25)
# worksheet.set_column(4, 4, 25)
# worksheet.set_column(5, 5, 16)
# worksheet.set_column(6, 6, 12)
# worksheet.set_column(7, 7, 12)
# worksheet.set_column(8, 8, 12)

counter = 0
dataArray.sort(key=sortKey, reverse=True)
for elem in dataArray:
    # Calculate days since the beginning of the month.

    print(elem[0], elem[1], elem[2])
    # snetRow = searchSnetDb(snetDb, account_id_new)
    # usageSnetRow = searchUsageSnetDb(usageDb, account_id_new)

    # worksheet.write(xlsxRow, 0, elem[0])
    # worksheet.write(xlsxRow, 1, elem[1])
    # worksheet.write(xlsxRow, 2, elem[2])
    # worksheet.write(xlsxRow, 3, elem[3])
    # worksheet.write(xlsxRow, 4, elem[4])
    # worksheet.write(xlsxRow, 5, elem[5])
    # worksheet.write(xlsxRow, 6, elem[6])
    # worksheet.write(xlsxRow, 7, elem[7])
    # worksheet.write(xlsxRow, 8, "{:0.2f}".format(elem[8]))
    # xlsxRow += 1
    counter += 1
    if counter >= 10:
        break

# # Close the workbook.
# workbook.close()
usageDb.close()
snetDb.close()

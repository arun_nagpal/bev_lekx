#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import signal
import mysql.connector

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import snet db dump.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='snet dump file in csv format.')
parser.add_argument('-p', '--partner', type=str, default='none',
                    required=True, help='partner (cloud, pa, p1, cms, alder).')
parser.add_argument('-c', '--cleardb', action="store_true",
                    help='Clear DB records first.')
args = parser.parse_args()

# Validate the partner name.
if (args.partner != 'cloud' and
    args.partner != 'alder' and
    args.partner != 'cms' and
    args.partner != 'pa' and
    args.partner != 'p1' and
    args.partner != 'cloud-au' and
    args.partner != 'cloud-eu-aws' and
    args.partner != 'cloud-eu-azure'):
    print "Invalid partner name"
    sys.exit(1)

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Create a cursor.
cursor = cnx.cursor()

# Optionally, clear out all records for the partner.
if args.cleardb:
    print("%s: clearing existing db records.") % (args.partner)
    cursor.execute('DELETE from snet_bureau WHERE partner = "' + args.partner + '"')

# 0 - bureau_id,
# 1 - address_line_1,
# 2 - suburb,
# 3 - state,
# 4 - post_code,
# 5 - country,
# 6 - contact_name_1,
# 7 - contact_name_2,
# 8 - email_address

counter = 0
try:
    csvfile = open(args.csvfile, 'rb')
except IOError:
    print("%s: error: \"%s\" could not be opened.") % (args.partner, args.csvfile)
    sys.exit(1)

csvreader = csv.reader(csvfile, delimiter='\t')
for row in csvreader:
    counter += 1
    bureau_id = row[0].strip()
    if 'bureau_id' in bureau_id:
        continue
    company_name = row[1].strip()
    address_line_1 = row[2].strip()
    address_line_2 = row[3].strip()
    suburb = row[4].strip()
    city = row[5].strip()
    state = row[6].strip()
    post_code = row[7].strip()
    country = row[8].strip()
    contact_name_1 = row[9].strip()
    contact_name_2 = row[10].strip()
    email_address = row[11].strip()

    # Determine if there is an existing record in the db.
    rowFound = False
    query = ('SELECT bureau_id FROM snet_bureau WHERE bureau_id="' + bureau_id + '"' +
             ' AND partner="' + args.partner + '"')
    # print query
    cursor.execute(query)
    qryRow = cursor.fetchone()
    while qryRow is not None:
        rowFound = True
        qryRow = cursor.fetchone()

    # Update or insert.
    if rowFound:
        query = ('UPDATE snet_bureau SET' +
                 ' partner="' + args.partner + '"'
                 ', company_name="' + company_name + '"'
                 ', address_line_1="' + address_line_1 + '"'
                 ', address_line_2="' + address_line_2 + '"'
                 ', suburb="' + suburb + '"' +
                 ', city="' + city + '"' +
                 ', state="' + state + '"' +
                 ', post_code="' + post_code + '"'
                 ', country="' + country + '"'
                 ', contact_name_1="' + contact_name_1 + '"'
                 ', contact_name_2="' + contact_name_2e + '"'
                 ', email_address=' + email_address +
                 ' WHERE bureau_id="' + bureau_id + '"' +
                 ' AND partner="' + args.partner + '"')
    else:
        query = ('INSERT INTO snet_bureau (bureau_id, partner, company_name, address_line_1, '
                 'address_line_2, suburb, city, state, post_code, country, contact_name_1, '
                 'contact_name_2, email_address) '
                 'VALUES ('
                 '"' + bureau_id + '"' +
                 ', "' + args.partner + '"' +
                 ', "' + company_name + '"' +
                 ', "' + address_line_1 + '"' +
                 ', "' + address_line_2 + '"' +
                 ', "' + suburb + '"' +
                 ', "' + city + '"' +
                 ', "' + state + '"' +
                 ', "' + post_code + '"' +
                 ', "' + country + '"' +
                 ', "' + contact_name_1 + '"' +
                 ', "' + contact_name_2 + '"' +
                 ', "' + email_address + '"' +
                 ')')
    # print query
    cursor.execute(query)
    cnx.commit()

    if not programRunning:
        break

cnx.commit()
cnx.close()

#!/usr/bin/python

import csv
import argparse
import sqlite3
import sys
import os
import mysql.connector

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(description="Deactivate cell lines")
parser.add_argument('--version', '-v', action='version', version='1.0')
parser.add_argument('--linesfile', '-f', type=str,
                    required=True, help='CSV file containing lines.')
args = parser.parse_args()

cnx = mysql.connector.connect(**config)
cursor = cnx.cursor()

# Delete any existing output files.
cursor.execute('SELECT distinct(carrier) from cellusage')
qryRows = cursor.fetchall()
for qryRow in qryRows:
    carrier = str(qryRow[0])
    print("%s") % (carrier)
    try:
        os.remove(carrier + '.csv')
    except:
        pass
try:
    os.remove('notfound.csv')
except:
    pass

with open(args.linesfile, 'r') as csvfile:
    csvreader = csv.reader(csvfile, quotechar="\"")
    for row in csvreader:
        try:
            deviceID = row[0]
            deviceID = deviceID.upper()
        except:
            print("Error parsing \"%s\"") % (row)
            pass

        # MAC addresses are 12 characters in length. Prepend "A1" to assemble
        # a MEID.
        deviceIdAdj = deviceID
        if len(deviceID) == 12:
            deviceIdAdj = 'A1' + deviceID

        carrier = ''
        rowFound = False
        query = ('SELECT carrier from cellusage where iccid="' + deviceIdAdj + '" '
                 'or imsi="' + deviceIdAdj + '" or msisdn="' + deviceIdAdj + '" '
                 'or imei="' + deviceIdAdj + '" or mdn="' + deviceIdAdj + '" '
                 'or meid="' + deviceIdAdj + '" or min="' + deviceIdAdj + '" '
                 'or ipaddress="' + deviceIdAdj + '"')
        # print(query)
        cursor.execute(query)
        qryRows = cursor.fetchall()
        for qryRow in qryRows:
            carrier = str(qryRow[0])
            rowFound = True
        print("%s %s %s") % (deviceIdAdj, str(rowFound), carrier)

        if not rowFound:
            carrier = 'notfound'

        delFile = open(carrier + '.csv', 'a+')
        delFile.write(deviceIdAdj + '\n')
        delFile.close()

cursor.close()
cnx.close()

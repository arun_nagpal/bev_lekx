#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter

programRunning = True

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

stateActiveList    = [ "ACTIVATED", "ACTIVE", "Active.live", "Bill" ]
statePreactiveList = [ "Activation Ready", "ACTIVATION_READY", "Active.Ready", "Active.Sleep",
                       "Active.Test", "ONHOLD", "Pre-active", "Test Ready", "TEST_READY" ]
stateDeactiveList  = [ "Active.Suspend", "Cancelled", "DEACTIVATED", "deactive", "Inactive",
                       "Inactive.Stopped", "suspend", "Suspended", "Terminated" ]

colNames = [ 'Carrier', 'ICCID', 'IMSI', 'MSISDN', 'IMEI', 'MDN', 'MEID', 'MIN',
             'IP Address', 'Deleted' ]

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

foundCounter = 0
notFoundCounter = 0
deletedCounter = 0
totalActiveLines = 0

# Start the output file.
dateString = time.strftime("%B %d, %Y", time.gmtime())
baseFileName = '/home/bev_lekx/CellLinesWithUsageNoAccount-' + time.strftime("%Y%m%d%H%M%S", time.gmtime())
xlsxFileName = baseFileName + '.xlsx'
xlsxFile = open(xlsxFileName, 'w')
xlsxRow = 0

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook(xlsxFileName)
worksheet = workbook.add_worksheet()
xlsxCol = 0
for colName in colNames:
   worksheet.write(xlsxRow, xlsxCol, colName)
   xlsxCol += 1
xlsxRow += 1
worksheet.set_column(0, 0, 9)
worksheet.set_column(1, 1, 23)
worksheet.set_column(2, 2, 20)
worksheet.set_column(3, 3, 18)
worksheet.set_column(4, 4, 20)
worksheet.set_column(5, 5, 16)
worksheet.set_column(6, 6, 18)
worksheet.set_column(7, 7, 12)
worksheet.set_column(8, 8, 18)
worksheet.set_column(9, 9, 10)

usageDb = mysql.connector.connect(**usageDbCfg)

# First, determine how many activated lines there are regardless of usage.
query = ('select state from cellusage')
cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    if cellRow[0] in stateActiveList:
        totalActiveLines += 1
cellCursor.close()

# Now we make a list of active lines with no SecureNet account.
query = ('select carrier, iccid, imsi, imei, meid, ipaddress, datausage, account_id_new, '
         'state, partner from cellusage ')

cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    carrier = cellRow[0]
    iccid = cellRow[1]
    imsi = cellRow[2]
    imei = cellRow[3]
    meid = cellRow[4]
    ipaddress = cellRow[5]
    datausage = cellRow[6]
    account_id_new = cellRow[7]
    state = cellRow[8]
    partner = cellRow[9]

    if state not in stateActiveList:
        continue

    deleted = False
    deleted_date = 0

    query = ('SELECT deleted_date '
             'FROM snet WHERE account_id_new={account_id_new}' .
              format(account_id_new=account_id_new))
    cursor = usageDb.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        deleted_date = row[0]
        if deleted_date > 0:
            deleted = True
    cursor.close()

    deletedMsg = ''
    if deleted:
        deletedCounter += 1
        try:
            deletedMsg = time.strftime('%Y-%m-%d', time.localtime(deleted_date/1000))
        except:
            deletedMsg = 'Unknown'
            pass

    if len(str(account_id_new)) < 6 or deleted:
        worksheet.write(xlsxRow, 0, carrier)
        worksheet.write(xlsxRow, 1, iccid)
        worksheet.write(xlsxRow, 2, imsi)
        worksheet.write(xlsxRow, 4, imei)
        worksheet.write(xlsxRow, 6, meid)
        worksheet.write(xlsxRow, 8, ipaddress)
        worksheet.write(xlsxRow, 9, deletedMsg)
        xlsxRow += 1

    if not programRunning:
        break

# Close the workbook.
workbook.close()

cellCursor.close()
usageDb.close()

msg = (('Total activated lines: %d\n'
        + 'Lines with activity .: %d\n'
        + 'With accounts .......: %d\n'
        + 'No accounts .........: %d\n'
        + 'With deleted accounts: %d\n'
        + 'Spreadsheet is in ...: %s\n') %
       (totalActiveLines, foundCounter + notFoundCounter, foundCounter,
        notFoundCounter, deletedCounter, xlsxFileName)
      )
print(msg)

# if os.path.isfile(xlsxFileName):
#     os.system('/usr/bin/sendemail '
#               + '-f no-reply@securenettech.com '
#               + '-t arun.nagpal@securenettech.com '
#               + '-o tls=no '
#               + '-u "Cell lines without accounts for ' + dateString + '" '
#               + '-m "' + msg + '" '
#               + '-a ' + xlsxFileName)
# #    os.system('rm -f ' + xlsxFileName)

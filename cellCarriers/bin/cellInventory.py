#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys

programRunning = True

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

stateActiveList    = [ "activated", "active", "active.live", "bill" ]
statePreactiveList = [ "activation ready", "activation_ready", "active.ready", "active.sleep",
                       "active.test", "onhold", "pre-active", "test ready", "test_ready" ]
stateDeactiveList  = [ "active.suspend", "cancelled", "deactivated", "deactive", "inactive",
                       "inactive.stopped", "suspend", "suspended", "terminated" ]

colNames = [ 'Carrier', 'ICCID', 'MEID', 'IMEI', 'IMSI', 'IP Address', 'State', 'Partner',
             'Manufacturer', 'Account ID', 'Last Used', 'Activation Date', 'Bureau ID',
             'Bureau Name', 'Bureau City', 'Bureau Country', 'Deleted Account' ]

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='', add_help=False)
parser.add_argument('-c', '--carrier', type=str, default='',
                    required=False, help='Carrier.')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -c, --carrier      Specific carrier (default = all).")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

allCounter = 0
foundCounter = 0
notFoundCounter = 0
deletedCounter = 0
totalActiveLines = 0
noAccountCounter = 0
cellStateActiveCounter = 0
cellStatePreactiveCounter = 0
cellStateDeactiveCounter = 0
cellStateUnknownCounter = 0

# Start the output file.
# dateString = time.strftime("%B %d, %Y", time.gmtime())
# baseFileName = '/home/bev_lekx/CellLinesWithUsageNoAccount-' + time.strftime("%Y%m%d%H%M%S", time.gmtime())
baseFileName = './cellInventory'
xlsxFileName = baseFileName + '.xlsx'
xlsxFile = open(xlsxFileName, 'w')
xlsxRow = 0

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook(xlsxFileName)
worksheet = workbook.add_worksheet()
xlsxCol = 0
for colName in colNames:
   worksheet.write(xlsxRow, xlsxCol, colName)
   xlsxCol += 1
xlsxRow += 1
worksheet.set_column(0, 0, 9)
worksheet.set_column(1, 1, 23)
worksheet.set_column(2, 2, 20)
worksheet.set_column(3, 3, 18)
worksheet.set_column(4, 4, 20)
worksheet.set_column(5, 5, 16)
worksheet.set_column(6, 6, 18)
worksheet.set_column(7, 7, 12)
worksheet.set_column(8, 8, 18)
worksheet.set_column(9, 9, 10)
worksheet.set_column(9, 10, 10)
worksheet.set_column(9, 11, 10)
worksheet.set_column(9, 12, 10)
worksheet.set_column(9, 13, 10)
worksheet.set_column(9, 14, 10)
worksheet.set_column(9, 15, 10)
worksheet.set_column(9, 16, 10)

usageDb = mysql.connector.connect(**usageDbCfg)

columnList = ('carrier, iccid, imsi, imei, meid, ipaddress, datausage, last_used, '
              'manufacturer, partner, account_id_new, activation_date, state')
if args.carrier:
    query = ('select {columnList} from cellusage where carrier="{carrier}"' .
              format(columnList=columnList, carrier=args.carrier))
else:
    query = ('select {columnList} from cellusage order by carrier' .
              format(columnList=columnList))

cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    allCounter += 1

    carrier = cellRow[0]
    iccid = cellRow[1]
    imsi = cellRow[2]
    imei = cellRow[3]
    meid = cellRow[4]
    ipaddress = cellRow[5]
    datausage = float(cellRow[6])
    lastused = cellRow[7]
    manufacturer = cellRow[8]
    partner = cellRow[9]
    accountidnew = cellRow[10]
    activationdate = cellRow[11]
    state = cellRow[12].lower()

    cellState = ""
    if state in stateActiveList:
        cellState = "active"
    elif state in statePreactiveList:
        cellState = "pre-active"
    elif state in stateDeactiveList:
        cellState = "deactive"
    else:
        cellState = "unknown"

    if cellState != 'active':
        continue

    deleted = False
    deleted_date = 0
    bureauid = 0
    bureauName = ""
    bureauSuburb = ""
    bureauCountry = ""
    deletedMsg = ''
    if cellState == 'active':
        if accountidnew != None and accountidnew > 0:
            acctFound = False
            query = ('SELECT bureau_id, deleted_date '
                     'FROM snet WHERE account_id_new={account_id_new} and partner="{partner}"' .
                     format(account_id_new=accountidnew, partner=partner))
            try:
                cursor = usageDb.cursor()
                cursor.execute(query)
                rows = cursor.fetchall()
                for row in rows:
                    bureauid = row[0]
                    deleted_date = row[1]
                    if deleted_date > 0:
                        deleted = True
                        deletedMsg = time.strftime('%Y-%m-%d', time.localtime(deleted_date/1000))
                    acctFound = True
                cursor.close()
            except:
                print(query)
                raise

            if not acctFound:
                query = ('SELECT deleted_date '
                         'FROM snet_changes WHERE account_id_new={account_id_new} '
                         'and partner="{partner}"' .
                         format(account_id_new=accountidnew, partner=partner))
                try:
                    cursor = usageDb.cursor()
                    cursor.execute(query)
                    rows = cursor.fetchall()
                    for row in rows:
                        bureauid = 0
                        deleted_date = row[0]
                        if deleted_date > 0:
                            deleted = True
                            deletedMsg = time.strftime('%Y-%m-%d', time.localtime(deleted_date/1000))
                        acctFound = True
                    cursor.close()
                except:
                    print(query)
                    raise

            if bureauid != 0:
                query = ('SELECT company_name, suburb, country '
                         'FROM snet_bureau WHERE bureau_id={bureau_id}' .
                         format(bureau_id=bureauid))
                try:
                    cursor = usageDb.cursor()
                    cursor.execute(query)
                    rows = cursor.fetchall()
                    for row in rows:
                        bureauName = row[0]
                        bureauSuburb = row[1]
                        bureauCountry = row[2]
                    cursor.close()
                except:
                    print(query)
                    raise
        else:
            noAccountCounter += 1
        if deleted:
            deletedCounter += 1

    lastused_str = ''
    activation_date_str = ''
    if lastused:
        lastused_str = time.strftime('%Y-%m-%d', time.localtime(int(lastused)))
    if activationdate:
        activation_date_str = time.strftime('%Y-%m-%d', time.localtime(activationdate))

    worksheet.write(xlsxRow, 0, carrier)
    worksheet.write(xlsxRow, 1, iccid)
    worksheet.write(xlsxRow, 2, meid)
    worksheet.write(xlsxRow, 3, imei)
    worksheet.write(xlsxRow, 4, imsi)
    worksheet.write(xlsxRow, 5, ipaddress)
    worksheet.write(xlsxRow, 6, cellState)
    worksheet.write(xlsxRow, 7, partner)
    worksheet.write(xlsxRow, 8, manufacturer)
    worksheet.write(xlsxRow, 9, str(accountidnew))
    worksheet.write(xlsxRow, 10, lastused_str)
    worksheet.write(xlsxRow, 11, activation_date_str)
    worksheet.write(xlsxRow, 12, str(bureauid))
    worksheet.write(xlsxRow, 13, bureauName)
    worksheet.write(xlsxRow, 14, bureauSuburb)
    worksheet.write(xlsxRow, 15, bureauCountry)
    worksheet.write(xlsxRow, 16, deletedMsg)
    xlsxRow += 1

    if allCounter % 500 == 0:
        print("%d          \r") % (allCounter),
        sys.stdout.flush()

    if not programRunning:
        break

workbook.close()
cellCursor.close()
usageDb.close()

print("Deleted accounts .......: %d" % deletedCounter)
print("Lines without an account: %d" % noAccountCounter)

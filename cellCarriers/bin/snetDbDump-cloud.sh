#!/bin/bash

# Cloud
dbHost="db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com"
dbUser="sn_www"
dbPassword="5n_www"
dbDatabase="securenet_6"

query="SELECT ag.account_id_new,
              ag.vendor_id,
              ag.vendor_id_2,
              ag.proxy_id,
              apc.json,
              get_name_value(\"phone_number\", ag.parameters) as phone_number,
              get_name_value(\"panel_type\", ag.parameters) as panel_type,
              a.bureau_id,
              a.system_id
FROM account_gateway ag
LEFT JOIN account_panel_configuration apc ON apc.id = 24
  AND apc.account_id_new = ag.account_id_new
LEFT JOIN account a on ag.account_id_new = a.id_new
GROUP by ag.account_id_new"

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cloud.csv

query="SELECT ag.account_id_new,
              ag.vendor_id,
              ag.vendor_id_2,
              ag.proxy_id,
              apc.json as json,
              get_name_value(\"phone_number\", ag.parameters) as phone_number,
              get_name_value(\"panel_type\", ag.parameters) as panel_type,
              ag.date as deldate,
              a.bureau_id,
              a.system_id
FROM account_gateway_change ag
LEFT JOIN account_panel_configuration apc ON apc.id = 24
  AND apc.account_id_new = ag.account_id_new
LEFT JOIN account a on ag.account_id_new = a.id_new
WHERE ag.type = \"delete\"
GROUP by ag.account_id_new"

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cloud-deleted.csv

query="SELECT b.id as bureau_id,
              b.company_name,
              b.address_line_1,
              b.address_line_2,
              b.suburb,
              b.city,
              b.state,
              b.post_code,
              c.name as country,
              b.contact_name_1,
              b.contact_name_2,
              b.email_address
       FROM bureau b, country c
       WHERE b.country_id = c.id
       GROUP BY b.id"

mysql -h ${dbHost} -D ${dbDatabase} --user=${dbUser} --password=${dbPassword} -e "${query}" > snetDbDump-cloud-bureau.csv

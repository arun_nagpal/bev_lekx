#!/usr/bin/python

# This script is used to identify cell lines that can be suspended or deactivated.
#
# The following command-line arguments determine the selection criteria:
# 1. activation threshold for those lines with a SecureNet account.
# 2. last-used threshold for those lines with a (deleted) SecureNet account.
# 3. deleted threshold for those lines with a deleted SecureNet account.
# 4. activation threshold for those lines without a SecureNet account.
# 5. lastused threshold for those lines without a SecureNet account.
#
# For each of these arguments, a zero value matches all lines.
#
# The deactivation selection criteria are as follows:
# 1. Ignore any line that is not in an active state. Lines that have been deactivated or
#    have not yet been activated are ignored.
# 2. Determine if a cell line has an associated SecureNet account. The selection critera
#    varies depending on this result.
#    - If a line has a SecureNet account:
#       a. Determine if the SecureNet account has been deleted. If the account has been
#          deleted, and the deletion date is outside of the deletion threshold, deactivate
#          the line.
#       b. Determine if the activation date is outside of the activation threshold.
#       c. Determine the last date the line has carried traffic. Determine if this date is
#          outside of the "last date threshold".
#       d. If the account is deleted and the deleted threshold has been exceeded or the account
#          is deleted and both the last used threshold and activation thresholds have been
#          exceeded, the line can be deactivated.
#    - If a line does not have a SecureNet account:
#       a. Determine if the activation date is outside of the activation threshold (currently
#          this criteria is ignored).
#       b. Determine the last date the line has carried traffic. Determine if this date is
#          outside of the "last date threshold".
#       c. If the activation threshold has been exceeded and the last used threshold has
#          been exceeded, deactive the line.
#
# There are a couple of exceptions:
# 1. For Numerex, a line is deactivated without regard to the delete threshold. As soon as
#    an account is deleted with a Numerex line, the line is deactivated.
# 2. For all carriers except Vodafone, we cannot make a usage determination if there is no
#    last used date. So the line is ignored. In the case of Vodafone we can assume the line
#    has not been used in at least one year if there is no last used date.

# Selection part of the script.
#    if haveAnSnetAccount:
#        if deleted_date > 0:
#            deleted = True
#            # Account deletion threshold is 0 days for Numerex. All other are 90 days.
#            if deleted_date < deleted_threshold or deleted_threshold == 0:
#                deleted_threshold_exceeded = True
#
#            if activation_threshold == 0 or activated_date < activation_threshold:
#                activation_threshold_exceeded = True
#
#            if lastused_threshold == 0 or last_used < lastused_threshold:
#                lastused_threshold_exceeded = True
#
#            if deleted_threshold_exceeded and
#               lastused_threshold_exceeded and
#               activation_threshold_exceeded:
#                deactivate = True
#    else:
#        if (activation_threshold_not_found == 0 or
#                activated_date < activation_threshold_not_found):
#            activation_threshold_exceeded = True
#        if lastused_threshold_not_found == 0 or last_used < lastused_threshold_not_found:
#            lastused_threshold_exceeded = True
#        if activation_threshold_exceeded and lastused_threshold_exceeded:
#            deactivate = True

import csv
import requests
import json
import base64
import sqlite3
import mysql.connector
import argparse
import sys
import os
import datetime
import time
import signal
import searchSnet

programRunning = True

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

stateActiveList    = [ "activated", "active", "active.live", "bill" ]
statePreactiveList = [ "activation ready", "activation_ready", "active.ready", "active.sleep",
                       "active.test", "onhold", "pre-active", "test ready", "test_ready" ]
stateDeactiveList  = [ "active.suspend", "cancelled", "deactivated", "deactive", "inactive",
                       "inactive.stopped", "suspend", "suspended", "terminated" ]

def checkIgnoreFile(ignoreFileName, iccid, imsi, msisdn, imei, mdn, meid, dmin, ipaddress):
   ignore = False

   try:
      csvfile = open(ignoreFileName, 'rb')
   except IOError:
      print "Error: \"" + ignoreFileName + "\" could not be opened."
      sys.exit(1)

   csvreader = csv.reader(csvfile, quotechar="\"")
   for row in csvreader:
      identifier = row[0].strip()
      if identifier == iccid or identifier == imsi or identifier == imei or identifier == meid or identifier == msisdn or identifier == mdn or identifier == dmin or identifier == ipaddress:
         ignore = True

   csvfile.close()
   return ignore

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

epoch_current = int(time.time())
curtime = int(time.time())

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Find cell lines that can be deactivated.')
parser.add_argument('-c', '--carrier', type=str, default=None,
                    required=False, help='Carrier.')
parser.add_argument('-d', '--deactive', action="store_true",
                    help='Print only lines suitable for deactivation.')
parser.add_argument('-a', '--activate', type=int, default=0, required=False,
                    help='Activated threshold in days.')
parser.add_argument('-l', '--lastused', type=int, default=0, required=False,
                    help='Last used threshold in days.')
parser.add_argument('-y', '--activatenotfound', type=int, default=0, required=False,
                    help='Activated threshold for lines without accounts in days.')
parser.add_argument('-z', '--lastusednotfound', type=int, default=0, required=False,
                    help='Last used threshold for lines without accounts in days.')
parser.add_argument('-t', '--deleted', type=int, default=0, required=False,
                    help='Account deleted threshold in days.')
parser.add_argument('-o', '--output', type=str, default=None,
                    required=False, help='Print only this field in the output.')
parser.add_argument('-i', '--ignore', type=str, default=None,
                    required=False, help='File of identifiers to ignore.')
parser.add_argument('-v', '--verbose', action="store_true",
                    help='Verbose output.')
args = parser.parse_args()

counter = 0
# Activation date threshold for lines with a SecureNet account.
activation_threshold = epoch_current - (365 * 86400)
if args.activate != 0:
    activation_threshold = epoch_current - (args.activate * 86400)

# Last-used date threshold for lines with a SecureNet account.
lastused_threshold = epoch_current - (365 * 86400)
if args.lastused != 0:
    lastused_threshold = epoch_current - (args.lastused * 86400)

# Deleted account date threshold.
deleted_threshold = epoch_current - (90 * 86400)
if args.deleted != 0:
    deleted_threshold = epoch_current - (args.deleted * 86400)

# Activation date threshold for lines with no SecureNet account.
activation_threshold_not_found = epoch_current - (365 * 86400)
if args.activatenotfound != 0:
    activation_threshold_not_found = epoch_current - (args.activatenotfound * 86400)

# Last-used date threshold for lines with no SecureNet account.
lastused_threshold_not_found = epoch_current - (90 * 86400)
if args.lastusednotfound != 0:
    lastused_threshold_not_found = epoch_current - (args.lastusednotfound * 86400)

if args.verbose:
    print(("activation_threshold .........: %s") % (
        time.strftime('%Y-%m-%d', time.localtime(activation_threshold))))
    print(("lastused_threshold ...........: %s") % (
        time.strftime('%Y-%m-%d', time.localtime(lastused_threshold))))
    print(("deleted_threshold ............: %s") % (
        time.strftime('%Y-%m-%d', time.localtime(deleted_threshold))))
    print(("activation_threshold_not_found: %s") % (
        time.strftime('%Y-%m-%d', time.localtime(activation_threshold_not_found))))
    print(("lastused_threshold_not_found .: %s") % (
        time.strftime('%Y-%m-%d', time.localtime(lastused_threshold_not_found))))

usageDb = mysql.connector.connect(**usageDbCfg)
columnList = ('id, iccid, imsi, msisdn, imei, mdn, meid, min, ipaddress, carrier, '
              'state, activation_date, last_used, datausage')
query = ('SELECT {columnList} FROM cellusage' . format(columnList=columnList))
if args.carrier:
    query = (query + ' WHERE carrier = "{carrier}"' . format(carrier=args.carrier))

cellCursor = usageDb.cursor()
cellCursor.execute(query)
cellRows = cellCursor.fetchall()
for cellRow in cellRows:
    usage_row_id = cellRow[0]
    iccid = cellRow[1]
    imsi = cellRow[2]
    msisdn = cellRow[3]
    imei = cellRow[4]
    mdn = cellRow[5]
    meid = cellRow[6]
    dmin = cellRow[7]
    ipaddress = cellRow[8]
    carrier = cellRow[9]
    state = ''
    if cellRow[10] == None:
        state = ''
    else:
        state = cellRow[10].lower()
    activated_date = cellRow[11]
    last_used = cellRow[12]
    usage = cellRow[13]

    # Ignore any lines that are not active.
    if state in stateActiveList:
        state = 'Active'
    else:
        state = 'Not active'
    if state != 'Active':
        continue

    deleted = False
    deleted_date = 0
    rowFound = False
    deactivate = False
    msg = ''
    deactivate_msg = 'no'
    deleted_threshold_msg = 'no'
    activation_threshold_msg = 'no'
    lastused_threshold_msg = 'no'
    lastused_str = ''
    activation_date_str = ''
    deleted_date_str = ''
    activation_threshold_exceeded = False
    lastused_threshold_exceeded = False
    deleted_threshold_exceeded = False

    snetResult = {}
    dbFilter = {}
    dbFilter['carrier'] = carrier
    dbFilter['iccid'] = iccid
    dbFilter['imsi'] = imsi
    dbFilter['msisdn'] = msisdn
    dbFilter['imei'] = imei
    dbFilter['mdn'] = mdn
    dbFilter['meid'] = meid
    dbFilter['dmin'] = dmin
    dbFilter['ipaddress'] = ipaddress

    snetResult = searchSnet.search(dbFilter)

    if snetResult['num_responses'] > 1:
        continue
    resultRow = {}
    if snetResult['num_responses'] == 1:
        resultRow = snetResult['rows'][0]

    account_id_new = 0
    partner = ''
    if 'account_id_new' in resultRow:
        account_id_new = resultRow['account_id_new']
    if 'partner' in resultRow:
        partner = resultRow['partner']

    if last_used > 0:
        lastused_str = time.strftime('%Y-%m-%d', time.localtime(int(last_used)))
    if activated_date > 0:
        activation_date_str = time.strftime('%Y-%m-%d', time.localtime(activated_date))

    # If a line has been found in the snet or snet_changes tables, we can use activity
    # and account deletion dates to determine if the line can be deactivated.
    if snetResult['rowFound']:
        msg = 'Found'

        deleted_date = resultRow['deleted_date']
        deleted = resultRow['deleted']
        if deleted and deleted_date > 0:
            deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(int(deleted_date)))
        # searchSnet['bureau_id'] = ''
        # searchSnet['panel_type'] = ''

        # If the account associated with a line has been deleted, and the deletion date
        # is outside of the deletion threshold, deactivate the line.
        if deleted:
            msg = ('Deleted ({deldate})' . format(deldate=deleted_date_str))
            # Account deletion threshold is 0 days for Numerex. All other are 90 days.
            if deleted_date < deleted_threshold or deleted_threshold == 0 or carrier == 'numerex':
                deleted_threshold_exceeded = True

            # Do not deactivate the line if the activation date is within the threshold.
            if activation_threshold == 0 or activated_date < activation_threshold:
                activation_threshold_exceeded = True

            # If last_used == 0, there was no activity for the cell line during whatever
            # reporting period was present in the usage report. This means the line was
            # not present in the report. We can't make a judgement about line activity
            # without this. For Vodafone we know this means there was no activity for at
            # least 1 year. For the other carriers we need to bypass this line by setting the
            # last_used date to now.
            if last_used == 0 and carrier != 'vodafone':
                last_used = epoch_current
            if lastused_threshold == 0 or last_used < lastused_threshold:
                lastused_threshold_exceeded = True

            if deleted_threshold_exceeded and deleted:
                deactivate = True
            if (lastused_threshold_exceeded and activation_threshold_exceeded) and deleted:
                deactivate = True

    # If the line was not found in the snet or snet_changes tables then we use the
    # last-used date and activation date to determine if it can be deactivated.
    else:
        msg = 'Not found'
        if (activation_threshold_not_found == 0 or
                activated_date < activation_threshold_not_found):
            activation_threshold_exceeded = True
        if lastused_threshold_not_found == 0 or last_used < lastused_threshold_not_found:
            lastused_threshold_exceeded = True
        if activation_threshold_exceeded and lastused_threshold_exceeded:
            deactivate = True

    if deactivate:
        deactivate_msg = 'yes'
    if deleted_threshold_exceeded:
        deleted_threshold_msg = 'yes'
    if activation_threshold_exceeded:
        activation_threshold_msg = 'yes'
    if lastused_threshold_exceeded:
        lastused_threshold_msg = 'yes'

    # Check the ignore file.
    ignore = False
    if args.ignore:
        ignore = checkIgnoreFile(args.ignore, iccid, imsi, msisdn, imei, mdn, meid,
                                 dmin, ipaddress)
        if ignore:
            msg = 'Ignore'
            deactivate = False

    # Print the selected output value.
    if not args.verbose and args.deactive and deactivate and args.output and not ignore:
        if args.output == 'imsi':
            print(imsi)
        elif args.output == 'imei':
            print(imei)
        elif args.output == 'iccid':
            print(iccid)
        elif args.output == 'meid':
            print(meid)
        elif args.output == 'mdn':
            print(mdn)
        else:
            print("Unknown field.")
            break

    # Print verbose output.
    printLine = False
    # if deactivate and msg == 'Found':
    if deactivate:
        printLine = True

    # if (printLine and args.verbose) and (not args.deactive or (args.deactive and deactivate)):
    if printLine and args.verbose:
        print(('%d: "%s" %s %s %s %s %s %s %s %s') %
              (counter, msg, carrier, iccid, imsi, meid, imei, mdn, ipaddress, state))
        print(("  Activation date ....: %s") % (activation_date_str))
        print(("  Last used date .....: %s") % (lastused_str))
        print(("  Acct deleted date ..: %s") % (deleted_date_str))
        print(("  Account ............: %s (%s)") % (str(account_id_new), partner))
        print(("  Usage ..............: %0.3f") % (usage))
        print(("  Activation threshold: %s") % (activation_threshold_msg))
        print(("  Last used threshold : %s") % (lastused_threshold_msg))
        print(("  Deleted threshold ..: %s") % (deleted_threshold_msg))
        print(("  Deactivate this line: %s") % (deactivate_msg))
        if 'text_msg1' in resultRow:
            print(("%s") % (resultRow['text_msg1']))
        print("")
        counter += 1

    if not programRunning:
        break

cellCursor.close()
usageDb.close()

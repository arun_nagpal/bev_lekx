#!/usr/bin/python

import csv
import requests
import json
import base64
import mysql.connector
import datetime
import time
import argparse
import sys
import os.path

usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def getKey(item):
   return item[2]

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Calculate data usage and list top users.')
parser.add_argument('-i', '--identifier', type=str, default="",
                    required=False, help='Select an identifier to filter.')
parser.add_argument('-t', '--topusers', type=int, default=0,
                    required=False, help='List top users.')
args = parser.parse_args()

# Connect to the usage database.
usageDb = mysql.connector.connect(**usageDbCfg)
usageCursor = usageDb.cursor()

# Determine the number of unique identifiers in the usage table. This will be the
# same as the number of activated lines.
numActiveDevices = 0
usageCursor.execute("SELECT count(distinct(identifier)) FROM cellusage")
numActiveDevices = usageCursor.fetchone()[0]

# Calculate total data usage.
totalDataUsage = 0
usageCursor.execute("select sum(datausage) from usage")
totalDataUsage = usageCursor.fetchone()[0]

# Top users or use for an identifier.
limit = 0
if args.identifier:
   limit = 1
   usageCursor.execute("SELECT identifier, datausage FROM usage WHERE identifier=?", (args.identifier,))
else:
   if args.topusers > 0:
      limit = args.topusers
   usageCursor.execute("SELECT identifier, datausage FROM usage order by datausage desc limit ?", (limit,))
selectResult = usageCursor.fetchall()
for row in selectResult:
   print("%s %.3f") % (row[0], row[1])

# Calculate total data usage for each rate plan.
print("Rate plans:")
usageCursor.execute("SELECT distinct(rateplan) FROM usage")
ratePlans = usageCursor.fetchall()
for ratePlan in ratePlans:
   usageCursor.execute("select sum(datausage) from usage where rateplan=?", (ratePlan[0],))
   ratePlanUsage = usageCursor.fetchone()[0]
   if ratePlanUsage == None:
      ratePlanUsage = 0.0
   usageCursor.execute("select count(distinct(identifier)) from usage where rateplan=?", (ratePlan[0],))
   ratePlanCount = usageCursor.fetchone()[0]
   if ratePlan[0] != None:
      print("   %s %d %.3f") % (ratePlan[0], ratePlanCount, ratePlanUsage)

# Print totals.
print("Active devices: %d") % (numActiveDevices)
print("Total mtd usage: %.3f") % (totalDataUsage)

usageDb.close()

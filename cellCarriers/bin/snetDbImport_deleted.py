#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import signal
import mysql.connector

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import snet db dump.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='snet dump file in csv format.')
parser.add_argument('-p', '--partner', type=str, default='none',
                    required=True, help='partner (cloud, pa, p1, cms, alder).')
parser.add_argument('-c', '--cleardb', action="store_true",
                    help='Clear DB records first.')
args = parser.parse_args()

# Validate the partner name.
if (args.partner != 'cloud' and
    args.partner != 'alder' and
    args.partner != 'pa' and
    args.partner != 'p1' and
    args.partner != 'cloud-au' and
    args.partner != 'cloud-eu-azure'):
    print "Invalid partner name"
    sys.exit(1)

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Create a cursor.
cursor = cnx.cursor()

# Optionally, clear out all records for the partner.
if args.cleardb:
    print("%s: clearing existing db records.") % (args.partner)
    cursor.execute('DELETE from snet_changes WHERE partner = "' + args.partner + '"')

# 0  account_id_new
# 1  vendor_id
# 2  vendor_id_2
# 3  proxy_id
# 4  json
# 5  phone_number
# 6  panel_type
# 7  deleted_date

counter = 0
try:
    csvfile = open(args.csvfile, 'rb')
except IOError:
    print("%s: error: \"%s\" could not be opened.") % (args.partner, args.csvfile)
    sys.exit(1)

csvreader = csv.reader(csvfile, delimiter='\t')
for row in csvreader:
    counter += 1

    account_id_new = row[0].strip()
    if 'account_id' in account_id_new:
        continue
    vendor_id = row[1].strip()
    vendor_id_2 = row[2].strip()

    # Some proxy_id columns include a colon and the port number.
    if ':' in row[3]:
        proxy_id = row[3][:row[3].find(':')]
    else:
        proxy_id = row[3]

    imei = ''
    iccid = ''
    phone_number = ''
    panel_type = ''
    deleted_date = 0
    try:
        jData = json.loads(row[4])
        rdo_info = jData['rdo.info']
        if 'imei' in rdo_info:
            imei = rdo_info['imei']
        if 'iccid' in rdo_info:
            iccid = rdo_info['iccid']
        if 'simNum' in rdo_info:
            iccid = rdo_info['simNum']
    except:
        pass

    phone_number = row[5].strip()
    panel_type = row[6].strip()

    try:
        deleted_date = row[7]
    except:
        print row

    bureau_id = row[8]

    # Determine if there is an existing record in the db.
    rowFound = False
    query = ('SELECT id FROM snet_changes WHERE account_id_new="' + account_id_new + '"' +
             ' AND vendor_id="' + vendor_id + '" AND partner="' + args.partner + '"' +
             ' AND vendor_id_2="' + vendor_id_2 + '" AND iccid="' + iccid + '"' +
             ' AND proxy_id="' + proxy_id + '" AND deleted_date=' + str(deleted_date))
    # print query
    qryCursor = cnx.cursor()
    qryCursor.execute(query)
    try:
        qryRow = qryCursor.fetchone()
        while qryRow is not None:
            rowFound = True
            qryRow = qryCursor.fetchone()
    except:
        pass
    qryCursor.close()

    # Update or insert.
    if rowFound:
        query = ('UPDATE snet_changes SET' +
                 ' partner="' + args.partner + '"'
                 ', vendor_id="' + vendor_id + '"'
                 ', vendor_id_2="' + vendor_id_2 + '"' +
                 ', proxy_id="' + proxy_id + '"' +
                 ', imei="' + imei + '"' +
                 ', iccid="' + iccid + '"' +
                 ', phone_number="' + phone_number + '"' +
                 ', panel_type="' + panel_type + '"' +
                 ', deleted_date=' + str(deleted_date) +
                 ', bureau_id=' + bureau_id +
                 ' WHERE account_id_new="' + account_id_new + '"' +
                 ' AND vendor_id="' + vendor_id + '"' +
                 ' AND partner="' + args.partner + '"')
    else:
        query = ('INSERT INTO snet_changes (partner, account_id_new, vendor_id, vendor_id_2, '
                 'proxy_id, imei, iccid, phone_number, panel_type, deleted_date, bureau_id) '
                 'VALUES ('
                 '"' + args.partner + '"' +
                 ', "' + account_id_new + '"' +
                 ', "' + vendor_id + '"' +
                 ', "' + vendor_id_2 + '"' +
                 ', "' + proxy_id + '"' +
                 ', "' + imei + '"' +
                 ', "' + iccid + '"' +
                 ', "' + phone_number + '"' +
                 ', "' + panel_type + '"' +
                 ', ' + str(deleted_date) +
                 ', ' + bureau_id +
                 ')')
    # if '10.10.0.83' in row:
    #     print query
    # print query
    updCursor = cnx.cursor()
    try:
        updCursor.execute(query)
    except:
        print("%s: MySQL error.") % (args.partner)
        print query
        break

    if counter % 500 == 0:
        cnx.commit()
    #     print("%s: %d") %(args.partner, counter)
    updCursor.close()

    if not programRunning:
        break

cnx.commit()
cnx.close()

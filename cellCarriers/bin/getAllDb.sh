#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellCarrierDir="${HOME}/cellCarriers"
securenetDir="${cellCarrierDir}/SecureNet"

cd ${scriptDir}

# cms - I think we are locked out of the CMS database.
for db in alder cloud cloud-au cloud-eu-azure p1 pa
do
    echo "${db}"

    if [ "${db}" == "alder" ]
    then
	dbHost="tools"
    elif [ "${db}" == "cloud" ]
    then
	dbHost="tools"
    elif [ "${db}" == "cloud-au" ]
    then
	dbHost="orange"
    elif [ "${db}" == "cloud-eu-azure" ]
    then
	dbHost="austria"
    elif [ "${db}" == "p1" ]
    then
        dbHost="server1.p1"
    elif [ "${db}" == "pa" ]
    then
	dbHost="phillies"
    fi

    if [ "${db}" == "cloud" ] || [ "${db}" == "alder" ]
    then
	./snetDbDump-${db}.sh
	mv snetDbDump-${db}.csv ${securenetDir}/.
	mv snetDbDump-${db}-deleted.csv ${securenetDir}/.
	mv snetDbDump-${db}-bureau.csv ${securenetDir}/.
    else
	scp snetDbDump-${db}.sh bev_lekx@${dbHost}:.
	ssh bev_lekx@${dbHost} "./snetDbDump-${db}.sh"
	scp bev_lekx@${dbHost}:snetDbDump-${db}.csv ${securenetDir}/.
	scp bev_lekx@${dbHost}:snetDbDump-${db}-deleted.csv ${securenetDir}/.
	scp bev_lekx@${dbHost}:snetDbDump-${db}-bureau.csv ${securenetDir}/.
	ssh bev_lekx@${dbHost} "rm snetDbDump-${db}.sh"
	ssh bev_lekx@${dbHost} "rm snetDbDump-${db}.csv"
	ssh bev_lekx@${dbHost} "rm snetDbDump-${db}-deleted.csv"
	ssh bev_lekx@${dbHost} "rm snetDbDump-${db}-bureau.csv"
    fi
done

#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys

programRunning = True
verboseOutput = False

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Export suspended Verizon lines.', add_help=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -h, -?, --help     Display this helpful message.")
    sys.exit(0)

# Connect to the Tools database.
usageDb = None
try:
    usageDb = mysql.connector.connect(**usageDbCfg)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Scan all cell lines and attempt to match them to snet accounts.
rowCounter = 0
query = ('select iccid, imsi, msisdn, imei, mdn, meid, min, ipaddress, '
         'datausage, last_used, activation_date, state from cellusage '
         'where carrier="verizon" and state="suspend"')
# print(query)
# sys.exit(0)

cellCursor = usageDb.cursor()
cellCursor.execute(query)
rows = cellCursor.fetchall()
for row in rows:
    outStr = ''
    ndx = 0
    for ndx in range(0, len(row)):
        if ndx > 0:
            outStr += ','

        outStr += str(row[ndx])
    print(outStr)

    if not programRunning:
        break

cellCursor.close()
usageDb.close()

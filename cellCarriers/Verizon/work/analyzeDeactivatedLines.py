#!/usr/bin/python

#
#

import mysql.connector
import datetime
import time
import signal
import os
import xlsxwriter
import argparse
import sys

programRunning = True
verboseOutput = False

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def searchSnetDbColumn(db, table, columnName, dbFilter, filterElement):
    curtime = int(time.time())
    response = {}
    response['rowFound'] = False
    response['account_id_new'] = ''
    response['partner'] = ''
    response['bureau_id'] = ''
    response['panel_type'] = ''
    response['vendor_id'] = ''
    response['vendor_id_2'] = ''
    response['proxy_id'] = ''
    response['imei'] = ''
    response['iccid'] = ''
    response['phone_number'] = ''
    response['deleted'] = False
    response['deleted_date'] = 0
    response['text_msg1'] = ''
    response['num_responses'] = 0
    response['table'] = ''
    response['found_by_ipaddress'] = False

    columnValue = dbFilter[filterElement]
    if not columnValue:
        return response

    # Search the snet table.
    query = ('SELECT account_id_new, partner, bureau_id, panel_type, vendor_id, vendor_id_2, '
             'proxy_id, imei, iccid, phone_number')
    if table == 'snet_changes':
        query += (', deleted_date')
    query += ((' FROM {table} WHERE {column}="{value}"') .
              format(table=table, column=columnName, value=columnValue))
    if table == 'snet_changes':
        query += (' ORDER BY deleted_date')

    cursor = db.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        response['num_responses'] += 1
        response['rowFound'] = True
        response['table'] = table
        response['account_id_new'] = row[0]
        response['partner'] = row[1]
        response['bureau_id'] = row[2]
        response['panel_type'] = row[3]
        response['vendor_id'] = row[4]
        response['vendor_id_2'] = row[5]
        response['proxy_id'] = row[6]
        response['imei'] = row[7]
        response['iccid'] = row[8]
        response['phone_number'] = row[9]
        if len(row) > 10:
            response['deleted_date'] = row[10] / 1000
            if row[10] > 0:
                response['deleted'] = True

    ndx = 0
    for row in rows:
        ndx += 1
        if ndx == 1:
            response['text_msg1'] = (
                ('Found {filterElement} "{value}" in {column} column in {table} (count={count}).') .
                 format(filterElement=filterElement, value=columnValue, column=columnName, table=table,
                        count=response['num_responses'])
                )
        deleted_date_str = ''
        if len(row) > 10:
            if row[10] > 0:
                deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(row[10] / 1000))
        response['text_msg1'] += (
            ('\n  {acct} {partner} {deldate} {vend1} {vend2} {proxid} {imei} {iccid} {phone}') .
             format(acct=row[0], partner=row[1], deldate=deleted_date_str, vend1=row[4],
                    vend2=row[5], proxid=row[6], imei=row[7], iccid=row[8], phone=row[9])
            )

    cursor.close()

    return response

def searchSnet(db, table, dbFilter):
    response = {}
    response['rowFound'] = False

    # Search for iccid in snet.iccid, snet.phone_number
    if 'iccid' in dbFilter:
        if dbFilter['iccid']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'iccid', dbFilter, 'iccid')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'iccid')

    # Search for imei in snet.imei, snet.phone_number
    if 'imei' in dbFilter:
        if dbFilter['imei']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'imei', dbFilter, 'imei')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'imei')

    # Search for meid in snet.vendor_id
    if 'meid' in dbFilter:
        if dbFilter['meid']:
            if not response['rowFound']:
                vendor_id = dbFilter['meid']
                if dbFilter['meid'][0:2] == 'A1':
                    vendor_id = dbFilter['meid'][2:]
                dbFilter['meid'] = vendor_id
                response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'meid')

    # Search for ipaddress in snet.proxy_id
    if 'ipaddress' in dbFilter:
        if dbFilter['ipaddress']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'proxy_id', dbFilter, 'ipaddress')
                if response['rowFound']:
                    response['found_by_ipaddress'] = True

    # Search for imsi in snet.iccid, snet.phone_number, snet.vendor_id
    if 'imsi' in dbFilter:
        if dbFilter['imsi']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'iccid', dbFilter, 'imsi')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'imsi')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'imsi')

            # For Numerex lines, try to match the IMSI to the vendor_id.
            if not response['rowFound']:
                if 'carrier' in dbFilter:
                    if dbFilter['carrier'] == 'numerex':
                        imsiSubStr = dbFilter['imsi'][3:]
                        dbFilter['imsi'] = imsiSubStr
                        response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'imsi')

    # Search for msisdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'msisdn' in dbFilter:
        if dbFilter['msisdn']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'iccid', dbFilter, 'msisdn')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'msisdn')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'msisdn')

    # Search for mdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'mdn' in dbFilter:
        if dbFilter['mdn']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'iccid', dbFilter, 'mdn')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'mdn')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'mdn')

    # Search for dmin in snet.iccid, snet.phone_number, snet.vendor_id
    if 'dmin' in dbFilter:
        if dbFilter['dmin']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'iccid', dbFilter, 'dmin')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'phone_number', dbFilter, 'dmin')
            if not response['rowFound']:
                response = searchSnetDbColumn(db, table, 'vendor_id', dbFilter, 'dmin')

    # For Vodafone lines, we need to try to match an equivalent IP address and vendor_id
    # to the Snet db record. For example Vodafone's 10.20.x.x subnet is mapped to P1's
    # 10.246.x.x subnet, and CMS's 10.248.0.0/16 subnet.
    if 'carrier' in dbFilter:
        if dbFilter['carrier'] == 'vodafone':
            if not response['rowFound']:
                if dbFilter['ipaddress'][0:6] == '10.20.':
                    tmpIpAddress = '10.246.' + ipaddress[6:]
                    dbFilter['ipaddress'] = tmpIpAddress
                    response = searchSnetDbColumn(db, table, 'proxy_id', dbFilter, 'ipaddress')
            if not response['rowFound']:
                if ipaddress[0:6] == '10.20.':
                    tmpIpAddress = '10.248.' + ipaddress[6:]
                    dbFilter['ipaddress'] = tmpIpAddress
                    response = searchSnetDbColumn(db, table, 'proxy_id', dbFilter, 'ipaddress')

    # print(response)

    return response

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Match cell lines to Snet accounts', add_help=False)
parser.add_argument('-c', '--carrier', type=str, default=None, required=False)
parser.add_argument('-u', '--updatedb', action='store_true')
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -v, --verbose      Print results for each line.")
    print("       -h, -?, --help     Display this helpful message.")
    sys.exit(0)

if args.verbose:
    verboseOutput = True

rowCounter = 0
countDeletedAccounts = 0
countWithAccount = 0
countIccids = 0
countMeids = 0
countWithoutAccount = 0
countIccidsWithoutAccount = 0
countMeidsWithoutAccount = 0
countSuspended = 0
countDeactivated = 0

# Connect to the Tools database.
usageDb = None
try:
    usageDb = mysql.connector.connect(**usageDbCfg)
except:
   print("Error connecting to the database.")
   sys.exit(1)

# Scan all cell lines and attempt to match them to snet accounts.
rowCounter = 0
query = ('select iccid, imsi, msisdn, imei, mdn, meid, min, ipaddress, '
         'datausage, last_used, activation_date, state, id from cellusage '
         'where carrier="verizon" and (state="deactive" or state="suspend") '
         'and activation_date >= 1514782800 and activation_date <= 1546232400')
# print(query)
# sys.exit(0)

cellCursor = usageDb.cursor()
cellCursor.execute(query)
rows = cellCursor.fetchall()
for row in rows:
    rowCounter += 1

    iccid = row[0]
    imsi = row[1]
    msisdn = row[2]
    imei = row[3]
    mdn = row[4]
    meid = row[5]
    dmin = row[6]
    ipaddress = row[7]
    usage = row[8]
    last_used = row[9]
    activation_date = row[10]
    state = row[11]
    cellusage_id = row[12]

    carrier = 'verizon'
    lastused_str = ''
    activation_date_str = ''
    deleted_date_str = ''
    msg = ''
    printEntry = False

    dbFilter = {}
    dbFilter['carrier'] = carrier
    dbFilter['iccid'] = iccid
    dbFilter['imsi'] = imsi
    dbFilter['msisdn'] = msisdn
    dbFilter['imei'] = imei
    dbFilter['mdn'] = mdn
    dbFilter['meid'] = meid
    dbFilter['dmin'] = dmin
    dbFilter['ipaddress'] = ipaddress

    withAccount = False
    withDeletedAccount = False
    snetResult = searchSnet(usageDb, 'snet', dbFilter)
    if snetResult['rowFound']:
        countWithAccount += 1
        withAccount = True
        msg = "Found"
    else:
        snetResultChanges = searchSnet(usageDb, 'snet_changes', dbFilter)
        if snetResultChanges['deleted']:
            countDeletedAccounts += 1
            withDeletedAccount = True
            msg = "Deleted"
    if not snetResult['rowFound'] and not snetResultChanges['rowFound']:
        countWithoutAccount += 1
        msg = "Not found"

    if last_used:
        lastused_str = time.strftime('%Y-%m-%d', time.localtime(last_used))
    if activation_date:
        activation_date_str = time.strftime('%Y-%m-%d', time.localtime(activation_date))
    if snetResultChanges['deleted_date']:
        deleted_date_str = time.strftime('%Y-%m-%d', time.localtime(snetResultChanges['deleted_date']))

    if len(iccid) == 20:
        countIccids += 1
    if len(meid) == 14:
        countMeids += 1

    if not snetResult['rowFound'] and not snetResultChanges['rowFound']:
        if len(iccid) == 20:
            countIccidsWithoutAccount += 1
        if len(meid) == 14:
            countMeidsWithoutAccount += 1
        printEntry = True

        if state == 'suspend':
            countSuspended += 1
        if state == 'deactive':
            countDeactivated += 1

    # Print detailed info.
    if printEntry:
        # print(('%d: "%s" %s %s %s %s %s %s %s') %
        #       (rowCounter, msg, carrier, iccid, imsi, imei, meid, ipaddress, state))
        # print(("  Activation date .: %s") % (activation_date_str))
        # print(("  Last used date ..: %s") % (lastused_str))
        # print(("  Acct deleted date: %s") % (deleted_date_str))
        # print(("  Account .........: %s (%s)") % (str(snetResult['account_id_new']), snetResult['partner']))
        # print(("  Usage ...........: %0.3f") % (usage))
        # if len(snetResult['text_msg1']) > 0:
        #     print(("  %s") % (snetResult['text_msg1']))
        # print("")

        # if len(meid) == 14 and state == 'suspend':
        #     print("%s") % (meid)
        # if len(iccid) == 20 and state == 'suspend':
        #     print("%s") % (iccid)
        # if len(meid) == 14 and state == 'deactive':
        #     print("%s") % (meid)
        if len(iccid) == 20 and state == 'deactive':
            print("%s") % (iccid)
        # print(iccid, imei, meid, mdn, lastused_str, activation_date_str, deleted_date_str)

    if not programRunning:
        break

# print("Total ...........: %d") % (rowCounter)
# print("Deleted accounts : %d") % (countDeletedAccounts)
# print("With accounts ...: %d") % (countWithAccount)
# print("Without account .: %d") % (countWithoutAccount)
# print("ICCIDs total ....: %d") % (countIccids)
# print("MEIDs total .....: %d") % (countMeids)
# print("ICCIDs no account: %d") % (countIccidsWithoutAccount)
# print("MEIDs no account : %d") % (countMeidsWithoutAccount)
# print("Suspended .......: %d") % (countSuspended)
# print("Deactivated .....: %d") % (countDeactivated)

cellCursor.close()
usageDb.close()

#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import argparse
import sys
import os
import mysql.connector
import re
import subprocess

carrier = 'verizon'

# snetDir = '/home/bev_lekx/cellCarriers/SecureNet'

control_chars = ''.join(map(unichr, range(0,32) + range(127,254)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))

def remove_control_chars(s):
   return control_char_re.sub('', s)

def scrub(s):
   r = ''
   if s != None:
      try:
         # stmp = remove_control_chars(s)
         r = str(s).encode('ascii')
      except:
         pass

   if r == 'None':
      r = ''
   return r

def searchDbCol(db, colName, identifier):
    found = False
    panel_type = ''
    partner = ''
    account_id = ''

    query = ('SELECT panel_type, partner, account_id_new FROM snet WHERE ' + colName + '="' + identifier + '"')
    usageCursor = db.cursor()
    usageCursor.execute(query)
    usageRows = usageCursor.fetchall()
    for usageRow in usageRows:
        panel_type = usageRow[0]
        partner = usageRow[1]
        account_id = str(usageRow[2])
        found = True
    usageCursor.close()

    return found, panel_type, partner, account_id

def searchDbSnet(db, iccid, imsi, msisdn, imei, ipaddress, meid):
    found = False
    panel_type = ''
    partner = ''
    account_id = ''

    if not found and len(imei) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'vendor_id', imei[3:])
    if not found and len(imei) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'imei', imei)
    if not found and len(imei) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'imei', imei[3:])
    if not found and len(iccid) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'iccid', iccid)
    if not found and len(ipaddress) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'proxy_id', ipaddress)
    if not found and len(msisdn) > 0:
        found, panel_type, partner, account_id = searchDbCol(db, 'phone_number', msisdn)

    return found, panel_type, partner, account_id

def searchDbUsage(db, identifier):
    iccid = ''
    imsi = ''
    msisdn = ''
    imei = ''
    meid = ''
    ipaddress = ''
    found = False

    if len(identifier) == 14 and identifier[0:2] == 'A1':
        query = ('SELECT iccid, imsi, msisdn, imei, meid, ipaddress '
                 'FROM cellusage WHERE meid="' + identifier + '"')
    else:
        query = ('SELECT iccid, imsi, msisdn, imei, meid, ipaddress '
                 'FROM cellusage WHERE iccid="' + identifier + '"')

    cursor = db.cursor()
    cursor.execute(query)
    dbRows = cursor.fetchall()
    for dbRow in dbRows:
        iccid = dbRow[0]
        imsi = dbRow[1]
        msisdn = dbRow[2]
        imei = dbRow[3]
        meid = dbRow[4]
        ipaddress = dbRow[5]
        found = True
    cursor.close()

    return found, iccid, imsi, msisdn, imei, meid, ipaddress

def usage():
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --connfile    Connection history file.")
   print("       -h, -?, --help    Display this helpful message.")

# Command-line arguments.
parser = argparse.ArgumentParser(description='', add_help=False)
parser.add_argument('-f', '--connfile', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   usage()
   sys.exit(0)

if not args.connfile:
   usage()
   sys.exit(0)

# Get database credentials from ~/.mylogin.cnf
dbUser = ''
dbPassword = ''
dbHost = 'localhost'
cmd = subprocess.Popen(['/usr/bin/my_print_defaults', '-s', 'tools'],
                       shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
result = cmd.stdout.readlines()
for r in result:
    rs = r.strip().split('=')
    if len(rs) == 2:
        if rs[0] == '--user':
            dbUser = rs[1]
        elif rs[0] == '--password':
            dbPassword = rs[1]
        elif rs[0] == '--host':
            dbHost = rs[1]

# Login to the database.
cnx = mysql.connector.connect(user=dbUser, password=dbPassword, host=dbHost, database='celldata')

try:
    connfile = open(args.connfile, 'rb')
except IOError:
    print "Error: \"" + args.connfile + "\" could not be opened."
    sys.exit(1)

identifier = ''
iccid = ''
imsi = ''
msisdn = ''
imei = ''
mdn = ''
meid = ''
dmin = ''
ipaddress = ''
account_id = ''
partner = ''
found = False
needStartLine = False
stopLine = ''

for line in connfile:
    lineTrimmed = line.strip()
    lineFields = lineTrimmed.split(' ')
    numFields = len(lineFields)
    if numFields > 4:
        continue
    if numFields == 1:
        if lineFields[0] == 'iccid':
            continue

    if numFields == 1:
        needStartLine = False
        identifier = lineFields[0]
        found, iccid, imsi, msisdn, imei, meid, ipaddress = searchDbUsage(cnx, identifier)
        if found:
            found, panel_type, partner, account_id = searchDbSnet(cnx, iccid, imsi, msisdn, imei, ipaddress, meid)
    else:
        if needStartLine and 'Start 0' in lineTrimmed:
            print("%s, %s, %s, %s, %s, %s") % (panel_type, identifier, partner, account_id, stopLine, lineTrimmed)
            needStartLine = False

        if 'Stop Admin_Reset' in lineTrimmed and found:
            stopLine = lineTrimmed
            needStartLine = True

connfile.close()
cnx.close()

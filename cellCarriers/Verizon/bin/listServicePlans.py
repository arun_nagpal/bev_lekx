#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import logging
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict
import signal
# import mysql.connector

# Database parameters.
# config = {
#   'user': 'bev_lekx',
#   'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
#   'host': '172.29.253.45',
#   'database': 'celldata',
#   'raise_on_warnings': False,
# }

carrier = 'verizon'
account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
programRunning = True
debugOutput = False

def listServicePlans(acctSrvc, account):
    device_info_req = acctSrvc.factory.create('ns1:GetServicePlanListRequest')
    device_info_resp = acctSrvc.factory.create('ns1:GetServicePlanListResponse')
    device_info_req.Account = account
    if debugOutput:
        print device_info_req
    try:
        device_info_resp = device.service.GetServicePlanList(device_info_req)
        # if debugOutput:
        #     print device_info_resp

        servicePlans = None
        for k, v in device_info_resp:
            if k == 'ServicePlans':
                tmpa = v
                for k, v in tmpa:
                    servicePlans = v
        if servicePlans != None:
            for elem in servicePlans:
                print elem


    except:
        haveError = True
        pass


# (GetServicePlanListResponse){
#    ServicePlans =
#       (ArrayOfServicePlan){
#          ServicePlan[] =
#             (ServicePlan){
#                Name = "SNet1MBLTEVOICE"
#                Code = "1MBLTEVOICE"
#                SizeKb = 0
#                CarrierServicePlanCode = "95081"
#                ExtendedAttributes = None
#             },

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
# month = datetime.datetime.now().strftime("%m")
# year = datetime.datetime.now().strftime("%Y")
# startDate = year + '-' + month + '-01T00:00:01Z'
# endDate = '2099-12-31T23:59:59Z'
parser = argparse.ArgumentParser(
   description='Download Verizon usage data using the API in verizon.db.')
parser.add_argument('-a', '--account', type=str, default='',
                    required=False, help='Account number.')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

# endDate = ''
# if args.startdate:
#     startDate = args.startdate
# if args.enddate:
#     endDate = args.enddate

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the account service
device_url = 'https://uws.apps.nphase.com/api/v2/AccountService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

if args.account:
    listServicePlans(device, args.account)
else:
    listServicePlans(device, '0442082380-00001')
    # listServicePlans(device, '0642085097-00001')

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict

carrier = 'verizon'
account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
programRunning = True
debugOutput = False

def getDeviceUsage(device, kind, identifier, startDate, endDate):
    haveError = False

    device_info_req = device.factory.create('ns3:GetDeviceUsageHistoryRequest')
    device_info_resp = device.factory.create('ns3:GetDeviceUsageHistoryResponse')
    device_info_req.Device.Kind = kind
    device_info_req.Device.Identifier = identifier
    device_info_req.TimestampFilter.Earliest = startDate
    device_info_req.TimestampFilter.Latest = endDate
    # device_info_req.TimestampFilter.Earliest = year + '-' + month + '-01T00:00:01Z'
    # device_info_req.TimestampFilter.Latest     = '2099-12-31T23:59:59Z'
    if debugOutput:
        print device_info_req
    try:
        device_info_resp = device.service.GetDeviceUsageHistory(device_info_req)
        if debugOutput:
            print device_info_resp
    except:
        haveError = True
        pass

    totalBytes = 0
    # lastDate = ''
    if not haveError:
        try:
            for nn in device_info_resp.UsageHistory.DeviceUsage:
                totalBytes += nn.BytesUsed
        except:
            pass

    # Return the total in MB.
    totalBytes /= (1024.0*1024.0)
    return totalBytes

# Command-line arguments.
month = datetime.datetime.now().strftime("%m")
year = datetime.datetime.now().strftime("%Y")
startDate = year + '-' + month + '-01T00:00:01Z'
endDate = '2099-12-31T23:59:59Z'
parser = argparse.ArgumentParser(
   description='Obtain Verizon usage for one device.')
parser.add_argument('-i', '--identifier', type=str, default='',
                    required=True, help='Device identifier.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=True, help='Identifier type.')
parser.add_argument('-s', '--startdate', type=str, default='',
                    required=False, help='Start date (default: ' + startDate + ').')
parser.add_argument('-e', '--enddate', type=str, default='2099-12-31T23:59:59Z',
                    required=False, help='End date (default: 2099-12-31T23:59:59Z).')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

if args.startdate:
    startDate = args.startdate
if args.enddate:
    endDate = args.enddate

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device


usage = getDeviceUsage(device, args.kind, args.identifier, startDate, endDate)
print("%s %.2f") % (args.identifier, usage)


# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

#!/usr/bin/python

# This script imports the Verizon "Aggregated device usage" report into the usage
# database.

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'verizon'

programRunning = True

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def updateDbCol(usageDb, colName, identifier, usage):
   rowFound = False
   usageCursor = usageDb.cursor()

   query = ('SELECT datausage FROM cellusage WHERE ' + colName + '="' + identifier + '"')
   # print query
   try:
      usageCursor.execute(query)
   except:
      pass

   try:
      usageRows = usageCursor.fetchall()
      # print usageRows
      for usageRow in usageRows:
         rowFound = True
   except:
      pass

   if rowFound:
      # print("Update by %s: %s") % (colName, identifier)
      query = ('UPDATE cellusage set datausage=' + "{:0.2f}".format(usage) +
               ' WHERE ' + colName + '="' + identifier + '"')
      try:
          usageCursor.execute(query)
          usageDb.commit()
      except:
          pass

   usageCursor.close()
   return rowFound

def updateDb(usageDb, mdn, imei, meid, usage):
   rowFound = False
   if mdn:
      rowFound = updateDbCol(usageDb, 'mdn', mdn, usage)
   if not rowFound and mdn:
      rowFound = updateDbCol(usageDb, 'msisdn', mdn, usage)
   if not rowFound and imei:
      rowFound = updateDbCol(usageDb, 'imei', imei, usage)
   if not rowFound and meid:
      rowFound = updateDbCol(usageDb, 'meid', meid, usage)

   if not rowFound:
       print("Added %s %s %s") % (mdn, imei, meid)
       cursor = usageDb.cursor()
       query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                'VALUES ("' + carrier + '", "", "", "", "", "' + imei + '", "' +
                mdn + '", "' + meid + '", "", "", "", ' + "{:0.2f}".format(usage) + ')')
       cursor.execute(query)
       usageDb.commit()
       cursor.close()
       rowFound = True

   return rowFound

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import Verizon aggregated usage report into a db file.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Verizon usage report in csv format.')
parser.add_argument('-c', '--cleardb', action="store_true",
                    help='Clear DB records first.')
args = parser.parse_args()

print datetime.datetime.today()

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Create a cursor.
cursor = cnx.cursor()

# Optionally, clear out all records for the carrier.
if args.cleardb:
    print("Clearing existing db records.")
    cursor.execute('DELETE from cellusage WHERE carrier = \"' + carrier + '\"')

# "Device Identifier","MDN/MSISDN","Account","Device Service","Service Plan","Usage Date","Usage(KB)","SMS Usage","Device Group","Usage Source"
# "351999090741958","13855051640","0642085097-00001",,"M2M_CPN","07/01/2018 - 07/31/2018","18.040039","0","Default: 0642085097-00001","Raw Usage"
# Device identifier is the IMEI or MEID

try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

# counter = 0

csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
    identifier = row[0].strip()
    if 'Device Identifier' in identifier:
        continue

    # counter += 1
    meid = ''
    imei = ''
    if 'A1' in identifier:
        meid = identifier
    else:
        imei = identifier

    mdn = row[1].strip()
    usage = float(row[6].replace(',',''))/1024

    foundRow = updateDb(cnx, mdn, imei, meid, usage)

    if not programRunning:
        break

cnx.commit()
cnx.close()

print datetime.datetime.today()

#!/usr/bin/python

import csv
import suds
import logging
from suds.client import Client
from suds.sax.element import Element
import argparse
import sys

account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'

def changePhoneNumber(params):
    carrierSrvc = None
    if 'carrierSrvc' in params:
        carrierSrvc = params['carrierSrvc']
    if not carrierSrvc:
        return

    carrier_req = carrierSrvc.factory.create('ns1:Change4GDeviceIdentifierRequest')
    carrier_req.Device = carrierSrvc.factory.create('ns2:DeviceIdentifier')
    carrier_req.Device.Kind = params['kind']
    carrier_req.Device.Identifier = params['identifier']

    carrier_req.Change4GOption = carrierSrvc.factory.create('ns1:Change4GOption')
    carrier_req.Change4GOption = 'ChangeMSISDN'
    print(carrier_req)

    carrier_resp = carrierSrvc.factory.create('ns1:Change4GDeviceIdentifierResponse')
    carrier_resp = carrierSrvc.service.Change4GDeviceIdentifier(carrier_req)
    # print(str(carrier_req))
    # print(carrier_resp)
    print(carrierSrvc.last_sent())
    print(carrierSrvc.last_received())


# Command-line arguments.
parser = argparse.ArgumentParser(description='Restore a device.')
parser.add_argument('-f', '--csvfile', type=str, default=None,
                    required=False, help='CSV file of identifiers to deactivate.')
parser.add_argument('-i', '--identifier', type=str, default=None,
                    required=False, help='Identifier.')
parser.add_argument('-k', '--kind', type=str, default=None,
                    required=True, help='The type of identifier (meid, mdn, iccid).')
args = parser.parse_args()

if not args.csvfile and not args.identifier:
    print("An identifier or CSV file of identifiers is required.")
    sys.exit(1)

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
# print session

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
login_resp = session.service.LogIn(login_req)

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the carrier service
device_url = 'https://uws.apps.nphase.com/api/v2/CarrierService.svc'
carrierSrvc = Client(device_url + '?wsdl')
carrierSrvc.set_options(location=device_url, soapheaders=token_element)
# print(carrierSrvc)

params = {}
params['kind'] = args.kind
params['carrierSrvc'] = carrierSrvc
if args.csvfile:
    # Iterate through the list of identifiers.
    csvfile = open(args.csvfile, 'rb')
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        identifier = row[0].strip()
        params['identifier'] = identifier
        changePhoneNumber(params)
else:
    params['identifier'] = args.identifier
    changePhoneNumber(params)

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
logout_resp = session.service.LogOut(logout_req)

#!/usr/bin/python

import csv
import suds
import logging
import argparse
import sys
from suds.client import Client
from suds.sax.element import Element
import signal

account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
debugOutput = False
programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

def getDeviceState(device, kind, identifier):
    device_info_req = device.factory.create('ns3:GetDeviceInformationRequest')
    device_info_resp = device.factory.create('ns3:GetDeviceInformationResponse')
    device_info_req.Device.Kind = kind
    device_info_req.Device.Identifier = identifier
    if debugOutput:
        print('')
        print('---  Request ---')
        print device_info_req

    haveError = False
    try:
        device_info_resp = device.service.GetDeviceInformation(device_info_req)
        if debugOutput:
            print('')
            print('---  Response ---')
            print device_info_resp
            # print device_info_req.last_sent()
            # print device.last_received()
    except:
        haveError = True
        pass

    devState = 'unknown'
    devIdentifier = identifier
    if not haveError:
       try:
           devState = device_info_resp.Device.CarrierInformation.CarrierInformation[0].State
       except:
           pass

       try:
           for k, v in device_info_resp.Device['DeviceIdentifiers']:
               if k == 'DeviceIdentifier':
                   for z in v:
                       if z['Kind'] == kind:
                           devIdentifier = z['Identifier']
       except:
           pass

    return(devIdentifier,devState)

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Check device status using Verizon API.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='Device file in CSV format.')
parser.add_argument('-c', '--column', type=int, default=0,
                    required=False, help='Identifier column number in CSV file.')
parser.add_argument('-i', '--identifier', type=str, default='',
                    required=False, help='Device identifier.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=False, help='Identifier kind (meid, imei, mdn, imsi, iccid).')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

if args.debug:
    debugOutput = True

if args.csvfile == '' and args.identifier == '':
    print("A CSV file or identifier must be specified.")
    sys.exit(1)

if args.kind == '':
    print("An identifier kind must be specified.")
    sys.exit(1)

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.resolver').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req
login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

programRunning = True
counter=0
if args.csvfile:
    # Iterate through the csv file.
    try:
        csvfile = open(args.csvfile, 'rb')
    except:
        print("Error opening \"%s\"") % (args.csvfile)
        sys.exit(1)

    csvreader = csv.reader(csvfile, quotechar="\"")
    for line in csvreader:
        identifier = line[args.column].strip()

        # Validate the identifier.
        if 'meid' in identifier.lower():
            continue
        if 'iccid' in identifier.lower():
            continue
        if 'imei' in identifier.lower():
            continue
        if 'imsi' in identifier.lower():
            continue
        if 'mdn' in identifier.lower():
            continue

        if args.kind == 'meid':
            if not identifier.startswith('A1'):
                continue
        else:
            if not identifier.isdigit():
                continue

        # Get the device state.
        counter += 1
        devState = getDeviceState(device, args.kind, identifier)
        strLine = ''
        ndx = 0
        while ndx < len(line):
            if len(strLine) > 0:
                strLine += ','
            strLine += line[ndx].strip()
            ndx += 1
        print("%s,%s,%d") % (strLine, devState[1], counter)

        if not programRunning:
            break
else:
    devState = getDeviceState(device, args.kind, args.identifier)
    print("%s,%s") % (devState[0], devState[1])

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

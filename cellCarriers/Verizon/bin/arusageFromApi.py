#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import logging
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict
import signal
import mysql.connector

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'arun',
  'raise_on_warnings': False,
}

carrier = 'verizon'
account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
programRunning = True
debugOutput = False

def getDeviceUsage(device, kind, identifier, startDate, endDate):
    haveError = False

    device_info_req = device.factory.create('ns3:GetDeviceUsageHistoryRequest')
    device_info_resp = device.factory.create('ns3:GetDeviceUsageHistoryResponse')
    device_info_req.Device.Kind = kind
    device_info_req.Device.Identifier = identifier
    device_info_req.TimestampFilter.Earliest = startDate
    device_info_req.TimestampFilter.Latest = endDate
    if debugOutput:
        print device_info_req
    try:
        device_info_resp = device.service.GetDeviceUsageHistory(device_info_req)
        if debugOutput:
            print device_info_resp
    except:
        haveError = True
        pass

    # print device_info_resp

    totalBytes = 0
    # lastDate = ''
    if not haveError:
        try:
            for nn in device_info_resp.UsageHistory.DeviceUsage:
                totalBytes += nn.BytesUsed
        except:
            pass

    # Return the total in MB.
    totalBytes /= (1024.0*1024.0)
    return totalBytes

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
# month = datetime.datetime.now().strftime("%m")
# year = datetime.datetime.now().strftime("%Y")
# startDate = year + '-' + month + '-01T00:00:01Z'
# endDate = '2099-12-31T23:59:59Z'
# Default start and end dates are the first of the month to now.
startDate = (datetime.datetime.now()).strftime("%Y-%m-01T00:00:01")
endDate = (datetime.datetime.now()).strftime("%Y-%m-%dT23:59:59")
parser = argparse.ArgumentParser(
   description='Download Verizon usage data using the API in verizon.db.')
parser.add_argument('-s', '--startdate', type=str, default='none',
                    required=False, help='Start date (default: ' + startDate + ').')
parser.add_argument('-e', '--enddate', type=str, default='none',
                    required=False, help='End date (default: 2099-12-31T23:59:59Z).')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

# if args.startdate:
#     startDate = args.startdate
# if args.enddate:
#     endDate = args.enddate

# Connect to the database.
cnx = mysql.connector.connect(**config)

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

device_info_req = device.factory.create('ns3:GetDeviceListRequest')
device_info_resp = device.factory.create('ns3:GetDeviceListResponse')

count = 0
LargestDeviceIdSeen = ''
IsComplete = False
while IsComplete == False:
    device_info_req.MaxNumberOfDevices = '500'
    if LargestDeviceIdSeen:
        device_info_req.LargestDeviceIdSeen = LargestDeviceIdSeen
    if debugOutput:
        print device_info_req
        # print device.last_sent()

    device_info_resp = device.service.GetDeviceList(device_info_req)
    if debugOutput:
        print device_info_resp['GetDeviceListResponse']
        # print device.last_received()

    Devices = None
    for k, v in device_info_resp:
        if k == 'Devices':
            Devices = v
        if k == 'IsComplete':
            IsComplete = v

    if Devices != None:
        DeviceInformation = None

        # Get "DeviceInformation" from "Devices".
        for k, v in Devices:
            if k =='DeviceInformation':
                DeviceInformation = v

        for dd in DeviceInformation:
            count += 1
            di_meid = ''
            di_mdn = ''
            di_min = ''
            di_imsi = ''
            di_imei = ''
            di_iccid = ''
            di_msisdn = ''
            ci_state = ''
            ratePlan = ''
            ipAddress = ''
            usage = 0.0
            account = ''

            # print dd
            if 'AccountName' in dd:
                account = dd['AccountName']

            if 'IPAddress' in dd:
                ipAddress = dd['IPAddress']

            # Get the device state from "CarrierInformation".
            for k, v in dd['CarrierInformation']:
                if k == 'CarrierInformation':
                    for z in v:
                        ci_state = z['State']
                        ratePlan = z['ServicePlan']

            # Get the device identifiers from "DeviceIdentifiers".
            for k, v in dd['DeviceIdentifiers']:
                if k == 'DeviceIdentifier':
                    for z in v:
                        kind = z['Kind'].lower()
                        if kind == 'mdn':
                            di_mdn = z['Identifier']
                        elif kind == 'imsi':
                            di_imsi = z['Identifier']
                        elif kind == 'imei':
                            di_imei = z['Identifier']
                        elif kind == 'iccid':
                            di_iccid = z['Identifier']
                        elif kind == 'meid':
                            di_meid = z['Identifier']
                        elif kind == 'msisdn':
                            di_msisdn = z['Identifier']
                        elif kind == 'min':
                            di_min = z['Identifier']

            # Get the "LargestDeviceIdSeen" from "ExtendedAttributes".
            for k, v in dd['ExtendedAttributes']:
                if k == 'ExtendedAttributesObj':
                    for z in v:
                        if z['Key'] == 'DeviceId':
                            LargestDeviceIdSeen = z['Value']

            if ci_state == 'active':
                # Get the device usage.
                identifier = ''
                kind = ''
                if di_iccid:
                    identifier = di_iccid
                    kind = 'iccid'
                elif di_meid:
                    identifier = di_meid
                    kind = 'meid'
                elif di_mdn:
                    identifier = di_mdn
                    kind = 'mdn'
                usage = getDeviceUsage(device, kind, identifier, startDate, endDate)

            if ci_state == None:
                ci_state = ''
            if di_iccid == None:
                di_iccid = ''
            if di_meid == None:
                di_meid = ''
            if ipAddress == None:
                ipAddress = ''
            if di_imsi == None:
                di_imsi = ''
            if di_msisdn == None:
                di_msisdn = ''
            if di_imei == None:
                di_imei = ''
            if di_mdn == None:
                di_mdn = ''
            if di_min == None:
                di_min = ''
            if ratePlan == None:
                ratePlan = ''
            if usage == None:
                usage = 0.0

            # Update the usage table.
            query = ('SELECT id FROM cellusage WHERE carrier="' + carrier + '" '
                     ' AND iccid="' + di_iccid + '" AND meid="' + di_meid + '"')
            # print query
            rowid = 0
            rowCount = 0
            cursor = cnx.cursor()
            cursor.execute(query)
            dbRows = cursor.fetchall()
            for dbRow in dbRows:
                rowCount += 1
                rowid = dbRow[0]
            cursor.close()

            msg = ''
            if rowCount == 0:
                query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                         'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                         'VALUES (\"' + carrier + '\", \"' + ci_state + '\", ' +
                         '\"' + di_iccid + '\", ' + '\"' + di_imsi + '\", ' +
                         '\"' + di_msisdn + '\", ' + '\"' + di_imei + '\", ' +
                         '\"' + di_mdn + '\", ' + '\"' + di_meid + '\", ' +
                         '\"' + di_min + '\", ' + '\"' + ipAddress + '\", ' +
                         '\"' + ratePlan + '\", ' + "{:0.2f}".format(usage) + ')')
                # print query
                cursor = cnx.cursor()
                cursor.execute(query)
                cnx.commit()
                cursor.close()
                msg = "insert"
            elif rowCount == 1:
                query = ('UPDATE cellusage set '
                         'carrier="' + carrier + '", ' +
                         'state="' + ci_state + '", ' +
                         'iccid="' + di_iccid + '", ' +
                         'imsi="' + di_imsi + '", ' +
                         'msisdn="' + di_msisdn + '", ' +
                         'imei="' + di_imei + '", ' +
                         'mdn="' + di_mdn + '", ' +
                         'meid="' + di_meid + '", ' +
                         'min="' + di_min + '", ' +
                         'ipaddress="' + ipAddress + '", ' +
                         'rateplan="' + ratePlan + '", ' +
                         'datausage=' + "{:0.2f}".format(usage) + ' '
                         'WHERE id=' + str(rowid))
                # print query
                cursor = cnx.cursor()
                cursor.execute(query)
                cnx.commit()
                cursor.close()
                msg = "update"
            else:
                print("Duplicate db rows while searching for ICCID: %s, MEID: %s") % (di_iccid, di_meid)
                msg = "error"

            print('%d,%s,%s,%s,%s,%s,%s,%s,%s,%s') % (
                count, account, ci_state, ratePlan, di_mdn, di_iccid, di_meid, ipAddress,
                "{:0.2f}".format(usage), msg)

            if not programRunning:
                break
        if not programRunning:
            break
    if not programRunning:
        break

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

# Close the database connection.
cnx.close()

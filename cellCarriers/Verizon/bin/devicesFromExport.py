#!/usr/bin/python

# This script imports the Verizon "Aggregated device usage" report into the usage
# database.

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'verizon'

programRunning = True

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def updateDbCol(usageDb, colName, identifier, mdn, imei, meid, ipaddress, status):
   rowFound = False
   usageCursor = usageDb.cursor()

   query = ('SELECT datausage FROM cellusage WHERE ' + colName + '="' + identifier + '"')
   # print query
   try:
      usageCursor.execute(query)
   except:
      pass

   try:
      usageRows = usageCursor.fetchall()
      # print usageRows
      for usageRow in usageRows:
         rowFound = True
   except:
      pass

   if rowFound:
      print("Update by %s: %s") % (colName, identifier)
      query = ('UPDATE cellusage set ipaddress="' + ipaddress +
               '", mdn="' + mdn + '", imei="' + imei + '", meid="' + meid +
               '", state="' + status + '"' +
               ' WHERE ' + colName + '="' + identifier + '"')
      print query
      try:
          usageCursor.execute(query)
          usageDb.commit()
      except:
          pass

   usageCursor.close()
   return rowFound

def updateDb(usageDb, mdn, imei, meid, ipaddress, status):
   rowFound = False
   if mdn:
      rowFound = updateDbCol(usageDb, 'mdn', mdn, mdn, imei, meid, ipaddress, status)
   if not rowFound and mdn:
      rowFound = updateDbCol(usageDb, 'msisdn', mdn, mdn, imei, meid, ipaddress, status)
   if not rowFound and imei:
      rowFound = updateDbCol(usageDb, 'imei', imei, mdn, imei, meid, ipaddress, status)
   if not rowFound and meid:
      rowFound = updateDbCol(usageDb, 'meid', meid, mdn, imei, meid, ipaddress, status)
   if not rowFound and iccid:
      rowFound = updateDbCol(usageDb, 'iccid', iccid, mdn, imei, meid, ipaddress, status)

   if not rowFound:
       print("Added %s %s %s %s") % (mdn, imei, meid, iccid)
       cursor = usageDb.cursor()
       query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                'VALUES ("' + carrier + '", "' + status + '", "' + iccid + '", "", "' + mdn +
                '", "' + imei + '", "' + mdn + '", "' + meid + '", "", "", "", 0.0)')
       cursor.execute(query)
       usageDb.commit()
       cursor.close()
       rowFound = True

   return rowFound

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import Verizon aggregated usage report into a db file.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Verizon usage report in csv format.')
parser.add_argument('-c', '--cleardb', action="store_true",
                    help='Clear DB records first.')
args = parser.parse_args()

print datetime.datetime.today()

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Create a cursor.
cursor = cnx.cursor()

# Optionally, clear out all records for the carrier.
if args.cleardb:
    print("Clearing existing db records.")
    cursor.execute('DELETE from cellusage WHERE carrier = \"' + carrier + '\"')

# "Device Identifier","MDN/MSISDN","IP Address","Device Status","Connected","Device Group","Service Plan","Activation Date","Roaming Status","Custom Field 1"
# Device identifier is the ICCID, IMEI or MEID
# ICCID is 20 chars: 89148000003240819073
# IMEI is 15 chars: 353650078459125
# MEID is 14 chars and starts with "A1": A1000047ADD2E0

try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
   identifier = row[0].strip()
   if 'Device Identifier' in identifier:
      continue

   meid = ''
   imei = ''
   iccid = ''
   if 'A1' in identifier:
      meid = identifier
   elif len(identifier) == 15:
      imei = identifier
   elif len(identifier) == 20:
      iccid = identifier

   mdn = row[1].strip()
   ipaddress = row[2].strip()
   status = row[3].strip()

   updateDb(cnx, mdn, imei, meid, ipaddress, status)

   if not programRunning:
      break

cnx.commit()
cnx.close()

print datetime.datetime.today()

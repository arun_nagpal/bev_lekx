#!/usr/bin/python

import csv
import suds
import logging
from suds.client import Client
from suds.sax.element import Element
import argparse
import sys

account = 'SECURENET INTERACTIVE TECHNOLOGIES LLC VPP NS PN'
#`username = 'benjensensnet1'
#password = 'S3cur3N3t!'
username = 'bnagpal'
password = 'May202015#'

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Using the Jasper API, capture month-to-date usage for all devices.')
parser.add_argument('-f', '--csvfile', type=str, default=None,
                    required=False, help='CSV file of identifiers to deactivate.')
parser.add_argument('-i', '--identifier', type=str, default=None,
                    required=False, help='Identifier.')
parser.add_argument('-k', '--kind', type=str, default=None,
                    required=True, help='The type of identifier (meid, mdn, iccid).')
args = parser.parse_args()

if not args.csvfile and not args.identifier:
    print("An identifier or CSV file of identifiers is required.")
    sys.exit(1)

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
login_resp = session.service.LogIn(login_req)

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the carrier service
device_url = 'https://uws.apps.nphase.com/api/v2/CarrierService.svc'
carrierSrvc = Client(device_url + '?wsdl')
carrierSrvc.set_options(location=device_url, soapheaders=token_element)

if args.csvfile:
    # Iterate through the list of identifiers.
    csvfile = open(args.csvfile, 'rb')
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        identifier = row[0].strip()
        if args.kind == 'meid' and identifier[0:2] != 'A1':
            continue

        carrier_req = carrierSrvc.factory.create('ns1:ChangeDeviceStateRequest')
        carrier_req.DeviceList.DeviceIdentifierCollection = carrierSrvc.factory.create('ns2:DeviceIdentifierCollection')
        carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers = [carrierSrvc.factory.create('ns2:ArrayOfDeviceIdentifier')]
        carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier = [carrierSrvc.factory.create('ns2:DeviceIdentifier')]
        carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier[0] = {'Kind': args.kind, 'Identifier': identifier}

        carrier_req.Deactivate = carrierSrvc.factory.create('ns1:DeactivateDeviceRequest')
        carrier_req.Deactivate.ReasonCode = 'FF'
        carrier_req.Deactivate.EtfWaiver = 'false'

        carrier_resp = carrierSrvc.factory.create('ns1:ChangeDeviceStateResponse')
        carrier_resp = carrierSrvc.service.ChangeDeviceState(carrier_req)
else:
    carrier_req = carrierSrvc.factory.create('ns1:ChangeDeviceStateRequest')
    carrier_req.DeviceList.DeviceIdentifierCollection = carrierSrvc.factory.create('ns2:DeviceIdentifierCollection')
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers = [carrierSrvc.factory.create('ns2:ArrayOfDeviceIdentifier')]
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier = [carrierSrvc.factory.create('ns2:DeviceIdentifier')]
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier[0] = {'Kind': args.kind, 'Identifier': args.identifier}

    carrier_req.Deactivate = carrierSrvc.factory.create('ns1:DeactivateDeviceRequest')
    carrier_req.Deactivate.ReasonCode = 'FF'
    carrier_req.Deactivate.EtfWaiver = 'false'

    carrier_resp = carrierSrvc.factory.create('ns1:ChangeDeviceStateResponse')
    carrier_resp = carrierSrvc.service.ChangeDeviceState(carrier_req)

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
logout_resp = session.service.LogOut(logout_req)

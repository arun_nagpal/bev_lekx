#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

csvfile=""

Usage()
{
   echo ""
   echo "${scriptName}: usage:"
   echo "  ${scriptName} <OPTIONS>"
   echo "  OPTIONS: -f <csv file> - File of ICCID, IMEI pairs to activate."
   echo "           -h            - Display this helpful message."
   echo ""
}

TEMP=`getopt -o f:h -- "$@"`
if [[ $? != 0 || $# < 1 ]]
then
   echo "Insufficient parameters."
   Usage
   exit 1
fi

eval set -- "$TEMP"
while true
do
   case "$1" in
      -f) csvfile=$2
          shift 2
          ;;
      --) shift
          break
          ;;
      *)  Usage
          exit 1
          ;;
   esac
done

if [ -z "${csvfile}" ]
then
    echo "${scriptName}: a csv file of ICCID, IMEI pairs is required."
    Usage
    exit 1
fi

sims=`cat "${csvfile}"`
OFS=$IFS
IFS="
"

scp bev_lekx@germany:/var/log/apache2/error.log .
scp bev_lekx@germany:/var/log/apache2/error.log.1 .
cat error.log.1 >> error.log
rm error.log.1

for sim in ${sims}
do
   id="$(cut -d ',' -f1 <<< "${sim}")"
   grep "${id}" ./error.log
   # grepOut=`ssh germany "sudo grep \"${id}\" /var/log/apache2/error.log"`
   # if [ -z "${grepOut}" ]
   # then
   #     grepOut=`ssh germany "sudo grep \"${id}\" /var/log/apache2/error.log.1"`
   # fi
   # echo "${grepOut}"
done

IFS=$OFS

#!/usr/bin/python

import signal
import time
import calendar
import argparse
from suds.client import Client
from suds.sax.element import Element
import mysql.connector
import logging

# Database parameters.
dbConfig = {
    'user': 'bev_lekx',
    'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
    'host': '172.29.253.45',
    'database': 'celldata',
    'raise_on_warnings': False,
}

account = 'S'
username = ''
password = ''
programRunning = True
debugOutput = False
carrier = 'verizon'

def sigint_handler(signum, frame):
    # pylint: disable=unused-argument, missing-docstring
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Download Verizon device list.')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

if args.debug:
    debugOutput = True

# Connect to the database.
cnx = mysql.connector.connect(**dbConfig)

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

device_info_req = device.factory.create('ns3:GetDeviceListRequest')
device_info_resp = device.factory.create('ns3:GetDeviceListResponse')

count = 0
LargestDeviceIdSeen = ''
IsComplete = False
while not IsComplete:
    device_info_req.MaxNumberOfDevices = '1000'
    if LargestDeviceIdSeen:
        device_info_req.LargestDeviceIdSeen = LargestDeviceIdSeen
    if debugOutput:
        print device_info_req
        # print device.last_sent()

    device_info_resp = device.service.GetDeviceList(device_info_req)
    if debugOutput:
        print device_info_resp['GetDeviceListResponse']
        # print device.last_received()
    # print device_info_resp

    Devices = None
    for k, v in device_info_resp:
        if k == 'Devices':
            Devices = v
        if k == 'IsComplete':
            IsComplete = v

    if Devices != None:
        DeviceInformation = None

        # Get "DeviceInformation" from "Devices".
        for k, v in Devices:
            if k == 'DeviceInformation':
                DeviceInformation = v
	print DeviceInformation
	print "----------Arun ----"
        for dd in DeviceInformation:
            # print dd
            di_meid = ''
            di_mdn = ''
            di_min = ''
            di_imsi = ''
            di_imei = ''
            di_iccid = ''
            di_msisdn = ''
            ci_state = ''
            ratePlan = ''

            ipAddress = dd['IPAddress']

            activated_date = 0
            last_used = 0
            if 'LastActivationDate' in dd:
                try:
                    activated_date = int(calendar.timegm(
                        time.strptime(str(dd['LastActivationDate']), '%Y-%m-%d %H:%M:%S')))
                except:
                    pass
            if 'LastConnectionDate' in dd:
                try:
                    last_used = int(calendar.timegm(
                        time.strptime(str(dd['LastConnectionDate']), '%Y-%m-%d %H:%M:%S')))
                except:
                    pass

            # Get the device state from "CarrierInformation".
            for k, v in dd['CarrierInformation']:
                if k == 'CarrierInformation':
                    for z in v:
                        ci_state = z['State']
                        ratePlan = z['ServicePlan']

            # Get the device identifiers from "DeviceIdentifiers".
            for k, v in dd['DeviceIdentifiers']:
                if k == 'DeviceIdentifier':
                    for z in v:
                        kind = z['Kind'].lower()
                        if kind == 'mdn':
                            di_mdn = z['Identifier']
                        elif kind == 'imsi':
                            di_imsi = z['Identifier']
                        elif kind == 'imei':
                            di_imei = z['Identifier']
                        elif kind == 'iccid':
                            di_iccid = z['Identifier']
                        elif kind == 'meid':
                            di_meid = z['Identifier']
                        elif kind == 'msisdn':
                            di_msisdn = z['Identifier']
                        elif kind == 'min':
                            di_min = z['Identifier']

            # Get the "LargestDeviceIdSeen" from "ExtendedAttributes".
            for k, v in dd['ExtendedAttributes']:
                if k == 'ExtendedAttributesObj':
                    for z in v:
                        if z['Key'] == 'DeviceId':
                            LargestDeviceIdSeen = z['Value']

            if not ratePlan:
                ratePlan = ''
            if not ci_state:
                ci_state = 'inactive'
            if not di_meid:
                di_meid = ''
            if not di_iccid:
                di_iccid = ''
            if not di_mdn:
                di_mdn = ''
            if not di_imsi:
                di_imsi = ''
            if not di_msisdn:
                di_msisdn = ''
            if not di_min:
                di_min = ''
            if not di_imei:
                di_imei = ''
            if not ipAddress:
                ipAddress = ''

            # Update the usage table.
            query = ''
            rowid = 0
            rowCount = 0
            if len(di_iccid) > 0:
                query = ('SELECT id FROM cellusage WHERE carrier="{carrier}" '
                         'AND iccid="{iccid}"' . format(carrier=carrier, iccid=di_iccid))
            elif len(di_meid) > 0:
                query = ('SELECT id FROM cellusage WHERE carrier="{carrier}" '
                         'AND meid="{meid}"' . format(carrier=carrier, meid=di_meid))
            if len(query) > 0:
                # print query
                cursor = cnx.cursor()
                cursor.execute(query)
                dbRows = cursor.fetchall()
                for dbRow in dbRows:
                    rowCount += 1
                    rowid = dbRow[0]
                cursor.close()

            msg = ''
            query = ''
            if rowCount == 0:
                query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                         'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                         'activation_date) '
                         'VALUES ("' + carrier + '", "' + ci_state + '", ' +
                         '"' + di_iccid + '", ' + '"' + di_imsi + '", ' +
                         '"' + di_msisdn + '", ' + '"' + di_imei + '", ' +
                         '"' + di_mdn + '", ' + '"' + di_meid + '", ' +
                         '"' + di_min + '", ' + '"' + ipAddress + '", ' +
                         '"' + ratePlan + '", 0, ' +
                         str(last_used) + ', ' + str(activated_date) + ')')
                msg = "insert"
            elif rowCount == 1:
                query = ('UPDATE cellusage set '
                         'carrier="' + carrier + '", ' +
                         'state="' + ci_state + '", ' +
                         'iccid="' + di_iccid + '", ' +
                         'imsi="' + di_imsi + '", ' +
                         'msisdn="' + di_msisdn + '", ' +
                         'imei="' + di_imei + '", ' +
                         'mdn="' + di_mdn + '", ' +
                         'meid="' + di_meid + '", ' +
                         'min="' + di_min + '", ' +
                         'ipaddress="' + ipAddress + '", ' +
                         'rateplan="' + ratePlan + '", ' +
                         'activation_date=' + str(activated_date) + ', ' +
                         'last_used=' + str(last_used) + ' ' +
                         'WHERE id=' + str(rowid))
                msg = "update"
            else:
                print(("Duplicate db rows while searching for ICCID: %s, MEID: %s") %
                      (di_iccid, di_meid))
                msg = "error"

            if len(query) > 0:
                # print query
                cursor = cnx.cursor()
                cursor.execute(query)
                if count % 100 == 0:
                    cnx.commit()
                cursor.close()

            print('%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %d, %s') % (
                count, di_meid, di_iccid, di_mdn, di_imsi, di_msisdn, di_min,
                di_imei, ipAddress, ci_state, ratePlan, activated_date, last_used, msg)

            count += 1
            if not programRunning:
                break
        if not programRunning:
            break
    if not programRunning:
        break

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

cnx.commit()
cnx.close()

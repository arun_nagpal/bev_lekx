#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict
import mysql.connector

carrier = 'verizon'
account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
programRunning = True
debugOutput = False

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def updateDbCol(usageDb, colName, identifier, usage):
   rowFound = False
   usageCursor = usageDb.cursor()

   query = ('SELECT datausage FROM cellusage WHERE ' + colName + '="' + identifier + '"')
   # print query
   try:
      usageCursor.execute(query)
   except:
      pass

   try:
      usageRows = usageCursor.fetchall()
      # print usageRows
      for usageRow in usageRows:
         rowFound = True
   except:
      pass

   if rowFound:
      # print("Update by %s: %s") % (colName, identifier)
      query = ('UPDATE cellusage set datausage=' + "{:0.2f}".format(usage) +
               ' WHERE ' + colName + '="' + identifier + '"')
      try:
          usageCursor.execute(query)
          usageDb.commit()
      except:
          pass

   usageCursor.close()
   return rowFound

def updateDb(usageDb, iccid, meid, usage):
   rowFound = False
   if iccid:
      rowFound = updateDbCol(usageDb, 'iccid', iccid, usage)
   if not rowFound and meid:
      rowFound = updateDbCol(usageDb, 'meid', meid, usage)

   if not rowFound:
       print("Added %s %s") % (iccid, meid)
       cursor = usageDb.cursor()
       query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                'VALUES ("' + carrier + '", "", "' + iccid + '", "", "", "", "", ' +
                '"' + meid + '", "", "", "", ' + "{:0.2f}".format(usage) + ')')
       cursor.execute(query)
       usageDb.commit()
       cursor.close()
       rowFound = True

   return rowFound

def getDeviceUsage(device, kind, identifier, startDate, endDate):
    haveError = False

    device_info_req = device.factory.create('ns3:GetDeviceUsageHistoryRequest')
    device_info_resp = device.factory.create('ns3:GetDeviceUsageHistoryResponse')
    device_info_req.Device.Kind = kind
    device_info_req.Device.Identifier = identifier
    device_info_req.TimestampFilter.Earliest = startDate
    device_info_req.TimestampFilter.Latest = endDate
    # device_info_req.TimestampFilter.Earliest = year + '-' + month + '-01T00:00:01Z'
    # device_info_req.TimestampFilter.Latest     = '2099-12-31T23:59:59Z'
    if debugOutput:
        print device_info_req
    try:
        device_info_resp = device.service.GetDeviceUsageHistory(device_info_req)
        if debugOutput:
            print device_info_resp
    except:
        haveError = True
        pass

    totalBytes = 0
    # lastDate = ''
    if not haveError:
        try:
            for nn in device_info_resp.UsageHistory.DeviceUsage:
                totalBytes += nn.BytesUsed
        except:
            pass

    # Return the total in MB.
    totalBytes /= (1024.0*1024.0)
    return totalBytes

# Command-line arguments.
#startDate = (datetime.datetime.now() - datetime.timedelta(1)).strftime("%Y-%m-%dT%H:%M:%S")
#endDate = (datetime.datetime.now()).strftime("%Y-%m-%dT%H:%M:%S")
startDate = (datetime.datetime.now()).strftime("%Y-%m-01T00:00:01")
endDate = (datetime.datetime.now()).strftime("%Y-%m-%dT23:59:59")

parser = argparse.ArgumentParser(
   description='Obtain Verizon usage for one device.')
parser.add_argument('-i', '--identifier', type=str, default='',
                    required=False, help='Device identifier.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=True, help='Identifier type.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='CSV file containing identifiers')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

# Connect to the database.
cnx = mysql.connector.connect(**config)

if args.csvfile:
    csvfile = open(args.csvfile, 'rb')
    for line in csvfile:
        identifier = line.strip()
        usage = getDeviceUsage(device, args.kind, identifier, startDate, endDate)
        updateDb(cnx, identifier, identifier, usage)
        print("%s,%.3f") % (identifier, usage)
    csvfile.close()
else:
    usage = getDeviceUsage(device, args.kind, args.identifier, startDate, endDate)
    print("%s %.2f") % (args.identifier, usage)

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

cnx.commit()
cnx.close()

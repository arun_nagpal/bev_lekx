#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import logging
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict

carrier = 'verizon'
account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
programRunning = True
debugOutput = False

def getDeviceConnectionHistory(device, kind, identifier, startDate, endDate):
    haveError = False

    print('%s') % (identifier)

    device_info_req = device.factory.create('ns3:GetDeviceConnectionHistoryRequest')
    device_info_resp = device.factory.create('ns3:GetDeviceConnectionHistoryResponse')
    device_info_req.Device.Kind = kind
    device_info_req.Device.Identifier = identifier
    device_info_req.OccurredAtFilter.Earliest = startDate
    device_info_req.OccurredAtFilter.Latest = endDate
    if debugOutput:
        print device_info_req
    try:
        device_info_resp = device.service.GetDeviceConnectionHistory(device_info_req)
        if debugOutput:
            print device_info_resp
        event = ''
        cause = ''
        for z in device_info_resp.ConnectionHistory.DeviceConnectionEvent:
            for k,v in z.ConnectionEventAttributes.ConnectionEventAttribute:
                if k[1] == 'Event':
                    event = v[1]
                if k[1] == 'AccountTerminateCause':
                    cause = v[1]
            print('    %s %s %s') % (z.OccurredAt, event, cause)
    except:
        haveError = True
        pass

# Command-line arguments.
month = datetime.datetime.now().strftime("%m")
year = datetime.datetime.now().strftime("%Y")
startDate = year + '-' + month + '-01T00:00:01Z'
endDate = '2099-12-31T23:59:59Z'
parser = argparse.ArgumentParser(
   description='Obtain Verizon usage for one device.')
parser.add_argument('-i', '--identifier', type=str, default='',
                    required=False, help='Device identifier.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=True, help='Identifier type.')
parser.add_argument('-s', '--startdate', type=str, default='',
                    required=False, help='Start date (default: ' + startDate + ').')
parser.add_argument('-e', '--enddate', type=str, default='2099-12-31T23:59:59Z',
                    required=False, help='End date (default: 2099-12-31T23:59:59Z).')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
parser.add_argument('-f', '--csvfile', type=str, default='', required=False)
args = parser.parse_args()

if args.startdate:
    startDate = args.startdate
if args.enddate:
    endDate = args.enddate

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/DeviceService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device

if args.identifier:
    getDeviceConnectionHistory(device, args.kind, args.identifier, startDate, endDate)
elif args.csvfile:
    csvfile = open(args.csvfile, 'rb')
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        if len(row) == 0:
            continue
        identifier = row[0].strip()
        if len(identifier) > 0:
            getDeviceConnectionHistory(device, args.kind, identifier, startDate, endDate)
    csvfile.close()
else:
    print('You must supply a file name or an identifier.')

# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req
logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

#!/usr/bin/python

import csv
import datetime
import time
import argparse
import os

linesPerBatchFile = 2000

# Command-line arguments.
parser = argparse.ArgumentParser(description='')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='IMSI list csv format.')
args = parser.parse_args()

try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

inFileDir = os.path.dirname(args.csvfile)
outFile = None
lineCounter = -1
fileCounter = 0

csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
    if lineCounter >= linesPerBatchFile or lineCounter == -1:
        if lineCounter >= linesPerBatchFile:
            outFile.close()
        fileCounter += 1
        if len(inFileDir) > 0:
            baseFileName = (inFileDir + '/' + 'verizon-batch-' + time.strftime("%Y%m%d", time.gmtime()))
        else:
            baseFileName = ('verizon-batch-' + time.strftime("%Y%m%d", time.gmtime()))

        outFileName = baseFileName + "-{:03d}".format(fileCounter) + ".txt"
        if os.path.exists(outFileName):
            lineCounter = -1
            continue
        outFile = open(outFileName, 'w')
        lineCounter = 0

    if len(row) > 0:
        identifier = row[0].strip()
        if len(identifier) > 0:
            if 'nohup: ignoring input' in identifier:
                continue
            outFile.write(identifier + '\n')
            lineCounter += 1

outFile.close()
csvfile.close()

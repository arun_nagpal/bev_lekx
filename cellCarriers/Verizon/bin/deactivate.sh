#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/Verizon"
workDir="${carrierDir}/work"
cdate=`date +%Y%m%d`

# Thresholds.
th_delDeleted=90      # Account deleted threshold in days (-t).
th_delActivate=365     # Activated threshold in days for lines with deleted accounts (-a).
th_delLastused=365     # Last used threshold in days for lines with deleted accounts (-l).
th_noacctActivate=365  # Activated threshold for lines without accounts in days (-y).
th_noacctLastused=365  # Last used threshold for lines without accounts in days (-z).

# Print a detailed report of the lines to be deactivated.
${cellDir}/bin/findCellLines.py -d -c verizon -v \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    > ${workDir}/verizon-deactivate-report-${cdate}.txt

# MEIDs
${cellDir}/bin/findCellLines.py -c verizon -d -o meid \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/verizon-deactivate-meid-${cdate}.csv

# ICCIDs
${cellDir}/bin/findCellLines.py -c verizon -d -o iccid \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/verizon-deactivate-iccid-${cdate}.csv

# Send an email.
/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t dinh.duong@securenettech.com \
                      arun.nagpal@securnettech.com \
                   -o tls=no \
                   -u "Verizon cell deactivations" \
                   -m "Verizon cell deactivations for `date +"%B %d, %Y"` are ready."

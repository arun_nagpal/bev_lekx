#!/usr/bin/python

import csv
import suds
import logging
from suds.client import Client
from suds.sax.element import Element
import datetime
import time
import argparse
import sys
import os

account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'
debugOutput = False

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Display device info using the Verizon API.')
parser.add_argument('-a', '--account', type=str, default='',
                    required=True, help='Account number.')
parser.add_argument('-d', '--debug', action="store_true",
                    help='Enable debug output.')
args = parser.parse_args()

if args.debug:
    debugOutput = True

if debugOutput:
    logging.basicConfig(level=logging.INFO)
    logging.getLogger('suds.client').setLevel(logging.DEBUG)
    # logging.getLogger('suds.transport').setLevel(logging.DEBUG)
    # logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
    # logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)

# create the session service
session_url = 'https://uws.apps.nphase.com/api/v2/SessionService.svc'
session = Client(session_url + '?wsdl')
session.set_options(location=session_url)
if debugOutput:
    print session
    print session.last_sent()
    print session.last_received()

# log in to the M2M Platform
login_req = session.factory.create('ns1:LogInRequest')
login_resp = session.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
if debugOutput:
    print login_req

login_resp = session.service.LogIn(login_req)
if debugOutput:
    print login_resp

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
session.set_options(soapheaders=token_element)

# create the device service
device_url = 'https://uws.apps.nphase.com/api/v2/AccountService.svc'
device = Client(device_url + '?wsdl')
device.set_options(location=device_url, soapheaders=token_element)
if debugOutput:
    print device





# Request information about a specific account
device_info_req = device.factory.create('ns1:GetAccountInformationRequest')
device_info_resp = device.factory.create('ns1:GetAccountInformationResponse')
device_info_req.AccountNumber = args.account
if debugOutput:
    print device_info_req

device_info_resp = device.service.GetAccountInformation(device_info_req)
print device_info_resp
if debugOutput:
    # print device_info_req.last_sent()
    print device.last_received()





# log out of the API service
logout_req = session.factory.create('ns1:LogOutRequest')
logout_resp = session.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
if debugOutput:
    print logout_req

logout_resp = session.service.LogOut(logout_req)
if debugOutput:
    print logout_resp

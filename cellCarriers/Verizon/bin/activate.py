#!/usr/bin/python
# https://web.archive.org/web/20160310112216/
# https://fedorahosted.org/suds/wiki/Documentation

import csv
import suds
import logging
from suds.client import Client
from suds.sax.element import Element
import argparse
import sys

account = 'SECURENET INTERACTIVE TECH'
username = 'SECURENETUWS'
password = 'alskDJFH!13542'

def activate(params):
    carrierSrvc = None
    if 'carrierSrvc' in params:
        carrierSrvc = params['carrierSrvc']
    if not carrierSrvc:
        return

    carrier_req = carrierSrvc.factory.create('ns1:ChangeDeviceStateRequest')
    carrier_req.DeviceList.DeviceIdentifierCollection = carrierSrvc.factory.create('ns2:DeviceIdentifierCollection')
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers = [carrierSrvc.factory.create('ns2:ArrayOfDeviceIdentifier')]
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier = [carrierSrvc.factory.create('ns2:DeviceIdentifier')]
    carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier[0] = {'Kind': 'iccid', 'Identifier': params['iccid']}

    if params['imei']:
        carrier_req.DeviceList.DeviceIdentifierCollection.DeviceIdentifiers[0].DeviceIdentifier.append({'Kind': 'imei', 'Identifier': params['imei']})

    carrier_req.AccountName  = params['account']
    carrier_req.Activate = carrierSrvc.factory.create('ns1:ActivateDeviceRequest')
    carrier_req.Activate.CarrierIpPoolName = params['ippool']
    carrier_req.Activate.ServicePlan = params['serviceplan']
    carrier_req.Activate.MdnZipCode  = params['zipcode']
    if params['sku']:
        carrier_req.Activate.SkuNumber  = params['sku']
    carrier_req.Activate.State  = 'Active'

    # carrier_req.Activate.CustomFields.CustomField = [carrierSrvc.factory.create('ns1:ArrayOfCustomField')]
    # carrier_req.Activate.CustomFields.CustomField[0] = { 'Name': 'CustomField1',
    #                                              'Value': 'Request submitted on Mon Dec 16 17:52:32 UTC 2019'}
    # carrier_req.Activate.CustomFields.CustomField.append({'Name': 'CustomField2', 'Value': '2019.12.16.01'})

    carrier_resp = carrierSrvc.factory.create('ns1:ChangeDeviceStateResponse')
    carrier_resp = carrierSrvc.service.ChangeDeviceState(carrier_req)
    # print(str(carrier_req))
    # print(carrier_resp)
    print(carrierSrvc.last_sent())
    print(carrierSrvc.last_received())

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Activate cell lines using the Verizon SOAP API.')
parser.add_argument('-f', '--csvfile', type=str, default=None,
                    required=False, help='CSV file of iccid/imei pairs.')
parser.add_argument('-i', '--iccid', type=str, default=None,
                    required=False, help='ICCID.')
parser.add_argument('-m', '--imei', type=str, default=None,
                    required=False, help='IMEI.')
parser.add_argument('-s', '--sku', type=str, default=None,
                    required=False, help='Verizon SKU.')
parser.add_argument('-p', '--ippool', type=str, default='MCPNASECURENETDIA',
                    required=False, help='Carrier IP pool name (MCPNASECURENETDIA).')
# parser.add_argument('-n', '--serviceplan', type=str, default='1MBLTEHDVOICE',
#                     required=False, help='Service plan (1MBLTEHDVOICE).')
parser.add_argument('-n', '--serviceplan', type=str, default='3647914x19023PriSta',
                 required=False, help='Service plan (3647914x19023PriSta).')
parser.add_argument('-z', '--zipcode', type=str, default='84043',
                    required=False, help='Zipcode (84043).')
parser.add_argument('-a', '--account', type=str, default='0642085097-00001',
                    required=False, help='Account (0642085097-00001).')
args = parser.parse_args()

if not args.csvfile and not args.iccid:
    print("An identifier or CSV file of identifiers is required.")
    sys.exit(1)

logging.basicConfig(level=logging.INFO)
# logging.getLogger('suds.client').setLevel(logging.DEBUG)
# logging.getLogger('suds.transport').setLevel(logging.DEBUG)
# logging.getLogger('suds.xsd.schema').setLevel(logging.DEBUG)
# logging.getLogger('suds.wsdl').setLevel(logging.DEBUG)
# logging.getLogger('suds.resolver').setLevel(logging.DEBUG)
# logging.getLogger('suds.xsd.query').setLevel(logging.DEBUG)
# logging.getLogger('suds.xsd.basic').setLevel(logging.DEBUG)
# logging.getLogger('suds.binding.marshaller').setLevel(logging.DEBUG)

# create the client service
client = Client('https://uws.apps.nphase.com/api/v2/SessionService.svc?wsdl')

# log in to the M2M Platform
login_req = client.factory.create('ns1:LogInRequest')
login_resp = client.factory.create('ns1:LogInResponse')
login_req.Username = username
login_req.Password = password
login_resp = client.service.LogIn(login_req)
# print(login_req)

# create a soap header element to hold the session token
token_element = Element('token')
token_element.setText('{}'.format(login_resp.SessionToken))
token_element.attributes.append('xmlns="http://nphase.com/unifiedwebservice/v2"')
client.set_options(soapheaders=token_element)

# create the carrier service
device_url = 'https://uws.apps.nphase.com/api/v2/CarrierService.svc'
carrierSrvc = Client(device_url + '?wsdl')
carrierSrvc.set_options(location=device_url, soapheaders=token_element)
# print(carrierSrvc)

params = {}
params['iccid'] = args.iccid
params['imei'] = args.imei
params['sku'] = args.sku
params['ippool'] = args.ippool
params['serviceplan'] = args.serviceplan
params['zipcode'] = args.zipcode
params['account'] = args.account
params['carrierSrvc'] = carrierSrvc

if args.csvfile:
    # Iterate through the ICCID/IMEI pair list.
    csvfile = open(args.csvfile, 'rb')
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        params['iccid'] = row[0]
        params['imei'] = None
        if len(row) > 1:
            params['imei'] = row[1]
        if params['iccid'][0:1] == '#':
            continue
        try:
            activate(params)
        except:
            print("Activation failure: ", params)
            pass
else:
    activate(params)

# log out of the API service
logout_req = client.factory.create('ns1:LogOutRequest')
logout_resp = client.factory.create('ns1:LogOutResponse')
logout_req.SessionToken = login_resp.SessionToken
logout_resp = client.service.LogOut(logout_req)

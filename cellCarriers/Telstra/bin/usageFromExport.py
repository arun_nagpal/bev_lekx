#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'telstra'

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def updateDbCol(usageDb, colName, identifier, iccid, imsi, msisdn, status,
                rateplan, usage):
   rowFound = False
   usageCursor = usageDb.cursor()

   query = ('SELECT datausage FROM cellusage WHERE ' + colName + '="' + identifier + '"')
   # print query
   try:
      usageCursor.execute(query)
   except:
      pass

   try:
      usageRows = usageCursor.fetchall()
      # print usageRows
      for usageRow in usageRows:
         rowFound = True
   except:
      pass

   if rowFound:
      print("Update by %s: %s") % (colName, identifier)
      query = ('UPDATE cellusage set iccid="' + iccid + '", imsi="' +
               imsi + '", msisdn="' + msisdn + '", state="' + status +
               '", rateplan="' + rateplan +
               '", datausage=' + "{:0.2f}".format(usage) +
               ' WHERE ' + colName + '="' + identifier + '"')
      try:
          usageCursor.execute(query)
          usageDb.commit()
      except:
          pass

   usageCursor.close()
   return rowFound

def updateDb(usageDb, iccid, imsi, msisdn, status, rateplan, usage):
   rowFound = False
   if iccid:
      rowFound = updateDbCol(usageDb, 'iccid', iccid, iccid, imsi, msisdn,
                             status, rateplan, usage)
   if not rowFound and msisdn:
      rowFound = updateDbCol(usageDb, 'msisdn', msisdn, iccid, imsi, msisdn,
                             status, rateplan, usage)
   if not rowFound and imsi:
      rowFound = updateDbCol(usageDb, 'imsi', imsi, iccid, imsi, msisdn,
                             status, rateplan, usage)

   if not rowFound:
       print("Added %s %s %s") % (iccid, msisdn, imsi)
       cursor = usageDb.cursor()
       query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                'VALUES ("' + carrier + '", "' + status + '", "' + iccid +
                '", "' + imsi + '", "' + msisdn + '", "", "", "", "", ""' +
                ', "", ' + "{:0.2f}".format(usage) + ')')
       cursor.execute(query)
       usageDb.commit()
       cursor.close()
       rowFound = True

   return rowFound

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import Telstra device spreadsheet into att.db.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Telstra spreadsheet in csv format.')
parser.add_argument('-c', '--cleardb', action="store_true",
                    help='Clear DB records first.')
args = parser.parse_args()

print datetime.datetime.today()

# Connect to the database.
cnx = mysql.connector.connect(**config)

# Create a cursor.
cursor = cnx.cursor()

# Optionally, clear out all records for the carrier.
if args.cleardb:
    print("Clearing existing db records.")
    cursor.execute('DELETE from cellusage WHERE carrier = \"' + carrier + '\"')

# Date Added,ICCID,Device ID,Cycle to Date Usage (MB),In Session,Modem ID,MSISDN,Customer,End Consumer ID,Rate Plan,Usage Limit Reached,IMSI,SIM Status,Activated,Operator Account ID,Migrated SIM
# 08/01/2017 07:43 PM EDT,89610185001973227241,8500197322721,0.85,Yes,,61487900286,,,1MB M2M Group Data Plan - Securenet,No,505016000729099,Activated,07/15/2018,04302753,No

# This loop iterates through the csv file created by exporting the xlsx downloaded
# from the carrier. The data usage value is stored in the database along with the
# iccid and timestamp.
try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
   iccid = row[1].strip()
   if iccid == 'ICCID':
      continue

#   deviceid = row[2].strip()
   usage = float(row[3].replace(',',''))
#   modemid = row[5].strip()
   msisdn = row[6].strip()
   rateplan = row[9].strip()
   imsi = row[11].strip()
   status = row[12].strip()

   # print("%s %s %s %s %s %s %s %f") % (iccid, deviceid, modemid, msisdn, imsi, rateplan, status, usage)
   updateDb(cnx, iccid, imsi, msisdn, status, rateplan, usage)

cnx.commit()
cnx.close()

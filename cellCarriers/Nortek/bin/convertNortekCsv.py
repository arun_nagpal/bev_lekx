#!/usr/bin/python

import argparse
import csv
import sys

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Convert Nortek csv to iccic/imei pairs.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=True, help='Nortek spreadsheet in csv format.')
args = parser.parse_args()

try:
   csvfile = open(args.csvfile, 'rb')
except:
   print("Error opening \"%s\"") % (args.csvfile)
   sys.exit(1)

csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
   imei = row[3].strip()
   iccid = row[7].strip()

   if iccid != 'ICCID':
      print("%s,%s") % (iccid, imei)

#!/usr/bin/python

import requests
import datetime
import time
import sys
import os
import signal
import mysql.connector
import csv
import argparse
import json

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

programRunning = True

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'

def getDeviceState(iccid):
   url = ('https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid)
   status = ''
   myResponse = requests.get(url, auth=(username,api_key))
   if(myResponse.ok):
      jData = json.loads(myResponse.content)
      status = jData['status']
   return status

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='AT&T cancel location script.', add_help=False)
parser.add_argument('-f', '--file', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --file         CSV-formatted file of ICCIDs.")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

if len(args.file) == 0:
    # Connect to the database.
    cnx = mysql.connector.connect(**config)

    query = ('SELECT iccid FROM cellusage WHERE carrier="att" and state="TEST_READY" and imei=""')
    cursor = cnx.cursor()
    cursor.execute(query)
    dbRows = cursor.fetchall()
    rowFound = False
    for dbRow in dbRows:
        iccid = dbRow[0]

        status = ''
        cancelLocCursor = cnx.cursor()
        cancelLocCursor.execute('select status from cancelloc where iccid="{iccid}"' . format(iccid=iccid))
        cancelLocRows = cancelLocCursor.fetchall()
        for cancelLocRow in cancelLocRows:
            status = row[0]
        cancelLocCursor.close()

        if status != 'complete':
            os.system('php /home/bev_lekx/cellCarriers/ATT/bin/sendCancelLocation.php ' + iccid)
        cancelLocCursor = cnx.cursor()
        cancelLocCursor.execute('update cancelloc set status="complete" where iccid="{iccid}"' . format(iccid=iccid))
        cnx.commit()
        cancelLocCursor.close()

        if not programRunning:
            break
    cursor.close()
    cnx.close()

else:
    try:
        csvfile = open(args.file, 'rb')
    except IOError:
        print("Error: %s could not be opened.") % (args.file)
        sys.exit(1)

    csvreader = csv.reader(csvfile, quotechar="\"")
    for row in csvreader:
        iccid = row[0].strip()

        status = getDeviceState(iccid)
        if status == 'TEST_READY':
            os.system('php /home/bev_lekx/cellCarriers/ATT/bin/sendCancelLocation.php ' + iccid)

        if not programRunning:
            break

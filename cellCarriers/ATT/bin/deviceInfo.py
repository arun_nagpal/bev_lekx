#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import pprint
import attApi

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'
programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

# # Get session details for the iccid.
# def getSession(iccid):
#    sessionStart = ''
#    sessionEnd = ''

#    try:
#       url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/sessionInfo'
#       myResponse = requests.get(url,auth=(username,api_key))
#       if(myResponse.ok):
#          jData = json.loads(myResponse.content)
#          if 'dateSessionStarted' in jData:
#             sessionStart = jData['dateSessionStarted']
#          if 'dateSessionEnded' in jData:
#             sessionEnd = jData['dateSessionEnded']
#    except:
#       pass

#    # 2018-03-16 19:15:51.408+0000
#    # session end is less than session start when the SIM is online.
#    online = "no"
#    d1 = datetime.datetime.now() - datetime.timedelta(days=1)
#    if sessionStart != None:
#        try:
#            d1 = datetime.datetime.strptime(sessionStart[0:sessionStart.find('.')], "%Y-%m-%d %H:%M:%S")
#        except:
#            print(sessionStart)
#            d1 = datetime.datetime.now() - datetime.timedelta(days=1)
#    if sessionEnd == None:
#        d2 = d1 - datetime.timedelta(days=1)
#    else:
#        try:
#            d2 = datetime.datetime.strptime(sessionEnd[0:sessionEnd.find('.')], "%Y-%m-%d %H:%M:%S")
#        except:
#            print(sessionEnd)
#            d2 = d1 - datetime.timedelta(days=1)

#    if d1 > d2:
#        online = "yes"

#    return [ sessionStart, sessionEnd, online ]

# Get usage for the selected cycle and iccid.
def getCycleUsage(iccid, cycleStartDate):
   result = 0.0
   usage = 0.0

   try:
      url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/usageInZone?cycleStartDate='+cycleStartDate
      myResponse = requests.get(url,auth=(username,api_key))
      if(myResponse.ok):
         jData = json.loads(myResponse.content)
         if 'deviceCycleUsageInZones' in jData:
            plans = jData['deviceCycleUsageInZones']
            for plan in plans:
               planData = plans[plan]
               usage += planData[0]['dataUsage']
            result = usage/(1024.0*1024.0)
   except:
      result = 0.0

   return result

# Get the current billing period usage for the iccid.
def getMtdUsage(iccid):
   result = 0.0

   try:
      url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/ctdUsages'
      myResponse = requests.get(url,auth=(username,api_key))
      if(myResponse.ok):
         jData = json.loads(myResponse.content)
         if 'ctdDataUsage' in jData:
            result = jData['ctdDataUsage']/(1024.0*1024.0)
   except:
      result = 0.0

   return result

def getDeviceInfo(iccid):
   # Assemble the API URL.
   url = ('https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid)

   year = int(datetime.datetime.now().strftime("%Y"))
   month = int(datetime.datetime.now().strftime("%m"))
   day = int(datetime.datetime.now().strftime("%d"))
   if day >= 19:
      month -= 1

   imsi = ''
   msisdn = ''
   imei = ''
   ipAddress = ''
   status = ''
   dateShipped = ''
   dateAdded = ''
   dateActivated = ''
   accountId = ''
   commPlan = ''
   ratePlan = ''

   # Execute the API call.
   myResponse = requests.get(url, auth=(username,api_key))
   if(myResponse.ok):
      jData = json.loads(myResponse.content)
      # print jData
      imsi = jData['imsi']
      msisdn = jData['msisdn']
      imei = jData['imei']
      ipAddress = jData['fixedIPAddress']
      status = jData['status']
      dateShipped = jData['dateShipped']
      dateAdded = jData['dateAdded']
      dateActivated = jData['dateActivated']
      accountId = jData['accountId']
      commPlan = jData['communicationPlan']
      ratePlan = jData['ratePlan']

   return [imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
           dateActivated, accountId, commPlan, ratePlan]

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Display device infor using the AT&T API.')
parser.add_argument('-i', '--iccid', type=str, default='',
                    required=False, help='Device ICCID.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='ICCID list in CSV format.')
parser.add_argument('-c', '--column', type=int, default=0,
                    required=False, help='Identifier column number in CSV file.')
parser.add_argument('-o', '--outputcsv', action="store_true",
                    required=False, help='Output in CSV format.')
parser.add_argument('-m', '--minimal', action="store_true",
                    required=False, help='Minimal output.')
parser.add_argument('-u', '--usage', action="store_true",
                    required=False, help='Include usage data.')
parser.add_argument('-s', '--skipto', type=str, default='',
                    required=False, help='Skip ahead to ICCID.')
args = parser.parse_args()

skip = False
if args.skipto != None and args.skipto[0:4] == '8901':
    skip = True

if args.outputcsv == True:
    if args.usage:
        print ('iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,' +
               'dateActivated, accountId, commPlan, ratePlan, mtdUsage, lastUsage,' +
               'sessionStart, sessionEnd, online')
    else:
        print ('iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,' +
               'dateActivated, accountId, commPlan, ratePlan, ' +
               'sessionStart, sessionEnd, online')
elif args.minimal == True:
    print ('iccid, ipAddress, status, ratePlan')

programRunning = True
if args.csvfile:
    csvfile = None
    try:
        csvfile = open(args.csvfile, 'rb')
    except:
        print("Error opening \"%s\"") % (args.csvfile)
        sys.exit(1)

    csvreader = csv.reader(csvfile, quotechar="\"")
    for line in csvreader:
        if not programRunning:
            break
        if len(line) < (args.column+1):
            continue
        iccid = line[args.column].strip()
        if iccid == '' or iccid == None or iccid[0:4] != '8901':
            continue

        # Skip ahead to the specified ICCID.
        if skip == True:
            if line[args.column].strip() == args.skipto:
                skip = False
                continue
        if skip == True:
            continue

        # Get device info.
        imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded, dateActivated, accountId, commPlan, ratePlan = getDeviceInfo(iccid)

        # Get session info.
        sessionStart = ''
        sessionEnd = ''
        online = ''
        sessionStart, sessionEnd, online = attApi.getSession(iccid)

        # Get usage.
        mtdUsage = 0.0
        lastUsage = 0.0
        if args.usage:
            if status == 'ACTIVATED':
                # Get the current billing period usage for the device.
                mtdUsage = getMtdUsage(iccid)

                # Get the specified cycle usage for the device.
                lastUsage = getCycleUsage(iccid, '2018-06-19Z')

        if args.outputcsv == True:
            if args.usage:
                print ('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%s,%s,%s') % (
                    iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
                    dateActivated, accountId, commPlan, ratePlan, mtdUsage, lastUsage,
                    sessionStart, sessionEnd, online)
            else:
                print ('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s') % (
                    iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
                    dateActivated, accountId, commPlan, ratePlan,
                    sessionStart, sessionEnd, online)
        elif args.minimal == True:
                print ('%s,%s,%s,%s') % (
                    iccid, ipAddress, status, ratePlan)
        else:
            print ("ICCID ........: %s") % (iccid)
            print ("IMSI .........: %s") % (imsi)
            print ("MSISDN .......: %s") % (msisdn)
            print ("IMEI .........: %s") % (imei)
            print ("IP Address ...: %s") % (ipAddress)
            print ("Status .......: %s") % (status)
            print ("Date shipped .: %s") % (dateShipped)
            print ("Date added ...: %s") % (dateAdded)
            print ("Date activated: %s") % (dateActivated)
            print ("Account ID ...: %s") % (accountId)
            print ("Comm plan ....: %s") % (commPlan)
            print ("Rate plan ....: %s") % (ratePlan)
            if args.usage:
                print ("MTD usage ....: %.3fMB") % (mtdUsage)
                print ("Last month ...: %.3fMB") % (lastUsage)
            print ("Session start : %s") % (sessionStart)
            print ("Session end ..: %s") % (sessionEnd)
            print ("Online .......: %s") % (online)

    csvfile.close()
else:
    iccid = args.iccid
    imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded, dateActivated, accountId, commPlan, ratePlan = getDeviceInfo(iccid)

    # Get session info.
    sessionInfo = attApi.getSession(iccid)
    sessionStart = sessionInfo['sessionStart']
    sessionEnd = sessionInfo['sessionEnd']
    online = sessionInfo['online']

    # Get usage.
    mtdUsage = 0.0
    lastUsage = 0.0
    if args.usage:
        if status == 'ACTIVATED':
            # Get the current billing period usage for the device.
            mtdUsage = getMtdUsage(iccid)

            # Get the specified cycle usage for the device.
            lastUsage = getCycleUsage(iccid, '2018-09-19Z')

    if args.outputcsv == True:
        if args.usage:
            print ('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.2f,%.2f,%s,%s,%s') % (
                iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
                dateActivated, accountId, commPlan, ratePlan,  mtdUsage, lastUsage,
                sessionStart, sessionEnd, online)
        else:
            print ('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s') % (
                iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
                dateActivated, accountId, commPlan, ratePlan,
                sessionStart, sessionEnd, online)
    else:
        print ("ICCID ........: %s") % (iccid)
        print ("IMSI .........: %s") % (imsi)
        print ("MSISDN .......: %s") % (msisdn)
        print ("IMEI .........: %s") % (imei)
        print ("IP Address ...: %s") % (ipAddress)
        print ("Status .......: %s") % (status)
        print ("Date shipped .: %s") % (dateShipped)
        print ("Date added ...: %s") % (dateAdded)
        print ("Date activated: %s") % (dateActivated)
        print ("Account ID ...: %s") % (accountId)
        print ("Comm plan ....: %s") % (commPlan)
        print ("Rate plan ....: %s") % (ratePlan)
        if args.usage:
            print ("MTD usage ....: %.3fMB") % (mtdUsage)
            print ("Last month ...: %.3fMB") % (lastUsage)
        print ("Session start : %s") % (sessionStart)
        print ("Session end ..: %s") % (sessionEnd)
        print ("Online .......: %s") % (online)

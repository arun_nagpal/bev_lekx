#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'


# curl -X PUT --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: Basic c25ldGFwaTplOTJiMTFjNy1iOTAxLTQyM2EtOWRmNS0wZDc0ZGNiYTlmNjYK" -d "{ \"communicationPlan\": \"SecureNet Tech-AT&T-GPRS/Voice/LTE/SMSMO/MT-157-ip\" }" "https://restapi1.jasper.com/rws/api/v1/devices/89011703278204958980"

# curl -X GET --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: Basic c25ldGFwaTplOTJiMTFjNy1iOTAxLTQyM2EtOWRmNS0wZDc0ZGNiYTlmNjYK" "https://restapi1.jasper.com/rws/api/v1/devices/89011703278204958592/sessionInfo"

# Change the comm plan for the iccid.
def changeCommPlan(iccid, commPlan):
   payload = { 'communicationPlan': commPlan }
   try:
      url = 'https://restapi1.jasper.com/rws/api/v1/devices/' + iccid
      #url = 'https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid
      myResponse = requests.put(url, auth=(username,api_key), data=payload)
      print myResponse
      if(myResponse.ok):
         jData = json.loads(myResponse.content)
         print jData
   except:
      pass

   return

def getSession(iccid):
   sessionStart = ''
   sessionEnd = ''

   try:
      url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/sessionInfo'
      #url = 'https://api-iotdevice.att.com/rws/api/v1/devices/'+iccid+'/sessionInfo'
      myResponse = requests.get(url,auth=(username,api_key))
      if(myResponse.ok):
         jData = json.loads(myResponse.content)
         if 'dateSessionStarted' in jData:
            sessionStart = jData['dateSessionStarted']
         if 'dateSessionEnded' in jData:
            sessionEnd = jData['dateSessionEnded']
   except:
      pass

   return [ sessionStart, sessionEnd ]

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Change the comm plan for an AT&T iccid.')
parser.add_argument('-i', '--iccid', type=str, default='',
                    required=True, help='Device ICCID.')
args = parser.parse_args()

#commPlan = 'SecureNet Tech-AT&T-GPRS/Voice/LTE/SMSMO/MT-157-ip'
commPlan = 'SecureNet Tech-AT&T-GPRS/Voice/LTE/SMSMO/MT-157'
#commPlan = 'SecureNet Tech-m2m.com.attz'
#commPlan = 'SecureNet Tech-AT&T-GPRS/Voice/LTE/SMS MO/MT-157'
changeCommPlan(args.iccid, commPlan)
# sessionStart, sessionEnd = getSession(args.iccid)
# print sessionStart, sessionEnd

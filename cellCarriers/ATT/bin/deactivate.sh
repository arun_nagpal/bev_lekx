#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/ATT"
workDir="${carrierDir}/work"
cdate=`date +%Y%m%d`

# Thresholds.
th_delDeleted=180      # Account deleted threshold in days (-t).
th_delActivate=365     # Activated threshold in days for lines with deleted accounts (-a).
th_delLastused=365     # Last used threshold in days for lines with deleted accounts (-l).
th_noacctActivate=365  # Activated threshold for lines without accounts in days (-y).
th_noacctLastused=365  # Last used threshold for lines without accounts in days (-z).

# Print a detailed report of the lines to be deactivated.
${cellDir}/bin/findCellLines.py -d -v -c att \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    > ${workDir}/att-deactivate-report-${cdate}.txt

# Create a list of MEIDs to deactivate.
${cellDir}/bin/findCellLines.py -d -o meid -c att \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/att-deactivate-meid-${cdate}.csv

# Create a list of ICCIDs to deactivate.
${cellDir}/bin/findCellLines.py -d -o iccid -c att \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/att-deactivate-iccid-${cdate}.csv

/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t dinh.duong@securenettech.com \
                      arun.nagpal@securnettech.com \
                   -o tls=no \
                   -u "AT&T cell cancellations" \
                   -m "AT&T cell cancellations for `date +"%B %d, %Y"` are ready."

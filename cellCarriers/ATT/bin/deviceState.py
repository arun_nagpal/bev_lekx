#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal
import pprint

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'
programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

def getDeviceState(iccid):
   # Assemble the API URL.
   url = ('https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid)
   status = ''
   myResponse = requests.get(url, auth=(username,api_key))
   if(myResponse.ok):
      jData = json.loads(myResponse.content)
      # print jData
      status = jData['status']
   return status

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Display device state using the AT&T API.')
parser.add_argument('-i', '--iccid', type=str, default='',
                    required=False, help='Device ICCID.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='ICCID list in CSV format.')
parser.add_argument('-c', '--column', type=int, default=0,
                    required=False, help='Identifier column number in CSV file.')
args = parser.parse_args()

programRunning = True
if args.csvfile:
    csvfile = None
    try:
        csvfile = open(args.csvfile, 'rb')
    except:
        print("Error opening \"%s\"") % (args.csvfile)
        sys.exit(1)

    csvreader = csv.reader(csvfile, quotechar="\"")
    for line in csvreader:
        if not programRunning:
            break
        if len(line) < (args.column+1):
            continue
        iccid = line[args.column].strip()
        if iccid == '' or iccid == None or iccid[0:4] != '8901':
            continue

        # Get device state.
        status = getDeviceState(iccid)
        print("%s %s") % (iccid, status)

    csvfile.close()
else:
    iccid = args.iccid
    status = getDeviceState(iccid)
    print("%s %s") % (iccid, status)

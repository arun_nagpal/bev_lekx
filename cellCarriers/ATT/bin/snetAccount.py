#!/usr/bin/python

#
#

import csv
import requests
import json
import base64
import sqlite3
import mysql.connector
import argparse
import sys
import os
import datetime
import time
import signal

programRunning = True

# Cell usage database parameters.
usageDbCfg = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def searchSnetDbColumn(db, columnName, columnValue):
   curtime = int(time.time())
   rowFound = False
   account_id_new = ''
   partner = ''
   deleted = False

   cursor = db.cursor()
   query = ('SELECT account_id_new, partner FROM snet WHERE ' + columnName + '="' + columnValue + '"')
   cursor.execute(query)
   rows = cursor.fetchall()
   for row in rows:
      # print("Found by %s %s") % (columnName, columnValue)
      account_id_new = row[0]
      partner = row[1]
      rowFound = True
   cursor.close()

   if not rowFound:
      cursor = db.cursor()
      query = ('SELECT account_id_new, partner, deleted_date FROM snet_changes WHERE ' +
               columnName + '="' + columnValue + '"')
      cursor.execute(query)
      rows = cursor.fetchall()
      for row in rows:
         deleted_date = row[2]/1000
         if curtime - deleted_date > (86400 * 90):
            # print("Deleted over 3 months ago: %s %s %d") % (
            #    columnName, columnValue, (curtime-deleted_date)/86400)
            account_id_new = row[0]
            partner = row[1]
            deleted = True
            rowFound = True
      cursor.close()

   return rowFound, account_id_new

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Find cell lines in an snet database.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='ICCID list in CSV format.')
args = parser.parse_args()

usageDb = mysql.connector.connect(**usageDbCfg)

csvfile = None
try:
    csvfile = open(args.csvfile, 'rb')
except:
    print("Error opening \"%s\"") % (args.csvfile)
    sys.exit(1)

csvreader = csv.reader(csvfile, quotechar="\"")
for line in csvreader:
    if not programRunning:
        break
    iccid = line[0]
    ipaddress = line[1]

    rowFound = False
    account_id_new = ''

    # Search for iccid in snet.iccid, snet.phone_number
    if iccid:
        rowFound, account_id_new = searchSnetDbColumn(usageDb, 'iccid', iccid)
    if not rowFound and iccid:
        rowFound, account_id_new = searchSnetDbColumn(usageDb, 'phone_number', iccid)

    # Search for ipaddress in snet.proxy_id
    if not rowFound and ipaddress:
        rowFound, account_id_new = searchSnetDbColumn(usageDb, 'proxy_id', ipaddress)

    print('%s, %s') % (iccid, account_id_new)

    if not programRunning:
        break

usageDb.close()

#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import signal

programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Display device infor using the AT&T API.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=True, help='deviceInfo.py output in CSV format.')
args = parser.parse_args()

programRunning = True

# print ('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%.3fMB,%.3fMB,%s,%s,%s') % (
#     iccid, imsi, msisdn, imei, ipAddress, status, dateShipped, dateAdded,
#     dateActivated, accountId, commPlan, ratePlan, mtdUsage, lastUsage,
#     sessionStart, sessionEnd, online)

try:
    csvfile = open(args.csvfile, 'rb')
except IOError:
    print "Error: \"" + args.csvfile + "\" could not be opened."
    sys.exit(1)

csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
    # print row
    if 'nohup: ignoring input' in row:
        continue

    if len(row) != 17:
        continue

    iccid = row[0]
    ipAddress = row[4]
    status = row[5]
    dateShipped = row[6]
    dateAdded = row[7]
    dateActivated = row[8]
    commPlan = row[10]

    if '10.' not in ipAddress:
        print ("%s,%s,%s,%s,%s,%s,%s") % (
            iccid, ipAddress, status, dateShipped, dateAdded, dateActivated, commPlan)
        # print ("ICCID ........: %s") % (iccid)
        # print ("IP Address ...: %s") % (ipAddress)
        # print ("Status .......: %s") % (status)
        # print ("Date shipped .: %s") % (dateShipped)
        # print ("Date added ...: %s") % (dateAdded)
        # print ("Date activated: %s") % (dateActivated)
        # print ("Comm plan ....: %s") % (commPlan)

csvfile.close()

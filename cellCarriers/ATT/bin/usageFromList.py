#!/usr/bin/python

import sys
import os
import multiprocessing
import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
from suds.client import Client
from suds.sax.element import Element
from suds.sudsobject import asdict
import mysql.connector
import attApi

carrier = 'att'
# # AT&T API credentials.
# username = 'snetapi'
# api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'
programRunning = True
debugOutput = False

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def updateDbCol(usageDb, colName, identifier, usage):
   rowFound = False
   usageCursor = usageDb.cursor()

   query = ('SELECT datausage FROM cellusage WHERE ' + colName + '="' + identifier + '"')
   # print query
   try:
      usageCursor.execute(query)
   except:
      pass

   try:
      usageRows = usageCursor.fetchall()
      # print usageRows
      for usageRow in usageRows:
         rowFound = True
   except:
      pass

   if rowFound:
      # print("Update by %s: %s") % (colName, identifier)
      query = ('UPDATE cellusage set datausage=' + "{:0.2f}".format(usage) +
               ' WHERE ' + colName + '="' + identifier + '"')
      try:
          usageCursor.execute(query)
          usageDb.commit()
      except:
          pass

   usageCursor.close()
   return rowFound

def updateDb(usageDb, identifier, usage):
   rowFound = False
   iccid = ''
   meid = ''

   if len(identifier) == 14:
       meid = identifier
   if len(identifier) == 20:
       iccid = identifier

   if iccid:
      rowFound = updateDbCol(usageDb, 'iccid', iccid, usage)
   if not rowFound and meid:
      rowFound = updateDbCol(usageDb, 'meid', meid, usage)

   if not rowFound:
       print("Added %s %s") % (iccid, meid)
       cursor = usageDb.cursor()
       query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                'imei, mdn, meid, min, ipaddress, rateplan, datausage) '
                'VALUES ("' + carrier + '", "", "' + iccid + '", "", "", "", "", ' +
                '"' + meid + '", "", "", "", ' + "{:0.2f}".format(usage) + ')')
       cursor.execute(query)
       usageDb.commit()
       cursor.close()
       rowFound = True

   return rowFound

def handleOneIccid(iccid, count):
    cnx = mysql.connector.connect(**config)
    usage = attApi.getMtdUsage(iccid)
    updateDb(cnx, iccid, usage)
    print("%d, %s, %.3f") % (count, iccid, usage)
    cnx.close()

def checkProcs(procList):
    ndx = 0
    while ndx < len(procList):
        p = procList[ndx]
        if not p.is_alive():
            p.join()
            procList.remove(p)
        ndx += 1
    runningProc = ndx
    return runningProc

# Command-line arguments.
parser = argparse.ArgumentParser(description='Obtain AT&T usage.')
parser.add_argument('-k', '--kind', type=str, default='',
                    required=True, help='Identifier type.')
parser.add_argument('-f', '--csvfile', type=str, default='',
                    required=False, help='CSV file containing identifiers')
args = parser.parse_args()

maxThreads = 10
plist = []
count = 0
csvfile = open(args.csvfile, 'rb')
for line in csvfile:
    iccid = line.strip()
    count += 1

    if iccid[0:5] != '89011':
        print("Invalid ICCID for AT&T: %s" % (iccid))
        continue

    # Check for completed processes. Wait until running process count is below
    # the number specified on the command line.
    runningProc = maxThreads + 1
    while runningProc >= maxThreads:
        runningProc = checkProcs(plist)
        time.sleep(.01)

    # Start a new process.
    p = multiprocessing.Process(target=handleOneIccid, args=((iccid, count,)))
    p.start()
    plist.append(p)

# Ensure all processes are stopped before exiting.
runningProc = 1
while runningProc > 0:
    runningProc = checkProcs(plist)
    time.sleep(.5)

csvfile.close()

#!/usr/bin/python

import requests
import json
import datetime
import time

carrier = 'att'

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'

# Get session details for the iccid.
def getSession(iccid):
    sessionInfo = {}
    sessionInfo['sessionStart'] = ''
    sessionInfo['sessionEnd'] = ''
    sessionInfo['online'] = ''

    attempts = 0
    success = False
    myResponse = None
    while not success and attempts < 5:
        try:
            url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/sessionInfo'
            myResponse = requests.get(url,auth=(username,api_key))
            if(myResponse.ok):
                jData = json.loads(myResponse.content)
                if 'dateSessionStarted' in jData:
                    sessionInfo['sessionStart'] = jData['dateSessionStarted']
                if 'dateSessionEnded' in jData:
                    sessionInfo['sessionEnd'] = jData['dateSessionEnded']
                success = True
        except:
            pass
        attempts += 1
        if not success:
            time.sleep(5)

    # 2018-03-16 19:15:51.408+0000
    # session end is less than session start when the SIM is online.
    sessionInfo['online'] = 'no'
    sstart = sessionInfo['sessionStart']
    send = sessionInfo['sessionEnd']
    d1 = datetime.datetime.now() - datetime.timedelta(days=1)
    if sstart != None:
        try:
            d1 = datetime.datetime.strptime(sstart[0:sstart.find('.')], "%Y-%m-%d %H:%M:%S")
        except:
            # print(sstart)
            d1 = datetime.datetime.now() - datetime.timedelta(days=1)
    if send == None:
        d2 = d1 - datetime.timedelta(days=1)
    else:
        try:
            d2 = datetime.datetime.strptime(send[0:send.find('.')], "%Y-%m-%d %H:%M:%S")
        except:
            # print(send)
            d2 = d1 - datetime.timedelta(days=1)

    if d1 > d2:
        sessionInfo['online'] = 'yes'

    return sessionInfo

# This function retrieves devices from AT&T in pages of 50 devices each page.
def iccidsFromApi(pageNumber):
    lastPage = False
    iccids = []

    # Execute the API call.
    attempts = 0
    success = False
    myResponse = None
    while not success and attempts < 5:
        try:
            url = ('https://api-iotdevice.att.com/rws/api/v1/devices?accountId=100617302&'
                   'modifiedSince=1900-01-01T00%3A00%3A00%2B00%3A00&'
                   'pageSize=5000&pageNumber=' + str(pageNumber))
            myResponse = requests.get(url, auth=(username, api_key))
            if(myResponse.ok):
                success = True
        except:
            pass
        attempts += 1
        if not success:
            time.sleep(5)

    if success:
        # The reponse is a json-formatted message containing the "lastPage" boolean
        # flag, and an array of devices.
        jData = json.loads(myResponse.content)
        if 'lastPage' in jData:
            lastPage = jData['lastPage']

        if 'devices' in jData:
            for device in jData['devices']:
                iccids.append(device['iccid'])

    return lastPage, iccids

# Get the IMSI, MSISDN, IMEI and IP address of the device.
def getDeviceDetails(iccid):
    result = {}
    result['imsi'] = ''
    result['msisdn'] = ''
    result['imei'] = ''
    result['ipAddress'] = ''
    result['status'] = ''
    result['dateShipped'] = ''
    result['dateAdded'] = ''
    result['dateActivated'] = ''
    result['accountId'] = ''
    result['commPlan'] = ''
    result['ratePlan'] = ''
    result['success'] = False

    # Execute the API call.
    attempts = 0
    success = False
    myResponse = None
    while not success and attempts < 5:
        try:
            url = ('https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid)
            myResponse = requests.get(url, auth=(username, api_key))
            if(myResponse.ok):
                success = True
        except:
            pass
        attempts += 1
        if not success:
            time.sleep(5)

    if success:
        jData = json.loads(myResponse.content)
        result['imsi'] = jData['imsi']
        result['msisdn'] = jData['msisdn']
        result['imei'] = jData['imei']
        result['ipAddress'] = jData['fixedIPAddress']
        result['status'] = jData['status']
        result['dateShipped'] = jData['dateShipped']
        result['dateAdded'] = jData['dateAdded']
        result['dateActivated'] = jData['dateActivated']
        result['accountId'] = jData['accountId']
        result['commPlan'] = jData['communicationPlan']
        result['ratePlan'] = jData['ratePlan']
        result['success'] = True

    return result

# Get the current billing period usage for the iccid.
def getMtdUsage(iccid):
    result = 0.0

    attempts = 0
    success = False
    myResponse = None
    while not success and attempts < 5:
        try:
            url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/ctdUsages'
            myResponse = requests.get(url, auth=(username,api_key))
            if(myResponse.ok):
                jData = json.loads(myResponse.content)
                if 'ctdDataUsage' in jData:
                    result = jData['ctdDataUsage']/(1024.0*1024.0)
                success = True
        except:
            pass
        attempts += 1
        if not success:
            time.sleep(5)

    return result

# Get usage for the selected cycle and iccid.
def getCycleUsage(iccid, cycleStartDate):
    result = 0.0

    attempts = 0
    success = False
    myResponse = None
    while not success and attempts < 5:
        try:
            url = 'https://restapi1.jasper.com/rws/api/v1/devices/'+iccid+'/usageInZone?cycleStartDate='+cycleStartDate
            myResponse = requests.get(url,auth=(username,api_key))
            if(myResponse.ok):
                jData = json.loads(myResponse.content)
                if 'deviceCycleUsageInZones' in jData:
                    for dcuz in jData['deviceCycleUsageInZones']:
                        a = jData['deviceCycleUsageInZones'][dcuz]
                        for k in a:
                            result += k['dataUsage']
                            if k['dataUsageUnit'] != 'bytes':
                                print("Error: unknown data units")
                                sys.exit(1)
                            result = result/(1024.0*1024.0)
                success = True
        except:
            pass
        attempts += 1
        if not success:
            time.sleep(5)

    return result

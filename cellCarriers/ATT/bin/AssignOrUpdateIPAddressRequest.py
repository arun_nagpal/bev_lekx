#!/usr/bin/python

import csv
import requests
import json
import base64
import sqlite3
import datetime
import time
import argparse
import sys
import os
import pprint

# AT&T API credentials.
username = 'snetapi'
api_key = 'e92b11c7-b901-423a-9df5-0d74dcba9f66'

# Change the status for the iccid.
def changeRatePlan(iccid, rateplan):
   payload = { 'ratePlan': rateplan }
   url = 'https://api-iotdevice.att.com/rws/api/v1/devices/' + iccid
   myResponse = requests.put(url, auth=(username,api_key), json=payload)
   if(myResponse.ok):
      jData = json.loads(myResponse.content)
      pp=pprint.PrettyPrinter(indent=4)
      pp.pprint(jData)
   else:
      myResponse.raise_for_status()

   return

# Command-line arguments.
parser = argparse.ArgumentParser(description='Change the rate plan for one or more AT&T ICCID(s).',
                                 add_help=False)
parser.add_argument('-i', '--iccid', type=str, default='', required=False)
parser.add_argument('-f', '--file', type=str, default='', required=False)
parser.add_argument('-r', '--rateplan', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -i, --iccid    Securenet account number (required).")
   print("       -f, --file     Cloud environment (default = cloud).")
   print("       -s, --state    Set the ICCID(s) to this state.")
   print("                         ACTIVATED")
   print("                         DEACTIVATED")
   print("       -h, -?, --help     Display this helpful message.")
   sys.exit(0)

if len(args.iccid) == 0 and len(args.file) == 0:
   print("An \"iccid\" or \"file\" argument is required.")
   sys.exit(1)
if len(args.rateplan) == 0:
   print("A \"rateplan\" argument is required.")
   sys.exit(1)
# if args.rateplan != 'ACTIVATED' and args.rateplan != 'DEACTIVATED':
#    print("Invalid state argument.")
#    sys.exit(1)

if len(args.iccid) > 0:
   #changeRatePlan(args.iccid, args.rateplan)
   #AssignOrUpdateIPAddressRequest(args.iccid,apn,ipaddress,pdpId)
elif len(args.file) > 0:
   csvfile = open(args.file, 'rb')
   for line in csvfile:
      iccid = line.strip()
      changeRatePlan(iccid, args.rateplan)
   csvfile.close()

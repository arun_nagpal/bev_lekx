#!/usr/bin/python

# AT&T billing cycle ends on the 18th of the month.

import signal
import sys
import os
import multiprocessing
import tempfile
import time
import datetime
import calendar
import argparse
import mysql.connector
import attApi

carrier = 'att'

# Database parameters.
config = {
    'user': 'bev_lekx',
    'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
    'host': '172.29.253.45',
    'database': 'celldata',
    'raise_on_warnings': False,
}

programRunning = True

def handleOneIccid(iccid, count, dbUpdates, dbInserts, dbErrors):
    # Device details.
    deviceDetails = attApi.getDeviceDetails(iccid)
    if not deviceDetails['success']:
        with dbErrors.get_lock():
            print("Error getting device details: %s") % (iccid)
            dbErrors.value += 1
        return

    # Last used date.
    last_used = 0
    sessionInfo = attApi.getSession(iccid)
    if sessionInfo['sessionEnd'] != None:
        se = sessionInfo['sessionEnd']
        last_used = int(calendar.timegm(
            time.strptime(se[0:se.find('.')], '%Y-%m-%d %H:%M:%S')))

    # Activation date.
    activation_date = 0
    if deviceDetails['dateActivated'] != None:
        da = deviceDetails['dateActivated']
        activation_date = int(calendar.timegm(
            time.strptime(da[0:da.find('.')], '%Y-%m-%d %H:%M:%S')))

    # Usage.
    mtdUsage = 0.0
    if deviceDetails['status'] == 'ACTIVATED':
        mtdUsage = attApi.getMtdUsage(iccid)

    # Update the usage table.
    rowFound = False
    cnx = mysql.connector.connect(**config)
    query = ('select count(*) from cellusage where iccid="{iccid}"' . format(iccid=iccid))
    try:
        cursor = cnx.cursor()
        cursor.execute(query)
        dbRows = cursor.fetchall()
        for dbRow in dbRows:
            if dbRow[0] > 0:
                rowFound = True
        cursor.close()
    except:
        cnx.close()
        with dbErrors.get_lock():
            dbErrors.value += 1
        print("Error getting cellusage count: %s (%d)") % (iccid, count)
        return

    # print(deviceDetails)

    if rowFound:
        with dbUpdates.get_lock():
            dbUpdates.value += 1
        query = ('UPDATE cellusage SET '
                 'carrier="{carrier}", state="{state}", imsi="{imsi}", msisdn="{msisdn}", '
                 'imei="{imei}", mdn="", meid="", min="", ipaddress="{ipaddress}", '
                 'rateplan="{rateplan}", datausage={datausage}, last_used={last_used}, '
                 'activation_date={activation_date} '
                 'WHERE iccid="{iccid}"' . format(
                     carrier=carrier,
                     state=deviceDetails['status'],
                     imsi=deviceDetails['imsi'],
                     msisdn=deviceDetails['msisdn'],
                     imei=deviceDetails['imei'],
                     ipaddress=deviceDetails['ipAddress'],
                     rateplan=deviceDetails['ratePlan'],
                     datausage="{:0.2f}".format(mtdUsage),
                     last_used=last_used,
                     activation_date=activation_date,
                     iccid=iccid
                 ))
    else:
        with dbInserts.get_lock():
            dbInserts.value += 1
        query = ('INSERT INTO cellusage (carrier, state, imsi, msisdn, '
                 'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                 'activation_date, iccid) '
                 'VALUES ("{carrier}", "{state}", "{imsi}", "{msisdn}", '
                 '"{imei}", "", "", "", "{ipaddress}", "{rateplan}", '
                 '{datausage}, {last_used}, {activation_date}, "{iccid}")' . format(
                     carrier=carrier,
                     state=deviceDetails['status'],
                     imsi=deviceDetails['imsi'],
                     msisdn=deviceDetails['msisdn'],
                     imei=deviceDetails['imei'],
                     ipaddress=deviceDetails['ipAddress'],
                     rateplan=deviceDetails['ratePlan'],
                     datausage="{:0.2f}".format(mtdUsage),
                     last_used=last_used,
                     activation_date=activation_date,
                     iccid=iccid
                 ))
    try:
        cursor = cnx.cursor()
        cursor.execute(query)
        cnx.commit()
        cursor.close()
    except:
        print("Error in db query: %s") % (query)
        with dbErrors.get_lock():
            dbErrors.value += 1
        pass

    cnx.close()

    print("%d: %s") % (count, iccid)

def getIccids(startPage, numPages):
    count = 0
    inserts = 0
    errors = 0
    pageNumber = 1
    lastPage = False

    while (not lastPage) and ((pageNumber < startPage + numPages) or (numPages == 0)):
        lastPage, iccids = attApi.iccidsFromApi(pageNumber)
        print("Page: %d (%d)") % (pageNumber, len(iccids))
        pageNumber += 1

        db = mysql.connector.connect(**config)

        for iccid in iccids:
            count += 1
            query = ('select count(*) from cellusage where iccid="{iccid}"' . format(iccid=iccid))
            try:
                cursor = db.cursor()
                cursor.execute(query)
                dbRows = cursor.fetchall()
                rowFound = False
                for dbRow in dbRows:
                    if dbRow[0] > 0:
                        rowFound = True
                cursor.close()
            except:
                errors += 1
                pass

            if not rowFound:
                inserts += 1
                query = ('INSERT INTO cellusage (carrier, iccid, state, imsi, msisdn, '
                        'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                        'activation_date) '
                        'VALUES ("{carrier}", "{iccid}", "", "", "", "", "", "", "", '
                        '"", "", 0, 0, 0)' .
                        format(carrier=carrier, iccid=iccid))
                try:
                    cursor = db.cursor()
                    cursor.execute(query)
                    db.commit()
                    cursor.close()
                except:
                    print("Error: %s") % (query)
                    errors += 1
                    pass
            if not programRunning:
                break

        db.close()
        if not programRunning:
            break

    print("Total .: %d") % (count)
    print("Inserts: %d") % (inserts)
    print("Errors : %d") % (errors)

def checkProcs(procList):
    ndx = 0
    while ndx < len(procList):
        p = procList[ndx]
        if not p.is_alive():
            p.join()
            procList.remove(p)
        ndx += 1
    runningProc = ndx
    return runningProc

def sigint_handler(signum, frame):
    # pylint: disable=unused-argument, missing-docstring
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Capture month-to-date AT&T usage for all devices.',
                                 add_help=False)
parser.add_argument('-s', '--skip', action='store_true')
parser.add_argument('-t', '--threads', type=int, default=1)
parser.add_argument('-p', '--page', type=int, default=1)
parser.add_argument('-n', '--numpages', type=int, default=0)
parser.add_argument('-v', '--verbose', action='store_true')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--help2', action='store_true')
args = parser.parse_args()

if args.help or args.help2:
    print("%s") % (os.path.basename(sys.argv[0]))
    print("Usage: -s, --skip         Skip retrival of ICCIDs (default=false).")
    print("       -t, --threads      Number of threads (default=1).")
    print("       -p, --page         Start with page number (default=1).")
    print("       -n, --numpages     Number of pages (default=0).")
    print("       -v, --verbose      Verbose output.")
    print("       -h, -?, --help     Display this helpful message.")
    sys.exit(0)

print("Start time: %s") % (datetime.datetime.now())

# These are "ctypes" shared objects. They are declared in this way so they
# can be shared with the child processes.
# https://docs.python.org/2/library/ctypes.html#module-ctypes
progStats_dbUpdates = multiprocessing.Value('i', 0)
progStats_dbInserts = multiprocessing.Value('i', 0)
progStats_dbErrors = multiprocessing.Value('i', 0)
# progStats_counter = multiprocessing.Value('i', 0)
count = 0

# Process list.
plist = []

# Get ICCIDs. This is a single-threaded function.
if not args.skip:
    getIccids(args.page, args.numpages)
    print("Get ICCIDs via API complete: %s") % (datetime.datetime.now())

try:
    fh = tempfile.TemporaryFile()
except:
    print("Error creating tmp file.")
    db.close()
    sys.exit(1)

# Write all ICCIDs to a tmp file.
db = mysql.connector.connect(**config)
query = ('SELECT iccid FROM cellusage WHERE carrier="{carrier}"' . format(carrier=carrier))
cursor = db.cursor()
cursor.execute(query)
dbRow = cursor.fetchone()
while dbRow is not None:
    fh.write('{iccid}\n' .format(iccid=dbRow[0]))
    dbRow = cursor.fetchone()
    count += 1
    if count % 5000 == 0:
        print("Writing ICCIDs: %d") % (count)
cursor.close()
db.close()
print("Writing ICCIDs: %d") % (count)

# Rewind to the beginning of the tmp file.
fh.seek(0)

# For each AT&T ICCID in the database:
# 1. Get device details for each ICCID.
# 2. Get last_used for each ICCID.
# 3. Get MTD usage for each ICCID.
count = 0
for fline in fh:
    iccid = fline.strip()
    count += 1

    # Check for completed processes. Wait until running process count is below
    # the number specified on the command line.
    runningProc = args.threads + 1
    while runningProc >= args.threads:
        runningProc = checkProcs(plist)
        time.sleep(.01)

    # Start a new process.
    p = multiprocessing.Process(target=handleOneIccid,
                                args=((iccid, count, progStats_dbUpdates, progStats_dbInserts,
                                       progStats_dbErrors,)))
    p.start()
    plist.append(p)

    if not programRunning:
        break

# Close and delete the tmp file.
fh.close()

# Ensure all processes are stopped before exiting.
runningProc = 1
while runningProc > 0:
    runningProc = checkProcs(plist)
    time.sleep(.5)

print(("ICCIDs: %d, updates: %d, inserts: %d, errors: %d") %
      (count, progStats_dbUpdates.value, progStats_dbInserts.value,
       progStats_dbErrors.value))

print("Stop time: %s") % (datetime.datetime.now())

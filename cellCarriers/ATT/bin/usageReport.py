#!/usr/bin/python

#
#
# mysql> describe snet;
# +----------------+--------------+------+-----+---------+----------------+
# | Field          | Type         | Null | Key | Default | Extra          |
# +----------------+--------------+------+-----+---------+----------------+
# | id             | int(11)      | NO   | PRI | NULL    | auto_increment |
# | partner        | varchar(255) | YES  |     | NULL    |                |
# | account_id_new | varchar(255) | YES  |     | NULL    |                |
# | vendor_id      | varchar(255) | YES  | MUL | NULL    |                |
# | vendor_id_2    | varchar(255) | YES  | MUL | NULL    |                |
# | proxy_id       | varchar(255) | YES  | MUL | NULL    |                |
# | imei           | varchar(255) | YES  | MUL | NULL    |                |
# | iccid          | varchar(255) | YES  | MUL | NULL    |                |
# | phone_number   | varchar(255) | YES  | MUL | NULL    |                |
# | panel_type     | varchar(255) | YES  |     | NULL    |                |
# +----------------+--------------+------+-----+---------+----------------+
#
# mysql> describe cellusage;
# +-----------------+--------------+------+-----+---------+----------------+
# | Field           | Type         | Null | Key | Default | Extra          |
# +-----------------+--------------+------+-----+---------+----------------+
# | id              | int(11)      | NO   | PRI | NULL    | auto_increment |
# | carrier         | varchar(255) | YES  | MUL | NULL    |                |
# | state           | varchar(255) | YES  | MUL | NULL    |                |
# | iccid           | varchar(255) | YES  | MUL | NULL    |                |
# | imsi            | varchar(255) | YES  | MUL | NULL    |                |
# | msisdn          | varchar(255) | YES  | MUL | NULL    |                |
# | imei            | varchar(255) | YES  | MUL | NULL    |                |
# | mdn             | varchar(255) | YES  | MUL | NULL    |                |
# | meid            | varchar(255) | YES  | MUL | NULL    |                |
# | min             | varchar(255) | YES  | MUL | NULL    |                |
# | ipaddress       | varchar(255) | YES  | MUL | NULL    |                |
# | rateplan        | varchar(255) | YES  |     | NULL    |                |
# | datausage       | double       | YES  |     | 0       |                |
# | last_used       | bigint(20)   | YES  |     | NULL    |                |
# | activation_date | bigint(20)   | YES  |     | NULL    |                |
# +-----------------+--------------+------+-----+---------+----------------+

import csv
import requests
import json
import base64
import sqlite3
import mysql.connector
import argparse
import sys
import os
import datetime
import time
import unicodedata, re
import xlsxwriter

carrier = 'att'

# Tools database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

# # Cloud database parameters.
# config = {
#    'user': 'sn_notifier',
#    'password': '5n_n0t1f13r',
#    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
#    'port': 3306,
#    'database': 'securenet_6',
#    'raise_on_warnings': False,
# }

colNames = [ 'Account', 'Partner', 'ICCID', 'IMSI', 'IP Address', 'Panel Type',
             'State', 'MTD Usage' ]
colWidths = [ 10, 10, 23, 18, 16, 14, 12, 12 ]

def searchSnetDbColumn(db, columnName, columnValue):
   rowFound = False
   account_id_new = ''
   partner = ''
   panel_type = ''

   cursor = db.cursor()
   query = ('SELECT account_id_new, partner, panel_type FROM snet WHERE ' + columnName + '="' + columnValue + '"')
   cursor.execute(query)
   rows = cursor.fetchall()
   for row in rows:
      # print("Found by %s %s") % (columnName, columnValue)
      account_id_new = row[0]
      partner = row[1]
      panel_type = row[2]
      rowFound = True
   cursor.close()

   return rowFound, account_id_new, partner, panel_type

# Start the output file.
dateString = time.strftime("%B %d, %Y", time.gmtime())
baseFileName = 'AttUsageReport-' + time.strftime("%Y%m%d%H%M%S", time.gmtime())
xlsxFileName = baseFileName + '.xlsx'
xlsxFile = open(xlsxFileName, 'w')
xlsxRow = 0

# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook(xlsxFileName)
worksheet = workbook.add_worksheet()
xlsxCol = 0
for colName in colNames:
   worksheet.write(xlsxRow, xlsxCol, colName)
   worksheet.set_column(xlsxRow, xlsxCol, colWidths[xlsxCol])
   xlsxCol += 1
xlsxRow += 1
counter = 0

#
usageDb = None
try:
   usageDb = mysql.connector.connect(**config)
except:
   print("Error connecting to the database.")
   sys.exit(1)

query = ('select state, '
         'iccid, '
         'imsi, '
         'msisdn, '
         'imei, '
         'mdn, '
         'meid, '
         'min, '
         'ipaddress, '
         'rateplan, '
         'datausage, '
         'last_used, '
         'activation_date '
         'from cellusage '
         'where carrier="att" '
         'order by datausage desc')

cursor = usageDb.cursor()
try:
   cursor.execute(query)
except:
   print("Query error.")
   print("%s") % (query)
   usageDb.close()
   sys.exit(1)
rows = cursor.fetchall()

for row in rows:
   state = row[0]
   iccid = row[1]
   imsi = row[2]
   msisdn = row[3]
   imei = row[4]
   mdn = row[5]
   meid = row[6]
   dmin = row[7]
   ipaddress = row[8]
   rateplan = row[9]
   datausage = row[10]
   last_used = row[11]
   activation_date = row[12]

   # Ignore any lines that are not active.
   if state != 'ACTIVATED':
      continue

   rowFound = False
   account_id_new = ''
   partner = ''
   panel_type = ''
   rmeid = meid
   if meid[0:2] == 'A1':
      rmeid = meid[2:]

   # Search for iccid in snet.iccid, snet.phone_number
   if iccid:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'iccid', iccid)
   if not rowFound and iccid:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', iccid)

   # Search for imei in snet.imei, snet.phone_number
   if not rowFound and imei:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'imei', imei)
   if not rowFound and imei:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', imei)

   # Search for ipaddress in snet.proxy_id
   if not rowFound and ipaddress:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'proxy_id', ipaddress)

   # Search for meid in snet.vendor_id
   if not rowFound and meid:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'vendor_id', rmeid)

   # Search for imsi in snet.iccid, snet.phone_number, snet.vendor_id
   if not rowFound and imsi:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'iccid', imsi)
   if not rowFound and imsi:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', imsi)
   if not rowFound and imsi:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'vendor_id', imsi)

   # Search for msisdn in snet.iccid, snet.phone_number, snet.vendor_id
   if not rowFound and msisdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'iccid', msisdn)
   if not rowFound and msisdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', msisdn)
   if not rowFound and msisdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'vendor_id', msisdn)

   # Search for mdn in snet.iccid, snet.phone_number, snet.vendor_id
   if not rowFound and mdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'iccid', mdn)
   if not rowFound and mdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', mdn)
   if not rowFound and mdn:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'vendor_id', mdn)

   # Search for dmin in snet.iccid, snet.phone_number, snet.vendor_id
   if not rowFound and dmin:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'iccid', dmin)
   if not rowFound and dmin:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'phone_number', dmin)
   if not rowFound and dmin:
      rowFound, account_id_new, partner, panel_type = searchSnetDbColumn(usageDb, 'vendor_id', dmin)

   # print('%s, %s, %s, %s, %s, %s, %s, %.3f') % (
   #       account_id_new, partner, iccid, imsi, ipaddress, panel_type, state,
   #       datausage)
   counter += 1
   if counter % 500 == 0:
      print counter

   # Write the worksheet data.
   worksheet.write(xlsxRow, 0, account_id_new)
   worksheet.write(xlsxRow, 1, partner)
   worksheet.write(xlsxRow, 2, iccid)
   worksheet.write(xlsxRow, 3, imsi)
   worksheet.write(xlsxRow, 4, ipaddress)
   worksheet.write(xlsxRow, 5, panel_type)
   worksheet.write(xlsxRow, 6, state)
   worksheet.write(xlsxRow, 7, "{:0.3f}".format(datausage))
   xlsxRow += 1

# Close the workbook.
workbook.close()

cursor.close()
usageDb.close()

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/Numerex"
workDir="${carrierDir}/work"
cdate=`date +%Y%m%d`


# Print a detailed report of the lines to be deactivated.
${cellDir}/bin/findCellLines.py -d -v -c numerex > \
    ${workDir}/numerex-deactivate-report-${cdate}.txt

# Create a list of lines to deactivate.
${cellDir}/bin/findCellLines.py -d -o iccid -c numerex | sort | uniq > \
    ${workDir}/numerex-deactivate-${cdate}.csv

# Send an email.
/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t arun.nagpal@securenettech.com \
                   -o tls=no \
                   -u "Numerex cell cancellations" \
                   -m "Numerex cell cancellations for `date +"%B %d, %Y"` are ready."

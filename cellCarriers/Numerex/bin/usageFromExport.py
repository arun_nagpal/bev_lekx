#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'numerex'

programRunning = True

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import Numerex usage spreadsheet into numerex.db.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Usage spreadsheet in csv format.')
args = parser.parse_args()

print datetime.datetime.today()

# Connect to the database.
cnx = mysql.connector.connect(**config)

# ICCID,ESN,IMSI,IMEI,MSISDN,Device Name,Device Details,Status,Rate Plan,Effective Date,NMRX MSISDN,MTD Usage (KB),MTD Usage (SMS),Carrier,IP Address
# ,Session Status
# ="8901640110238111581F",,310640101811158,,1011696408,,,DEACTIVATED,,12/11/2017 6:12:02 PM,,0,0,Numerex,,Inactive

# This loop iterates through the csv file downloaded from the Numerex portal.
# The data usage value is stored in the database along with the iccid.
try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

count = 0
csvreader = csv.reader(csvfile, quotechar="\"")
for row in csvreader:
    iccid = row[0].strip().replace('=','').replace('"','')
    if iccid == 'ICCID':
        continue

    imsi = row[2].strip()
    imei = row[3].strip()
    msisdn = row[4].strip()
    usage = float(row[11].replace(',','')) / 1024.0
    status = row[7].strip()
    ratePlan = row[8].strip()
    ipAddress = row[14].strip()

    dmin = ''
    meid = ''
    mdn = ''
    count += 1

    rowFound = False
    query = ('select count(*) from cellusage where iccid="{iccid}" and carrier="{carrier}"' .
             format(iccid=iccid, carrier=carrier))
    cursor = cnx.cursor()
    cursor.execute(query)
    dbRows = cursor.fetchall()
    for dbRow in dbRows:
        if dbRow[0] > 0:
            rowFound = True
    cursor.close()

    if rowFound:
        query = ('UPDATE cellusage SET '
                 'state="{state}", imsi="{imsi}", msisdn="{msisdn}", '
                 'imei="{imei}", mdn="", meid="", min="", ipaddress="{ipaddress}", '
                 'rateplan="{rateplan}", datausage={datausage}, last_used=0, '
                 'activation_date=0 '
                 'WHERE iccid="{iccid}" and carrier="{carrier}"' . format(
                     state=status,
                     imsi=imsi,
                     msisdn=msisdn,
                     imei=imei,
                     ipaddress=ipAddress,
                     rateplan=ratePlan,
                     datausage="{:0.2f}".format(usage),
                     iccid=iccid,
                     carrier=carrier
                 ))
    else:
        query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                 'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                 'activation_date, iccid) '
                 'VALUES ("{carrier}", "{state}", "{imsi}", "{msisdn}", '
                 '"{imei}", "", "", "", "{ipaddress}", "{rateplan}", '
                 '{datausage}, 0, 0, "{iccid}"' . format(
                     carrier=carrier,
                     state=status,
                     imsi=imsi,
                     msisdn=msisdn,
                     imei=imei,
                     ipaddress=ipAddress,
                     rateplan=ratePlan,
                     datausage="{:0.2f}".format(usage),
                     iccid=iccid
                 ))
    # print query
    cursor = cnx.cursor()
    cursor.execute(query)
    cursor.close()
    if not programRunning:
        break

    if count % 500 == 0:
        print(count)
        cnx.commit()

    # print("%d %s %s %s %s %.3f %s %s") % (count, iccid, imsi, imei, ratePlan, usage, status, str(rowFound))

print(count)
cnx.commit()
cnx.close()

print datetime.datetime.today()

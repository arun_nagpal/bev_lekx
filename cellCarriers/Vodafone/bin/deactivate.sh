#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/Vodafone"
workDir="${carrierDir}/work"
exportsDir="${carrierDir}/exports"
cdate=`date +%Y%m%d`

# Thresholds.
th_delDeleted=180      # Account deleted threshold in days (-t).
th_delActivate=365     # Activated threshold in days for lines with deleted accounts (-a).
th_delLastused=365     # Last used threshold in days for lines with deleted accounts (-l).
th_noacctActivate=365  # Activated threshold for lines without accounts in days (-y).
th_noacctLastused=365  # Last used threshold for lines without accounts in days (-z).

# Print a detailed report of the lines to be deactivated.
${cellDir}/bin/findCellLines.py -c vodafone -d -v \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    > ${workDir}/vodafone-deactivate-report-${cdate}.txt

# Generate the list of lines to deactivate.
${cellDir}/bin/findCellLines.py -d -o imsi -c vodafone \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/vodafone-deactivate-${cdate}.csv

# Create the deactivation batch files.
${cellDir}/Vodafone/bin/createBatchFile.py -f ${workDir}/vodafone-deactivate-${cdate}.csv

Send an email.
/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t dinh.duong@securenettech.com \
                      arun.nagpal@securnettech.com \
                   -o tls=no \
                   -u "Vodafone cell cancellations" \
                   -m "Vodafone cell cancellations for `date +"%B %d, %Y"` are ready."

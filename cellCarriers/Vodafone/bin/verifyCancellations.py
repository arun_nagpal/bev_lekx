#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import calendar
import argparse
import sys
import os
# import signal
import mysql.connector
import subprocess

carrier = 'vodafone'

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

snetDir = '/home/bev_lekx/cellCarriers/SecureNet'

def searchDb(db, iccid):
   state = ''
   imsi = ''
   msisdn = ''
   imei = ''
   mdn = ''
   meid = ''
   dmin = ''
   ipaddress = ''
   last_used = 0
   activation_date = 0

   query = ('SELECT state, imsi, msisdn, imei, mdn, meid, min, ipaddress, last_used, '
            'activation_date FROM cellusage WHERE iccid="' + iccid + '"')
   cursor = db.cursor()
   cursor.execute(query)
   dbRows = cursor.fetchall()
   for dbRow in dbRows:
      state = dbRow[0]
      imsi = dbRow[1]
      msisdn = dbRow[2]
      imei = dbRow[3]
      mdn = dbRow[4]
      meid = dbRow[5]
      dmin = dbRow[6]
      ipaddress = dbRow[7]
      last_used = dbRow[8]
      activation_date = dbRow[9]
   cursor.close()

   return state, imsi, msisdn, imei, mdn, meid, dmin, ipaddress, last_used, activation_date

def searchSnet(iccid, imsi, msisdn, imei, ipaddress):
    found = False
    files = os.listdir('/home/bev_lekx/cellCarriers/SecureNet')
    for fileName in files:
        if fileName[-4:] != '.csv':
            continue
        if fileName[-12:] == '-deleted.csv':
            continue

        with open('/home/bev_lekx/cellCarriers/SecureNet/' + fileName) as f:
            for line in f:
                lineFields = line.strip().split('\t')
                for field in lineFields:
                    if len(iccid) > 0 and iccid == field:
                        found = True
                    if len(ipaddress) > 0 and ipaddress == field:
                        found = True
                    if len(imsi) > 0 and imsi == field:
                        found = True
                    if len(msisdn) > 0 and msisdn == field:
                        found = True
                    if len(imei) > 0 and imei == field:
                        found = True
                    if found:
                        break
                if found:
                    # print lineFields
                    break
        if found:
            break

    return found

def searchSnetDeleted(iccid, imsi, msisdn, imei, ipaddress):
    found = False
    files = os.listdir('/home/bev_lekx/cellCarriers/SecureNet')
    for fileName in files:
        if fileName[-12:] != '-deleted.csv':
            continue

        with open('/home/bev_lekx/cellCarriers/SecureNet/' + fileName) as f:
            for line in f:
                lineFields = line.strip().split('\t')
                for field in lineFields:
                    if len(iccid) > 0 and iccid == field:
                        found = True
                    if len(ipaddress) > 0 and ipaddress == field:
                        found = True
                    if len(imsi) > 0 and imsi == field:
                        found = True
                    if len(msisdn) > 0 and msisdn == field:
                        found = True
                    if len(imei) > 0 and imei == field:
                        found = True
                    if found:
                        break
                if found:
                    # print lineFields
                    break
        if found:
            break

    return found

#  0 - Year,
#  1 - IMSI,
#  2 - Sum_Bytes_upload,
#  3 - Sum_Bytes_download,
#  4 - Sum_Total_Kbytes,
#  5 - Sum_Bytes_upload_Test,
#  6 - Sum_Bytes_download_Test,
#  8 - Sum_Total_KbytesTest,
#  9 - Nr._of_PS_Sessions

def searchUsageExport(exportFile, imsi):
    try:
        csvfile = open(exportFile, 'rb')
    except IOError:
        print "Error: \"" + exportFile + "\" could not be opened."

    found = False
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        if row[1] == imsi:
            found = True
            break
    csvfile.close()
    if not found:
        row = []
    return row

def searchExportByIMSI(exportFile, imsi):
    try:
        csvfile = open(exportFile, 'rb')
    except IOError:
        print "Error: \"" + exportFile + "\" could not be opened."

    found = False
    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        file_imsi = row[10]
        if file_imsi == imsi:
            found = True
            break
    csvfile.close()
    if not found:
        row = []
    return row

def searchSnetDbColumn(db, columnName, columnValue):
    curtime = int(time.time())
    response = {}
    response['rowFound'] = False
    response['account_id_new'] = ''
    response['partner'] = ''
    response['bureau_id'] = ''
    response['deleted'] = False
    response['deleted_date'] = 0

    if not columnValue:
        return response

    cursor = db.cursor()
    query = ('SELECT account_id_new, partner, bureau_id '
             'FROM snet '
             'WHERE ' + columnName + '="' + columnValue + '"')
    # print(query)
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        # print("Found by %s %s") % (columnName, columnValue)
        response['account_id_new'] = row[0]
        response['partner'] = row[1]
        response['bureau_id'] = row[2]
        response['rowFound'] = True
    cursor.close()

    if not response['rowFound']:
        cursor = db.cursor()
        query = ('SELECT account_id_new, partner, deleted_date FROM snet_changes WHERE ' +
                 columnName + '="' + columnValue + '"')
        cursor.execute(query)
        rows = cursor.fetchall()
        for row in rows:
            response['account_id_new'] = row[0]
            response['partner'] = row[1]
            response['deleted_date'] = row[2]
            response['deleted'] = True
            response['rowFound'] = True
        cursor.close()

    return response

def searchSnet(db, dbFilter):
    response = {}
    response['rowFound'] = False

    # Search for iccid in snet.iccid, snet.phone_number
    if 'iccid' in dbFilter:
        if dbFilter['iccid']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'iccid', dbFilter['iccid'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', dbFilter['iccid'])

    # Search for imei in snet.imei, snet.phone_number
    if 'imei' in dbFilter:
        if dbFilter['meid']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'imei', imei)
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', imei)

    # Search for meid in snet.vendor_id
    if 'meid' in dbFilter:
        if dbFilter['meid']:
            if not response['rowFound']:
                vendor_id = dbFilter['meid']
                if dbFilter['meid'][0:2] == 'A1':
                    vendor_id = meid[2:]
                response = searchSnetDbColumn(db, 'vendor_id', vendor_id)

    # Search for ipaddress in snet.proxy_id
    if 'ipaddress' in dbFilter:
        if dbFilter['ipaddress']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'proxy_id', dbFilter['ipaddress'])

    # Search for imsi in snet.iccid, snet.phone_number, snet.vendor_id
    if 'imsi' in dbFilter:
        if dbFilter['imsi']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'iccid', dbFilter['imsi'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', dbFilter['imsi'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'vendor_id', dbFilter['imsi'])

            # For Numerex lines, try to match the IMSI to the vendor_id.
            if not response['rowFound']:
                if 'carrier' in dbFilter:
                    if dbFilter['carrier'] == 'numerex':
                        imsiSubStr = imsi[3:]
                        response = searchSnetDbColumn(usageDb, 'vendor_id', imsiSubStr)

    # Search for msisdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'msisdn' in dbFilter:
        if dbFilter['msisdn']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'iccid', dbFilter['msisdn'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', dbFilter['msisdn'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'vendor_id', dbFilter['msisdn'])

    # Search for mdn in snet.iccid, snet.phone_number, snet.vendor_id
    if 'mdn' in dbFilter:
        if dbFilter['mdn']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'iccid', dbFilter['mdn'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', dbFilter['mdn'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'vendor_id', dbFilter['mdn'])

    # Search for dmin in snet.iccid, snet.phone_number, snet.vendor_id
    if 'dmin' in dbFilter:
        if dbFilter['dmin']:
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'iccid', dbFilter['dmin'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'phone_number', dbFilter['dmin'])
            if not response['rowFound']:
                response = searchSnetDbColumn(db, 'vendor_id', dbFilter['dmin'])

    # For Vodafone lines, we need to try to match an equivalent IP address and vendor_id
    # to the Snet db record. For example Vodafone's 10.20.x.x subnet is mapped to P1's
    # 10.246.x.x subnet, and CMS's 10.248.0.0/16 subnet.
    if 'carrier' in dbFilter:
        if dbFilter['carrier'] == 'vodafone':
            if not response['rowFound']:
                if ipaddress[0:6] == '10.20.':
                    tmpIpAddress = '10.246.' + ipaddress[6:]
                    response = searchSnetDbColumn(db, 'proxy_id', tmpIpAddress)
            if not response['rowFound']:
                if ipaddress[0:6] == '10.20.':
                    tmpIpAddress = '10.248.' + ipaddress[6:]
                    response = searchSnetDbColumn(usageDb, 'proxy_id', tmpIpAddress)

    # print(response)

    return response

def usage():
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --csvfile     List of ICCIDs.")
   # print("       -s, --skipto      Skip to the ICCID.")
   # print("       -e, --export      Search the export file.")
   # print("       -u, --usagefile   Search the usage file.")
   print("       -h, -?, --help    Display this helpful message.")

# Command-line arguments.
parser = argparse.ArgumentParser(description='', add_help=False)
parser.add_argument('-f', '--csvfile', type=str, default='', required=False)
# parser.add_argument('-s', '--skipto', type=str, default='', required=False)
# parser.add_argument('-e', '--export', type=str, default='', required=False)
# parser.add_argument('-u', '--usagefile', type=str, default='', required=False)
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   usage()
   sys.exit(0)

if not args.csvfile:
   usage()
   sys.exit(0)

cnx = mysql.connector.connect(**config)

try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

# imsiFound = True
# if args.skipto:
#     imsiFound = False

counter = 0
foundCounter = 0
lastUsedCounter = 0
csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
    if len(row) == 0:
        continue
    imsi = row[0].strip()
    # if args.skipto and imsiFound == False:
    #     if args.skipto == imsi:
    #         imsiFound = True
    #     else:
    #         continue

    iccid = ''
    msisdn = ''
    imei = ''
    state = ''
    ipaddress = ''
    data_usage = 0
    last_used = 0
    activation_date = 0
    query = ('select iccid, msisdn, imei, mdn, meid, min, ipaddress, state, '
             'datausage, last_used, activation_date '
             'from cellusage '
             'where carrier="{carrier}" and imsi="{imsi}"' . format(carrier=carrier, imsi=imsi))
    cursor = cnx.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        iccid = row[0]
        msisdn = row[1]
        imei = row[2]
        mdn = row[3]
        meid = row[4]
        dmin = row[5]
        ipaddress = row[6]
        state = row[7]
        data_usage = row[8]
        last_used = row[9]
        activation_date = row[10]
    cursor.close()
    if len(iccid) > 0:
        foundCounter += 1
    if last_used == 0:
        lastUsedCounter += 1
    counter += 1

    if last_used > 0:
        continue

    print(("%s,%s,%s,%s,%s,%s") % (imsi, iccid, msisdn, imei, ipaddress, state))

    # # Search the deleted or current accounts tables.
    # foundInDeleted = searchSnetDeleted(iccid, imsi, msisdn, imei, ipaddress)
    # foundInAccounts = searchSnet(iccid, imsi, msisdn, imei, ipaddress)

    # # Ignore any that have an SNet account.
    # if foundInAccounts and not foundInDeleted:
    #     continue

print(lastUsedCounter)
print(foundCounter)
print(counter)


csvfile.close()
cnx.close()


# 10 IMSI,
# 11 SIM_Type,
# 12 ICCID,
# 13 MSISDN,
# 14 CSD_MSISDN,
# 15 IMEI,
# 16 Last_det._IMEI,
# 17 Last_det._IMEI_timestamp,
# 18 SIM_State,
# 19 Last_state_change,
# 20 Home_Country,
# 26 Created_on,
# 27 Has_Been_Active.Live,
# 28 Data_session_status,
# 29 First_time_Active.Live,
# 30 First_Used,
# 31 Has_Been_Active.Test,
# 32 Active.test_started_on,
# 34 Last_Country,
# 35 Last_Serving_Network,
# 36 Last_APN,
# 37 Last_Cell_Id,
# 38 Last_IP_Address,
# 39 Last_IP_address_type,

# Operator_Reseller,
# Parent_organisation,
# Customer,
# Customer_Id,
# BAN,
# Service_Profile,
# Service_profile_Id,
# Group,
# Tariff,
# Tariff_Id,
# IMSI,
# SIM_Type,
# ICCID,
# MSISDN,
# CSD_MSISDN,
# IMEI,
# Last_det._IMEI,
# Last_det._IMEI_timestamp,
# SIM_State,
# Last_state_change,
# Home_Country,
# Custom_Attribute_1,
# Custom_Attribute_2,
# Custom_Attribute_3,
# Custom_Attribute_4,
# Custom_Attribute_5,
# Created_on,
# Has_Been_Active.Live,
# Data_session_status,
# First_time_Active.Live,
# First_Used,
# Has_Been_Active.Test,
# Active.test_started_on,
# Nr._of_SIMs,
# Last_Country,
# Last_Serving_Network,
# Last_APN,Last_Cell_Id,
# Last_IP_Address,
# Last_IP_address_type,
# Static_IPv4_address_per_APN,
# Framed_IPv6_prefix_per_APN,
# Delegated_IPv6_prefix_per_APN,
# eCall_mode,
# VPN_group,
# Order_reference,
# Linked_device,
# EID,
# eUICC_Profile_State,
# POL1,
# POL2,
# SMDP,Fallback,
# eUICC_Profile_Downloaded_Date,
# SIM_Form_Factor,
# SIM_Profile_Code

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`
cd ${scriptDir}
scriptDir=`pwd`

cellCarrierDir="/home/bev_lekx/cellCarriers"


echo "Vodafone"
nohup ${cellCarrierDir}/Vodafone/bin/ardevicesFromExport.py \
      -f ${cellCarrierDir}/Vodafone/exports/vodafoneDevices.csv \
      > ${cellCarrierDir}/Vodafone/work/ARvodafoneDevices.out 2>&1 &
nohup ${cellCarrierDir}/Vodafone/bin/arusageFromExport.py \
      -m ${cellCarrierDir}/Vodafone/exports/vodafoneUsage.csv \
      -y ${cellCarrierDir}/Vodafone/exports/vodafoneUsageYearly.csv \
      > ${cellCarrierDir}/Vodafone/work/ARvodafoneUsage.out 2>&1 &

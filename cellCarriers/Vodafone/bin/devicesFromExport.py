#!/usr/bin/python

import csv
import requests
import json
import base64
import datetime
import time
import calendar
import argparse
import sys
import os
import signal
import mysql.connector

carrier = 'vodafone'

# Database parameters.
config = {
  'user': 'bev_lekx',
  'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
  'host': '172.29.253.45',
  'database': 'celldata',
  'raise_on_warnings': False,
}

programRunning = True

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(
   description='Import device list report into a db file.')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='Vodafone device report in csv format.')
args = parser.parse_args()

# Connect to the database.
cnx = mysql.connector.connect(**config)

#  0: Operator_Reseller:   VFAU,
#  1: Parent_organisation: VFAU,
#  2: Customer: Securenet,
#  3: Customer_Id:  630,
#  4: BAN:  "GER - 385027",
#  5: Service_Profile:  Default_Securenet_CSP,
#  6: Service_profile_Id:  3208,
#  7: Group:  ,
#  8: Tariff:  "Securenet_TARIFF_001 ",
#  9: Tariff_Id:  784,
# 10: IMSI:  204043254314645,
# 11: SIM_Type:  USIM,
# 12: ICCID:  89314404000112535191,
# 13: MSISDN:  882393254314645,
# 14: CSD_MSISDN:  ,
# 15: IMEI:  ,
# 16: Last_det._IMEI:  3515800502424806,
# 17: Last_det._IMEI_timestamp:  "2016-08-25 15:44:11 +01:00",
# 18: SIM_State:  Active.Live,
# 19: Last_state_change:  "2018-11-18 11:02:51 +01:00",
# 20: Home_Country:  Australia,
# 21: Custom_Attribute_1:  ,
# 22: Custom_Attribute_2:  ,
# 23: Custom_Attribute_3:  ,
# 24: Custom_Attribute_4:  ,
# 25: Custom_Attribute_5:  ,
# 26: Created_on:  "2014-07-30 11:52:07 +01:00",
# 27: Has_Been_Active.Live:  Y,
# 28: Data_session_status:  G,
# 29: First_time_Active.Live:  "2018-11-18 11:02:50 +01:00",
# 30: First_Used:  "2018-11-17 00:56:04 +01:00",
# 31: Has_Been_Active.Test:  Y,
# 32: Active.test_started_on:  "2016-02-11 02:15:02 +01:00",
# 33: Nr._of_SIMs:  1,
# 34: Last_Country:  ,
# 35: Last_Serving_Network:  ,
# 36: Last_APN:  ,
# 37: Last_Cell_Id:  ,
# 38: Last_IP_Address:  ,
# 39: Last_IP_address_type:  ,
# 40: Static_IPv4_address_per_APN:  p1.securenet.au:10.20.52.18;p1.securenet.au:10.20.52.18;,
# 41: Framed_IPv6_prefix_per_APN:  ,
# 42: Delegated_IPv6_prefix_per_APN:  ,
# 43: eCall_mode:  "No E-Call support",
# 44: VPN_group:  ,
# 45: Order_reference:  3399490,
# 46: Linked_device,EID:  ,
# 47: eUICC_Profile_State:  N/A,
# 48: POL1:  N/A,
# 49: POL2:  N/A,
# 50: SMDP:  N/A,
# 51: Fallback:  N/A,
# 52: eUICC_Profile_Downloaded_Date:  N/A,
# 53: SIM_Form_Factor:  N/A,
# 54: SIM_Profile_Code:  A,
# 55: : M2MMP0005

try:
    csvfile = open(args.csvfile, 'rb')
except IOError:
    print "Error: \"" + args.csvfile + "\" could not be opened."
    sys.exit(1)

counter = 0
fileType = ''
csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
    counter += 1
    if len(row) < 2:
        continue
    if 'Operator_Reseller' in row[0]:
        fileType = 'Itemised_SIM_inventory'
        # print(fileType)
        continue
    if len(fileType) == 0:
        continue
    if fileType != 'Itemised_SIM_inventory':
        print("Unrecognized file type.")
        cnx.close()
        sys.exit(1)

    imsi = row[10]
    iccid = row[12]
    msisdn = row[13]
    imei = row[15]
    if len(imei) == 0:
        imei = row[16]
    state = row[18]
    data_session_status = row[28]

    activated_date = 0
    if len(row[29]) > 0:
        activated_date = int(calendar.timegm(time.strptime(row[29], '%Y-%m-%d %H:%M:%S +01:00')))
    elif len(row[30]) > 0:
        activated_date = int(calendar.timegm(time.strptime(row[30], '%Y-%m-%d %H:%M:%S +01:00')))

    ipAddress = row[38]
    if len(ipAddress) == 0:
        w = row[40][0:row[40].find(';')]
        ipAddress = w[w.find(':')+1:]

    # A = Not connected last 24 hours
    # R = Connected last 24 hours
    # G - currently connected
    # if state == 'Active.Live':
    #     # print('\"%s\"') % (data_session_status)
    # if imsi == '204043251378429':
    #     print(data_session_status)

    rowCount = 0
    rowid = 0
    query = ('SELECT id FROM cellusage WHERE imsi="{imsi}" and carrier="{carrier}"' .
              format(imsi=imsi, carrier=carrier))
    cursor = cnx.cursor()
    cursor.execute(query)
    rows = cursor.fetchall()
    for row in rows:
        rowid = row[0]
        rowCount += 1
    cursor.close()

    query = ''
    if rowCount == 0:
        query = ('INSERT INTO cellusage (carrier, state, iccid, imsi, msisdn, '
                 'imei, mdn, meid, min, ipaddress, rateplan, datausage, last_used, '
                 'activation_date) '
                 'VALUES ("{carrier}", "{state}", "{iccid}", "{imsi}", "{msisdn}", '
                 '"{imei}", "", "", "", "{ipaddress}", "", '
                 '0, {last_used}, {activated_date})' .
                 format(carrier=carrier, state=state, iccid=iccid, imsi=imsi, msisdn=msisdn,
                        imei=imei, ipaddress=ipAddress, last_used=activated_date,
                        activated_date=activated_date))
    elif rowCount == 1:
        query = ('UPDATE cellusage set imsi="{imsi}", imei="{imei}", msisdn="{msisdn}", '
                 'ipaddress="{ipaddress}", state="{state}", activation_date={activated_date}, '
                 'last_used={activated_date} '
                 'WHERE id={rowid}' .
                 format(imsi=imsi, imei=imei, msisdn=msisdn, ipaddress=ipAddress,
                        state=state, activated_date=activated_date, last_used=activated_date,
                        rowid=rowid))
    else:
        print("Duplicate row found when updating cellusage with IMSI: %s") % (imsi)

    if len(query) > 0:
        # print(query)
        cursor = cnx.cursor()
        cursor.execute(query)
        cursor.close()

    # print imsi, iccid, msisdn, imei, activated_date, ipAddress

    if counter % 500 == 0:
        print counter
        cnx.commit()

    if not programRunning:
        break

cnx.commit()
cnx.close()

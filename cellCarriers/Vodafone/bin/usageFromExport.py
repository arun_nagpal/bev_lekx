#!/usr/bin/python

# Import Vodafone usage export file.
# This script accepts the "PacketDataUsageByImsi" and "Summarised data usage per ICCID"
# reports.
# Note that this script will not create cellusage records. The cell line must already
# exist in the database.

import signal
# import sys
# import os
import csv
# import requests
# import json
# import base64
# import sqlite3
# import datetime
import time
import calendar
import argparse
import mysql.connector

carrier = 'vodafone'

# Database parameters.
config = {
    'user': 'bev_lekx',
    'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
    'host': '172.29.253.45',
    'database': 'celldata',
    'raise_on_warnings': False,
}

programRunning = True

def readUsageFile(db, imsiList, usageFile):
    span = ''
    counter = 0

    print("Reading %s") % (usageFile)

    try:
        csvfile = open(usageFile, 'rb')
    except IOError:
        print "Error: \"" + usageFile + "\" could not be opened."
        return

    csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvreader:
        if 'IMSI' in row[1] and 'Sum_Bytes_upload' in row[2]:
            print("File type is \"Summarised data usage per IMSI\"")
            span = 'Month'
            if 'Year' in row[0]:
                span = 'Year'
            continue

        imsi = row[1]
        usage = float(row[4].replace(',', '')) / 1024
        if imsi not in imsiList:
            imsiList.append(imsi)
        last_used = 0
        dateString = ''
        if span == 'Year':
            dateString = row[0] + '-01-01'
        else:
            dateString = row[0] + '-01'
        last_used = int(calendar.timegm(time.strptime(dateString, '%Y-%m-%d')))

        if imsi == '204043728915555':
            print(imsi, span, dateString, last_used)
        # print(imsi, span, dateString, last_used)

        query = ('UPDATE cellusage set datausage={usage}, last_used={last_used} '
                 'WHERE imsi="{imsi}" AND carrier="{carrier}"' .
                 format(usage="{:0.2f}".format(usage), imsi=imsi, carrier=carrier,
                        last_used=last_used))
        cursor = db.cursor()
        cursor.execute(query)
        if counter % 500 == 0:
            print("%d") % (counter)
            cnx.commit()
        cursor.close()
        counter += 1
        if not programRunning:
            break

    db.commit()

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='Import Vodafone usage reports.')
parser.add_argument('-m', '--monthly', type=str, default='none',
                    required=True, help='Monthly usage report.')
parser.add_argument('-y', '--yearly', type=str, default='none',
                    required=True, help='Yearly usage report.')
args = parser.parse_args()

# Summarised_data_usage_per_IMSI_...
# Month,IMSI,Sum_Bytes_upload,Sum_Bytes_download,Sum_Total_Kbytes,Sum_Bytes_upload_Test,
#  Sum_Bytes_download_Test,Sum_Total_KbytesTest,Nr._of_PS_Sessions
# 2019-06,204043729093673,0,0,0.00,0,0,0.00,1
# 2019-06,204043726681145,0,0,0.00,0,0,0.00,1

# Year,IMSI,Sum_Bytes_upload,Sum_Bytes_download,Sum_Total_Kbytes,Sum_Bytes_upload_Test,
#  Sum_Bytes_download_Test,Sum_Total_KbytesTest,Nr._of_PS_Sessions
# 2019,204046205524773,0,0,0,0,0,0,1
# 2019,204043728918118,0,0,0,0,0,0,1

cnx = mysql.connector.connect(**config)
IMSIs = []

# First, apply the yearly values. Keep a list of touched IMSIs.
readUsageFile(cnx, IMSIs, args.yearly)

# Then apply the monthly values. Keep a list of touched IMSIs.
readUsageFile(cnx, IMSIs, args.monthly)

# For each IMSI not touched by monthly or yearly values, set last_used and usage to zero.
print("Update IMSIs not in usage files.")
counter = 0
query = ('SELECT imsi FROM cellusage WHERE carrier="{carrier}"' . format(carrier=carrier))
cursor = cnx.cursor()
cursor.execute(query)
dbRows = cursor.fetchall()
for dbRow in dbRows:
    imsi = dbRow[0]
    if imsi not in IMSIs:
        updCursor = cnx.cursor()
        updCursor.execute('UPDATE cellusage SET last_used=0, datausage=0 '
                          'WHERE carrier="{carrier}" and imsi="{imsi}"' .
                          format(carrier=carrier, imsi=imsi))
        updCursor.close()
    counter += 1
    if counter % 500 == 0:
        cnx.commit()
        print("%d") % (counter)
    if not programRunning:
        break
print("%d") % (counter)
cursor.close()
cnx.commit()
cnx.close()

#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cellDir="/home/bev_lekx/cellCarriers"
carrierDir="${cellDir}/Vodafone"
workDir="${carrierDir}/work"
exportsDir="${carrierDir}/exports"
cdate=`date +%Y%m%d`

# Thresholds.
th_delDeleted=180      # Account deleted threshold in days (-t).
th_delActivate=365     # Activated threshold in days for lines with deleted accounts (-a).
th_delLastused=365     # Last used threshold in days for lines with deleted accounts (-l).
th_noacctActivate=365  # Activated threshold for lines without accounts in days (-y).
th_noacctLastused=365  # Last used threshold for lines without accounts in days (-z).

# Print a detailed report of the lines to be deactivated.
${cellDir}/bin/arfindCellLines.py -c vodafone -d -v \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    > ${workDir}/ARvodafone-deactivate-report-${cdate}.txt

# Generate the list of lines to deactivate.
${cellDir}/bin/arfindCellLines.py -d -o imsi -c vodafone \
    -t ${th_delDeleted} \
    -a ${th_delActivate} \
    -l ${th_delLastused} \
    -z ${th_noacctLastused} \
    -y ${th_noacctActivate} \
    | sort | uniq \
    > ${workDir}/ARvodafone-deactivate-${cdate}.csv

# Create the deactivation batch files.
#${cellDir}/Vodafone/bin/arcreateBatchFile.py -f ${workDir}/ARvodafone-deactivate-${cdate}.csv

Send an email.
/usr/bin/sendemail -f "no-reply@securenettech.com" \
                   -t   arun.nagpal@securnettech.com \
                   -o tls=no \
                   -u "Vodafone cell cancellations" \
                   -m "Vodafone cell cancellations for `date +"%B %d, %Y"` are ready."

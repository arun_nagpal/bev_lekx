#!/usr/bin/python

import csv
import datetime
import time
import argparse
import os

# FORMAT01        Change Device State     CHANGE_STATE
# IMSI    STATE
# 204043726652004 INACTIVE
# 204043726652079 INACTIVE
# END     2

# Command-line arguments.
parser = argparse.ArgumentParser(description='')
parser.add_argument('-f', '--csvfile', type=str, default='none',
                    required=True, help='IMSI list csv format.')
parser.add_argument('-s', '--state', type=str, default='INACTIVE_STOPPED',
                    required=False, help='Desired SIM state.')
args = parser.parse_args()

try:
 csvfile = open(args.csvfile, 'rb')
except IOError:
   print "Error: \"" + args.csvfile + "\" could not be opened."
   sys.exit(1)

inFileDir = os.path.dirname(args.csvfile)
outFile = None
lineCounter = 0
fileCounter = 0

csvreader = csv.reader(csvfile, delimiter=',', quotechar='"')
for row in csvreader:
    if fileCounter == 0 or lineCounter >= 500:
        if lineCounter >= 500:
            outFile.write('END\t' + str(lineCounter) + '\n')
            outFile.close()
            lineCounter = 0
        fileCounter += 1
        if len(inFileDir) > 0:
            baseFileName = (inFileDir + '/' + 'vodafone-batch-' + args.state + '-'
                            + time.strftime("%Y%m%d", time.gmtime()))
        else:
            baseFileName = ('vodafone-batch-' + args.state + '-'
                            + time.strftime("%Y%m%d", time.gmtime()))

        outFileName = baseFileName + "-{:03d}".format(fileCounter) + ".txt"
        outFile = open(outFileName, 'w')
        outFile.write('FORMAT01\tChange Device State\tCHANGE_STATE\n')
        outFile.write('IMSI\tSTATE\n')

    imsi = row[0].strip()
    if 'nohup: ignoring input' in imsi:
        continue
    outFile.write(imsi + '\t' + args.state + '\n')
    lineCounter += 1

outFile.write('END\t' + str(lineCounter) + '\n')
outFile.close()
csvfile.close()

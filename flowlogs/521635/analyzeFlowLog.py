#!/usr/bin/python

import argparse
import csv
import gzip
import math
import os
import requests
import sys
import time
import calendar

iplocationFile = "/home/bev_lekx/flowlogs/ip2location.csv"
iplocationCache = {}
dstPorts = {}
elements = {}

def ip2int(ip):
    o = map(int, ip.split('.'))
    res = (pow(256, 3) * o[0]) + (pow(256, 2) * o[1]) + (256 * o[2]) + o[3]
    return res

def int2ip(ipnum):
    o1 = int(ipnum / pow(256, 3)) % 256
    o2 = int(ipnum / pow(256, 2)) % 256
    o3 = int(ipnum / 256) % 256
    o4 = int(ipnum) % 256
    return '%(o1)s.%(o2)s.%(o3)s.%(o4)s' % locals()

def analyzeFile(fileName):
    fh = None
    if fileName.endswith('.gz'):
        fh = gzip.open(fileName, 'r')
    else:
        fh = open(fileName, "r")

    for line in fh:
        values = map(str, line.strip().split(' '))
        if values[0] == 'version':
            continue

        interface = values[2]
        srcaddr = values[3]
        dstaddr = values[4]
        srcport = values[5]
        dstport = values[6]
        protocol = values[7]
        packets = values[8]
        numbytes = values[9]
        starttime = values[10]
        endtime = values[11]
        action = values[12]

        starttimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(starttime)))
        endtimeStr = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(int(endtime)))

        outside_addr = ''
        outside_port = ''
        inside_addr = ''
        inside_port = ''
        if srcaddr[0:7] == '172.29.' and dstaddr[0:7] == '172.29.':
            continue
        if srcaddr[0:7] == '172.29.':
            outside_addr = dstaddr
            outside_port = dstport
            inside_addr = srcaddr
            inside_port = srcport
        else:
            outside_addr = srcaddr
            outside_port = srcport
            inside_addr = dstaddr
            inside_port = dstport

        if outside_addr != '10.64.120.104':
            continue

        if starttimeStr not in elements:
            elements[starttimeStr] = 0
        elements[starttimeStr] += int(numbytes)

    fh.close()

# Command-line arguments.
parser = argparse.ArgumentParser(description='Analyze flow log.', add_help=False)
parser.add_argument('-f', '--flowid', type=str, default='', required=False, help='')
parser.add_argument('-h', '--help', action='store_true')
parser.add_argument('-?', '--_help', action='store_true')
args = parser.parse_args()

if args.help or args._help:
   print("%s") % (os.path.basename(sys.argv[0]))
   print("Usage: -f, --flowid     Flow log ID (required).")
   print("       -h, -?, --help   Display this helpful message.")
   sys.exit(0)

if len(args.flowid) == 0:
   print("The \"flowid\" argument is required.")
   sys.exit(1)

flowids = []
flowids = map(str, args.flowid.split(' '))

flowLogFiles = []
for root, dirs, files in os.walk('.'):
    for file in files:
        if '898116962670_vpcflowlogs_' in file and args.flowid in file:
            flowLogFiles.append(os.path.join(root, file))

for flowLogFile in sorted(flowLogFiles):
    for flowid in flowids:
        if len(flowid) > 0:
            if flowid in flowLogFile:
                if '_20190801T' in flowLogFile:
                    analyzeFile(flowLogFile)

# Convert results to bytes-per-second.
last_t = 0
for element in sorted(elements):
    t = int(calendar.timegm(time.strptime(element, '%Y-%m-%d %H:%M:%S')))
    if last_t != 0:
        diff = t - last_t
        value = elements[element] / diff
        print("%s %d") % (element, value)
    last_t = t

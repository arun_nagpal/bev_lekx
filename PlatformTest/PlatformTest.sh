#!/bin/bash

cd /home/securenet/bin/Cloud_Australia

FILE="platform_test.lck"

psOut=`ps ax | grep -v grep | grep 'java \-jar SNIP_test.jar \-\-platform au.securenettech.com' | awk 'BEGIN{FS=" "}{print $1}'`
if [ -n "${psOut}" ]
then
   echo "Killing previous process(es): ${psOut}." >> monitor.log
   for p in ${psOut}
   do
      kill ${p}
   done
   sleep 2
fi


touch $FILE

export JAVA_HOME="/opt/jdk1.8.0_171/jre"

PARTNER=$1
JAVA="${JAVA_HOME}/bin/java"

#export JAVA_HOME="/usr/lib/jvm/java-8-oracle"

PARTNER=$1
JAVA="${JAVA_HOME}/bin/java"
DEBUG="-Djavax.net.debug=all"
DEBUG=""

${JAVA} ${DEBUG} -jar SNIP_test.jar --platform au.securenettech.com --snip-port 1234 --account-panel-id 9995 --email-username "monitoring@securenettech.com" --email-password "m0n1t0r1ng123" --email-sender do_not_reply@securenettech.com --account-keyholder-name "Monitoring Cloud Australia" --web-host https://au.secure.direct --web-login-path1 /7.95/html/login.php --web-login-path2 /7.95/plugins/panel/html/account_lite_alarm_control.php? --web-username snet_eu --web-password 123123 --gateway-id 010203040506  --camera-id 0123456789AB --ftp-host "au.securenettech.com" --mobile-gateway-enable false --api-host='https://au.secure.direct' --api-path '/smarttech/' --oh-primary-host "59.100.120.70" --oh-primary-port 9999 --oh-secondary-host "59.100.120.54" --oh-secondary-port 9999 --oh-encryption-key 'r$yCfBMN)J9Jeqy_^j@#np3Y' --openvpn-primary-host dubbo.securenettech.com --openvpn-primary-port 8080 --openvpn-primary-protocol TCP --openvpn-secondary-host orange.securenettech.com  --openvpn-secondary-port 8080 --openvpn-secondary-protocol TCP

result=$?

# Uncomment to disable notification
#result=0

if [ $result -ne 0 ]; then
   echo ""
   sendemail -o tls=no -f support@securenettech.com -t  4073758672@vtext.com 4074887986@vtext.com 5199381269@txt.bell.ca << EOF
Cloud Australia Platform Down
`tail -5 /home/securenet/bin/Cloud_Australia/monitor.log`
EOF
   text="Cloud Australia Platform Down `tail -10 /home/securenet/bin/Cloud_Australia/monitor.log`"
fi

rm $FILE


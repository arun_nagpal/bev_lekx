#!/usr/bin/python

# https://xlsxwriter.readthedocs.io/
#

import csv
import requests
import json
import base64
import sqlite3
import mysql.connector
import argparse
import sys
import os
import datetime
import time
from time import sleep
import signal
import requests
import unicodedata, re
import xlsxwriter

programRunning = True
notFoundCounter = 0

# Cloud database parameters.
config = {
    'user': 'sn_notifier',
    'password': '5n_n0t1f13r',
    'host': 'db-cluster.cluster-ro-cd8ohozdxf8u.us-east-1.rds.amazonaws.com',
    'port': 3306,
    'database': 'securenet_6',
    'raise_on_warnings': False,
}

usageConfig = {
    'user': 'bev_lekx',
    'password': 'UoP6ne3Oogee9BievuGho6mieth0hohd',
    'host': '172.29.253.45',
    'database': 'celldata',
    'raise_on_warnings': False,
}

usageColName = 'Usage(MB/day)'
colNames = [ 'snet_id', 'system_id', 'bureau_id', 'dealer_name', 'account_type',
             'account_name', 'vendor_id', 'proxy_id', 'snet firmware', 'vendor firmware',
             'hardware', 'cell_provider', usageColName, 'Heartbeat', 'SNIP host', 'imei',
             'mdn', 'iccid', 'last_comm_date', 'connections', 'snet usage' ]

def getCarrierFromId(dbconn, carrierId):
    carrier = ''
    cursor = dbconn.cursor()
    query = ('SELECT name FROM cell_provider WHERE id=' + carrierId)
    try:
        cursor.execute(query)
    except:
        pass

    try:
        rows = cursor.fetchall()
        for row in rows:
            carrier = row[0]
    except:
        pass
    cursor.close()

    if carrier == 'Unknown':
        carrier = ''

    return carrier

# "{\"rdo.info\":{\"primPhoneNum\":\"+13852858140\",\"mnc\":\"480\",\"simNum\":\"89148000003426600875\",\"imei\":\"353238064008161\",\"simActivated\":true,\"mcc\":\"311\",\"cellDevType\":\"LE910-SVG\",\"towerId\":\"1FFCD03\"}}"
def getImeiFromSnetDb(dbconn, snet_id):
    imeiCursor = dbconn.cursor()
    query = ('SELECT json '
             'FROM account_panel_configuration '
             'WHERE account_panel_configuration.account_id_new = %s '
             'AND json LIKE \"%rdo.info%\"')
    try:
        imeiCursor.execute(query, (snet_id,))
    except:
        pass

    mdn = ''
    imei = ''
    iccid = ''
    # jsonString = ''
    jsonRows = imeiCursor.fetchall()
    for jsonRow in jsonRows:
        try:
           ri = json.loads(jsonRow[0])
           if 'rdo.info' in ri:
               p = ri['rdo.info']
               if 'primPhoneNum' in p:
                   mdn = p['primPhoneNum'].replace('+', '')
               if 'simNum' in p:
                   iccid = p['simNum']
               if 'imei' in p:
                   imei = p['imei']
        except:
           print jsonRow

    imeiCursor.close()

    return mdn, imei, iccid

def getSnipHeartbeat(snipHost, proxyId):
    heartbeat = ''
    try:
        r = requests.get('http://' + snipHost + ':10008/snip/?id=' + proxyId + '&cmd=REQUEST_SNIP_DATA&p=4e')
        j = r.json()
        if 'payload' in j:
            for i in j['payload']:
                if 'value' in i:
                    heartbeat = str(i['value'])
    except:
        pass

    return heartbeat

def getUsageFromDbCol(usageDb, colName, identifier):
    rowFound = False
    usage = 0.0
    carrier = ''
    usageCursor = usageDb.cursor()
    query = ('SELECT datausage, carrier FROM cellusage WHERE ' + colName + '="' + identifier + '"')
    try:
        usageCursor.execute(query)
    except:
        pass

    try:
        usageRows = usageCursor.fetchall()
        for usageRow in usageRows:
            # usage = "{:0.2f}".format(float(usageRow[0]))
            usage = usageRow[0]
            carrier = usageRow[1]
            rowFound = True
    except:
        pass

    usageCursor.close()
    return rowFound, usage, carrier

def getUsageFromDb(usageDb, mdn, imei, iccid, meid, proxyId):
    global notFoundCounter
    rowFound = False
    usage = 0.0
    carrier = ''
    if iccid:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'iccid', iccid)
    if not rowFound and imei:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'imei', imei)
    if not rowFound and meid:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'meid', meid)
    if not rowFound and proxyId:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'ipaddress', proxyId)
    if not rowFound and mdn:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'mdn', mdn)
    if not rowFound and mdn:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'msisdn', mdn)
    if not rowFound and mdn:
        rowFound, usage, carrier = getUsageFromDbCol(usageDb, 'min', mdn)

    if carrier == 'Unknown':
        carrier = ''

    if not rowFound:
        notFoundCounter += 1
        # print("%s,%s,%s,%s,%s,%s") % (mdn, imei, iccid, meid, proxyId, carrier)
    return usage, carrier, rowFound

def getHeartbeat(dbconn, snet_id):
    heartbeat = ''
    cursor = dbconn.cursor()
    query = ('SELECT value FROM account_panel_configuration WHERE account_id_new=' +
             str(snet_id) + ' AND id=78')
    try:
        cursor.execute(query)
    except:
        pass

    try:
        rows = cursor.fetchall()
        for row in rows:
            heartbeat = str(row[0])
            if heartbeat[0:1] == '{':
                heartbeat = ''
    except:
        pass
    cursor.close()

    return heartbeat

def getSnipHost(dbconn, snet_id):
    sniphost = ''
    cursor = dbconn.cursor()
    query = ('SELECT host_address FROM account_gateway WHERE account_id_new=' + str(snet_id))
    try:
        cursor.execute(query)
    except:
        pass

    try:
        rows = cursor.fetchall()
        for row in rows:
            sniphost = str(row[0])
    except:
        pass
    cursor.close()

    return sniphost

def getConnectionsAndSnetUsage(dbconn, accountGatewayId):
    connections = '0'
    data = '0'
    dobj = datetime.date.today() - datetime.timedelta(days=1)
    cursor = dbconn.cursor()
    query = ('SELECT connections, data FROM log_account_gateway_stats ' +
             'WHERE account_gateway_id=' + accountGatewayId +
             ' AND year=' + str(dobj.year) +
             ' AND month=' + str(dobj.month) +
             ' AND day=' + str(dobj.day))
    try:
        cursor.execute(query)
    except:
        pass

    try:
        rows = cursor.fetchall()
        for row in rows:
            connections = str(row[0])
            data = str(row[1])
    except:
        pass
    cursor.close()

    return connections, data

def calcRelativeUsage(usageDb, accountId, usage):
    relUsage = 0.0
    lastUsage = 0.0
    lastTime = 0
    usageCursor = usageDb.cursor()
    query = ('SELECT datausage, timestamp FROM iotega_data_usage WHERE account_id_new=' + accountId)
    try:
        usageCursor.execute(query)
        usageRows = usageCursor.fetchall()
        for usageRow in usageRows:
            lastUsage = usageRow[0]
            lastTime = usageRow[1]
    except:
        pass
    usageCursor.close()

    if lastTime > 0:
        diffInDays = (time.time() - lastTime) / 86400.0
        relUsage = (usage - lastUsage) / diffInDays
    if relUsage < 0:
        relUsage = 0.0
    return relUsage

def updateRelativeUsage(usageDb, accountId, usage):
    cursor = usageDb.cursor()

    rowFound = False
    query = ('select id from iotega_data_usage where account_id_new=' + accountId)
    cursor.execute(query)
    usageRows = cursor.fetchall()
    for usageRow in usageRows:
        rowFound = True

    query = ''
    if rowFound:
        query = ('update iotega_data_usage set datausage=' + str(usage) +
                 ', timestamp=' + str(time.time()) + ' where account_id_new=' + accountId)
    else:
        query = ('insert into iotega_data_usage (account_id_new, datausage, timestamp) '
                 'values(' + accountId + ',' + str(usage) + ',' + str(time.time()) + ')')
    cursor.execute(query)
    usageDb.commit()
    cursor.close()

control_chars = ''.join(map(unichr, range(0,32) + range(127,254)))
control_char_re = re.compile('[%s]' % re.escape(control_chars))

def remove_control_chars(s):
    return control_char_re.sub('', s)

def sortKey(elem):
    return elem[12]

def sigint_handler(signum, frame):
    global programRunning
    programRunning = False
    # print('---- ctrl-c ----')

signal.signal(signal.SIGINT, sigint_handler)
signal.signal(signal.SIGHUP, sigint_handler)

# Command-line arguments.
parser = argparse.ArgumentParser(description='List iotega cell usage.')
parser.add_argument('-g', '--generate', default=False,
                    required=False, action="store_true",
                    help='Generate identifier lists.')
args = parser.parse_args()

# Connect to the usage database.
try:
    usageDb = mysql.connector.connect(**usageConfig)
except:
    print("Error connecting to the database.")
    sys.exit(1)

# Connect to the SecureNet database.
try:
    cnx = mysql.connector.connect(**config)
except:
    print("Error connecting to the database.")
    sys.exit(1)

# Select all wsa's from the SecureNet database.
query = ('select distinct(account.id_new), '
         'account.system_id, '
         'account.bureau_id, '
         'bureau.company_name, '
         'account.account_type, '
         'account.name, '
         'account.suburb, '
         'account.state, '
         'account_gateway.vendor_id, '
         'account_gateway.vendor_id_2, '
         'account_gateway.proxy_id, '
         'account_gateway.cell_carrier, '
         'get_name_value("firmware", parameters) as fw, '
         'get_name_value("fw_vendor", parameters) as pa, '
         'get_name_value("hw", parameters) as pb, '
         'from_unixtime(updated_date/1000, "%c/%e/%Y %h:%i:%s") as last_communicated_date, '
         'account_gateway.id '
         'FROM (account_gateway, bureau) '
         'LEFT JOIN account on account_gateway.account_id_new = account.id_new '
         'WHERE account_gateway.gateway_type_id=12 '
         'AND account.system_id = bureau.system_id '
         'AND account.bureau_id = bureau.id '
         'order by account.id_new')

cursor = cnx.cursor()
try:
    cursor.execute(query)
except:
    cursor.close()
    cnx.close()
    sys.exit(1)
rows = cursor.fetchall()

dataArray = []

for row in rows:
    accountId = str(row[0]).strip()
    systemId = str(row[1]).strip()
    bureauId = str(row[2]).strip()
    dealerName = remove_control_chars(row[3].replace(',', ''))
    accountType = row[4].strip()
    accountName = remove_control_chars(row[5].replace(',', ''))
    accountSuburb = remove_control_chars(row[6].replace(',', ''))
    accountState = remove_control_chars(row[7])
    vendorId1 = str(row[8]).strip()
    # vendorId2 = row[9].strip()
    proxyId = str(row[10]).strip()
    carrierId = str(row[11]).strip()
    snet_firmware = remove_control_chars(str(row[12]))
    vendor_firmware = remove_control_chars(str(row[13]))
    hardware = str(row[14]).strip()
    lastCommDate = str(row[15])
    accountGatewayId = str(row[16])

    meid = 'A1' + vendorId1
    mdn, imei, iccid = getImeiFromSnetDb(cnx, accountId)

    snipHost = getSnipHost(cnx, accountId)

    connections, snetUsage = getConnectionsAndSnetUsage(cnx, accountGatewayId)

    if proxyId == None:
        proxyId = ''
    if snipHost == None:
        snipHost = ''

    heartbeat = getHeartbeat(cnx, accountId)
    # if len(heartbeat) == 0 and len(snipHost) > 0 and len(proxyId) > 0:
    #    heartbeat = getSnipHeartbeat(snipHost, proxyId)

    usage, carrier, rowFound = getUsageFromDb(usageDb, mdn, imei, iccid, meid, proxyId)
    if len(carrier) == 0:
        carrier = getCarrierFromId(cnx, carrierId).lower()
        if carrier == 'vodafone telit':
            carrier = 'vodafone'
        if carrier == 'at&t':
            carrier = 'att'
        if carrier == 'bell':
            carrier = 'bellcanada'

    if args.generate:
        if len(carrier) != 0:
            identifier = ''
            kind = ''
            if iccid != '':
                kind = 'iccid'
                identifier = iccid
            elif meid != '':
                kind = 'meid'
                identifier = meid

        # Ignore AT&T MEIDs.
        if carrier == 'att' and kind == 'meid':
            continue

        identifierFile = open(carrier + '-' + kind + '.csv', 'a+')
        identifierFile.write(identifier + '\n')
        identifierFile.close()
    else:
        # Ignore Vodafone for now.
        if carrier == 'vodafone':
            relUsage = 0
            usage = 0

        relUsage = calcRelativeUsage(usageDb, accountId, usage)
        updateRelativeUsage(usageDb, accountId, usage)
        dataArray.append((accountId, systemId, bureauId, dealerName, accountType[0:3],
                          accountName, vendorId1, proxyId, snet_firmware, vendor_firmware,
                          hardware, carrier, relUsage, heartbeat, snipHost,
                          imei, mdn, iccid, lastCommDate, connections, snetUsage))
        if not programRunning:
            break

# Create an xlsx spreadsheet sorted in descending usage order.
if not args.generate:
    xlsxRow = 0

    xlsxFileName = 'iotegaDataUsage.xlsx'
    csvFileName = 'iotegaDataUsage.csv'

    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(xlsxFileName)
    worksheet = workbook.add_worksheet()
    xlsxCol = 0
    for colName in colNames:
        worksheet.write(xlsxRow, xlsxCol, colName)
        xlsxCol += 1
    xlsxRow += 1

    # Sort the list of tuples.
    dataArray.sort(key=sortKey, reverse=True)

    # Write the worksheet data.
    for dataElem in dataArray:
        xlsxCol = 0
        while xlsxCol < len(dataElem):
            if colNames[xlsxCol] == usageColName:
                worksheet.write(xlsxRow, xlsxCol, "{:0.3f}".format(dataElem[xlsxCol]))
            else:
                worksheet.write(xlsxRow, xlsxCol, dataElem[xlsxCol])
            xlsxCol += 1
        xlsxRow += 1

    # Close the workbook.
    workbook.close()

    # ---------------------------------------------------------------------------
    # Write the results to a CSV file as well.
    csvFile = open(csvFileName, 'w')
    csvCol = 0
    for colName in colNames:
        if csvCol > 0:
            csvFile.write(',')
        csvFile.write(colName)
        csvCol += 1
    csvFile.write('\n')

    for dataElem in dataArray:
        csvCol = 0
        while csvCol < len(dataElem):
            if csvCol > 0:
                csvFile.write(',')
            if colNames[csvCol] == usageColName:
                csvFile.write("{:0.3f}".format(dataElem[csvCol]))
            else:
                csvFile.write(str(dataElem[csvCol]))
            csvCol += 1
        csvFile.write('\n')
    csvFile.close()
    # ---------------------------------------------------------------------------

    # Email the results.
    dateString = time.strftime("%B %d, %Y", time.gmtime())
    dobj = datetime.date.today()
    dow = dobj.weekday()
    recipients = ('arun.nagpal@securenettech.com '
                  'david.wilson@securenettech.com '
                  'cat.mineo@securenettech.com ')
    if dow == 0:      # Monday
        recipients = (recipients + ' ' +
                      'dinh.duong@securenettech.com '
                      'hector.perez@jci.com ')
    subject = ('SecureNet Iotega data usage report for ' + dateString)
    msg = ('SecureNet Iotega data usage report.\n\n')
    if dow >= 0 and dow <= 4:
        os.system('/usr/bin/sendemail ' +
                  '-f no-reply@securenettech.com ' +
                  '-t ' + recipients + ' ' +
                  '-o tls=no ' +
                  '-u "' + subject + '" ' +
                  '-m "' + msg + '" ' +
                  '-a ' + xlsxFileName)

# Close the cursor.
cursor.close()

# Close the database connections.
cnx.close()
usageDb.close()

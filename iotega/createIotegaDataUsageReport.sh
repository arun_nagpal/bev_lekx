#!/bin/bash

scriptName=`basename $0`
scriptDir=`dirname $0`

cd ${scriptDir}

carriers="att verizon"
kinds="iccid meid"

# Remove any existing carrier panel list files.
for carrier in ${carriers}
do
    for kind in ${kinds}
    do
	rm -f ${carrier}-${kind}.csv
    done
done

# Generate the list of iotega panels broken out by carrier. Each carrier will
# have a list of panels in a csv file.
./iotegaList.py -g

# Get the data usage for each panel from the carrier. Use the lists obtained in
# previous step.
for carrier in ${carriers}
do
    for kind in ${kinds}
    do
	if [ -f "${carrier}-${kind}.csv" ]
	then
	    echo "${carrier}-${kind}"
	    directory=""
	    if [ "${carrier}" == "att" ]
	    then
		directory="ATT"
	    elif [ "${carrier}" == "verizon" ]
	    then
		directory="Verizon"
	    elif [ "${carrier}" == "vodafone" ]
	    then
		directory="Vodafone"
	    fi
	    ../cellCarriers/${directory}/bin/usageFromList.py -k ${kind} -f ${carrier}-${kind}.csv
	fi
    done
done

# Generate the report.
./iotegaList.py

rm *iccid.csv
rm *meid.csv

